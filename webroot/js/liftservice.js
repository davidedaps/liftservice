'use strict';

$(document).ready(function () {
  
  
  
  
      $('#myTable').DataTable({
                dom: 'Blfrtip',
       buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
        responsive: true,
                exportOptions: {
            stripHtml: false
        },
        lengthMenu: [[20, 50, 100, -1], ['20 righe', '50 righe', '100 righe', 'Tutti']],
        language: {
            decimal: ",",
            thousands: ".",
            url: "//cdn.datatables.net/plug-ins/1.10.20/i18n/Italian.json"
        },
        order: [[4, 'desc'], [0, 'desc']]
    });
    /*------------------------------------------------
        Datetime picker (Flatpickr)
    ------------------------------------------------*/
    // Date and time
    if($('.datetime-picker')[0]) {
        $('.datetime-picker').flatpickr({
            enableTime: true,
            nextArrow: '<i class="zmdi zmdi-long-arrow-right" />',
            prevArrow: '<i class="zmdi zmdi-long-arrow-left" />',
            time_24hr: true,
            locale: 'it',
            defaultHour: 18,
            defaultMinute: 0 
        });
    }
  
  if($('.datetime-picker-editaction')[0]) {
        $('.datetime-picker-editaction').flatpickr({
            enableTime: true,
            nextArrow: '<i class="zmdi zmdi-long-arrow-right" />',
            prevArrow: '<i class="zmdi zmdi-long-arrow-left" />',
            time_24hr: true,
            locale: 'it'
        });
    }
  
    // Date only
    if($('.date-picker')[0]) {
        $('.date-picker').flatpickr({
            enableTime: false,
            nextArrow: '<i class="zmdi zmdi-long-arrow-right" />',
            prevArrow: '<i class="zmdi zmdi-long-arrow-left" />',
            locale: 'it'
        });
    }

    // Time only
    if($('.time-picker')[0]) {
        $('.time-picker').flatpickr({
            noCalendar: true,
            enableTime: true,
            time_24hr: true,
            theme: 'material_orange'
        });
    }



});