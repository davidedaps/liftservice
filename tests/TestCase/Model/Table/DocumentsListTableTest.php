<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DocumentsListTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DocumentsListTable Test Case
 */
class DocumentsListTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\DocumentsListTable
     */
    public $DocumentsList;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.DocumentsList',
        'app.DocumentsOrderCustomer',
        'app.DocumentsOrderSupplier'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('DocumentsList') ? [] : ['className' => DocumentsListTable::class];
        $this->DocumentsList = TableRegistry::getTableLocator()->get('DocumentsList', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DocumentsList);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
