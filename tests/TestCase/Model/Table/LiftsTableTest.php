<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\LiftsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\LiftsTable Test Case
 */
class LiftsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\LiftsTable
     */
    public $Lifts;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Lifts',
        'app.OrdersCustomers'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Lifts') ? [] : ['className' => LiftsTable::class];
        $this->Lifts = TableRegistry::getTableLocator()->get('Lifts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Lifts);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
