<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\OrderPhase3Table;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\OrderPhase3Table Test Case
 */
class OrderPhase3TableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\OrderPhase3Table
     */
    public $OrderPhase3;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.OrderPhase3'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('OrderPhase3') ? [] : ['className' => OrderPhase3Table::class];
        $this->OrderPhase3 = TableRegistry::getTableLocator()->get('OrderPhase3', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->OrderPhase3);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
