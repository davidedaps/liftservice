<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PaymentsCustomersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PaymentsCustomersTable Test Case
 */
class PaymentsCustomersTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\PaymentsCustomersTable
     */
    public $PaymentsCustomers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.PaymentsCustomers',
        'app.OrderCustomers'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('PaymentsCustomers') ? [] : ['className' => PaymentsCustomersTable::class];
        $this->PaymentsCustomers = TableRegistry::getTableLocator()->get('PaymentsCustomers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PaymentsCustomers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
