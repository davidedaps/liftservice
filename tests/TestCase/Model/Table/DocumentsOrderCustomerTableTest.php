<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DocumentsOrderCustomerTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DocumentsOrderCustomerTable Test Case
 */
class DocumentsOrderCustomerTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\DocumentsOrderCustomerTable
     */
    public $DocumentsOrderCustomer;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.DocumentsOrderCustomer',
        'app.OrderCustomers',
        'app.DocumentsLists'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('DocumentsOrderCustomer') ? [] : ['className' => DocumentsOrderCustomerTable::class];
        $this->DocumentsOrderCustomer = TableRegistry::getTableLocator()->get('DocumentsOrderCustomer', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DocumentsOrderCustomer);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
