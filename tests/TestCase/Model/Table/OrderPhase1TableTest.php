<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\OrderPhase1Table;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\OrderPhase1Table Test Case
 */
class OrderPhase1TableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\OrderPhase1Table
     */
    public $OrderPhase1;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.OrderPhase1'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('OrderPhase1') ? [] : ['className' => OrderPhase1Table::class];
        $this->OrderPhase1 = TableRegistry::getTableLocator()->get('OrderPhase1', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->OrderPhase1);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
