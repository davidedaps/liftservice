<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DocumentsOrderSupplierTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DocumentsOrderSupplierTable Test Case
 */
class DocumentsOrderSupplierTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\DocumentsOrderSupplierTable
     */
    public $DocumentsOrderSupplier;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.DocumentsOrderSupplier',
        'app.OrderSuppliers',
        'app.DocumentsLists'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('DocumentsOrderSupplier') ? [] : ['className' => DocumentsOrderSupplierTable::class];
        $this->DocumentsOrderSupplier = TableRegistry::getTableLocator()->get('DocumentsOrderSupplier', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DocumentsOrderSupplier);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
