<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MemoTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MemoTable Test Case
 */
class MemoTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\MemoTable
     */
    public $Memo;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Memo',
        'app.OrderCustomers'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Memo') ? [] : ['className' => MemoTable::class];
        $this->Memo = TableRegistry::getTableLocator()->get('Memo', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Memo);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
