<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\OrdersCustomersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\OrdersCustomersTable Test Case
 */
class OrdersCustomersTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\OrdersCustomersTable
     */
    public $OrdersCustomers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.OrdersCustomers',
        'app.Quotations',
        'app.Lifts'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('OrdersCustomers') ? [] : ['className' => OrdersCustomersTable::class];
        $this->OrdersCustomers = TableRegistry::getTableLocator()->get('OrdersCustomers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->OrdersCustomers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
