<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\OrderPhase2Table;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\OrderPhase2Table Test Case
 */
class OrderPhase2TableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\OrderPhase2Table
     */
    public $OrderPhase2;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.OrderPhase2',
        'app.Orders'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('OrderPhase2') ? [] : ['className' => OrderPhase2Table::class];
        $this->OrderPhase2 = TableRegistry::getTableLocator()->get('OrderPhase2', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->OrderPhase2);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
