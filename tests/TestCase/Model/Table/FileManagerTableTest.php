<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FileManagerTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FileManagerTable Test Case
 */
class FileManagerTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\FileManagerTable
     */
    public $FileManager;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.FileManager'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('FileManager') ? [] : ['className' => FileManagerTable::class];
        $this->FileManager = TableRegistry::getTableLocator()->get('FileManager', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->FileManager);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
