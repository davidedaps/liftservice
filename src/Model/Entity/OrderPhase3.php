<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * OrderPhase3 Entity
 *
 * @property int $order_id
 * @property int $phase_completed
 * @property string $description_step_1
 * @property \Cake\I18n\FrozenDate $step_1_created
 * @property int $step_1_completed
 * @property \Cake\I18n\FrozenDate $step_1_completedtime
 * @property string $description_step_2
 * @property \Cake\I18n\FrozenDate $step_2_created
 * @property int $step_2_completed
 * @property \Cake\I18n\FrozenDate $step_2_completedtime
 * @property string $description_step_3
 * @property \Cake\I18n\FrozenDate $step_3_created
 * @property int $step_3_completed
 * @property \Cake\I18n\FrozenDate $step_3_completedtime
 * @property string $description_step_4
 * @property \Cake\I18n\FrozenDate $step_4_created
 * @property int $step_4_completed
 * @property \Cake\I18n\FrozenDate $step_4_completedtime
 * @property string $description_step_5
 * @property \Cake\I18n\FrozenDate $step_5_created
 * @property int $completed_step_5
 * @property int $step_5_completedtime
 * @property string $description_step_6
 * @property \Cake\I18n\FrozenDate $step_6_created
 * @property int $completed_step_6
 * @property int $step_6_completedtime
 */
class OrderPhase3 extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'phase_completed' => true,
        'description_step_1' => true,
        'step_1_created' => true,
        'step_1_completed' => true,
        'step_1_completedtime' => true,
        'description_step_2' => true,
        'step_2_created' => true,
        'step_2_completed' => true,
        'step_2_completedtime' => true,
        'description_step_3' => true,
        'step_3_created' => true,
        'step_3_completed' => true,
        'step_3_completedtime' => true,
        'description_step_4' => true,
        'step_4_created' => true,
        'step_4_completed' => true,
        'step_4_completedtime' => true,
        'description_step_5' => true,
        'step_5_created' => true,
        'completed_step_5' => true,
        'step_5_completedtime' => true,
        'description_step_6' => true,
        'step_6_created' => true,
        'completed_step_6' => true,
        'step_6_completedtime' => true,        
        'step_1_notes' => true,
        'step_2_notes' => true,
        'step_3_notes' => true,
        'step_4_notes' => true,
        'step_5_notes' => true,
        'step_6_notes' => true,
        'step_7_notes' => true,
        'step_8_notes' => true,
        'step_9_notes' => true,
       'step_1_user' => true, 
       'step_2_user' => true, 
       'step_3_user' => true, 
       'step_4_user' => true, 
       'step_5_user' => true, 
       'step_6_user' => true, 
       'step_7_user' => true, 
       'step_8_user' => true, 
       'step_9_user' => true,
    ];
}
