<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
/**
 * Quotation Entity
 *
 * @property int $id
 * @property int $customer_id
 * @property string $quotation_number
 * @property float $value
 * @property string $reference
 * @property string $address
 * @property int $cap
 * @property string $city
 * @property string $status
 * @property \Cake\I18n\FrozenDate $quotation_date
 * @property string $balance_note
 * @property string $commission
 * @property int $archived
 * @property string|null $order_number
 * @property string $ referent
 *
 * @property \App\Model\Entity\Customer $customer
 * @property \App\Model\Entity\Action[] $actions
 * @property \App\Model\Entity\OrdersCustomer[] $orders_customers
 * @property \App\Model\Entity\Upload[] $uploads
 */
class Quotation extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true
    ];
  
    protected function _getLabel()
    {
        //pr($this);
        
        $customers = TableRegistry::get('Customers');
        $customers = $customers->find('all')
          ->where(['id' => ($this->_properties['customer_id'])]);
        
        $customers = $customers->first();
        $this->set(compact('customers'));
            
        return "Rif. impianto: ".$this->_properties['reference'] . ' - Data prev: ' . $this->_properties['quotation_date'] ." - ". $customers->name;
    }
}
