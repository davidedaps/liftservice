<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * OrdersCustomer Entity
 *
 * @property int $id
 * @property int $quotation_id
 * @property \Cake\I18n\FrozenDate $contract_sign_date
 * @property int $payment_type
 * @property float $order_amount
 * @property int $document_order_customer
 * @property int $lift_id
 *
 * @property \App\Model\Entity\Quotation $quotation
 * @property \App\Model\Entity\Lift $lift
 */
class OrdersCustomer extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
      '*' => true
    ];
}
