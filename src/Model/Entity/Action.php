<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Action Entity
 *
 * @property int $id
 * @property int $customer_id
 * @property int $quotation_id
 * @property string $description
 * @property \Cake\I18n\FrozenTime $exec_date
 * @property int $user_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Customer $customer
 * @property \App\Model\Entity\Quotation $quotation
 * @property \App\Model\Entity\User $user
 */
class Action extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'customer_id' => true,
        'quotation_id' => true,
        'description' => true,
        'exec_date' => true,
        'user_id' => true,
        'created' => true,
        'modified' => true,
        'customer' => true,
        'quotation' => true,
        'user' => true,
        'creator_id' => true,
        'is_action' => true,
        'action_type' => true,
        'status' => true
    ];  
}
