<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Supplier Entity
 *
 * @property int $ID
 * @property string $name
 * @property string $email1
 * @property string|null $email2
 * @property string|null $email3
 * @property string $cell1
 * @property string|null $cell2
 * @property string|null $cell3
 * @property string $tel1
 * @property string|null $tel2
 * @property string|null $tel3
 * @property string $contact1
 * @property string|null $contact2
 * @property string $vat_number
 * @property string $fiscal_code
 * @property string|null $sdi_code
 * @property string|null $pec
 *
 * @property \App\Model\Entity\Quotation[] $quotations
 */
class Supplier extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'email1' => true,
        'email2' => true,
        'email3' => true,
        'cell1' => true,
        'cell2' => true,
        'cell3' => true,
        'tel1' => true,
        'tel2' => true,
        'tel3' => true,
        'contact1' => true,
        'contact2' => true,
        'vat_number' => true,
        'fiscal_code' => true,
        'sdi_code' => true,
        'pec' => true
    ];
}
