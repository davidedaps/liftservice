<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * DocumentsList Entity
 *
 * @property int $id
 * @property string $folder
 *
 * @property \App\Model\Entity\DocumentsOrderCustomer[] $documents_order_customer
 * @property \App\Model\Entity\DocumentsOrderSupplier[] $documents_order_supplier
 */
class DocumentsList extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'folder' => true,
        'documents_order_customer' => true,
        'documents_order_supplier' => true
    ];
}
