<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * OrderPhase1 Entity
 *
 * @property int $order_id
 * @property int $phase_completed
 * @property string $description_step_1
 * @property \Cake\I18n\FrozenDate $step_1_created
 * @property int $step_1_completed
 * @property int $description_step_2
 * @property int $step_2_created
 * @property int $step_2_completed
 * @property int $description_step_3
 * @property int $step_3_created
 * @property int $step_3_completed
 * @property int $description_step_4
 * @property int $step_4_created
 * @property int $step_4_completed
 * @property int $description_step_5
 * @property int $step_5_created
 * @property int $completed_step_5
 */
class OrderPhase1 extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'phase_completed' => true,
        'description_step_1' => true,
        'step_1_created' => true,
        'step_1_completed' => true,
        'step_1_user' => true, 
        'step_1_notes' => true, 
        'description_step_2' => true,
        'step_2_created' => true,
        'step_2_completed' => true,
        'step_2_user' => true, 
        'step_2_notes' => true, 
        'description_step_3' => true,
        'step_3_created' => true,
        'step_3_completed' => true,
        'step_3_user' => true, 
        'step_3_notes' => true, 
        'description_step_4' => true,
        'step_4_created' => true,
        'step_4_completed' => true,
        'step_4_user' => true,
        'step_4_notes' => true, 
        'description_step_5' => true,
        'step_5_created' => true,
        'completed_step_5' => true,
        'step_5_user' => true,
        'step_5_notes' => true, 
      'expected_delivery_date' => true,
    ];
}
