<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Memo Entity
 *
 * @property int $id
 * @property int $order_customer_id
 * @property string $description
 * @property \Cake\I18n\FrozenDate $memo_expire_date
 * @property int $status
 *
 * @property \App\Model\Entity\OrderCustomer $order_customer
 */
class Memo extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'order_customer_id' => true,
        'description' => true,
        'memo_expire_date' => true,
        'status' => true,
        'order_customer' => true
    ];
}
