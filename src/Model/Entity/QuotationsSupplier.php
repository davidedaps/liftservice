<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * QuotationsSupplier Entity
 *
 * @property int $ID
 * @property int $supplier_id
 * @property int $quotation_id
 * @property string $description
 * @property \Cake\I18n\FrozenDate $date
 * @property float $price
 * @property string $offer_number
 *
 * @property \App\Model\Entity\Supplier $supplier
 * @property \App\Model\Entity\Quotation $quotation
 */
class QuotationsSupplier extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true
    ];
}
