<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * DocumentsOrderSupplier Entity
 *
 * @property int $id
 * @property int $order_supplier_id
 * @property int $documents_list_id
 * @property string $attachment
 *
 * @property \App\Model\Entity\OrderSupplier $order_supplier
 * @property \App\Model\Entity\DocumentsList $documents_list
 */
class DocumentsOrderSupplier extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'order_supplier_id' => true,
        'documents_list_id' => true,
        'attachment' => true,
        'order_supplier' => true,
        'documents_list' => true
    ];
}
