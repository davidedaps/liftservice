<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PaymentsCustomer Entity
 *
 * @property int $id
 * @property int $order_customer_id
 * @property \Cake\I18n\FrozenDate $date
 * @property float $amount
 * @property int $type
 * @property int $status
 * @property string $note
 *
 * @property \App\Model\Entity\OrderCustomer $order_customer
 */
class PaymentsCustomer extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
  
  protected function _getFullName() {
        return $this->_properties['order_number'];
    }
  
    protected $_accessible = [
        '*' => true
    ];
}
