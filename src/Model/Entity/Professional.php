<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Professional Entity
 *
 * @property int $id
 * @property string $titolo
 * @property string $nome
 * @property string $cognome
 * @property int $telefono
 * @property string $email
 */
class Professional extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
  
    protected function _getFullName() {
        return $this->_properties['titolo'] . ' ' . $this->_properties['nome'] . ' ' . $this->_properties['cognome'];
    }
  
    protected $_accessible = [
        'titolo' => true,
        'nome' => true,
        'cognome' => true,
        'telefono' => true,
        'email' => true,
        'studio_tecnico' => true
    ];
}
