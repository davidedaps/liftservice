<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OrderPhase2 Model
 *
 * @method \App\Model\Entity\OrderPhase2 get($primaryKey, $options = [])
 * @method \App\Model\Entity\OrderPhase2 newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\OrderPhase2[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OrderPhase2|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OrderPhase2 saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OrderPhase2 patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OrderPhase2[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\OrderPhase2 findOrCreate($search, callable $callback = null, $options = [])
 */
class OrderPhase2Table extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('order_phase2');
        $this->setDisplayField('order_id');
        $this->setPrimaryKey('order_id');
      
      
      
        $this->belongsTo('OrdersCustomers', [
            'foreignKey' => 'order_id',
            'joinType' => 'INNER'
        ]);
      
      
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('order_id')
            ->allowEmptyString('order_id', 'create');

        $validator
            ->integer('phase_completed')
            ->requirePresence('phase_completed', 'create')
            ->allowEmptyString('phase_completed', false);

        $validator
            ->scalar('description_step_1')
            ->maxLength('description_step_1', 255)
            ->requirePresence('description_step_1', 'create')
            ->allowEmptyString('description_step_1', false);

        $validator
            ->date('step_1_created')
            ->requirePresence('step_1_created', 'create')
            ->allowEmptyDate('step_1_created', false);

        $validator
            ->integer('step_1_completed')
            ->requirePresence('step_1_completed', 'create')
            ->allowEmptyString('step_1_completed', false);

        $validator
            ->date('step_1_completedtime')
            ->requirePresence('step_1_completedtime', 'create')
            ->allowEmptyDate('step_1_completedtime', false);

        $validator
            ->scalar('description_step_2')
            ->maxLength('description_step_2', 255)
            ->requirePresence('description_step_2', 'create')
            ->allowEmptyString('description_step_2', false);

        $validator
            ->date('step_2_created')
            ->requirePresence('step_2_created', 'create')
            ->allowEmptyDate('step_2_created', false);

        $validator
            ->integer('step_2_completed')
            ->requirePresence('step_2_completed', 'create')
            ->allowEmptyString('step_2_completed', false);

        $validator
            ->date('step_2_completedtime')
            ->requirePresence('step_2_completedtime', 'create')
            ->allowEmptyDate('step_2_completedtime', false);

        $validator
            ->scalar('description_step_3')
            ->maxLength('description_step_3', 255)
            ->requirePresence('description_step_3', 'create')
            ->allowEmptyString('description_step_3', false);

        $validator
            ->date('step_3_created')
            ->requirePresence('step_3_created', 'create')
            ->allowEmptyDate('step_3_created', false);

        $validator
            ->integer('step_3_completed')
            ->requirePresence('step_3_completed', 'create')
            ->allowEmptyString('step_3_completed', false);

        $validator
            ->date('step_3_completedtime')
            ->requirePresence('step_3_completedtime', 'create')
            ->allowEmptyDate('step_3_completedtime', false);

        $validator
            ->scalar('description_step_4')
            ->maxLength('description_step_4', 255)
            ->requirePresence('description_step_4', 'create')
            ->allowEmptyString('description_step_4', false);

        $validator
            ->date('step_4_created')
            ->requirePresence('step_4_created', 'create')
            ->allowEmptyDate('step_4_created', false);

        $validator
            ->integer('step_4_completed')
            ->requirePresence('step_4_completed', 'create')
            ->allowEmptyString('step_4_completed', false);

        $validator
            ->date('step_4_completedtime')
            ->requirePresence('step_4_completedtime', 'create')
            ->allowEmptyDate('step_4_completedtime', false);

        $validator
            ->scalar('description_step_5')
            ->maxLength('description_step_5', 255)
            ->requirePresence('description_step_5', 'create')
            ->allowEmptyString('description_step_5', false);

        $validator
            ->date('step_5_created')
            ->requirePresence('step_5_created', 'create')
            ->allowEmptyDate('step_5_created', false);

        $validator
            ->integer('completed_step_5')
            ->requirePresence('completed_step_5', 'create')
            ->allowEmptyString('completed_step_5', false);

        $validator
            ->integer('step_5_completedtime')
            ->requirePresence('step_5_completedtime', 'create')
            ->allowEmptyString('step_5_completedtime', false);

        $validator
            ->scalar('description_step_6')
            ->maxLength('description_step_6', 255)
            ->requirePresence('description_step_6', 'create')
            ->allowEmptyString('description_step_6', false);

        $validator
            ->date('step_6_created')
            ->requirePresence('step_6_created', 'create')
            ->allowEmptyDate('step_6_created', false);

        $validator
            ->integer('completed_step_6')
            ->requirePresence('completed_step_6', 'create')
            ->allowEmptyString('completed_step_6', false);

        $validator
            ->integer('step_6_completedtime')
            ->requirePresence('step_6_completedtime', 'create')
            ->allowEmptyString('step_6_completedtime', false);

        $validator
            ->scalar('description_step_7')
            ->maxLength('description_step_7', 255)
            ->requirePresence('description_step_7', 'create')
            ->allowEmptyString('description_step_7', false);

        $validator
            ->date('step_7_created')
            ->requirePresence('step_7_created', 'create')
            ->allowEmptyDate('step_7_created', false);

        $validator
            ->integer('completed_step_7')
            ->requirePresence('completed_step_7', 'create')
            ->allowEmptyString('completed_step_7', false);

        $validator
            ->integer('step_7_completedtime')
            ->requirePresence('step_7_completedtime', 'create')
            ->allowEmptyString('step_7_completedtime', false);

        $validator
            ->scalar('description_step_8')
            ->maxLength('description_step_8', 255)
            ->requirePresence('description_step_8', 'create')
            ->allowEmptyString('description_step_8', false);

        $validator
            ->date('step_8_created')
            ->requirePresence('step_8_created', 'create')
            ->allowEmptyDate('step_8_created', false);

        $validator
            ->integer('completed_step_8')
            ->requirePresence('completed_step_8', 'create')
            ->allowEmptyString('completed_step_8', false);

        $validator
            ->integer('step_8_completedtime')
            ->requirePresence('step_8_completedtime', 'create')
            ->allowEmptyString('step_8_completedtime', false);

        $validator
            ->scalar('description_step_9')
            ->maxLength('description_step_9', 255)
            ->requirePresence('description_step_9', 'create')
            ->allowEmptyString('description_step_9', false);

        $validator
            ->date('step_9_created')
            ->requirePresence('step_9_created', 'create')
            ->allowEmptyDate('step_9_created', false);

        $validator
            ->integer('completed_step_9')
            ->requirePresence('completed_step_9', 'create')
            ->allowEmptyString('completed_step_9', false);

        $validator
            ->integer('step_9_completedtime')
            ->requirePresence('step_9_completedtime', 'create')
            ->allowEmptyString('step_9_completedtime', false);

        return $validator;
    }
}
