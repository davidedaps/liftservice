<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Customers Model
 *
 * @property \App\Model\Table\ActionsTable|\Cake\ORM\Association\HasMany $Actions
 * @property \App\Model\Table\QuotationsTable|\Cake\ORM\Association\HasMany $Quotations
 *
 * @method \App\Model\Entity\Customer get($primaryKey, $options = [])
 * @method \App\Model\Entity\Customer newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Customer[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Customer|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Customer saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Customer patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Customer[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Customer findOrCreate($search, callable $callback = null, $options = [])
 */
class CustomersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('customers');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->hasMany('Actions', [
            'foreignKey' => 'customer_id'
        ]);
        $this->hasMany('Quotations', [
            'foreignKey' => 'customer_id'
        ]);
      
        //DC - proffer plugin per upload file al preventivo cliente
        $this->hasMany('Uploads', [
            'foreignKey' => 'customer_id'
        ]);
        //DC - comportamento al caricamento dei file
        $this->addBehavior('Proffer.Proffer', [
                'filename' => [	// The name of your upload field
                         'root' => WWW_ROOT . 'files', // Customise the root upload folder here, or omit to use the default
                          'dir' => 'dir'	// The name of the field to store the folder
                ]
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->allowEmptyString('name', false);

        $validator
            ->scalar('email1')
            ->email('email1')
            ->maxLength('email1', 80)
            ->allowEmptyString('email1');

        $validator
            ->scalar('email2')
            ->maxLength('email2', 150)
            ->allowEmptyString('email2');

        $validator
            ->scalar('email3')
            ->maxLength('email3', 150)
            ->allowEmptyString('email3');

        $validator
            ->scalar('cell1')
            ->maxLength('cell1', 50)
            ->requirePresence('cell1', 'create')
            ->allowEmptyString('cell1');

        $validator
            ->scalar('cell2')
            ->maxLength('cell2', 50)
            ->allowEmptyString('cell2');

        $validator
            ->scalar('cell3')
            ->maxLength('cell3', 50)
            ->allowEmptyString('cell3');

        $validator
            ->scalar('tel1')
            ->maxLength('tel1', 20)
            ->requirePresence('tel1', 'create')
            ->allowEmptyString('tel1');

        $validator
            ->scalar('tel2')
            ->maxLength('tel2', 20)
            ->allowEmptyString('tel2');

        $validator
            ->scalar('tel3')
            ->maxLength('tel3', 20)
            ->allowEmptyString('tel3');

        $validator
            ->scalar('contact1')
            ->maxLength('contact1', 255)
            ->requirePresence('contact1', 'create')
            ->allowEmptyString('contact1');

        $validator
            ->scalar('contact2')
            ->maxLength('contact2', 255)
            ->allowEmptyString('contact2');

        $validator
            ->scalar('vat_number')
            ->maxLength('vat_number', 20)
            ->requirePresence('vat_number', 'create')
            ->allowEmptyString('vat_number');

        $validator
            ->scalar('fiscal_code')
            ->maxLength('fiscal_code', 20)
            ->requirePresence('fiscal_code', 'create')
            ->allowEmptyString('fiscal_code');

        $validator
            ->scalar('sdi_code')
            ->maxLength('sdi_code', 7)
            ->requirePresence('sdi_code', 'create')
            ->allowEmptyString('sdi_code');

        $validator
            ->scalar('pec')
            ->maxLength('pec', 80)
            ->requirePresence('pec', 'create')
            ->allowEmptyString('pec');

        return $validator;
    }
}
