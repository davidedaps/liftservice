<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Suppliers Model
 *
 * @property \App\Model\Table\QuotationsTable|\Cake\ORM\Association\BelongsToMany $Quotations
 *
 * @method \App\Model\Entity\Supplier get($primaryKey, $options = [])
 * @method \App\Model\Entity\Supplier newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Supplier[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Supplier|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Supplier saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Supplier patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Supplier[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Supplier findOrCreate($search, callable $callback = null, $options = [])
 */
class SuppliersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('suppliers');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

//         $this->belongsToMany('Quotations', [
//             'foreignKey' => 'supplier_id',
//             'targetForeignKey' => 'quotation_id',
//             'joinTable' => 'quotations_suppliers'
//         ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->allowEmptyString('name', false);

        $validator
            ->scalar('email1')
            ->maxLength('email1', 80)
            ->requirePresence('email1', 'create')
            ->allowEmptyString('email1', true);

        $validator
            ->scalar('email2')
            ->maxLength('email2', 80)
            ->allowEmptyString('email2');

        $validator
            ->scalar('email3')
            ->maxLength('email3', 80)
            ->allowEmptyString('email3');

        $validator
            ->scalar('cell1')
            ->maxLength('cell1', 20)
            ->requirePresence('cell1', 'create')
            ->allowEmptyString('cell1', true);

        $validator
            ->scalar('cell2')
            ->maxLength('cell2', 20)
            ->allowEmptyString('cell2');

        $validator
            ->scalar('cell3')
            ->maxLength('cell3', 20)
            ->allowEmptyString('cell3');

        $validator
            ->scalar('tel1')
            ->maxLength('tel1', 20)
            ->requirePresence('tel1', 'create')
            ->allowEmptyString('tel1', true);

        $validator
            ->scalar('tel2')
            ->maxLength('tel2', 20)
            ->allowEmptyString('tel2');

        $validator
            ->scalar('tel3')
            ->maxLength('tel3', 20)
            ->allowEmptyString('tel3');

        $validator
            ->scalar('contact1')
            ->maxLength('contact1', 255)
            ->requirePresence('contact1', 'create')
            ->allowEmptyString('contact1', true);

        $validator
            ->scalar('contact2')
            ->maxLength('contact2', 255)
            ->allowEmptyString('contact2');

        $validator
            ->scalar('vat_number')
            ->maxLength('vat_number', 20)
            ->requirePresence('vat_number', 'create')
            ->allowEmptyString('vat_number', true);

        $validator
            ->scalar('fiscal_code')
            ->maxLength('fiscal_code', 20)
            ->requirePresence('fiscal_code', 'create')
            ->allowEmptyString('fiscal_code', true);

        $validator
            ->scalar('sdi_code')
            ->maxLength('sdi_code', 7)
            ->allowEmptyString('sdi_code');

        $validator
            ->scalar('pec')
            ->maxLength('pec', 80)
            ->allowEmptyString('pec');

        return $validator;
    }
}
