<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PaymentsCustomers Model
 *
 * @property \App\Model\Table\OrderCustomersTable|\Cake\ORM\Association\BelongsTo $OrderCustomers
 *
 * @method \App\Model\Entity\PaymentsCustomer get($primaryKey, $options = [])
 * @method \App\Model\Entity\PaymentsCustomer newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PaymentsCustomer[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PaymentsCustomer|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PaymentsCustomer saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PaymentsCustomer patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PaymentsCustomer[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PaymentsCustomer findOrCreate($search, callable $callback = null, $options = [])
 */
class PaymentsCustomersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('payments_customers');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('OrdersCustomers', [
            'foreignKey' => 'order_customer_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->date('date')
            ->requirePresence('date', 'create')
            ->allowEmptyDate('date', false);

        $validator
            ->numeric('amount')
            ->requirePresence('amount', 'create')
            ->allowEmptyString('amount', false);



        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['order_customer_id'], 'OrdersCustomers'));

        return $rules;
    }
}
