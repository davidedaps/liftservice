<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OrdersSuppliers Model
 *
 * @property \App\Model\Table\QuotationsTable|\Cake\ORM\Association\BelongsTo $Quotations
 *
 * @method \App\Model\Entity\OrdersSupplier get($primaryKey, $options = [])
 * @method \App\Model\Entity\OrdersSupplier newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\OrdersSupplier[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OrdersSupplier|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OrdersSupplier saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OrdersSupplier patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OrdersSupplier[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\OrdersSupplier findOrCreate($search, callable $callback = null, $options = [])
 */
class OrdersSuppliersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('orders_suppliers');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('QuotationsSuppliers', [
            'foreignKey' => 'quotation_supplier_id',
            'joinType' => 'INNER'
        ]);
      
        $this->hasMany('PaymentsSuppliers', [
            'foreignKey' => 'order_supplier_id',
            'joinType' => 'INNER'
        ]);
      
        $this->belongsTo('Quotations', [
            'foreignKey' => 'quotation_id',
            'joinType' => 'INNER'
        ]);
      
        $this->belongsTo('Suppliers', [
            'foreignKey' => 'id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->date('confirmation_date')
            ->requirePresence('confirmation_date', 'create')
            ->allowEmptyDate('confirmation_date', false);

        $validator
            ->numeric('order_amount')
            ->requirePresence('order_amount', 'create')
            ->allowEmptyString('order_amount', false);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['quotation_id'], 'Quotations'));
        return $rules;
    }
}
