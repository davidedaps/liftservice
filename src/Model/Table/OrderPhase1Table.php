<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OrderPhase1 Model
 *
 * @method \App\Model\Entity\OrderPhase1 get($primaryKey, $options = [])
 * @method \App\Model\Entity\OrderPhase1 newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\OrderPhase1[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OrderPhase1|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OrderPhase1 saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OrderPhase1 patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OrderPhase1[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\OrderPhase1 findOrCreate($search, callable $callback = null, $options = [])
 */
class OrderPhase1Table extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('order_phase1');
        $this->setDisplayField('order_id');
        $this->setPrimaryKey('order_id');
      
        //proffer plugin per upload file all'ordine cliente
        $this->hasMany('Uploads', [
            'foreignKey' => 'order_customer_id'
        ]);
      
              $this->belongsTo('OrdersCustomers', [
            'foreignKey' => 'order_id',
            'joinType' => 'INNER'
        ]);
      

      
    }
  
  

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('order_id')
            ->allowEmptyString('order_id', 'create');

        $validator
            ->integer('phase_completed')
            ->requirePresence('phase_completed', 'create')
            ->allowEmptyString('phase_completed', false);

        $validator
            ->scalar('description_step_1')
            ->maxLength('description_step_1', 255)
            ->requirePresence('description_step_1', 'create')
            ->allowEmptyString('description_step_1', false);

        $validator
            ->date('step_1_created')
            ->requirePresence('step_1_created', 'create')
            ->allowEmptyDate('step_1_created', false);

        $validator
            ->integer('step_1_completed')
            ->requirePresence('step_1_completed', 'create')
            ->allowEmptyString('step_1_completed', false);

        $validator
            ->integer('description_step_2')
            ->requirePresence('description_step_2', 'create')
            ->allowEmptyString('description_step_2', false);

        $validator
            ->date('step_2_created')
            ->requirePresence('step_2_created', 'create')
            ->allowEmptyString('step_2_created', false);

        $validator
            ->integer('step_2_completed')
            ->requirePresence('step_2_completed', 'create')
            ->allowEmptyString('step_2_completed', false);

        $validator
            ->integer('description_step_3')
            ->requirePresence('description_step_3', 'create')
            ->allowEmptyString('description_step_3', false);

        $validator
            ->date('step_3_created')
            ->requirePresence('step_3_created', 'create')
            ->allowEmptyString('step_3_created', false);

        $validator
            ->integer('step_3_completed')
            ->requirePresence('step_3_completed', 'create')
            ->allowEmptyString('step_3_completed', false);

        $validator
            ->integer('description_step_4')
            ->requirePresence('description_step_4', 'create')
            ->allowEmptyString('description_step_4', false);

        $validator
            ->date('step_4_created')
            ->requirePresence('step_4_created', 'create')
            ->allowEmptyString('step_4_created', false);

        $validator
            ->integer('step_4_completed')
            ->requirePresence('step_4_completed', 'create')
            ->allowEmptyString('step_4_completed', false);

        $validator
            ->integer('description_step_5')
            ->requirePresence('description_step_5', 'create')
            ->allowEmptyString('description_step_5', false);

        $validator
            ->date('step_5_created')
            ->requirePresence('step_5_created', 'create')
            ->allowEmptyString('step_5_created', false);

        $validator
            ->integer('completed_step_5')
            ->requirePresence('completed_step_5', 'create')
            ->allowEmptyString('completed_step_5', false);

        return $validator;
    }
}
