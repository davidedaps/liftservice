<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Professionals Model
 *
 * @method \App\Model\Entity\Professional get($primaryKey, $options = [])
 * @method \App\Model\Entity\Professional newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Professional[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Professional|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Professional saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Professional patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Professional[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Professional findOrCreate($search, callable $callback = null, $options = [])
 */
class ProfessionalsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
  
    protected function _getFullName() {
        return $this->_properties['titolo'] . ' ' . $this->_properties['nome']. ' ' . $this->_properties['cognome'];
        return $FullName;
    }
  
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('professionals');
        $this->setDisplayField('full_name');
        $this->setPrimaryKey('id');
      
        $this->hasMany('Quotations', [
            'foreignKey' => 'id'
        ]);
      
   } 

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('titolo')
            ->maxLength('titolo', 255)
            ->requirePresence('titolo', 'create')
            ->allowEmptyString('titolo', false);

        $validator
            ->scalar('nome')
            ->maxLength('nome', 255)
            ->requirePresence('nome', 'create')
            ->allowEmptyString('nome', false);

        $validator
            ->scalar('cognome')
            ->maxLength('cognome', 255)
            ->requirePresence('cognome', 'create')
            ->allowEmptyString('cognome', false);

        $validator
            ->integer('telefono')
            ->requirePresence('telefono', 'create')
            ->allowEmptyString('telefono', false);

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->allowEmptyString('email', false);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));

        return $rules;
    }
}
