<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Uploads Model
 *
 * @method \App\Model\Entity\Upload get($primaryKey, $options = [])
 * @method \App\Model\Entity\Upload newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Upload[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Upload|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Upload saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Upload patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Upload[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Upload findOrCreate($search, callable $callback = null, $options = [])
 */
class UploadsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('uploads');
        $this->setDisplayField('filename');
        $this->setPrimaryKey('id');
      
        //proffer plugin per upload file sul preventivo cliente
        $this->belongsTo('Quotations', [
          'foreignKey' => 'quotation_id',
          'joinType' => 'INNER'
        ]);
      
        //proffer plugin per upload file sul cliente
        $this->belongsTo('Customers', [
          'foreignKey' => 'customer_id',
          'joinType' => 'INNER'
        ]);
      
        //proffer plugin per upload file sul quotazione fornitor
        $this->belongsTo('QuotationsSuppliers', [
          'foreignKey' => 'quotation_supplier_id',
          'joinType' => 'INNER'
        ]);
      
        //proffer plugin per upload file sul ordine cliente
        $this->belongsTo('OrderPhase1', [
          'foreignKey' => 'order_customer_id',
          'joinType' => 'INNER'
        ]);
      
        //Proffer Plugin per gestire upload multipli
        $this->addBehavior('Proffer.Proffer', [
            'filename' => [	// The name of your upload field
                'root' => WWW_ROOT . 'files', // Customise the root upload folder here, or omit to use the default
                'dir' => 'dir'	// The name of the field to store the folder
            ]
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    { 
        $validator->setProvider('proffer', 'Proffer\Model\Validation\ProfferRules');
      
        $validator
            ->add('filename', [
                  ])
            ->allowEmpty('filename');

        return $validator;
    }
}
