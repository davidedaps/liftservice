<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * DocumentsOrderCustomer Model
 *
 * @property \App\Model\Table\OrderCustomersTable|\Cake\ORM\Association\BelongsTo $OrderCustomers
 * @property \App\Model\Table\DocumentsListsTable|\Cake\ORM\Association\BelongsTo $DocumentsLists
 *
 * @method \App\Model\Entity\DocumentsOrderCustomer get($primaryKey, $options = [])
 * @method \App\Model\Entity\DocumentsOrderCustomer newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\DocumentsOrderCustomer[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\DocumentsOrderCustomer|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DocumentsOrderCustomer saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DocumentsOrderCustomer patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\DocumentsOrderCustomer[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\DocumentsOrderCustomer findOrCreate($search, callable $callback = null, $options = [])
 */
class DocumentsOrderCustomerTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('documents_order_customer');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('OrderCustomers', [
            'foreignKey' => 'order_customer_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('DocumentsLists', [
            'foreignKey' => 'documents_list_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('attachment')
            ->maxLength('attachment', 255)
            ->requirePresence('attachment', 'create')
            ->allowEmptyString('attachment', false);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['order_customer_id'], 'OrderCustomers'));
        $rules->add($rules->existsIn(['documents_list_id'], 'DocumentsLists'));

        return $rules;
    }
}
