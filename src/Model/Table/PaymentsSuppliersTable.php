<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PaymentsSuppliers Model
 *
 * @property |\Cake\ORM\Association\BelongsTo $OrderSuppliers
 *
 * @method \App\Model\Entity\PaymentsSupplier get($primaryKey, $options = [])
 * @method \App\Model\Entity\PaymentsSupplier newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PaymentsSupplier[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PaymentsSupplier|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PaymentsSupplier saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PaymentsSupplier patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PaymentsSupplier[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PaymentsSupplier findOrCreate($search, callable $callback = null, $options = [])
 */
class PaymentsSuppliersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('payments_suppliers');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('OrdersCustomers', [
        'foreignKey' => 'order_supplier_id',
        'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
//         $validator
//             ->integer('id')
//             ->allowEmptyString('id', 'create');

//         $validator
//             ->date('date')
//             ->requirePresence('date', 'create')
//             ->allowEmptyDate('date', false);

//         $validator
//             ->numeric('amount')
//             ->requirePresence('amount', 'create')
//             ->allowEmptyString('amount', false);


//         $validator
//             ->scalar('note')
//             ->requirePresence('note', 'create')
//             ->allowEmptyString('note', false);

       return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
//     public function buildRules(RulesChecker $rules)
//     {
//         //$rules->add($rules->existsIn(['order_supplier_id'], 'OrderSuppliers'));

//        // return $rules;
//     }
}
