<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * DocumentsOrderSupplier Model
 *
 * @property \App\Model\Table\OrderSuppliersTable|\Cake\ORM\Association\BelongsTo $OrderSuppliers
 * @property \App\Model\Table\DocumentsListsTable|\Cake\ORM\Association\BelongsTo $DocumentsLists
 *
 * @method \App\Model\Entity\DocumentsOrderSupplier get($primaryKey, $options = [])
 * @method \App\Model\Entity\DocumentsOrderSupplier newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\DocumentsOrderSupplier[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\DocumentsOrderSupplier|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DocumentsOrderSupplier saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DocumentsOrderSupplier patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\DocumentsOrderSupplier[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\DocumentsOrderSupplier findOrCreate($search, callable $callback = null, $options = [])
 */
class DocumentsOrderSupplierTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('documents_order_supplier');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('OrderSuppliers', [
            'foreignKey' => 'order_supplier_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('DocumentsLists', [
            'foreignKey' => 'documents_list_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('attachment')
            ->maxLength('attachment', 255)
            ->requirePresence('attachment', 'create')
            ->allowEmptyString('attachment', false);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['order_supplier_id'], 'OrderSuppliers'));
        $rules->add($rules->existsIn(['documents_list_id'], 'DocumentsLists'));

        return $rules;
    }
}
