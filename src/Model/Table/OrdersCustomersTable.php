<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OrdersCustomers Model
 *
 * @property \App\Model\Table\QuotationsTable|\Cake\ORM\Association\BelongsTo $Quotations
 * @property \App\Model\Table\LiftsTable|\Cake\ORM\Association\BelongsTo $Lifts
 *
 * @method \App\Model\Entity\OrdersCustomer get($primaryKey, $options = [])
 * @method \App\Model\Entity\OrdersCustomer newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\OrdersCustomer[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OrdersCustomer|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OrdersCustomer saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OrdersCustomer patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OrdersCustomer[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\OrdersCustomer findOrCreate($search, callable $callback = null, $options = [])
 */
class OrdersCustomersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('orders_customers');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Quotations', [
            'foreignKey' => 'quotation_id',
            'joinType' => 'INNER'
        ]);
      
        $this->belongsTo('Customers', [
            'foreignKey' => 'customer_id',
            'joinType' => 'INNER'
        ]);
      
          $this->hasMany('Payments_suppliers', [
            'foreignKey' => 'order_supplier_id'
        ]);
      
         $this->hasMany('Order_phase1', [
            'foreignKey' => 'order_id',
            'joinType' => 'INNER'
        ]);
               $this->hasMany('Order_phase2', [
            'foreignKey' => 'order_id',
            'joinType' => 'INNER'
        ]);
               $this->hasMany('Order_phase3', [
            'foreignKey' => 'order_id',
            'joinType' => 'INNER'
        ]);
      
        $this->hasMany('OrdersSuppliers', [
            'foreignKey' => 'orders_customer_id'
        ]);
      
        $this->belongsTo('Lifts', [
            'foreignKey' => 'lift_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->date('contract_sign_date')
            ->requirePresence('contract_sign_date', 'create')
            ->allowEmptyDate('contract_sign_date', false);

        $validator
            ->numeric('order_amount')
            ->requirePresence('order_amount', 'create')
            ->allowEmptyString('order_amount', false);

//         $validator
//             ->integer('document_order_customer')
//             ->requirePresence('document_order_customer', 'create')
//             ->allowEmptyString('document_order_customer', true);
        

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['quotation_id'], 'Quotations',['order_customer_id'], 'PaymentsCustomers'));
//         $rules->add($rules->existsIn(['lift_id'], 'Lifts'));

        return $rules;
    }
}
