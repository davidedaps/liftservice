<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Lifts Model
 *
 * @property \App\Model\Table\OrdersCustomersTable|\Cake\ORM\Association\HasMany $OrdersCustomers
 *
 * @method \App\Model\Entity\Lift get($primaryKey, $options = [])
 * @method \App\Model\Entity\Lift newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Lift[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Lift|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Lift saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Lift patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Lift[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Lift findOrCreate($search, callable $callback = null, $options = [])
 */
class LiftsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('lifts');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('OrdersCustomers', [
            'foreignKey' => 'lift_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->allowEmptyString('name', false);

        $validator
            ->integer('serial_number')
            ->requirePresence('serial_number', 'create')
            ->allowEmptyString('serial_number', false);

        return $validator;
    }
}
