<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * DocumentsList Model
 *
 * @property \App\Model\Table\DocumentsOrderCustomerTable|\Cake\ORM\Association\HasMany $DocumentsOrderCustomer
 * @property \App\Model\Table\DocumentsOrderSupplierTable|\Cake\ORM\Association\HasMany $DocumentsOrderSupplier
 *
 * @method \App\Model\Entity\DocumentsList get($primaryKey, $options = [])
 * @method \App\Model\Entity\DocumentsList newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\DocumentsList[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\DocumentsList|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DocumentsList saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DocumentsList patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\DocumentsList[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\DocumentsList findOrCreate($search, callable $callback = null, $options = [])
 */
class DocumentsListTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('documents_list');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('DocumentsOrderCustomer', [
            'foreignKey' => 'documents_list_id'
        ]);
        $this->hasMany('DocumentsOrderSupplier', [
            'foreignKey' => 'documents_list_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('folder')
            ->maxLength('folder', 255)
            ->requirePresence('folder', 'create')
            ->allowEmptyString('folder', false);

        return $validator;
    }
}
