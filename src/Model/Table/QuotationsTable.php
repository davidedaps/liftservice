<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Quotations Model
 *
 * @property \App\Model\Table\CustomersTable|\Cake\ORM\Association\BelongsTo $Customers
 * @property \App\Model\Table\ActionsTable|\Cake\ORM\Association\HasMany $Actions
 * @property \App\Model\Table\OrdersCustomersTable|\Cake\ORM\Association\HasMany $OrdersCustomers
 * @property |\Cake\ORM\Association\HasMany $OrdersSuppliers
 * @property \App\Model\Table\UploadsTable|\Cake\ORM\Association\HasMany $Uploads
 * @property |\Cake\ORM\Association\BelongsToMany $Suppliers
 *
 * @method \App\Model\Entity\Quotation get($primaryKey, $options = [])
 * @method \App\Model\Entity\Quotation newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Quotation[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Quotation|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Quotation saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Quotation patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Quotation[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Quotation findOrCreate($search, callable $callback = null, $options = [])
 */
class QuotationsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('quotations');
        $this->setDisplayField('label');
        $this->setPrimaryKey('id');

        $this->belongsTo('Customers', [
            'foreignKey' => 'customer_id',
            'joinType' => 'INNER'
        ]);
      
        $this->belongsTo('Professionals', [
          'foreignKey' => 'professional_2',
          'joinType' => 'INNER'
        ]);
        
        $this->belongsTo('Professionals', [
          'foreignKey' => 'professional_1',
          'joinType' => 'INNER'
        ]);
      
        $this->hasMany('Actions', [
            'foreignKey' => 'quotation_id'
        ]);
        $this->hasMany('OrdersCustomers', [
            'foreignKey' => 'quotation_id'
        ]);
        $this->hasMany('OrdersSuppliers', [
            'foreignKey' => 'quotation_id'
        ]);
//         $this->hasMany('Uploads', [
//             'foreignKey' => 'quotation_id'
//         ]);
        $this->belongsToMany('Suppliers', [
            'foreignKey' => 'quotation_id',
            'targetForeignKey' => 'supplier_id',
            'joinTable' => 'quotations_suppliers'
        ]);
      
        //proffer plugin per upload file al preventivo cliente
        $this->hasMany('Uploads', [
            'foreignKey' => 'quotation_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
//         $validator
//             ->integer('id')
//             ->allowEmptyString('id', 'create');

//         $validator
//             ->scalar('quotation_number')
//             ->maxLength('quotation_number', 255)
//             ->requirePresence('quotation_number', 'create')
//             ->allowEmptyString('quotation_number', false);

//         $validator
//             ->decimal('value')
//             ->requirePresence('value', 'create')
//             ->allowEmptyString('value', false);

//         $validator
//             ->scalar('reference')
//             ->maxLength('reference', 255)
//             ->requirePresence('reference', 'create')
//             ->allowEmptyString('reference', false);

//         $validator
//             ->scalar('address')
//             ->maxLength('address', 255)
//             ->requirePresence('address', 'create')
//             ->allowEmptyString('address', false);

//         $validator
//             ->integer('cap')
//             ->requirePresence('cap', 'create')
//             ->allowEmptyString('cap', false);

//         $validator
//             ->scalar('city')
//             ->maxLength('city', 255)
//             ->requirePresence('city', 'create')
//             ->allowEmptyString('city', false);

//         $validator
//             ->scalar('status')
//             ->maxLength('status', 255)
//             ->requirePresence('status', 'create')
//             ->allowEmptyString('status', false);

//         $validator
//             ->date('quotation_date')
//             ->requirePresence('quotation_date', 'create')
//             ->allowEmptyDate('quotation_date', false);

//         $validator
//             ->scalar('balance_note')
//             ->maxLength('balance_note', 255)
//             ->requirePresence('balance_note', 'create')
//             ->allowEmptyString('balance_note', false);

//         $validator
//             ->scalar('commission')
//             ->maxLength('commission', 255)
//             ->requirePresence('commission', 'create')
//             ->allowEmptyString('commission', false);

//         $validator
//             ->scalar('order_number')
//             ->maxLength('order_number', 255)
//             ->allowEmptyString('order_number');

//         $validator
//             ->scalar(' referent')
//             ->maxLength(' referent', 255)
//             ->requirePresence(' referent', 'create')
//             ->allowEmptyString(' referent', false);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['customer_id'], 'Customers'));

        return $rules;
    }
}
