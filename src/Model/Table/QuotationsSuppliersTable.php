<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * QuotationsSuppliers Model
 *
 * @property \App\Model\Table\SuppliersTable|\Cake\ORM\Association\BelongsTo $Suppliers
 * @property \App\Model\Table\QuotationsTable|\Cake\ORM\Association\BelongsTo $Quotations
 *
 * @method \App\Model\Entity\QuotationsSupplier get($primaryKey, $options = [])
 * @method \App\Model\Entity\QuotationsSupplier newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\QuotationsSupplier[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\QuotationsSupplier|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\QuotationsSupplier saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\QuotationsSupplier patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\QuotationsSupplier[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\QuotationsSupplier findOrCreate($search, callable $callback = null, $options = [])
 */
class QuotationsSuppliersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('quotations_suppliers');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Suppliers', [
            'foreignKey' => 'supplier_id',
            'joinType' => 'INNER'
        ]);
              $this->belongsTo('OrdersSuppliers', [
            'foreignKey' => 'supplier_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Quotations', [
            'foreignKey' => 'quotation_id',
            'joinType' => 'INNER'
        ]);
      
              $this->belongsTo('QuotationsSuppliers', [
            'foreignKey' => 'quotation_supplier_id',
            'joinType' => 'INNER'
        ]);
      
        //DC - proffer plugin per upload file al preventivo cliente
        $this->hasMany('Uploads', [
            'foreignKey' => 'quotation_supplier_id'
        ]);

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('description')
            ->requirePresence('description', 'create')
            ->allowEmptyString('description', true);

        $validator
            ->date('date')
            ->requirePresence('date', 'create')
            ->allowEmptyDate('date', true);

        $validator
            ->decimal('price')
            ->requirePresence('price', 'create')
            ->allowEmptyString('price', true);

        $validator
            ->scalar('offer_number')
            ->maxLength('offer_number', 255)
            ->requirePresence('offer_number', 'create')
            ->allowEmptyString('offer_number', true);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['supplier_id'], 'Suppliers'));
        $rules->add($rules->existsIn(['quotation_id'], 'Quotations'));

        return $rules;
    }
}
