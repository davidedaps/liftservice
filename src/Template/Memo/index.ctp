<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Memo[]|\Cake\Collection\CollectionInterface $memo
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Memo'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="memo index large-9 medium-8 columns content">
    <h3><?= __('Memo') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('order_customer_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('memo_expire_date') ?></th>
                <th scope="col"><?= $this->Paginator->sort('status') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($memo as $memo): ?>
            <tr>
                <td><?= $this->Number->format($memo->id) ?></td>
                <td><?= $this->Number->format($memo->order_customer_id) ?></td>
                <td><?= h($memo->memo_expire_date) ?></td>
                <td><?= $this->Number->format($memo->status) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $memo->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $memo->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $memo->id], ['confirm' => __('Are you sure you want to delete # {0}?', $memo->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
