<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Memo $memo
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $memo->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $memo->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Memo'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="memo form large-9 medium-8 columns content">
    <?= $this->Form->create($memo) ?>
    <fieldset>
        <legend><?= __('Edit Memo') ?></legend>
        <?php
            echo $this->Form->control('order_customer_id');
            echo $this->Form->control('description');
            echo $this->Form->control('memo_expire_date');
            echo $this->Form->control('status');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
