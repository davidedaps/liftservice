

<div class="actions listview__actions">
        <div class="dropdown actions__item">
        <i class="zmdi zmdi-more-vert" data-toggle="dropdown"></i>
        <div class="dropdown-menu dropdown-menu-right">
        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#modal-<?php echo $elementId ?>">Segnala come chiusa</a>
        </div>
        </div>
        </div> 

<!--         <div>
          <button class="btn btn-danger btn--raised" data-toggle="modal" data-target="#modal-default" type="button"><i class="zmdi zmdi-delete zmdi-hc-fw"></i> Elimina</button>

          <?= $this->Html->link(__("<i class=\"zmdi zmdi-edit zmdi-hc-fw\"></i> Aggiungi"), ['controller' => 'Actions','action' => 'exec', $elementId],['title'=>'Aggiungi','escape'=>false,'class'=>'btn btn-warning btn--raised']) ?>
        </div> -->

        <!-- Modal -->
        <div class="modal fade" id="modal-<?php echo $elementId ?>" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title pull-left">Segno questa azione come completata?</h5>
                    </div>
                    <div class="modal-body">
<div class="card bg-white card-shadow new-contact">
    <div class="card-body">
         <h3><?= __('Aggiungi memo commerciale') ?></h3>
          <br/>
          <?= $this->Form->create() ?>
          <fieldset>
            <div class="row">
               <div class="col-md-6">
                  <div class="form-group">
                          <?php echo $this->Form->control('customer_id' , ['options' => $customers, 'label' => 'Seleziona il cliente']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
            </div>
        </fieldset>
     <?= $this->Form->button(__("<i class=\"zmdi zmdi-arrow-forward zmdi-hc-fw\"></i> Avanti"), ['class' => 'btn btn-info','action' => 'exec', 'escape' => false]) ?>
    <?= $this->Form->end() ?>
  </div>
</div>
                    </div>
                    <div class="modal-footer">
                        <?= $this->Form->postLink(__("<i class=\"zmdi zmdi-check zmdi-hc-fw\"></i> Sì,avanti tutta!"), 
                              ['controller' => 'Actions','action' => 'changestatus', $elementId],
                              ['title'=>'delete','escape'=>false,'class'=>'btn btn-success btn--raised']) ?>
                        <button type="button" class="btn btn-link btn-secondary" data-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        </div>