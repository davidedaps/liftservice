          <aside class="sidebar">
                <div class="scrollbar-inner">
                    <div class="user">
                        <div class="user__info" data-toggle="dropdown">
                            <img class="user__img" src="/img/lift-service-italia-logo.png" alt="logo">
 
                        </div>
                        <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(-5px, 62px, 0px); top: 0px; left: 0px; will-change: transform;">
                              <a class="dropdown-item" href="/users/login"><i class="zmdi zmdi-shield-check zmdi-hc-fw"></i> Login</a>
                              <a class="dropdown-item" href="/users/logout"><i class="zmdi zmdi-power zmdi-hc-fw"></i> Logout</a>
                        </div>
                    </div>
                    <br/>
                    <ul class="navigation">
                        <li><a href="/"><i class="zmdi zmdi-home"></i> Home</a></li>
                       <?php if ( $logged && $role == 'admin') { ?>
                        <?php if ( $this->request->getParam('controller') == "Users" ) { ?>
                            <li class="navigation__sub navigation__sub--active navigation__sub--toggled">
                        <?php } else { ?> 
                            <li class="navigation__sub">
                         <?php } ?>
<!--                               //// UTENTI //// -->
                            <a href=""><i class="zmdi zmdi-account-circle zmdi-hc-fw"></i> Utenti </a>

                            <ul>
                                <li><?= $this->Html->link(__('Nuovo utente'), ['controller' => 'Users', 'action' => 'add']) ?></li>
                                <li><?= $this->Html->link(__('Lista utenti'), ['controller' => 'Users', 'action' => 'index']) ?></li>
                            </ul>
                        </li>
                       <?php } ?>
<!--                               //// CLIENTI e FORNITORI //// -->
                        <?php if ( $this->request->getParam('controller') == "Customers" || $this->request->getParam('controller') == "Suppliers" ) { ?>
                            <li class="navigation__sub navigation__sub--active navigation__sub--toggled">
                        <?php } else { ?> 
                            <li class="navigation__sub">
                         <?php } ?>
                            <a href=""><i class="zmdi zmdi-accounts-alt zmdi-hc-fw"></i> Contatti / Fornitori </a>

                            <ul>
                                <li><?= $this->Html->link(__('Lista contatti'), ['controller' => 'Customers', 'action' => 'listcontact']) ?></li>
                                <li><?= $this->Html->link(__('Lista clienti'), ['controller' => 'Customers', 'action' => 'listcustomer']) ?></li>
                                <li><?= $this->Html->link(__('Lista fornitori'), ['controller' => 'Suppliers', 'action' => 'index']) ?></li>
                            </ul>
                        </li>
<!--                               //// PREVENTIVI CLIENTI //// -->
                      <?php if ( $this->request->getParam('controller') == "Quotations" ) { ?>
                            <li class="navigation__sub navigation__sub--active navigation__sub--toggled">
                        <?php } else { ?> 
                            <li class="navigation__sub">
                         <?php } ?>
                            <a href=""><i class="zmdi zmdi-file-text zmdi-hc-fw"></i> Preventivi clienti </a>

                            <ul>
                                <li><?= $this->Html->link(__('Nuovo preventivo cliente'), ['controller' => 'Quotations', 'action' => 'add']) ?></li>
                                <li><?= $this->Html->link(__('Lista preventivi cliente'), ['controller' => 'Quotations', 'action' => 'index']) ?></li>
                                <li><?= $this->Html->link(__('Lista preventivi archiviati'), ['controller' => 'Quotations', 'action' => 'archived']) ?></li>
                            </ul>
                        </li>
                      <!--                               //// ORDINI CLIENTI //// -->
                      <?php if ( $this->request->getParam('controller') == "OrdersCustomers" ) { ?>
                            <li class="navigation__sub navigation__sub--active navigation__sub--toggled">
                        <?php } else { ?> 
                            <li class="navigation__sub">
                         <?php } ?>
                            <a href=""><i class="zmdi zmdi-label-alt-outline zmdi-hc-fw"></i> Ordini clienti </a>

                            <ul>
                                <li><?= $this->Html->link(__('Lista ordini cliente'), ['controller' => 'OrdersCustomers', 'action' => 'index']) ?></li>
                                <li><?= $this->Html->link(__('Lista ordini cliente per DATA CONS.PREVISTA'), ['controller' => 'OrdersCustomers', 'action' => 'expecteddate']) ?></li>
                                <li><?= $this->Html->link(__('Lista ordini cliente ARCHIVIATI'), ['controller' => 'OrdersCustomers', 'action' => 'archived']) ?></li>
                                <li><?= $this->Html->link(__('Ordini cliente fase 1 - avanzamento'), ['controller' => 'OrderPhase1', 'action' => 'order-status']) ?></li>
                               <li><?= $this->Html->link(__('Ordini cliente fase 2 - avanzamento'), ['controller' => 'OrderPhase2', 'action' => 'order-status']) ?></li>
                               <li><?= $this->Html->link(__('Ordini cliente fase 3 - avanzamento'), ['controller' => 'OrderPhase3', 'action' => 'order-status']) ?></li>
                            </ul>
                        </li>
                      <!--                               //// PAGAMENTI  //// -->
                       <?php if ( $this->request->getParam('controller') == "PaymentsCustomers" ) { ?>
                            <li class="navigation__sub navigation__sub--active navigation__sub--toggled">
                        <?php } else { ?> 
                            <li class="navigation__sub">
                         <?php } ?>
                            <a href=""><i class="zmdi zmdi-money-box zmdi-hc-fw"></i> Pagamenti clienti </a>

                            <ul>
                                <li><?= $this->Html->link(__('Inserisci pagamento cliente'), ['controller' => 'PaymentsCustomers', 'action' => 'add']) ?></li>
                               <li><?= $this->Html->link(__('Elenco pagamenti clienti'), ['controller' => 'PaymentsCustomers', 'action' => 'index']) ?></li>
                            </ul>
                        </li>
<!--                               //// PREVENTIVI FORNITORI //// -->
                       <?php if ( $this->request->getParam('controller') == "QuotationsSuppliers" ) { ?>
                            <li class="navigation__sub navigation__sub--active navigation__sub--toggled">
                        <?php } else { ?> 
                            <li class="navigation__sub">
                         <?php } ?>
                            <a href=""><i class="zmdi zmdi-assignment-o zmdi-hc-fw"></i> Preventivi fornitori </a>

                            <ul>
                                <li><?= $this->Html->link(__('Nuovo preventivo fornitore'), ['controller' => 'QuotationsSuppliers', 'action' => 'add']) ?></li>
                                <li><?= $this->Html->link(__('Lista preventivi fornitore'), ['controller' => 'QuotationsSuppliers', 'action' => 'index']) ?></li>
                            </ul>
                        </li>
                          <!--                               //// ORDINI FORNITORI //// -->
                      <?php if ( $this->request->getParam('controller') == "OrdersSuppliers" ) { ?>
                            <li class="navigation__sub navigation__sub--active navigation__sub--toggled">
                        <?php } else { ?> 
                            <li class="navigation__sub">
                         <?php } ?>
                            <a href=""><i class="zmdi zmdi-markunread-mailbox zmdi-hc-fw"></i> Ordini fornitori </a>

                            <ul>
                                <li><?= $this->Html->link(__('Lista ordini fornitori'), ['controller' => 'OrdersSuppliers', 'action' => 'index']) ?></li>
                            </ul>
                        </li>
                       <!--                               //// PAGAMENTI FORNITORI //// -->
                       <?php if ( $this->request->getParam('controller') == "PaymentsSuppliers" ) { ?>
                            <li class="navigation__sub navigation__sub--active navigation__sub--toggled">
                        <?php } else { ?> 
                            <li class="navigation__sub">
                         <?php } ?>
                            <a href=""><i class="zmdi zmdi-money-box zmdi-hc-fw"></i> Pagamenti fornitori </a>

                            <ul>
                                <li><?= $this->Html->link(__('Inserisci pagamento fornitore'), ['controller' => 'PaymentsSuppliers', 'action' => 'add']) ?></li>
                              <li><?= $this->Html->link(__('Visualizza pagamenti fornitori'), ['controller' => 'PaymentsSuppliers', 'action' => 'index']) ?></li>
                            </ul>
                        </li>
<!--                               //// AZIONI COMMERCIALI //// -->
                         <?php if ( $this->request->getParam('controller') == "Actions" ) { ?>
                            <li class="navigation__sub navigation__sub--active navigation__sub--toggled">
                        <?php } else { ?> 
                            <li class="navigation__sub">
                         <?php } ?>
                            <a href=""><i class="zmdi zmdi-labels zmdi-hc-fw"></i> Attività commerciali </a>

                            <ul>
                                <li><?= $this->Html->link(__('Nuova azione/aggiornamento'), ['controller' => 'Actions', 'action' => 'add']) ?></li>
                                <li><?= $this->Html->link(__('Lista azioni'), ['controller' => 'Actions', 'action' => 'index','is_action' => 1]) ?></li>
                                <li><?= $this->Html->link(__('Lista aggiornamenti'), ['controller' => 'Actions', 'action' => 'index','is_action' => 0]) ?></li>
                            </ul>
                        </li>
<!--                               //// UPLOADS //// -->
                         <?php if ( $this->request->getParam('controller') == "Uploads" ) { ?>
                            <li class="navigation__sub navigation__sub--active navigation__sub--toggled">
                        <?php } else { ?> 
                            <li class="navigation__sub">
                         <?php } ?>
                            <a href=""><i class="zmdi zmdi-download zmdi-hc-fw"></i> Uploads </a>

                            <ul>
                                <li><?= $this->Html->link(__('Elenco uploads'), ['controller' => 'Uploads', 'action' => 'index']) ?></li>
                            </ul>
                        </li>
<!--                               //// PROFESSIONISTI //// -->
                         <?php if ( $this->request->getParam('controller') == "Professionals" ) { ?>
                            <li class="navigation__sub navigation__sub--active navigation__sub--toggled">
                        <?php } else { ?> 
                            <li class="navigation__sub">
                         <?php } ?>
                            <a href=""><i class="zmdi zmdi-male zmdi-hc-fw"></i> Professionisti </a>

                            <ul>
                                <li><?= $this->Html->link(__('Nuovo professionista'), ['controller' => 'Professionals', 'action' => 'add']) ?></li>
                                <li><?= $this->Html->link(__('Elenco professionisti'), ['controller' => 'Professionals', 'action' => 'index']) ?></li>
                            </ul>
                        </li>
<!--                               //// IMPOSTAZIONI //// -->
                         <?php if ( $this->request->getParam('controller') == "Settings" ) { ?>
                            <li class="navigation__sub navigation__sub--active navigation__sub--toggled">
                        <?php } else { ?> 
                            <li class="navigation__sub">
                         <?php } ?>
                            <a href=""><i class="zmdi zmdi-settings zmdi-hc-fw"></i>Impostazioni </a>

                            <ul>
                                <li><?= $this->Html->link(__('Nuova impostazione'), ['controller' => 'Settings', 'action' => 'add']) ?></li>
                                <li><?= $this->Html->link(__('Lista impostazioni'), ['controller' => 'Settings', 'action' => 'index']) ?></li>
                            </ul>
                        </li>
                      
                    </ul>
                </div>
            </aside>