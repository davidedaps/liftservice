 <button class="btn btn-danger btn--raised" data-toggle="modal" data-target="#modal-phase1-1" type="button">
   <i class="zmdi zmdi-delete zmdi-hc-fw"></i> 
   Resetta
</button>
<div class="modal fade" id="modal-phase1-1" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title pull-left">
                           Resetta step</h5>
                    </div>
                            <div class="modal-body">
                      <div class="card-body">
                      <?= $this->Form->create() ?>
                      <div class="form-body">
                      <hr>
                      <p>Per confermare il reset dello step scrivi la parola CONFERMA:</p>
                      <div class="row">
                      <div class="col-md-6">
                      <?php echo $this->Form->control('confirm', ['type' => 'text', 'label' => 'Conferma (attento alle maiuscole)']); ?> 
                      <?php echo $this->Form->hidden('elementId', ['value' => $elementId]); ?>    
                      </div>  
                      </div>
                      <!--/row-->
                      </div>
                      </div>
                    </div>
                    <div class="modal-footer">
                                          <?= $this->Form->postLink(__("<i class=\"zmdi zmdi-delete zmdi-hc-fw\"></i> Sì, elimina!"), 
                              ['action' => 'update', $elementId],
                              ['title'=>'delete','escape'=>false,'class'=>'btn btn-danger btn--raised']) ?>
                        <button type="button" class="btn btn-link btn-secondary" data-dismiss="modal">No</button>
                    </div>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
