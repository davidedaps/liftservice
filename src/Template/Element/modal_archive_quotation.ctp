        <div class="card-body">
          <button class="btn btn-warning btn--raised" data-toggle="modal" data-target="#modal-default" type="button"><i class="zmdi zmdi-delete zmdi-hc-fw"></i> Salva e archivia</button> 
          
        </div>

        <!-- Modal -->
        <div class="modal fade" id="modal-default" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title pull-left">
                            <?php if ($modalTitle) echo $modalTitle;
                                  else echo "Elimina ";?> 
                            <?= h($elementName) ?></h5>
                    </div>
                    <div class="modal-body">
                       <?php if ($modalText) echo $modalText;
                            else echo "Sei sicuro di voler archiviare ?";?>
                    </div>
                    <div class="modal-footer">
                        <?= $this->Form->postLink(__("<i class=\"zmdi zmdi-delete zmdi-hc-fw\"></i> Sì, salva e archivia!"), 
                              ['action' => 'archived', $elementId],
                              ['title'=>'delete','escape'=>false,'class'=>'btn btn-success btn--raised']) ?>
                        <button type="button" class="btn btn-link btn-secondary" data-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        </div>