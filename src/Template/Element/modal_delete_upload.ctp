
                    <!-- Button trigger modal -->
                    <div>
                      <button class="btn btn-danger btn--raised" data-toggle="modal" data-target=<?= "#modal" . h($upload->id) ?> type="button"><i class="zmdi zmdi-delete zmdi-hc-fw"></i> </button>
                    </div>
                    <!-- Modal -->
                    <div class="modal fade" id=<?= "modal" . h($upload->id) ?> tabindex="-1">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title pull-left">Elimina file <?= h($upload->filename)?></h5>
                                </div>
                                <div class="modal-body">
                                    Sei sicuro di voler eliminare questo file?
                                </div>
                                <div class="modal-footer">
                                    <?= $this->Form->postLink(__("<i class=\"zmdi zmdi-delete zmdi-hc-fw\"></i> Sì, elimina!"), 
                                          ['controller'=>'uploads','action' => 'delete', $upload->id],
                                          ['title'=>'delete','class'=>'btn btn-danger btn--raised','action' => 'delete', $upload->id,'escape'=>false]
                                          ) ?>
                                    <button type="button" class="btn btn-link btn-secondary" data-dismiss="modal">No</button>
                                </div>
                            </div>
                        </div>
                    </div>