    <nav aria-label="Page navigation">
        <ul class="pagination justify-content-center">
            <?php
              $this->Paginator->setTemplates([
                'prevActive' => '<li class="page-item pagination-prev"><a class="page-link" href="{{url}}"></a></li>',
                'prevDisabled' => '<li class="page-item pagination-prev disabled"><a class="page-link" href="{{url}}"></a></li>',
                'number' => '<li class="page-item"><a class="page-link" href="{{url}}">{{text}}</a></li>',
                'current' => '<li class="page-item active"><a class="page-link" href="{{url}}">{{text}}</a></li>',
                'nextActive' => '<li class="page-item pagination-next"><a class="page-link" href="{{url}}"></a></li>',
                'nextDisabled' => '<li class="page-item pagination-next disabled"><a class="page-link" href="{{url}}"></a></li>'            
              ]); ?>
            
            <?
            $this->Paginator->getTemplates('prevActive');
            $this->Paginator->getTemplates('prevDisabled');
            $this->Paginator->getTemplates('number');
            $this->Paginator->getTemplates('current');
            $this->Paginator->getTemplates('nextActive');
            $this->Paginator->getTemplates('nextDisabled');
            ?>
          
            <?= $this->Paginator->prev() ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next() ?>
        </ul>
        <p class="text-center"><?= $this->Paginator->counter(['format' => __('Pagina {{page}} di {{pages}}, sto mostrando {{current}} record(s) su un totale di {{count}}.')]) ?></p>
    </nav>