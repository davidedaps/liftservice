          <button class="btn btn-success btn--raised btn-sm" data-toggle="modal" data-target=<?= "#modal-" . h($element_id) ?> type="button"><i class="zmdi zmdi-check zmdi-hc-fw"></i> </button>
          <button class="btn btn-danger btn--raised btn-sm" data-toggle="modal" data-target=<?= "#modal-delete-" . h($element_id) ?> type="button"><i class="zmdi zmdi-delete zmdi-hc-fw"></i> </button>

<!--         <div>
          <button class="btn btn-danger btn--raised" data-toggle="modal" data-target="#modal-default" type="button"><i class="zmdi zmdi-delete zmdi-hc-fw"></i> Elimina</button>
          <?= $this->Html->link(__("<i class=\"zmdi zmdi-edit zmdi-hc-fw\"></i> Modifica"), ['action' => 'edit', $element_id],['title'=>'Modifica','escape'=>false,'class'=>'btn btn-warning btn--raised']) ?>
        </div> -->

        <!-- Modal -->
<div class="modal-wrapper">
        <div class="modal fade" id=<?= "modal-" . h($element_id) ?> tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title pull-left">Segno questa azione come completata?</h5>
                    </div>
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer">
                        <?= $this->Form->postLink(__("<i class=\"zmdi zmdi-check zmdi-hc-fw\"></i> Sì, ho fatto!"), 
                              ['controller' => 'Actions','action' => 'changestatus', $element_id, $quotation_id],
                              ['title'=>'checked','escape'=>false,'class'=>'btn btn-success btn--raised']) ?>
                        <button type="button" class="btn btn-link btn-secondary" data-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        </div>
</div>

<div class="modal-wrapper">
        <div class="modal fade" id=<?= "modal-delete-" . h($element_id) ?> >
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title pull-left">Vuoi eliminare?</h5>
                    </div>
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer">
                        <?= $this->Form->postLink(__("<i class=\"zmdi zmdi-delete zmdi-hc-fw\"></i> Sì,elimina!"), 
                              ['controller' => 'Actions','action' => 'delete', $element_id, $quotation_id],
                              ['title'=>'delete','escape'=>false,'class'=>'btn btn-danger btn--raised']) ?>
                        <button type="button" class="btn btn-link btn-secondary" data-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        </div>
</div>