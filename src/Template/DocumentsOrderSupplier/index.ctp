<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DocumentsOrderSupplier[]|\Cake\Collection\CollectionInterface $documentsOrderSupplier
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Documents Order Supplier'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="documentsOrderSupplier index large-9 medium-8 columns content">
    <h3><?= __('Documents Order Supplier') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('order_supplier_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('documents_list_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('attachment') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($documentsOrderSupplier as $documentsOrderSupplier): ?>
            <tr>
                <td><?= $this->Number->format($documentsOrderSupplier->id) ?></td>
                <td><?= $this->Number->format($documentsOrderSupplier->order_supplier_id) ?></td>
                <td><?= $this->Number->format($documentsOrderSupplier->documents_list_id) ?></td>
                <td><?= h($documentsOrderSupplier->attachment) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $documentsOrderSupplier->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $documentsOrderSupplier->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $documentsOrderSupplier->id], ['confirm' => __('Are you sure you want to delete # {0}?', $documentsOrderSupplier->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
