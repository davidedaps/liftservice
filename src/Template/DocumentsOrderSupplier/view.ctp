<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DocumentsOrderSupplier $documentsOrderSupplier
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Documents Order Supplier'), ['action' => 'edit', $documentsOrderSupplier->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Documents Order Supplier'), ['action' => 'delete', $documentsOrderSupplier->id], ['confirm' => __('Are you sure you want to delete # {0}?', $documentsOrderSupplier->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Documents Order Supplier'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Documents Order Supplier'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="documentsOrderSupplier view large-9 medium-8 columns content">
    <h3><?= h($documentsOrderSupplier->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Attachment') ?></th>
            <td><?= h($documentsOrderSupplier->attachment) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($documentsOrderSupplier->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Order Supplier Id') ?></th>
            <td><?= $this->Number->format($documentsOrderSupplier->order_supplier_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Documents List Id') ?></th>
            <td><?= $this->Number->format($documentsOrderSupplier->documents_list_id) ?></td>
        </tr>
    </table>
</div>
