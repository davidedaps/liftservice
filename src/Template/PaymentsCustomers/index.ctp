<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\PaymentsCustomer[]|\Cake\Collection\CollectionInterface $paymentsCustomers
 */
?>
<div class="card bg-white card-shadow">
    <div class="card-body">
    <h3><?= __('Pagamenti clienti') ?></h3>
     <table class="table table-responsive" id="data-table" >
        <thead>
            <tr>
                <th scope="col"><?= h('Riferimento ordine') ?></th>
                <th scope="col"><?= h('Numero ordine') ?></th>
                <th scope="col"><?= h('Descrizione')?></th>
                <th scope="col"><?= h('Data') ?></th>
                <th scope="col"><?= h('Importo') ?></th>
                <th scope="col"><?= h('Tipo') ?></th>
                <th scope="col"><?= h('Stato') ?></th>              
                <th scope="col" class="actions"><?= __('Attività') ?></th>            
            </tr>
        </thead>
        <tbody>
            <?php foreach ($paymentsCustomers as $paymentsCustomer): ?>
            <tr>
                
                <td><?= $paymentsCustomer->has('orders_customer') ? $this->Html->link($paymentsCustomer->orders_customer->id, ['controller' => 'OrdersCustomers', 'action' => 'view', $paymentsCustomer->orders_customer->id]) : '' ?></td>
                <td><?= h($paymentsCustomer->orders_customer->order_number) ?></td>
                <td><?= h($paymentsCustomer->description) ?></td>
                <td><?= h($paymentsCustomer->date) ?></td>
                <td><?= $this->Number->format($paymentsCustomer->amount) ?></td>
                <td><?= h($paymentsCustomer->type) ?></td>
                <td><?= h($paymentsCustomer->status) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Visualizza'), ['action' => 'view', $paymentsCustomer->id]) ?>
                    <?= $this->Html->link(__('Modifica'), ['action' => 'edit', $paymentsCustomer->id]) ?>
                    <?= $this->Form->postLink(__('Cancella'), ['action' => 'delete', $paymentsCustomer->id], ['confirm' => __('Sei sicuro di voler eliminare?', $paymentsCustomer->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
</div>
