<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\PaymentsCustomer $paymentsCustomer
 */
?>
<div class="card bg-white card-shadow new-contact">
    <div class="card-body">
         <h3><?= __('Modifica pagamento cliente') ?></h3>
    <?= $this->Form->create($paymentsCustomer) ?>
    <fieldset>
<br/>
       <div class="row">
              <div class="col-md-4">
                  <div class="form-group">
                          <?php  echo $this->Form->control('description', ['label' => 'Descrizione']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="form-group">
                          <?php  echo $this->Form->control('date', ['label' => 'Data']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>              
         
            <div class="col-md-4">
                  <div class="form-group">
                          <?php  echo $this->Form->control('amount', ['label' => 'Importo']);?>
                      <i class="form-group__bar"></i>
                  </div>
         </div>
            </div>
       <div class="row">
              <div class="col-md-4">
                  <div class="form-group">
                          <?php  echo $this->Form->control('type', ['label' => 'Tipo']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="form-group">
                          <?php  echo $this->Form->control('status', ['label' => 'Stato']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>  
              <div class="col-md-4">
                  <div class="form-group">
                          <?php  echo $this->Form->control('note', ['label' => 'Note']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
            </div>
    </fieldset>
    <?= $this->Form->button(__('Salva')) ?>
    <?= $this->Form->end() ?>
</div>
