<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Supplier $supplier
 */
?>
<div class="card bg-white card-shadow new-contact">
    <div class="card-body">
         <h3><?= __('Aggiungi pagamento ordine cliente') ?></h3>
          <br/>
          <?= $this->Form->create($paymentsCustomer) ?>
          <fieldset>
            <div class="row">
               <div class="col-md-4">
                  <div class="form-group">
                    <label>Seleziona ordine cliente:</label>
                    <?= $this->Form->select('order_customer_id', $orderListDetail); ?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="form-group">
                          <?php echo $this->Form->control('description' , ['label' => 'Descrizione aggiuntiva']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="form-group">
                          <?php echo $this->Form->control('date' , ['label' => 'Data']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
            </div>
            <div class="row">
               <div class="col-md-4">
                  <div class="form-group">
                          <?php echo $this->Form->control('amount' , ['label' => 'Importo']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="form-group">
                          <?php echo $this->Form->control('type' , ['label' => 'Tipo di pagamento']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="form-group">
                          <?php echo $this->Form->control('status' , ['label' => 'Stato']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
            </div>
                        <div class="row">
              
              <div class="col-md-8">
                  <div class="form-group">
                          <?php echo $this->Form->control('note' , ['label' => 'Note aggiuntive']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
            </div>
        </fieldset>
<!--         <legend>Uploads</legend>
            <label>(seleziona più file per caricare più di un allegato per volta):</label>
            <?php
            //echo $this->Form->control('uploads[]', ['type' => 'file','required'=>false,'multiple' => true,'class' => 'form-control','label' => '']);
            ?>
        </fieldset> -->
     <?= $this->Form->button(__('Salva'), ['class' => 'btn btn-primary']) ?>
    <?= $this->Form->end() ?>
  </div>
</div>
