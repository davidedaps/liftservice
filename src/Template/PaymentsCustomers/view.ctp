<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\PaymentsCustomer $paymentsCustomer
 */
?>
<div class="card bg-white card-shadow">
    <div class="card-body">
    <h3><?= h($orderDetailQuery->reference." - ".$orderDetailQuery->city) ?></h3>
    <table class="vertical-table">
        <tr>
            <td><?= $paymentsCustomer->has('orders_customer') ? $this->Html->link($paymentsCustomer->orders_customer->id, ['controller' => 'OrdersCustomers', 'action' => 'view', $paymentsCustomer->orders_customer->id]) : '' ?></td>
        </tr>
        <div style="clear:both"></div>
        <tr>
            <th scope="row"><?= __('Descrizione') ?></th>
            <td><?= h($paymentsCustomer->description) ?></td>
        </tr>
        <div style="clear:both"></div>      
        <tr>
            <th scope="row"><?= __('Tipo') ?></th>
            <td><?= h($paymentsCustomer->type) ?></td>
        </tr>
        <div style="clear:both"></div>
        <tr>
            <th scope="row"><?= __('Stato') ?></th>
            <td><?= h($paymentsCustomer->status) ?></td>
        </tr>
        <div style="clear:both"></div>
        <tr>
            <th scope="row"><?= __('Importo') ?></th>
            <td><?= $this->Number->format($paymentsCustomer->amount) ?></td>
        </tr>
        <div style="clear:both"></div>
        <tr>
            <th scope="row"><?= __('Data') ?></th>
            <td><?= h($paymentsCustomer->date) ?></td>
        </tr>
        <tr>
        <th><?= __('Note') ?></th>
          <td><?= $this->Text->autoParagraph(h($paymentsCustomer->note)); ?></td>
        </tr>
    </table>
        <div style="clear:both"></div>
</div>
</div>
