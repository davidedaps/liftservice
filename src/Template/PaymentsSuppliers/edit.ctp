<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\PaymentsSupplier $paymentsSupplier
 */
?>

<div class="card bg-white card-shadow new-contact">
    <div class="card-body">
         <h3><?= __('Modifica pagamento fornitore') ?></h3>
    <?= $this->Form->create($paymentsSupplier) ?>
    <fieldset>
         <td><?= h($orderSupplierList[0]->order_number) ?></td>
        <?php
  //echo $this->Form->control ($orderSupplierList[0]->order_number);
             echo $this->Form->control($orderSupplierList[0]->order_number, ['label' => 'Numero ordine fornitore']);
             echo $this->Form->control('date', ['label' => 'Data']);
             echo $this->Form->control('amount', ['label' => 'Importo']);
             echo $this->Form->control('type', ['label' => 'Tipo']);
            echo $this->Form->control('status', ['label' => 'Stato']);
           echo $this->Form->control('note', ['label' => 'Note']);

        ?>
    </fieldset>
    <?= $this->Form->button(__('Salva')) ?>
    <?= $this->Form->end() ?>
</div>
