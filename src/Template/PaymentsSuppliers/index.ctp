<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\PaymentsSupplier[]|\Cake\Collection\CollectionInterface $paymentsSuppliers
 */
?>
<div class="card bg-white card-shadow">
    <div class="card-body">
    <h3><?= __('Pagamenti fornitori') ?></h3>
     <table class="table table-responsive" id="data-table" >
        <thead>
            <tr>
                <th scope="col"><?= h('Numero ordine fornitore') ?></th>
                <th scope="col"><?= h('Descrizione')?></th>
                <th scope="col"><?= h('Data') ?></th>
                <th scope="col"><?= h('Importo') ?></th>
                <th scope="col"><?= h('Tipo') ?></th>
                <th scope="col"><?= h('Nota aggiuntiva') ?></th>              
                <th scope="col" class="actions"><?= __('Attività') ?></th>            
            </tr>
        </thead>
        <tbody>
            <?php foreach ($paymentsSuppliers as $paymentsSupplier): ?>
            <tr>

                <td><?= h($paymentsSupplier->orders_customer->order_number) ?></td>
                <td><?= h($paymentsSupplier->description) ?></td>
                <td><?= h($paymentsSupplier->date) ?></td>
                <td><?= $this->Number->format($paymentsSupplier->amount) ?></td>
                <td><?= h($paymentsSupplier->type) ?></td>
                <td><?= h($paymentsSupplier->note) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Modifica'), ['action' => 'edit', $paymentsSupplier->id]) ?>
                    <?= $this->Form->postLink(__('Cancella'), ['action' => 'delete', $paymentsSupplier->id], ['confirm' => __('Are you sure you want to delete # {0}?', $paymentsSupplier->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
