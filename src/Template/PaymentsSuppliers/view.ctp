<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\PaymentsSupplier $paymentsSupplier
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Payments Supplier'), ['action' => 'edit', $paymentsSupplier->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Payments Supplier'), ['action' => 'delete', $paymentsSupplier->id], ['confirm' => __('Are you sure you want to delete # {0}?', $paymentsSupplier->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Payments Suppliers'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Payments Supplier'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="paymentsSuppliers view large-9 medium-8 columns content">
    <h3><?= h($paymentsSupplier->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($paymentsSupplier->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Order Supplier Id') ?></th>
            <td><?= $this->Number->format($paymentsSupplier->order_supplier_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Amount') ?></th>
            <td><?= $this->Number->format($paymentsSupplier->amount) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Type') ?></th>
            <td><?= $this->Number->format($paymentsSupplier->type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= $this->Number->format($paymentsSupplier->status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Date') ?></th>
            <td><?= h($paymentsSupplier->date) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Note') ?></h4>
        <?= $this->Text->autoParagraph(h($paymentsSupplier->note)); ?>
    </div>
</div>
