<div class="card bg-white card-shadow">
    <div class="card-body">
        <h3><?= __('Uploads') ?></h3>
        <table class="table table-responsive" id="data-table" >
            <thead class="thead-light">
                <tr>
                    <th scope="col"><?= h('Filename') ?></th>
                    <th scope="col"><?= h('Preventivo') ?></th>
                    <th scope="col"><?= h('Cliente') ?></th>
                    <th scope="col"><?= h('Azioni') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($uploads as $upload): ?>
                 <?php $imagepath = '/files/uploads/filename/' . $upload->dir; ?>
                <tr>
                    <td><?= h($upload->filename) ?></td>
                    <td><?php if(isset($quotations[$upload->quotation_id])) echo ($quotations[$upload->quotation_id]) ?></td>
                    <td><?php if(isset($customers[$upload->customer_id])) echo ($customers[$upload->customer_id]) ?></td>
                    <td class="actions" width="150">
                      <div class="btn-group">
                        <a href="<?= $imagepath . '/' . $upload->filename ?>" class="btn btn-info btn--raised"><i class="zmdi zmdi-download zmdi-hc-fw"></i></a>
                        <?= $this->element('modal_delete_upload',['upload'=>$upload]) ?>
                      </div>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>