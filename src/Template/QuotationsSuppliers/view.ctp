<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Supplier $customer
 */
?>
<div class="card bg-white card-shadow new-contact">
    <div class="card-body">
      
         <h3><?= __('Dettaglio preventivo fornitore') ?></h3>
                   <div class="responsive" style="float:left;padding-left:30px;">
                      <?= // link per inserimento nuovo memo per questo preventivo
                $this->Html->link(__("<i class=\"zmdi zmdi-trending-up zmdi-hc-fw\"></i> Trasforma in ordine"), 
                            ['action' => 'add', 'controller' => 'OrdersSuppliers',$quotationsSupplier->id,],
                            ['title'=>'new quotation','escape'=>false,'class'=>'btn btn-success btn--raised']) ?>
          </div>
    <table class="table vertical-table">
      <tbody>
        <tr>
          <th scope="row"><?= __('Fornitore') ?></th>
          <td><?=  $quotationsSupplier->has('supplier') ? $this->Html->link($quotationsSupplier->supplier->name, ['controller' => 'Suppliers', 'action' => 'view', $quotationsSupplier->supplier->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Descrizione') ?></th>
            <td><?= h($quotationsSupplier->description) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Data preventivo') ?></th>
            <td><?= h($quotationsSupplier->date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Numero preventivo') ?></th>
            <td><?= h($quotationsSupplier->offer_number) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Prezzo') ?></th>
            <td><?= h($this->Number->currency($quotationsSupplier->price, 'EUR')) ?></td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
<!-- Button trigger modal -->
<div class="card bg-white card-shadow">
    <?= $this->element('modal_view', [
              // passo la variabile elementVariable all'elemento. 
              "elementId" => $quotationsSupplier->id,
              "elementName" => $quotationsSupplier->description,
              "modalTitle" => "Elimina preventivo fornitore",
              "modalText" => "Sei sicuro di eliminare questo preventivo?"
          ]) ?>
</div>

<div class="row">
  <div class="card bg-white card-shadow">
      <div class="card-body">
          <h3><?= __('Elenco uploads') ?></h3>
          <table class="table table-responsive" >
              <thead class="thead-light">
          <tr>
            <th scope="row"><?= __('Nome') ?></th>
            <th scope="row"><?= __('Scarica') ?></th>
          </tr>
            </thead>
              <tbody>
              <?php
                foreach ($uploads as $upload): ?>
                  <?php $imagepath = '/files/uploads/filename/' . $upload->dir; ?>
                      <tr>         
                        <td><?= h($upload->filename) ?></td>
                        <td><a href="<?= $imagepath . '/' . $upload->filename ?>" target="_blank"><i class="zmdi zmdi-download zmdi-hc-fw"></i></a></td>
                      </tr> 

              <?php endforeach; ?>
              </tbody>
          </table>
    </div>
  </div>
</div>
    
