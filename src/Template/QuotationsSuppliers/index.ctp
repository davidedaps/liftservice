<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Supplier[]|\Cake\Collection\CollectionInterface $suppliers
 */
?>
<div class="card bg-white card-shadow">
    <div class="card-body">
        <h3><?= __('Elenco preventivi fornitori') ?></h3>
            <table class="table table-responsive" id="data-table" >
            <thead class="thead-light">
            <tr>
                <th scope="col"><?= h('Numero preventivo') ?></th>
                <th scope="col"><?= h('Data preventivc')?></th>
                <th scope="col"><?= h('Descrizione')?></th>
                <th scope="col"><?= h('Tipo impianto')?></th>              
                <th scope="col"><?= h('Fornitore') ?></th>
                <th scope="col"><?= h('Importo preventivo') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($quotationsSuppliers as $quotationSupplier): ?>
            <tr>
                <td><?= $this->Html->link(__($quotationSupplier->offer_number), ['action' => 'view', $quotationSupplier->id]) ?></td>
                <td><?= h($quotationSupplier->date) ?></td>
                <td><?= h($quotationSupplier->description) ?></td>
                <td><?= $this->Html->link(__($quotationSupplier->quotation->label), ['action' => 'view', 'controller' => 'Quotations', $quotationSupplier->quotation_id])  ?></td>
                <td><?= $quotationSupplier->has('supplier') ? $this->Html->link($quotationSupplier->supplier->name, ['controller' => 'Suppliers', 'action' => 'view', $quotationSupplier->supplier->id]) : '' ?></td>
                <td><?= h($this->Number->currency($quotationSupplier->price, 'EUR')) ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
</div>