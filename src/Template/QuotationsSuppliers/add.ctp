<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Quotation $quotation
 */
?>

<div class="card bg-white card-shadow new-contact">
    <div class="card-body">
         <h3><?= __('Aggiungi preventivo fornitore') ?></h3>
          <br/>
          <?= $this->Form->create($quotationsSupplier,['type' => 'file']) ?>
          <fieldset>
            <div class="row">
              <div class="col-md-6">
                  <div class="form-group">
                          <?php echo $this->Form->control('supplier_id', ['options' => $suppliers, 'label' => 'Seleziona il fornitore','empty' => 'Scegli il nome...']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
               <div class="col-md-6">
                  <div class="form-group">
                          <?php echo $this->Form->control('description' , ['label' => 'Descrizione preventivo']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
            </div>
            <div class="row">
               <div class="col-md-2">
                  <div class="form-group">
                          <?php echo $this->Form->control('offer_number' , ['label' => 'Numero preventivo']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
              <div class="col-md-2">
                  <div class="form-group">
                          <?php echo $this->Form->control('price' , ['label' => 'Importo']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
              <div class="col-md-3">
                  <div class="form-group">
                          <?php echo $this->Form->control('date', ['label' => 'Data']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
              <div class="col-md-5">
              <?php echo $this->Form->control('quotation_id', ['options' => $quotations,'label' => 'Preventivo cliente di riferimento']); ?>
              </div>
            </div>
        </fieldset>
        <fieldset>
        <legend>Uploads</legend>
            <label>(seleziona più file per caricare più di un allegato per volta):</label>
            <?php
            echo $this->Form->control('uploads[]', ['type' => 'file','required'=>false,'multiple' => true,'class' => 'form-control','label' => '']);
            ?>
        </fieldset>
     <?= $this->Form->button(__('Salva'), ['class' => 'btn btn-primary']) ?>
    <?= $this->Form->end() ?>
  </div>
</div>

