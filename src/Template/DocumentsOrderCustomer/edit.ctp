<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DocumentsOrderCustomer $documentsOrderCustomer
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $documentsOrderCustomer->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $documentsOrderCustomer->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Documents Order Customer'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="documentsOrderCustomer form large-9 medium-8 columns content">
    <?= $this->Form->create($documentsOrderCustomer) ?>
    <fieldset>
        <legend><?= __('Edit Documents Order Customer') ?></legend>
        <?php
            echo $this->Form->control('order_customer_id');
            echo $this->Form->control('documents_list_id');
            echo $this->Form->control('attachment');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
