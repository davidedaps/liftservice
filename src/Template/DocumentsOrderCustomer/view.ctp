<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DocumentsOrderCustomer $documentsOrderCustomer
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Documents Order Customer'), ['action' => 'edit', $documentsOrderCustomer->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Documents Order Customer'), ['action' => 'delete', $documentsOrderCustomer->id], ['confirm' => __('Are you sure you want to delete # {0}?', $documentsOrderCustomer->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Documents Order Customer'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Documents Order Customer'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="documentsOrderCustomer view large-9 medium-8 columns content">
    <h3><?= h($documentsOrderCustomer->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Attachment') ?></th>
            <td><?= h($documentsOrderCustomer->attachment) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($documentsOrderCustomer->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Order Customer Id') ?></th>
            <td><?= $this->Number->format($documentsOrderCustomer->order_customer_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Documents List Id') ?></th>
            <td><?= $this->Number->format($documentsOrderCustomer->documents_list_id) ?></td>
        </tr>
    </table>
</div>
