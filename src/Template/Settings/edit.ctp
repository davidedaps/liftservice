<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $setting
 */
?>
<div class="card bg-white card-shadow">
    <div class="card-body">
      <?= $this->Form->create($setting) ?>
        <h3 class="card-title"><?= __('Modifica impostazione') ?></h3>
        <fieldset>
          
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
              <?php echo $this->Form->control('description', ['label' => 'Descrizione impostazione']);?>
              <i class="form-group__bar"></i>
              </div>
            </div>  
             </div>
           <div class="row">
            <div class="col-md-6">
            <label>Seleziona il tipo di azione</label>
              <div class="form-group">
                 <?php $provenienza = ['tipo azione'=>'azione','tipo pagamento'=>'pagamento', 'provenienza'=>'provenienza', 'stato preventivo'=>'stato preventivo','stepfase1'=>'step fase 1','stepfase2'=>'step fase 2','stepfase3'=>'step fase 3','destinatario_email'=> 'destinatario email','tipologia cartella' => 'tipologia cartella']; ?>
                 <?php echo $this->Form->select('type', $provenienza, ['empty' => 'seleziona il tipo di impostazione','label' => 'indica il tipo di impostazione']);?>
              <i class="form-group__bar"></i>
              </div>
            </div>
          </div>
          <?php if($setting->type == 'stepfase1' || $setting->type == 'stepfase2' || $setting->type == 'stepfase3') { ?>
           <?php /* <div class="row">
            <div class="col-md-6">
            <label>Indica gli utenti abilitati ad eseguire questo step</label>
              <div class="form-group">
                <?php 
                echo $this->Form->select('users', $usersList, ['empty' => 'seleziona utente abilitato all esecuzione','label' => 'indica utente']);
//                   echo $this->Form->select('users', array('label' => false,
//                   'placeholder' => 'seleziona utente abilitato all esecuzione',
//                   'type' => 'select',
//                   'class' => 'form-control',
//                   'id'=>'users',
//                   'multiple' => 'multiple',
//                   'options' => $usersList,
//                   'default' => array_keys($usersList))
//                 );
                ?>
              <i class="form-group__bar"></i>
              </div>
            </div>
          </div> */ ?>
          <div class="row">
            <div class="col-md-6">
            <label>Destinatari email:</label>
              <div class="form-group">
                <?php echo $this->Form->control('email', ['label' => '','type'=> 'text']);?>
              <i class="form-group__bar"></i>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
            <label>Timing:(espresso in giorni dalla fase precedente)</label>
              <div class="form-group">
                <?php echo $this->Form->control('timing', ['label' => '']);?>
              <i class="form-group__bar"></i>
              </div>
            </div>
          </div>
                    <div class="row">
            <div class="col-md-6">
            <label>Ordinamento</label>
              <div class="form-group">
                <?php echo $this->Form->control('position', ['label' => '']);?>
              <i class="form-group__bar"></i>
              </div>
            </div>
          </div>
          <?php } ?>
      </fieldset>
      <br/>
        <?= $this->Form->button(__('Salva'), ['class' => 'btn btn-primary']) ?>
        <?= $this->Form->end() ?>
  </div>
</div>