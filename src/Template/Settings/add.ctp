<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Setting $setting
 */
?>
<div class="card bg-white card-shadow new-contact">
    <div class="card-body">
    <?= $this->Form->create($setting) ?>
    <fieldset>
        <legend><?= __('Aggiungi impostazione') ?></legend>
                  <div class="row">
               <div class="col-md-4">
                  <div class="form-group">
                    <?php
            echo $this->Form->control('description', ['label' => 'Descrizione']);
            if ( $logged && $role == 'admin')
            {
            $provenienza = ['tipo azione'=>'azione','tipo pagamento'=>'pagamento', 'provenienza'=>'provenienza', 'stato preventivo'=>'stato preventivo','stepfase1'=>'step fase 1','stepfase2'=>'step fase 2','stepfase3'=>'step fase 3','destinatario_email'=> 'destinatario email','tipologia cartella' => 'tipologia cartella'];
            }
            elseif( $logged )
            {
            $provenienza = ['tipo azione'=>'azione','tipo pagamento'=>'pagamento', 'provenienza'=>'provenienza', 'stato preventivo'=>'stato preventivo'];
            }
            ?> <i class="form-group__bar"></i>
                  </div>
              </div>
      </div>
                  <div class="row">
               <div class="col-md-4">
                  <div class="form-group"><p>
                    Indica il tipo di impostazione</p>
                    </p>
                    <?php
            echo $this->Form->select('type', $provenienza, ['empty' => 'seleziona lo stato','label' => 'indica il tipo di impostazione']);

            //echo $this->Form->select('type', $settings_type, ['label' => 'Tipo impostazione','empty' => 'indica il tipo di impostazione']);
        ?>                      <i class="form-group__bar"></i>
                  </div>
              </div>
      </div>
        <div class="row">
               <div class="col-md-4">
                  <div class="form-group">
                    <?php
            echo $this->Form->control('email', ['label' => 'Destinatari email (separati da ;)']);

            //echo $this->Form->select('type', $settings_type, ['label' => 'Tipo impostazione','empty' => 'indica il tipo di impostazione']);
        ?>                      <i class="form-group__bar"></i>
                  </div>
              </div>
      </div>
         <div class="row">
               <div class="col-md-4">
                  <div class="form-group">
                    <?php
            echo $this->Form->control('timing', ['label' => 'Timing']);

            //echo $this->Form->select('type', $settings_type, ['label' => 'Tipo impostazione','empty' => 'indica il tipo di impostazione']);
        ?>                      <i class="form-group__bar"></i>
                  </div>
              </div>
      </div>
    </fieldset>
    <?= $this->Form->button(__('Salva')) ?>
    <?= $this->Form->end() ?>
</div>
</div>
