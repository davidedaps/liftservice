<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Supplier[]|\Cake\Collection\CollectionInterface $suppliers
 */
?>
<div class="card bg-white card-shadow">
    <div class="card-body">
        <h3><?= __('Elenco impostazioni') ?></h3>
            <table class="table table-responsive" id="data-table" >
            <thead class="thead-light">
            <tr>
                <th scope="col"><?= h('Descrizione') ?></th>
                <th scope="col"><?= h('Tipo impostazione')?></th>
                <th scope="col"><?= h('Azioni')?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($settings as $setting): ?>
            <tr>
                <td><?= h($setting->description) ?></td>
                <td><?= h($setting->type) ?></td>
                <td class="actions">
                            <?= $this->Html->link(__("<i class=\"zmdi zmdi-zoom-in zmdi-hc-fw\"></i>"), 
                                  ['action' => 'view', 'controller' => 'settings', $setting->id],
                                  ['title'=>'view','escape'=>false,'class'=>'btn btn-info btn--raised btn-sm']) ?> 
                            <?= $this->Html->link(__("<i class=\"zmdi zmdi-edit zmdi-hc-fw\"></i>"), 
                                  ['action' => 'edit', 'controller' => 'settings', $setting->id],
                                  ['title'=>'edit','escape'=>false,'class'=>'btn btn-warning btn--raised btn-sm']) ?>
                        </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
</div>