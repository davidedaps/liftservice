<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Supplier $supplier
 */
?>
<div class="card bg-white card-shadow new-contact">
    <div class="card-body">
         <h3><?= __('Modifica professionista') ?></h3>
          <br/>
          <?= $this->Form->create($professional) ?>
          <fieldset>
            <div class="row">
               <div class="col-md-4">
                  <div class="form-group">
                          <?php echo $this->Form->control('titolo' , ['label' => 'Titolo']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="form-group">
                          <?php echo $this->Form->control('nome' , ['label' => 'Nome']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="form-group">
                          <?php echo $this->Form->control('cognome' , ['label' => 'Cognome']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
            </div>
            <div class="row">
               <div class="col-md-4">
                  <div class="form-group">
                          <?php echo $this->Form->control('email' , ['label' => 'Email']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="form-group">
                          <?php echo $this->Form->control('telefono' , ['label' => 'Telefono']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
                            <div class="col-md-4">
                  <div class="form-group">
                          <?php echo $this->Form->control('studio_tecnico' , ['label' => 'Studio tecnico']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
            </div>
        </fieldset>
<!--         <legend>Uploads</legend>
            <label>(seleziona più file per caricare più di un allegato per volta):</label>
            <?php //echo $this->Form->control('uploads[]', ['type' => 'file','required'=>false,'multiple' => true,'class' => 'form-control','label' => '']); ?>
        </fieldset> -->
     <?= $this->Form->button(__('Salva'), ['class' => 'btn btn-primary']) ?>
    <?= $this->Form->end() ?>
  </div>
</div>
