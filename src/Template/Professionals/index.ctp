<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Customer[]|\Cake\Collection\CollectionInterface $customers
 */
?>
<div class="card bg-white card-shadow">
    <div class="card-body">
        <div class="responsive" style="float:left;">
          <h3><?= __('Elenco professionisti') ?></h3>
        </div>
         <div class="responsive" style="float:left;padding-left:30px;">
              <?= // link al nuovo preventivo per questo cliente
              $this->Html->link(__("<i class=\"zmdi zmdi-account-add zmdi-hc-fw\"></i> Nuovo professionista"), 
                          ['action' => 'add', 'controller' => 'Professionals'],
                          ['title'=>'new customer','escape'=>false,'class'=>'btn btn-info btn--raised']) ?>
          </div>
        <div style="clear:both"></div>
          <br/>
            <table class="table table-responsive" id="data-table" >
            <thead class="thead-light">
            <tr>
                <th scope="col"><?= h('Nominativo') ?></th>
                <th scope="col"><?= h('Email')?></th>
                <th scope="col"><?= h('Telefono') ?></th>
                <th scope="col"><?= h('Studio tecnico') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($professionals as $professional): ?>
            <?php $nominativo = $professional->titolo . " " . $professional->nome ." ". $professional->cognome;?>
            <tr>
                <td><?= $this->Html->link(__($nominativo), ['action' => 'view', $professional->id]) ?></td>
                <td><?= h($professional->email) ?></td>
                <td><?= h($professional->telefono) ?></td>
                <td><?= h($professional->studio_tecnico) ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
</div>
