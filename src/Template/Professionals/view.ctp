<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Supplier $customer
 */
?>
<div class="card bg-white card-shadow new-contact">
    <div class="card-body">
     <h3><?= __('Dettaglio professionista') ?> </h3>
      <br/> 
            <div class="row">
               <div class="col-md-4">
                      <p class="font-weight-bold">Titolo</p>
                          <?= h($professional->titolo) ?>
              </div>
              <div class="col-md-4">
                      <p class="font-weight-bold">Nome</p>
                          <?= h($professional->nome) ?>
              </div>
              <div class="col-md-4">
                      <p class="font-weight-bold">Cognome</p>
                          <?= h($professional->cognome) ?>
              </div>
            </div>
      <br/> 
            <div class="row">
               <div class="col-md-4">
                      <p class="font-weight-bold">Email</p>
                          <?= h($professional->email) ?>
              </div>
              <div class="col-md-4">
                      <p class="font-weight-bold">Telefono</p>
                          <?= h($professional->telefono) ?>
              </div>
              <div class="col-md-4">
                      <p class="font-weight-bold">Studio tecnico</p>
                          <?= h($professional->studio_tecnico) ?>
              </div>
            </div>
      <br/> 
  </div>
</div>
<!-- Button trigger modal -->
<div class="card bg-white card-shadow">
    <?= $this->element('modal_view', [
              // passo la variabile elementVariable all'elemento. 
              "elementId" => $professional->id,
              "elementName" => $professional->name,
              "modalTitle" => "Elimina professionista",
              "modalText" => "Sei sicuro di eliminare questo professionista?"
          ]) ?>
</div>

    
