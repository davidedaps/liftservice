<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Lift $lift
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $lift->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $lift->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Lifts'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Orders Customers'), ['controller' => 'OrdersCustomers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Orders Customer'), ['controller' => 'OrdersCustomers', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="lifts form large-9 medium-8 columns content">
    <?= $this->Form->create($lift) ?>
    <fieldset>
        <legend><?= __('Edit Lift') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('serial_number');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
