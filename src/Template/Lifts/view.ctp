<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Lift $lift
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Lift'), ['action' => 'edit', $lift->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Lift'), ['action' => 'delete', $lift->id], ['confirm' => __('Are you sure you want to delete # {0}?', $lift->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Lifts'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Lift'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Orders Customers'), ['controller' => 'OrdersCustomers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Orders Customer'), ['controller' => 'OrdersCustomers', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="lifts view large-9 medium-8 columns content">
    <h3><?= h($lift->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($lift->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($lift->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Serial Number') ?></th>
            <td><?= $this->Number->format($lift->serial_number) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Orders Customers') ?></h4>
        <?php if (!empty($lift->orders_customers)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Quotation Id') ?></th>
                <th scope="col"><?= __('Contract Sign Date') ?></th>
                <th scope="col"><?= __('Payment Type') ?></th>
                <th scope="col"><?= __('Order Amount') ?></th>
                <th scope="col"><?= __('Document Order Customer') ?></th>
                <th scope="col"><?= __('Lift Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($lift->orders_customers as $ordersCustomers): ?>
            <tr>
                <td><?= h($ordersCustomers->id) ?></td>
                <td><?= h($ordersCustomers->quotation_id) ?></td>
                <td><?= h($ordersCustomers->contract_sign_date) ?></td>
                <td><?= h($ordersCustomers->payment_type) ?></td>
                <td><?= h($ordersCustomers->order_amount) ?></td>
                <td><?= h($ordersCustomers->document_order_customer) ?></td>
                <td><?= h($ordersCustomers->lift_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'OrdersCustomers', 'action' => 'view', $ordersCustomers->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'OrdersCustomers', 'action' => 'edit', $ordersCustomers->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'OrdersCustomers', 'action' => 'delete', $ordersCustomers->id], ['confirm' => __('Are you sure you want to delete # {0}?', $ordersCustomers->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
