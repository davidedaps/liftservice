<script type="text/javascript">
var x=1;
$(document).ready(function(){
  
  //Aggiunge una riga in fondo alla tabella
  $("#button").click(function(){
    var countRow = x++;
    console.log(countRow);

var htmlToAppend="";
htmlToAppend += "<tr><td><div class=\"form-group text\"><label for=\"contract-products-"+countRow+"-description\"><\/label><input class=\"form-control\" type=\"text\" name=\"contract_products["+countRow+"][description]\" empty=\"1\" id=\"contract-products-"+countRow+"-description\"><i class=\"form-group__bar\"><\/i><\/div><\/td>";
htmlToAppend += "<td><div class=\"form-group checkbox\"><input class=\"form-control\" type=\"hidden\" name=\"contract_products["+countRow+"][service]\" value=\"0\"><i class=\"form-group__bar\"><\/i><input type=\"checkbox\" class=\"js-switch\" data-color=\"#2A9EFB\" data-size=\"small\" data-switchery=\"true\" style=\"display: none;\" name=\"contract_products["+countRow+"][service]\" id=\"contract-products-"+countRow+"-service\" value=\"1\"><span class=\"switchery switchery-small\" style=\"box-shadow: rgb(22["+countRow+"], 223, 223) 0px 0px 0px 0px inset; border-color: rgb(223, 223, 223); background-color: rgb(255, 255, 255); transition: border 0.4s ease 0s, box-shadow 0.4s ease 0s;\"><small style=\"left: 0px; transition: background-color 0.4s ease 0s, left 0.2s ease 0s;\"><\/small><\/span><label for=\"name=&quot;contract_products["+countRow+"][service]&quot;\" id=\"contract-products-"+countRow+"[-service\"><\/label><label for=\"contract-products-"+countRow+"[-service\"><\/label><\/div><\/td>";
htmlToAppend += "<td><div class=\"form-group checkbox\"><input class=\"form-control\" type=\"hidden\" name=\"contract_products["+countRow+"][copy_detection]\" value=\"0\"><i class=\"form-group__bar\"><\/i><input type=\"checkbox\" class=\"js-switch\" data-color=\"#2A9EFB\" data-size=\"small\" data-switchery=\"true\" style=\"display: none;\" name=\"contract_products["+countRow+"][copy_detection]\" id=\"contract-products-"+countRow+"[-copy-detection\" value=\"1\"><span class=\"switchery switchery-small\" style=\"box-shadow: rgb(223, 223, 223) 0px 0px 0px 0px inset; border-color: rgb(223, 223, 223); background-color: rgb(255, 255, 255); transition: border 0.4s ease 0s, box-shadow 0.4s ease 0s;\"><small style=\"left: 0px; transition: background-color 0.4s ease 0s, left 0.2s ease 0s;\"><\/small><\/span><label for=\"name=&quot;contract_products["+countRow+"][copy_detection]&quot;\" id=\"contract-products-"+countRow+"[-copy-detection\"><\/label><label for=\"contract-products-"+countRow+"[-copy-detection\"><\/label><\/div><\/td>";
htmlToAppend += "<td><div class=\"form-group text\"><label for=\"contract-products-"+countRow+"[-serial-number\"><\/label><input class=\"form-control\" type=\"text\" name=\"contract_products["+countRow+"][serial_number]\" empty=\"1\" id=\"contract-products-"+countRow+"[-serial-number\"><i class=\"form-group__bar\"><\/i><\/div><\/td>";
htmlToAppend += "<td><div class=\"form-group number\"><label for=\"contract-products-"+countRow+"[-quantity\"><\/label><input class=\"form-control\" type=\"number\" name=\"contract_products["+countRow+"][quantity]\" empty=\"1\" min=\"0\" step=\"1\" id=\"contract-products-"+countRow+"[-quantity\"><i class=\"form-group__bar\"><\/i><\/div><\/td>";
htmlToAppend += "<td><div class=\"form-group number\"><label for=\"contract-products-"+countRow+"[-item-price\"><\/label><input class=\"form-control\" type=\"number\" name=\"contract_products["+countRow+"][item_price]\" empty=\"1\" min=\"0\" step=\"0.01\" id=\"contract-products-"+countRow+"[-item-price\"><i class=\"form-group__bar\"><\/i><\/div><\/td>";
htmlToAppend += "<td><div class=\"form-group number\"><label for=\"contract-products-"+countRow+"[-item-price-next-year\"><\/label><input class=\"form-control\" type=\"number\" name=\"contract_products["+countRow+"][item_price_next_year]\" empty=\"1\" min=\"0\" step=\"0.01\" id=\"contract-products-"+countRow+"[-item-price-next-year\"><i class=\"form-group__bar\"><\/i><\/div><\/td>";
htmlToAppend += "<\/tr>";

    $("#tabellaDinamica > tbody").append(htmlToAppend);
  });
  
  //Rimuove l'ultima riga della tabella
  $("#buttonRemove").click(function(){
     $('#tabellaDinamica tr:last').remove();
  });
});
</script>
<div id="button" data-role="button">Aggiungi</div>
<div id="buttonRemove" data-role="button">Rimuovere</div>
<div class="table-responsive">
                      <table class="table" id="tabellaDinamica">
                        <thead>
                            <tr>
                                <th>Descrizione</th>
                                <th width="10">Servizio</th>
                                <th width="10">Ril. Copie</th>
                                <th width="200">Serial Number</th>
                                <th width="50">Quantità</th>
                                <th width="150">Anno corrente</th>
                                <th width="150">Anno prossimo</th>
                            </tr>
                        </thead>
                        <tbody>
                                                      <tr>
                                <td>
<td><div class="form-group text"><label for="contract-products-3-description"></label><input class="form-control" type="text" name="contract_products[3][description]" empty="1" id="contract-products-3-description"><i class="form-group__bar"></i></div></td>                                <td><div class="form-group checkbox"><input class="form-control" type="hidden" name="contract_products[1][service]" value="0"><i class="form-group__bar"></i><input type="checkbox" class="js-switch" data-color="#2A9EFB" data-size="small" data-switchery="true" style="display: none;" name="contract_products[1][service]" id="contract-products-1-service" value="1"><span class="switchery switchery-small" style="box-shadow: rgb(223, 223, 223) 0px 0px 0px 0px inset; border-color: rgb(223, 223, 223); background-color: rgb(255, 255, 255); transition: border 0.4s ease 0s, box-shadow 0.4s ease 0s;"><small style="left: 0px; transition: background-color 0.4s ease 0s, left 0.2s ease 0s;"></small></span><label for="name=&quot;contract_products[1][service]&quot;" id="contract-products-1-service"></label><label for="contract-products-1-service"></label></div></td>
                                <td><div class="form-group checkbox"><input class="form-control" type="hidden" name="contract_products[1][copy_detection]" value="0"><i class="form-group__bar"></i><input type="checkbox" class="js-switch" data-color="#2A9EFB" data-size="small" data-switchery="true" style="display: none;" name="contract_products[1][copy_detection]" id="contract-products-1-copy-detection" value="1"><span class="switchery switchery-small" style="box-shadow: rgb(42, 158, 251) 0px 0px 0px 11px inset; border-color: rgb(42, 158, 251); background-color: rgb(42, 158, 251); transition: border 0.4s ease 0s, box-shadow 0.4s ease 0s, background-color 1.2s ease 0s;"><small style="left: 13px; transition: background-color 0.4s ease 0s, left 0.2s ease 0s; background-color: rgb(255, 255, 255);"></small></span><label for="name=&quot;contract_products[1][copy_detection]&quot;" id="contract-products-1-copy-detection"></label><label for="contract-products-1-copy-detection"></label></div></td>
                                <td><div class="form-group text"><label for="contract-products-1-serial-number"></label><input class="form-control" type="text" name="contract_products[1][serial_number]" empty="1" id="contract-products-1-serial-number"><i class="form-group__bar"></i></div></td>
                                <td><div class="form-group number"><label for="contract-products-1-quantity"></label><input class="form-control" type="number" name="contract_products[1][quantity]" empty="1" min="0" step="1" id="contract-products-1-quantity"><i class="form-group__bar"></i></div></td>
                                <td><div class="form-group number"><label for="contract-products-1-item-price"></label><input class="form-control" type="number" name="contract_products[1][item_price]" empty="1" min="0" step="0.01" id="contract-products-1-item-price"><i class="form-group__bar"></i></div></td>
                                <td><div class="form-group number"><label for="contract-products-1-item-price-next-year"></label><input class="form-control" type="number" name="contract_products[1][item_price_next_year]" empty="1" min="0" step="0.01" id="contract-products-1-item-price-next-year"><i class="form-group__bar"></i></div></td>
                            </tr>    
                                                  </tbody>
                    </table>  
                    </div>