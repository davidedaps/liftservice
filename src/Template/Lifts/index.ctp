<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Lift[]|\Cake\Collection\CollectionInterface $lifts
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Lift'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Orders Customers'), ['controller' => 'OrdersCustomers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Orders Customer'), ['controller' => 'OrdersCustomers', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="lifts index large-9 medium-8 columns content">
    <h3><?= __('Lifts') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('serial_number') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($lifts as $lift): ?>
            <tr>
                <td><?= $this->Number->format($lift->id) ?></td>
                <td><?= h($lift->name) ?></td>
                <td><?= $this->Number->format($lift->serial_number) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $lift->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $lift->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $lift->id], ['confirm' => __('Are you sure you want to delete # {0}?', $lift->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
