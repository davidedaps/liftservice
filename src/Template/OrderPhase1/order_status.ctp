<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Supplier[]|\Cake\Collection\CollectionInterface $suppliers
 */
?>
<div class="card bg-white card-shadow">
    <div class="card-body">
        <h3><?= __('Elenco ordini clienti da attenzionare') ?></h3>
            <table class="table table-responsive" id="data-table" >
              <br/>
              <h3>Ordini in fase 1</h3>
            <thead class="thead-light">
            <tr>
                <th scope="col"><?= h('Numero ordine') ?></th>
                <th scope="col"><?= h('Numero impianto')?></th>
                <th scope="col"><?= h('Data ordine') ?></th>
                <th scope="col"><?= h('Cliente') ?></th>
                <th scope="col"><?= h('Indirizzo') ?></th>
                <th scope="col"><?= h('Step da eseguire') ?></th>
                <th scope="col"><?= h('Descrizione step da eseguire') ?></th>
                <th scope="col"><?= h('Utente incaricato') ?></th>
                <th scope="col"><?= h('Link') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($ordersToShow as $orderToShow): ?>
            <?php //pr($ordersToShow); ?>
            <?php
                  if($orderToShow->step_1_completed == 0)
                  {
            ?>
                  <tr>
                    <td><?= h($orderToShow->orders_customer->order_number); ?></td>
                    <td><?= h($orderToShow->orders_customer->installation_number); ?></td>
                    <td><?= h($orderToShow->orders_customer->contract_sign_date); ?></td>
                    <td><?= h($orderToShow->orders_customer->customer->name); ?></td>
                    <td><?= h($orderToShow->orders_customer->quotation->address." - ".$orderToShow->orders_customer->quotation->city); ?></td>
                    <td><?//= h($orderToShow->step_1_completed); ?>1</td>
                    <td><?= h($orderToShow->description_step_1); ?></td>
                    <td>
                    <?php
                      if($orderToShow->step_1_user == 1)
                        {
                        echo "Luca";
                        }
                        elseif($orderToShow->step_1_user == 10)
                        {
                        echo "Amministrazione";
                        }
                        elseif($orderToShow->step_1_user == 100)
                        {
                        echo "Commerciale";
                        }
                        elseif($orderToShow->step_1_user == 101)
                        {
                        echo "Olti";
                        } 
                    ?>
                    </td>
                    <td><?= $this->Html->link('Visualizza dettaglio', ['controller' => 'OrderPhase1', 'action' => 'view', $orderToShow->orders_customer->id], array('id' => 'action1')) ?></td>
                  </tr>
          <?php
                 }
                  elseif($orderToShow->step_2_completed == 0)
                  {
                  ?>
                  <tr>
                    <td><?= h($orderToShow->orders_customer->order_number); ?></td>
                    <td><?= h($orderToShow->orders_customer->installation_number); ?></td>
                    <td><?= h($orderToShow->orders_customer->contract_sign_date); ?></td>
                    <td><?= h($orderToShow->orders_customer->customer->name); ?></td>
                    <td><?= h($orderToShow->orders_customer->quotation->address." - ".$orderToShow->orders_customer->quotation->city); ?></td>
                    <td><?//= h($orderToShow->step_2_completed); ?>2</td>
                    <td><?= h($orderToShow->description_step_2); ?></td>
                                        <td>
                    <?php
                      if($orderToShow->step_2_user == 1)
                        {
                        echo "Luca";
                        }
                        elseif($orderToShow->step_2_user == 10)
                        {
                        echo "Amministrazione";
                        }
                        elseif($orderToShow->step_2_user == 100)
                        {
                        echo "Commerciale";
                        }
                        elseif($orderToShow->step_2_user == 101)
                        {
                        echo "Olti";
                        } 
                    ?>
                    </td>
                    <td><?= $this->Html->link('Visualizza dettaglio', ['controller' => 'OrderPhase1', 'action' => 'view', $orderToShow->orders_customer->id], array('id' => 'action1')) ?></td>
                  </tr>
                <?php
                  }
                  elseif($orderToShow->step_3_completed == 0)
                  {
                  ?>
                  <tr>
                    <td><?= h($orderToShow->orders_customer->order_number); ?></td>
                    <td><?= h($orderToShow->orders_customer->installation_number); ?></td>
                    <td><?= h($orderToShow->orders_customer->contract_sign_date); ?></td>
                    <td><?= h($orderToShow->orders_customer->customer->name); ?></td>
                    <td><?= h($orderToShow->orders_customer->quotation->address." - ".$orderToShow->orders_customer->quotation->city); ?></td>
                    <td><?//= h($orderToShow->step_3_completed); ?>3</td>
                    <td><?= h($orderToShow->description_step_3); ?></td>
                                        <td>
                    <?php
                      if($orderToShow->step_3_user == 1)
                        {
                        echo "Luca";
                        }
                        elseif($orderToShow->step_3_user == 10)
                        {
                        echo "Amministrazione";
                        }
                        elseif($orderToShow->step_3_user == 100)
                        {
                        echo "Commerciale";
                        }
                        elseif($orderToShow->step_3_user == 101)
                        {
                        echo "Olti";
                        } 
                    ?>
                    </td>
                    <td><?= $this->Html->link('Visualizza dettaglio', ['controller' => 'OrderPhase1', 'action' => 'view', $orderToShow->orders_customer->id], array('id' => 'action1')) ?></td>
                  </tr>
                  <?
                  }
                  elseif($orderToShow->step_4_completed == 0)
                  {
                  ?>
                  <tr>
                    <td><?= h($orderToShow->orders_customer->order_number); ?></td>
                    <td><?= h($orderToShow->orders_customer->installation_number); ?></td>
                    <td><?= h($orderToShow->orders_customer->contract_sign_date); ?></td>
                    <td><?= h($orderToShow->orders_customer->customer->name); ?></td>
                    <td><?= h($orderToShow->orders_customer->quotation->address." - ".$orderToShow->orders_customer->quotation->city); ?></td>           
                    <td><?//= h($orderToShow->step_4_completed); ?>4</td>
                    <td><?= h($orderToShow->description_step_4); ?></td>
                                        <td>
                    <?php
                      if($orderToShow->step_4_user == 1)
                        {
                        echo "Luca";
                        }
                        elseif($orderToShow->step_4_user == 10)
                        {
                        echo "Amministrazione";
                        }
                        elseif($orderToShow->step_4_user == 100)
                        {
                        echo "Commerciale";
                        }
                        elseif($orderToShow->step_4_user == 101)
                        {
                        echo "Olti";
                        } 
                    ?>
                    </td>
                    <td><?= $this->Html->link('Visualizza dettaglio', ['controller' => 'OrderPhase1', 'action' => 'view', $orderToShow->orders_customer->id], array('id' => 'action1')) ?></td>
                  </tr>
                  <?
                  }
                  elseif($orderToShow->step_5_completed == 0)
                  {
                  ?>
                  <tr>
                    <td><?= h($orderToShow->orders_customer->order_number); ?></td>
                    <td><?= h($orderToShow->orders_customer->installation_number); ?></td>
                    <td><?= h($orderToShow->orders_customer->contract_sign_date); ?></td>
                    <td><?= h($orderToShow->orders_customer->customer->name); ?></td>
                    <td><?= h($orderToShow->orders_customer->quotation->address." - ".$orderToShow->orders_customer->quotation->city); ?></td>         
                    <td><?//= h($orderToShow->step_5_completed); ?>5</td>
                    <td><?= h($orderToShow->description_step_5); ?></td>
                                        <td>
                    <?php
                      if($orderToShow->step_5_user == 1)
                        {
                        echo "Luca";
                        }
                        elseif($orderToShow->step_5_user == 10)
                        {
                        echo "Amministrazione";
                        }
                        elseif($orderToShow->step_5_user == 100)
                        {
                        echo "Commerciale";
                        }
                        elseif($orderToShow->step_5_user == 101)
                        {
                        echo "Olti";
                        } 
                    ?>
                    </td>
                    <td><?= $this->Html->link('Visualizza dettaglio', ['controller' => 'OrderPhase1', 'action' => 'view', $orderToShow->orders_customer->id], array('id' => 'action1')) ?></td>
                  </tr>
                  <?
                  }?>
            <!-- fine foreach -->
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
</div>