<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $orderPhase1
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $orderPhase1->order_id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $orderPhase1->order_id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Order Phase1'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="orderPhase1 form large-9 medium-8 columns content">
    <?= $this->Form->create($orderPhase1) ?>
    <fieldset>
        <legend><?= __('Edit Order Phase1') ?></legend>
        <?php
            echo $this->Form->control('step_2_user');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
