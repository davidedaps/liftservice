<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $orderPhase1
 */
?>
<div class="card bg-white card-shadow new-contact">
    <div class="card-body">
       <div class="row">
              <div class="col-md-8">
    <?= $this->Form->create($orderPhase1) ?>
    <fieldset>
        <legend><?= __('Assegnazione utente') ?></legend>
        <?php
            if($step == 1)
            {
            echo $this->Form->control('step_1_user' , ['options' => $usersQuery, 'label' => 'Seleziona utente']);
            echo $this->Form->control('step_1_created' , ['label' => 'Data prevista di esecuzione']);
            echo $this->Form->control('step_1_notes' , ['label' => 'Note relative allo step']);
            }
            if($step == 2)
            {
            echo $this->Form->control('step_2_user' , ['options' => $usersQuery, 'label' => 'Seleziona utente']);
            echo $this->Form->control('step_2_created' , ['label' => 'Data prevista di esecuzione 2']);
            echo $this->Form->control('step_2_notes' , ['label' => 'Note relative allo step']);
            }
            if($step == 3)
            {
            echo $this->Form->control('step_3_user' , ['options' => $usersQuery, 'label' => 'Seleziona utente']);
            echo $this->Form->control('step_3_created' , ['label' => 'Data prevista di esecuzione 3']);
            echo $this->Form->control('step_3_notes' , ['label' => 'Note relative allo step']);
            }
            if($step == 4)
            {
            echo $this->Form->control('step_4_user' , ['options' => $usersQuery, 'label' => 'Seleziona utente']);
            echo $this->Form->control('step_4_created' , ['label' => 'Data prevista di esecuzione']);
            echo $this->Form->control('step_4_notes' , ['label' => 'Note relative allo step']);
            }
            if($step == 5)
            {
            echo $this->Form->control('step_5_user' , ['options' => $usersQuery, 'label' => 'Seleziona utente']);
            echo $this->Form->control('step_5_created' , ['label' => 'Data prevista di esecuzione']);
            echo $this->Form->control('step_5_notes' , ['label' => 'Note relative allo step']);
            }
        ?>
    </fieldset>
      <?= $this->Form->button(__('Assegna'), ['class' => 'btn btn-primary']) ?>
    <?= $this->Form->end() ?>
         </div>
      </div>
         </div>
      </div>
