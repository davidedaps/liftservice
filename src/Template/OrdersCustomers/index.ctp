<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Customer[]|\Cake\Collection\CollectionInterface $customers
 */
?>
<div class="card bg-white card-shadow">
    <div class="card-body">
        <h3><?= __('Elenco ordini clienti') ?></h3>
            <table class="table table-responsive" id="myTable" >
            <thead class="thead-light">
            <tr>
                <th scope="col"><?= h('Riferimento ordine') ?></th>
                <th scope="col"><?= h('Riferimento preventivo') ?></th>
                <th scope="col"><?= h('Cliente') ?></th>
                <th scope="col"><?= h('Numero impianto') ?></th>             
                <th scope="col"><?= h('Data ordine')?></th>
                <th scope="col"><?= h('Modalità di pagamento') ?></th>
                <th scope="col"><?= h('Totale ordine') ?></th>
                <th scope="col"><?= h('Dettaglio avanzamento ordine') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($ordersCustomers as $orderCustomer): ?>
            <tr>
                <td><?= $this->Html->link(__($orderCustomer->order_number), ['action' => 'view', $orderCustomer->id]) ?></td>
                <td><?= $this->Html->link(__($orderCustomer->quotation->reference), ['action' => 'view', $orderCustomer->quotation->id,'controller'=>'Quotations']) ?></td>
                <td><?= $this->Html->link(__($orderCustomer->customer->name), ['action' => 'view', $orderCustomer->customer->id,'controller'=>'Customers']) ?></td>
                <td><?= h($orderCustomer->installation_number) ?></td>
                <?php   
                  $formatData = $orderCustomer->contract_sign_date;
                 
                    if($formatData == NULL)
                    {
                    $dataCorretta = '1799-01-01';
                    }
                  else
                  {
                     $dataCorretta = $formatData->i18nFormat('yyyy-MM-dd'); 
                  }
                ?> 
                <td><?= h($dataCorretta) ?></td>
                <td><?= h($orderCustomer->payment_type) ?></td>
                <td><?= h($this->Number->currency($orderCustomer->order_amount, "EUR")) ?></td>
                <td><?= $this->Html->link(__($orderCustomer->order_number), ['action' => 'view', $orderCustomer->id,'controller'=>'OrderPhase1']) ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
</div>
