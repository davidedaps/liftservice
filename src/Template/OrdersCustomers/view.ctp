<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="card bg-white card-shadow">
  <div class="card-body"><?php //pr($ordersCustomer); ?>
    <h3 class="card-title">Riferimento ordine: <?= h($ordersCustomer->order_number) ?></h3>
    <table class="table vertical-table">
        <tr>
            <th scope="row"><?= __('Numero ordine') ?></th>
            <td><?= h($ordersCustomer->order_number) ?></td>
        </tr>
          <tr>
            <th scope="row"><?= __('Cliente') ?></th>
            <td><?= $this->Html->link(__($ordersCustomer->customer->name), ['action' => 'view', $ordersCustomer->customer->id,'controller'=>'Customers']) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Riferimento preventivo') ?></th>
            <td><?= $this->Html->link(__($ordersCustomer->quotation->reference), ['action' => 'view', $ordersCustomer->quotation->id,'controller'=>'Quotations']) ?></td>
        </tr>
      <tr>
            <th scope="row"><?= __('Numero impianto') ?></th>
            <td><?= $this->Html->link(__($ordersCustomer->installation_number)) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Data ordine') ?></th>
            <td><?= h($ordersCustomer->contract_sign_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Data consegna ordine') ?></th>
            <td><?= h($ordersCustomer->expected_delivery_date	) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Totale ordine') ?></th>
            <td><?= h($this->Number->currency($ordersCustomer->order_amount)) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modalità di pagamento') ?></th>
            <td><?= h($ordersCustomer->payment_type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Abilitato invio email') ?></th>
            <td>
              <?php if($ordersCustomer->email_enabled == 0 )
              {
                echo "Ordine NON ABILITATO all'invio email.";
              } 
              else
              {
                echo "Ordine ABILITATO all'invio email.";
              }
              ?>
            </td>
        </tr>
             <tr>
            <th scope="row"><?= __('Archiviato') ?></th>
            <td><?if($ordersCustomer->archived == 1 )
              {
                echo "Ordine archiviato";
              } 
              else
              {
                echo "Ordine non archiviato.";
              }?></td>
        </tr>
    </table>
  </div>   
</div>
  <!-- Button trigger modal -->
  <div class="card bg-white card-shadow">
      <?= $this->element('modal_view', [
                // passo la variabile elementVariable all'elemento. 
                "elementId" => $ordersCustomer->id,
                "elementName" => $ordersCustomer->name,
                "modalTitle" => "Elimina ordine cliente",
                "modalText" => "Sei sicuro di eliminare questo ordine cliente?"
            ]) ?>
  </div>