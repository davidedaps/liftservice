<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="card bg-white card-shadow">
    <div class="card-body">
      <?= $this->Form->create($ordersCustomer) ?>
        <h3 class="card-title"><?= __('Modifica ordine cliente') ?></h3>
        <fieldset>
            <?php
                echo $this->Form->control('order_amount', ['label' => 'Totale ordine']);
                echo $this->Form->control('payment_type');
                echo $this->Form->control('expected_delivery_date');
                echo $this->Form->control('contract_sign_date');
                echo $this->Form->control('installation_number');
                echo $this->Form->control('archived',['label' => 'Archiviare ordine? digitare 1 per archiviare,0 per NON archiviare']);
                /*if ( $logged && $role == 'admin')
                {
                    echo $this->Form->control('archived', ['label' => 'Abilitazione invio email, digitare 0 per disabilitare, 1 per abilitare.']);
                }*/
            ?>
        </fieldset>
        <?= $this->Form->button(__('Salva'), ['class' => 'btn btn-primary']) ?>
        <?= $this->Form->end() ?>
  </div>
</div>