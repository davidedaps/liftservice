<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\OrdersCustomer $ordersCustomer
 */
?>
<script>
  
$(document).ready(function(){
    $("#customer-id").change(function(){
        var selectedCustomer = $(this).children("option:selected").val();
    });
});

$(document).ready(function() { 
  $("#is_customer").click(function() {
    $("#new_customer").attr('disabled', !$("#new_customer").attr('disabled'));
  }); 
}); 
  

 $(document).ready(function(){
		$("#order_number").prop("disabled", true);
		$(".checkEnable").click(function(){
            if($(this).prop("checked") == true){
                $("#order_number").prop("disabled", false);
            }
            else if($(this).prop("checked") == false){
                $("#order_number"]).prop("disabled", true);
            }
        });
    });

  
</script>

<script type="text/javascript">
         $(document).ready(function () {
            $('#btn1').click(function () {
               $('#order-number').attr('readonly', 'readonly');
            });
            $('#btn2').click(function () {
                $('#order-number').removeAttr('readonly');
            });
         });
      </script>
<?php
$last_num_order_value->description;
$num_order_year_value->description;
$num_order_suggested = $num_order_year_value->description."-".$last_num_order_value->description;
?>
<div class="card bg-white card-shadow new-contact">
    <div class="card-body">
         <h3><?= __('Verifica dati preliminare') ?></h3>
         <h4><?= __('Riferimento preventivo: ').$quotationsToAdd->reference; ?></h4>
          <br/>
          <?= $this->Form->create($ordersCustomer) ?>
          <fieldset>
            <div class="row">
              <div class="col-md-4">
                  <div class="form-group">
                          <?php //echo $this->Form->control('customer_id', ['options' => $customers]);?>
                          <?php echo $this->Form->control('customer_id', ['value' => $quotationsToAdd->customer_id,'label'=> 'Indica il contatto']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="form-group">
                          <?php echo $this->Form->control('order_number',['value' => $num_order_suggested, 'label'=> 'Numero ordine','readonly'=>'readonly']);?>
                      <i class="form-group__bar"></i>
         <button type="button" id="btn2">Vuoi modificare il numero ordine?</button>
                  </div>
              </div>
							<div class="col-md-4">
                  <div class="form-group">
                          <?php echo $this->Form->control('installation_number',['label'=> 'Numero impianto']);?>
                      <i class="form-group__bar"></i>
        
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="form-group">                 
                          <?php echo $this->Form->hidden('quotation_id', ['value' => $quotationsToAdd->id]); ?>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="form-group">
                    <label>Diventa cliente</label>
                          <?php 
                            echo $this->Form->checkbox('is_customer', ['id' => 'is_customer']);
                           ?>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="form-group">
                    <label>Crea nuovo cliente</label>
                          <?php 
                            echo $this->Form->checkbox('new_customer', ['id' => 'new_customer']);
                           ?>
                  </div>
              </div>
           </div>
            <div class="row">
              <div class="col-md-4">
                  <div class="form-group">
                          <?php echo $this->Form->control('contract_sign_date',['label'=> 'Data ordine']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="form-group">
                          <?php echo $this->Form->control('payment_type',['label'=> 'Modalità di pagamento']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="form-group">
                          <?php echo $this->Form->control('order_amount',['label'=> 'Importo ordine']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
            </div>
        <?php
                    //echo $this->Form->control('document_order_customer');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Salva'), ['class' => 'btn btn-primary']) ?>
    <?= $this->Form->end() ?>
</div>
</div>
