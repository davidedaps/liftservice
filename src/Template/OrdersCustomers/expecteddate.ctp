<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Customer[]|\Cake\Collection\CollectionInterface $customers
 */
?>
<script>
var historiektable = $('#data-table').DataTable({
    "paging" : false,
    "ordering" : true,
    "scrollCollapse" : true,
    "searching" : false,
    "columnDefs" : [{"targets":3, "type":"date-eu"}],
    "bInfo": true
});
</script>
<div class="card bg-white card-shadow">
    <div class="card-body">
        <h3><?= __('Elenco ordini clienti per data di scadenza') ?></h3>
            <table class="table table-responsive" id="data-table" data-order="[[ 5, &quot;desc&quot; ]]">
            <thead class="thead-light">
            <tr>
                <th scope="col"><?= h('Riferimento ordine') ?></th>
                <th scope="col"><?= h('Riferimento preventivo') ?></th>
                <th scope="col"><?= h('Cliente') ?></th>
                <th scope="col"><?= h('Numero impianto') ?></th>             
                <th scope="col"><?= h('Data ordine')?></th>
                <th scope="col"><?= h('Settimana consegna prevista')?></th>
                <th scope="col"><?= h('Totale ordine') ?></th>
                <th scope="col"><?= h('Dettaglio avanzamento ordine') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($ordersCustomers as $orderCustomer): ?>
            <tr>
                <td><?= $this->Html->link(__($orderCustomer->order_number), ['action' => 'view', $orderCustomer->id]) ?></td>
                <td><?= $this->Html->link(__($orderCustomer->quotation->reference), ['action' => 'view', $orderCustomer->quotation->id,'controller'=>'Quotations']) ?></td>
                <td><?= $this->Html->link(__($orderCustomer->customer->name), ['action' => 'view', $orderCustomer->customer->id,'controller'=>'Customers']) ?></td>
                <td><?= h($orderCustomer->installation_number) ?></td>
                <td><?= h($orderCustomer->contract_sign_date) ?></td> 
                <?php   
                  $formatData = $orderCustomer->expected_delivery_date;
                 
                    if($formatData == NULL)
                    {
                    $dataCorretta = '';

                    }
                  else
                  {
                     $dataCorretta = date("W", strtotime($orderCustomer->expected_delivery_date->format("Y-m-d")));
                  }
                  
                ?>
                <td>
                <?= $formatData === null ? '' : h(date("W", strtotime($orderCustomer->expected_delivery_date->format("Y-m-d"))))." del ".$orderCustomer->expected_delivery_date->format("Y") ?>
                <?= $formatData === null ? '' :  $this->Html->link(' <i class="zmdi zmdi-calendar-check zmdi-hc-lg"></i>', 
                                             ['action' => 'ics',$orderCustomer->id] , ['escape'=>false, "title" => "aggiungi al calendario"]); ?>
                </td>
                <td><?= h($this->Number->currency($orderCustomer->order_amount, "EUR")) ?></td>
                <td><?= $this->Html->link(__($orderCustomer->order_number), ['action' => 'view', $orderCustomer->id,'controller'=>'OrderPhase1']) ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
</div>
