<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\OrderPhase3 $orderPhase3
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Order Phase3'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="orderPhase3 form large-9 medium-8 columns content">
    <?= $this->Form->create($orderPhase3) ?>
    <fieldset>
        <legend><?= __('Add Order Phase3') ?></legend>
        <?php
            echo $this->Form->control('phase_completed');
            echo $this->Form->control('description_step_1');
            echo $this->Form->control('step_1_created');
            echo $this->Form->control('step_1_completed');
            echo $this->Form->control('step_1_completedtime');
            echo $this->Form->control('description_step_2');
            echo $this->Form->control('step_2_created');
            echo $this->Form->control('step_2_completed');
            echo $this->Form->control('step_2_completedtime');
            echo $this->Form->control('description_step_3');
            echo $this->Form->control('step_3_created');
            echo $this->Form->control('step_3_completed');
            echo $this->Form->control('step_3_completedtime');
            echo $this->Form->control('description_step_4');
            echo $this->Form->control('step_4_created');
            echo $this->Form->control('step_4_completed');
            echo $this->Form->control('step_4_completedtime');
            echo $this->Form->control('description_step_5');
            echo $this->Form->control('step_5_created');
            echo $this->Form->control('completed_step_5');
            echo $this->Form->control('step_5_completedtime');
            echo $this->Form->control('description_step_6');
            echo $this->Form->control('step_6_created');
            echo $this->Form->control('completed_step_6');
            echo $this->Form->control('step_6_completedtime');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
