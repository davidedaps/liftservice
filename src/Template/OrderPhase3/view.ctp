<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $orderPhase1
 */
?>
<div class="content__inner">
                    <div class="invoice">
                        <div class="invoice__header">
                            <img class="invoice__logo" src="demo/img/invoice-logo.png" alt="">
                        </div>
                     <div class="row">
                     <div class="col-8">
                      <h3>Dettaglio ordine: <?= $this->Html->link($orderCustomerValues->name, ['controller' => 'OrdersCustomers', 'action' => 'view', $orderDetailValues->id]) ?></h3>
                      <h3>Indirizzo installazione: <?= h($orderQuotationValues->reference. " - " .$orderQuotationValues->address. " - " .$orderQuotationValues->city ) ?></h3>
                      <h3>Numero impianto: <?= h($orderDetailValues->installation_number) ?></h3>
                      </div>
                          <div class="col-4">      
                            <?php
                            if($orderDetailValues->email_enabled = 0)
                            {
                            ?>
                              <div class="invoice__attrs__item">
                              <label class="" for="customCheck1" style="background-color:red;color:white">INVIO EMAIL NON ABILITATO</label>
                              </div>
                            <?php
                            }
                            else
                            {
                            ?>
                              <div class="invoice__attrs__item">
                              <label class="" for="customCheck1" style="background-color:green;color:white">INVIO EMAIL ABILITATO</label>
                              </div>
                            <?php
                            }
                            ?>
                          </div>
                       </div>
                      <br/>
                        <div class="row invoice__attrs">
                            <div class="col-3">
                                <div class="invoice__attrs__item">
                                    <small>Numero ordine</small>
                                    <h3><?= h($orderDetailValues->order_number) ?></h3>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="invoice__attrs__item">
                                    <small>Data ordine</small>
                                    <h3><?= h($orderDetailValues->contract_sign_date) ?>	</h3>
                                </div>
                            </div>

                            <div class="col-3">
                                <div class="invoice__attrs__item">
                                    <small>Cliente</small>
                                    <h3><?= $this->Html->link($orderCustomerValues->name, ['controller' => 'Customers', 'action' => 'view', $orderCustomerValues->id]) ?></h3>
                                </div>
                            </div>

                            <div class="col-3">
                                <div class="invoice__attrs__item">
                                    <small>Totale ordine</small>
                                     <h3><?= h($this->Number->currency($orderDetailValues->order_amount, "EUR")) ?></h3>
                                </div>
                            </div>
                        </div>
                      <div class="tab-container">
                                <ul class="nav nav-tabs nav-fill" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link show active" data-toggle="tab" href="#pagamenti" role="tab" aria-selected="true">Pagamenti</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link show" data-toggle="tab" href="#gestione_documentale" role="tab" aria-selected="false">Caricamento documenti</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#area3" role="tab">Elenco documenti</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#area4" role="tab"></a>
                                    </li>
                                </ul>

                                <div class="tab-content">
                                    <div class="tab-pane fade active show" id="pagamenti" role="tabpanel">
  <div class="row">
    <div class="col-4"> 
        <span>
          <b>Pagamenti cliente</b>
        </span>
        <table class="table table-sm table-responsive">
                                        <thead class="thead-light">
                                          <tr>
                                            <th scope="col"><?= h('Data') ?></th>
                                            <th scope="col"><?= h('Descrizione')?></th>
                                            <th scope="col"><?= h('Importo') ?></th>
                                            <th scope="col"><?= h('Tipo') ?></th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($orderPaymentList as $orderPayment): ?>
                                          <tr>
                                            <td><?= h($orderPayment->date) ?></td>
                                            <td><?= h($orderPayment->description) ?></td>
                                            <td><?= h($orderPayment->amount) ?></td>
                                            <td><?= h($orderPayment->type) ?></td>
                                          </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                      </table>
    </div>
    <div class="col-4">        
        <span>
          <strong>Pagamenti fornitore</strong>
        </span>
        <table class="table table-sm table-responsive">
                                        <thead class="thead-light">
                                          <tr>
                                            <th scope="col"><?= h('Data') ?></th>
                                            <th scope="col"><?= h('Descrizione')?></th>
                                            <th scope="col"><?= h('Importo') ?></th>
                                            <th scope="col"><?= h('Tipo') ?></th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        if(!empty($orderPaymentSupplierList))
                                        {
                                        ?>
                                        <?php foreach ($orderPaymentSupplierList as $orderPaymentSupplier): ?>
                                          <tr>
                                            <td><?= h($orderPaymentSupplier->date) ?></td>
                                            <td><?= h($orderPaymentSupplier->description) ?></td>
                                            <td><?= h($orderPaymentSupplier->amount) ?></td>
                                            <td><?= h($orderPaymentSupplier->type) ?></td>
                                          </tr>
                                        <?php endforeach; ?>
                                        <?php 
                                        }
                                        else
                                        {
                                          
                                        }
                                        ?>
                                        </tbody>
      </table>
    </div>
    <div class="col-4">        
      <span>
          <b>Andamento ordine</b>
      </span>
      <div class="invoice__attrs__item">
                                    <small>Totale incassi</small>
                                     <h3><?= h($this->Number->currency($somma_incassi)) ?></h3>

                                    <small>Totale spese</small>
                                    <h3><?= h($this->Number->currency($somma_spese)) ?></h3>

                                    <small>Saldo ordine</small>
                                     <h3><?= h($this->Number->currency($saldo)) ?></h3>
                                </div>
</div>
</div>
                            
                                    </div>
                                    <div class="tab-pane fade" id="gestione_documentale" role="tabpanel">
                                           <?= $this->Form->create($uploadOrder,['type' => 'file']) ?>
                                            <fieldset>
                                              <legend>Uploads</legend>
                                              <?php
                                              echo $this->Form->control('filename', ['type' => 'file','required'=>false,'multiple' => false,'class' => 'form-control','label' => '']);
                                              echo $this->Form->hidden('order_customer_id', ['value'=>$orderDetailValues->id]);
                                              echo $this->Form->hidden('order_number', ['value'=>$orderDetailValues->order_number]);
                                              echo $this->Form->hidden('quotation_id', ['value'=>$orderDetailValues->quotation_id]);
                                              echo $this->Form->control('docfolder' , 
                                              ['options' => $folder_list, 'label' => 'Seleziona la cartella']);
                                              ?>
                                            </fieldset>
                                            <?= $this->Form->button(__('Salva'), ['class' => 'btn btn-primary']) ?>
                                            <?= $this->Form->end() ?>
                                    </div>
                                    <div class="tab-pane fade" id="area3" role="tabpanel">
                                                <table class="table table-responsive" id="data-table" >
                                                          <thead class="thead-light">
                                                          <tr>
                                                              <th scope="col"><?= h('Nome del file') ?></th>
                                                              <th scope="col"><?= h('Cartella') ?></th>
                                                          </tr>
                                                      </thead>
                                                      <tbody>
                                                          <?php foreach ($uploadsList as $uploadsList): ?>
                                                          <tr>
                                                            <td><?= $this->Html->link($uploadsList->filename,
                                                                                      "files/uploads/filename/".$uploadsList->dir."/".$uploadsList->filename,
                                                                                      [ 'target' => '_blank']
                                                                                     ) ?></td>
                                                              <td><?php echo $uploadsList->docfolder ?></td>
                                                          </tr>
                                                          <?php endforeach; ?>
                                                      </tbody>
                                                  </table>
                                    </div>
                                    <div class="tab-pane fade" id="area4" role="tabpanel">
                                        <p>Nullam id dolor id nibh ultricies vehicula ut id elit. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Maecenas faucibus mollis interdum.</p>
                                    </div>
                                </div>
                            </div>
                     
                      <h4>
                        Avanzamento fasi
                      </h4>  
                      <span>PREPARAZIONE ALLA PRODUZIONE</span>
                      <div class="progress">
                         <?php
                         if($orderPhase1->step_1_completed == 0)
                         {
                           $barLength = 0;
                         }
                         if($orderPhase1->step_1_completed == 1)
                         {
                           $barLength = 20;
                         }
                         if($orderPhase1->step_2_completed == 1)
                         {
                           $barLength = 40;
                         }
                        if($orderPhase1->step_3_completed == 1)
                         {
                           $barLength = 60;
                         }
                        if($orderPhase1->step_4_completed == 1)
                         {
                           $barLength = 80;
                         }
                        if($orderPhase1->step_5_completed == 1)
                         {
                           $barLength = 100;
                         }
                        // fase 2
                         if($orderPhase2->step_1_completed == 0)
                         {
                           $barLength2 = 0;
                         }
                         if($orderPhase2->step_1_completed == 1)
                         {
                           $barLength2 = 11;
                         }
                         if($orderPhase2->step_2_completed == 1)
                         {
                           $barLength2 = 22;
                         }
                        if($orderPhase2->step_3_completed == 1)
                         {
                           $barLength2 = 33;
                         }
                        if($orderPhase2->step_4_completed == 1)
                         {
                           $barLength2 = 44;
                         }
                        if($orderPhase2->step_5_completed == 1)
                         {
                           $barLength2 = 55;
                         }
                         if($orderPhase2->step_6_completed == 1)
                         {
                           $barLength2 = 66;
                         }
                         if($orderPhase2->step_7_completed == 1)
                         {
                           $barLength2 = 77;
                         }
                         if($orderPhase2->step_8_completed == 1)
                         {
                           $barLength2 = 88;
                         }
                          if($orderPhase2->step_9_completed == 1)
                         {
                           $barLength2 = 100;
                         }
                        // fase 3
                          if($orderPhase3->step_1_completed == 0)
                         {
                           $barLength3 = 0;
                         }
                         if($orderPhase3->step_1_completed == 1)
                         {
                           $barLength3 = 16;
                         }
                         if($orderPhase3->step_2_completed == 1)
                         {
                           $barLength3 = 32;
                         }
                        if($orderPhase3->step_3_completed == 1)
                         {
                           $barLength3 = 48;
                         }
                        if($orderPhase3->step_4_completed == 1)
                         {
                           $barLength3 = 72;
                         }
                        if($orderPhase3->step_5_completed == 1)
                         {
                           $barLength3 = 84;
                         }
                        if($orderPhase3->step_6_completed == 1)
                         {
                           $barLength3 = 100;
                         }
                         ?>
                            <div class="progress-bar bg-success progress-bar-striped" role="progressbar" style="width:<?php echo $barLength;?>%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                                            <span>PREPARAZIONE AL MONTAGGIO</span>
                      <div class="progress">
                            <div class="progress-bar bg-info progress-bar-striped" role="progressbar" style="width:<?php echo $barLength2;?>%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                                            <span>PREPARAZIONE ALLA MESSA IN FUNZIONE</span>
                      <div class="progress">
                            <div class="progress-bar bg-warning progress-bar-striped" role="progressbar" style="width:<?php echo $barLength3;?>%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <br/><br/>
                        <table class="table table-bordered invoice__table">
                            <thead>
                                <tr class="text-uppercase">
                                    <th>PREPARAZIONE ALLA PRODUZIONE - STEP</th>
                                    <th>DATA PREVISTA ESECUZIONE</th>
                                    <th>ESEGUITO?</th>
                                    <th>ASSEGNA UTENTE</th>
                                    <th>UTENTE INCARICATO</th>
                                    <th>STATO AZIONE</th>
                                    <th>DATA EFFETTIVA ESECUZIONE</th>
                                    <th>NOTE RELATIVE</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                  <?php
                                  $step1 = '1';
                                  ?>
                                    <td><?= h($orderPhase1->description_step_1) ?></td>
                                    <td><?= h($orderPhase1->step_1_created) ?></td>
                                    <td>
                                      <?php
                                        if($orderPhase1->step_1_completed)
                                        { 
                                      ?>
                                       <i class="zmdi zmdi-thumb-up zmdi-hc-fw text-green"></i>
                                            <?php 
                                            }
                                            else
                                            {
                                            ?>
                                              <i class="zmdi zmdi-thumb-up zmdi-hc-fw text-red"></i>
                                            <?php 
                                            } 
                                            ?>
                                    </td>  
                                    <?php
                                            if($orderPhase1->step_1_user != '')
                                            { 
                                    ?>
                                    <td>
                                      <?php echo "Utente gia assegnato";?>
                                    </td>
                                    <td>
                                   <?php 
                                              if($orderPhase1->step_1_user == 1)
                                              {
                                              echo "Luca";
                                              }
                                              elseif($orderPhase1->step_1_user == 10)
                                              {
                                              echo "Amministrazione";
                                              }
                                              elseif($orderPhase1->step_1_user == 100)
                                              {
                                              echo "Commerciale";
                                              }
                                              elseif($orderPhase1->step_1_user == 101)
                                              {
                                              echo "Olti";
                                              } 
                                   ?>
                                   </td>
                                   <?php
                                            }
                                            else
                                            {
                                    ?>
                                    <td>
                                    <?= $this->Html->link("Assegna utente", ['controller' => 'OrderPhase1', 'action' => 'edit', $orderDetailValues->id,$step1], array('id' => 'action1')) ?>
                                    </td>
                                    <td></td>
                                    <?php
                                            }
                                    ?>
                                    <?php
                                            if($orderPhase1->step_1_completed == 0)
                                            { 
                                    ?>
                                    <td><i class="zmdi zmdi-play-circle zmdi-hc-fw text-green"></i>
                                      <?= $this->Html->link("Esegui azione", ['controller' => 'OrderPhase1', 'action' => 'execStep1', $orderDetailValues->id], array('id' => 'action1')) ?>
                                    </td>
                                    <?php
                                            }
                                            else
                                            {
                                    ?>
                                    <td><i class="zmdi zmdi-play-circle zmdi-hc-fw text-red"></i>
                                      <?= h("Azione già eseguita")?>
                                    </td>
                                   
                                      <?php
                                      if($orderPhase1->step_1_completedtime == '')
                                            {
                                        ?>
                                      <td></td>
                                        <?php
                                      }
                                              else
                                              {
                                       ?><td>
                                       <?= h($orderPhase1->step_1_completedtime) ?>
                                      </td>
                                      <?php
                                       }
                                       ?>
                                
                                    <?php
                                            }
                                    ?>          
<!--                                <td><button class="btn btn-light" type="submit">Azione</button></td> -->
                                  <?php if($orderPhase1->step_1_notes == '')
                                          {
                                  ?>
                                          <td><?= $this->Html->link('Aggiungi nota', ['controller' => 'OrderPhase1', 'action' => 'edit', $orderDetailValues->id,$step1], array('id' => 'action1')) ?></td>
                                  <?php
                                          } 
                                        else
                                          {
                                  ?>
                                          <td><?php 
                                          echo "".$this->Html->link(substr($orderPhase1->step_1_notes,0,15)." ... ", ['controller' => 'OrderPhase1', 'action' => 'edit', $orderDetailValues->id,$step1], array('id' => 'action1')) ?></td>
                                  <?php
                                          }
                                  ?>
                                </tr>
                                <tr>
                                  <?php
                                  $step2 = '2';
                                  ?>
                                    <td><?= h($orderPhase1->description_step_2) ?></td>
                                    <td><?= h($orderPhase1->step_2_created) ?></td>
                                                                        <td><?php
                                            if($orderPhase1->step_2_completed)
                                            { ?>
                                            <i class="zmdi zmdi-thumb-up zmdi-hc-fw text-green"></i>
                                            <?php }
                                            else
                                            {?>
                                              <i class="zmdi zmdi-thumb-up zmdi-hc-fw text-red"></i>
                                            <?php } ?>
                                    </td>
                                    <?php
                                            if($orderPhase1->step_2_user !='')
                                            { 
                                    ?>
                                  <td><?php echo "Utente gia assegnato";?></td>
                                  <td>
                                   <?php 
                                              if($orderPhase1->step_2_user == 1)
                                              {
                                              echo "Luca";
                                              }
                                              elseif($orderPhase1->step_2_user == 10)
                                              {
                                              echo "Amministrazione";
                                              }
                                              elseif($orderPhase1->step_2_user == 100)
                                              {
                                              echo "Commerciale";
                                              }
                                              elseif($orderPhase1->step_2_user == 101)
                                              {
                                              echo "Olti";
                                              } 
                                   ?>
                                  </td>
                                   <?php
                                            }
                                            else
                                            {
                                    ?>
                                    <td>
                                   <?= $this->Html->link("Assegna utente", ['controller' => 'OrderPhase1', 'action' => 'edit', $orderDetailValues->id,$step2], array('id' => 'action1')) ?>
                                  
                                    </td>
                                  <td></td>
                                  <?php
                                            }
                                    ?> <?php
                                            if($orderPhase1->step_2_completed == 0)
                                            { 
                                    ?>
                                    <td><i class="zmdi zmdi-play-circle zmdi-hc-fw text-green"></i>
                                      <?= $this->Html->link("Esegui azione", ['controller' => 'OrderPhase1', 'action' => 'execStep2', $orderDetailValues->id], array('id' => 'action2')) ?>
                                    </td>
                                    <?php
                                            }
                                            else
                                            {
                                    ?>
                                    <td><i class="zmdi zmdi-play-circle zmdi-hc-fw text-red"></i>
                                      <?= h("Azione già eseguita")?>
                                    </td>
                                                                      <?php
                                            }
                                    ?>   
                                      <?php
                                      if($orderPhase1->step_2_completedtime == '')
                                            {
                                        ?>
                                      <td></td>
                                        <?php
                                      }
                                      else
                                      {
                                       ?><td>
                                       <?= h($orderPhase1->step_2_completedtime) ?>
                                      </td>
                                      <?php
                                       }
                                       ?>
                                      <?php if($orderPhase1->step_2_notes == '')
                                          {
                                  ?>
                                          <td><?= $this->Html->link('Aggiungi nota', ['controller' => 'OrderPhase1', 'action' => 'edit', $orderDetailValues->id,$step2], array('id' => 'action1')) ?></td>
                                  <?php
                                          } 
                                        else
                                        {
                                  ?>
                                          <td><?php 
                                          echo "".$this->Html->link(substr($orderPhase1->step_2_notes,0,15)." ... ", ['controller' => 'OrderPhase1', 'action' => 'edit', $orderDetailValues->id,$step2], array('id' => 'action1')) ?></td>
                                  <?php
                                        }
                                  ?>
                                </tr>
                                <tr>
                                  <?php
                                  $step3 = '3';
                                  ?>
                                    <td><?= h($orderPhase1->description_step_3) ?></td>
                                    <td><?= h($orderPhase1->step_3_created) ?></td>
                                                                        <td><?php
                                            if($orderPhase1->step_3_completed)
                                            { ?>
                                             <i class="zmdi zmdi-thumb-up zmdi-hc-fw text-green"></i>
                                            <?php }
                                            else
                                            {?>
                                              <i class="zmdi zmdi-thumb-up zmdi-hc-fw text-red"></i>
                                            <?php } ?>
                                    </td>
                                          <?php
                                            if($orderPhase1->step_3_user !='')
                                            { 
                                           ?>
                                  <td><?php echo "Utente gia assegnato";?></td>
                                  <td>
                                   <?php 
                                              if($orderPhase1->step_3_user == 1)
                                              {
                                              echo "Luca";
                                              }
                                              elseif($orderPhase1->step_3user == 10)
                                              {
                                              echo "Amministrazione";
                                              }
                                              elseif($orderPhase1->step_3_user == 100)
                                              {
                                              echo "Commerciale";
                                              }
                                              elseif($orderPhase1->step_3_user == 101)
                                              {
                                              echo "Olti";
                                              } 
                                   ?>
                                  </td>
                                  
                                   <?php
                                            }
                                            else
                                            {
                                    ?>
                                    <td>
                                   <?= $this->Html->link("Assegna utente", ['controller' => 'OrderPhase1', 'action' => 'edit', $orderDetailValues->id,$step3], array('id' => 'action3')) ?>
                                  
                                    </td><td></td>
                                  <?php
                                            }
                                    ?>
 <?php
                                            if($orderPhase1->step_3_completed == 0)
                                            { 
                                    ?>
                                    <td><i class="zmdi zmdi-play-circle zmdi-hc-fw text-green"></i>
                                      <?= $this->Html->link("Esegui azione", ['controller' => 'OrderPhase1', 'action' => 'execStep3', $orderDetailValues->id], array('id' => 'action3')) ?>
                                    </td>
                                    <?php
                                            }
                                            else
                                            {
                                    ?>
                                    <td><i class="zmdi zmdi-play-circle zmdi-hc-fw text-red"></i>
                                      <?= h("Azione già eseguita")?>
                                    </td>
                                                                      <?php
                                            }
                                    ?>
                                      <?php
                                      if($orderPhase1->step_3_completedtime == '')
                                            {
                                        ?>
                                      <td></td>
                                        <?php
                                      }
                                      else
                                      {
                                       ?><td>
                                       <?= h($orderPhase1->step_3_completedtime) ?>
                                      </td>
                                      <?php
                                       }
                                       ?>

                                  <?php if($orderPhase1->step_3_notes == '')
                                          {
                                  ?>
                                          <td><?= $this->Html->link('Aggiungi nota', ['controller' => 'OrderPhase1', 'action' => 'edit', $orderDetailValues->id,$step3], array('id' => 'action1')) ?></td>
                                  <?php
                                          } 
                                        else
                                        {
                                  ?>
                                          <td><?php 
                                          echo "".$this->Html->link(substr($orderPhase1->step_3_notes,0,15)." ... ", ['controller' => 'OrderPhase1', 'action' => 'edit', $orderDetailValues->id,$step3], array('id' => 'action1')) ?></td>
                                  <?php
                                        }
                                  ?>
                                </tr>
                                <tr>
                                  <?php
                                  $step4 = '4';
                                  ?>
                                    <td><?= h($orderPhase1->description_step_4) ?></td>
                                    <td><?= h($orderPhase1->step_4_created) ?></td>
                                                                        <td><?php
                                            if($orderPhase1->step_4_completed)
                                            { ?>
                                            <i class="zmdi zmdi-thumb-up zmdi-hc-fw text-green"></i>
                                            <?php }
                                            else
                                            {?>
                                              <i class="zmdi zmdi-thumb-up zmdi-hc-fw text-red"></i>
                                            <?php } ?>
                                    </td>
                                    <?php
                                            if($orderPhase1->step_4_user !='')
                                            { 
                                    ?>
                                  <td><?php echo "Utente gia assegnato";?></td>
                                  <td>
                                   <?php 
                                              if($orderPhase1->step_4_user == 1)
                                              {
                                              echo "Luca";
                                              }
                                              elseif($orderPhase1->step_4_user == 10)
                                              {
                                              echo "Amministrazione";
                                              }
                                              elseif($orderPhase1->step_4_user == 100)
                                              {
                                              echo "Commerciale";
                                              }
                                              elseif($orderPhase1->step_4_user == 101)
                                              {
                                              echo "Olti";
                                              } 
                                   ?>
                                  </td>
                                  
                                   <?php
                                            }
                                            else
                                            {
                                    ?>
                                    <td>
                                   <?= $this->Html->link("Assegna utente", ['controller' => 'OrderPhase1', 'action' => 'edit', $orderDetailValues->id,$step4], array('id' => 'action1')) ?>
                                  
                                    </td><td></td>
                                  <?php
                                            }
                                    ?> <?php
                                            if($orderPhase1->step_4_completed == 0)
                                            { 
                                    ?>
                                    <td><i class="zmdi zmdi-play-circle zmdi-hc-fw text-green"></i>
                                      <?= $this->Html->link("Esegui azione", ['controller' => 'OrderPhase1', 'action' => 'execStep4', $orderDetailValues->id], array('id' => 'action2')) ?>
                                    </td>
                                    <?php
                                            }
                                            else
                                            {
                                    ?>
                                    <td><i class="zmdi zmdi-play-circle zmdi-hc-fw text-red"></i>
                                      <?= h("Azione già eseguita")?>
                                    </td>
                                       <?php
                                            }

                                      if($orderPhase1->step_4_completedtime == '')
                                            {
                                        ?>
                                      <td></td>
                                        <?php
                                      }
                                      else
                                      {
                                       ?><td>
                                       <?= h($orderPhase1->step_4_completedtime) ?>
                                      </td>
                                      <?php
                                       }
                                       ?>

                                    <?php if($orderPhase1->step_4_notes == '')
                                          {
                                  ?>
                                          <td><?= $this->Html->link('Aggiungi nota', ['controller' => 'OrderPhase1', 'action' => 'edit', $orderDetailValues->id,$step4], array('id' => 'action1')) ?></td>
                                  <?php
                                          } 
                                        else
                                        {
                                  ?>
                                          <td><?php 
                                          echo "".$this->Html->link(substr($orderPhase1->step_4_notes,0,15)." ... ",  ['controller' => 'OrderPhase1', 'action' => 'edit', $orderDetailValues->id,$step4], array('id' => 'action1')) ?></td>
                                  <?php
                                        }
                                  ?>
                                </tr>
                              
                                 <tr>
                                  <?php
                                  $step5 = '5';
                                  ?>
                                    <td><?= h($orderPhase1->description_step_5) ?></td>
                                    <td><?= h($orderPhase1->step_5_created) ?></td>
                                                                        <td><?php
                                            if($orderPhase1->step_5_completed)
                                            { ?>
                                            <i class="zmdi zmdi-thumb-up zmdi-hc-fw text-green"></i>
                                            <?php }
                                            else
                                            {?>
                                              <i class="zmdi zmdi-thumb-up zmdi-hc-fw text-red"></i>
                                            <?php } ?>
                                    </td>
                                    <?php
                                            if($orderPhase1->step_5_user !='')
                                            { 
                                    ?>
                                  <td><?php echo "Utente gia assegnato";?></td>
                                  <td>
                                  <?php 
                                              if($orderPhase1->step_5_user == 1)
                                              {
                                              echo "Luca";
                                              }
                                              elseif($orderPhase1->step_5_user == 10)
                                              {
                                              echo "Amministrazione";
                                              }
                                              elseif($orderPhase1->step_5_user == 100)
                                              {
                                              echo "Commerciale";
                                              }
                                              elseif($orderPhase1->step_5_user == 101)
                                              {
                                              echo "Olti";
                                              } 
                                   ?>
                                  </td>
                                  
                                   <?php
                                            }
                                            else
                                            {
                                    ?>
                                    <td>
                                   <?= $this->Html->link("Assegna utente", ['controller' => 'OrderPhase1', 'action' => 'edit', $orderDetailValues->id,$step5], array('id' => 'action1')) ?>
                                  
                                    </td><td></td>
                                  <?php
                                            }
                                    ?> <?php
                                            if($orderPhase1->step_5_completed == 0)
                                            { 
                                    ?>
                                    <td><i class="zmdi zmdi-play-circle zmdi-hc-fw text-green"></i>
                                      <?= $this->Html->link("Esegui azione", ['controller' => 'OrderPhase1', 'action' => 'execStep5', $orderDetailValues->id], array('id' => 'action2')) ?>
                                    </td>
                                    <?php
                                            }
                                            else
                                            {
                                    ?>
                                    <td><i class="zmdi zmdi-play-circle zmdi-hc-fw text-red"></i>
                                      <?= h("Azione già eseguita")?>
                                    </td>                                    <?php
                                            }
                                    ?>
                                      <?php
                                      if($orderPhase1->step_5_completedtime == '')
                                            {
                                        ?>
                                      <td></td>
                                        <?php
                                      }
                                      else
                                      {
                                       ?><td>
                                       <?= h($orderPhase1->step_5_completedtime) ?>
                                      </td>
                                      <?php
                                       }
                                       ?>

                                   <?php if($orderPhase1->step_5_notes == '')
                                          {
                                  ?>
                                          <td><?= $this->Html->link('Aggiungi nota', ['controller' => 'OrderPhase1', 'action' => 'edit', $orderDetailValues->id,$step5], array('id' => 'action1')) ?></td>
                                  <?php
                                          } 
                                        else
                                        {
                                  ?>
                                          <td>
                                            <?php 
                                          echo "".$this->Html->link(substr($orderPhase1->step_5_notes,0,15)." ... ", ['controller' => 'OrderPhase1', 'action' => 'edit', $orderDetailValues->id,$step5], array('id' => 'action1')) 
                                            ?></td>
                                  <?php
                                        }
                                  ?>
                                </tr>
                                <!--  -->
                            </tbody>
                        </table>
                        <!-- -->
                        <table class="table table-bordered invoice__table">
                            <thead>
                                <tr class="text-uppercase">
                                    <th>PREPARAZIONE ALLA PRODUZIONE - STEP</th>
                                    <th>DATA PREVISTA ESECUZIONE</th>
                                    <th>ESEGUITO?</th>
                                    <th>ASSEGNA UTENTE</th>
                                    <th>UTENTE INCARICATO</th>
                                    <th>STATO AZIONE</th>
                                    <th>DATA EFFETTIVA ESECUZIONE</th>
                                    <th>NOTE RELATIVE</th>
                                </tr>
                            </thead>
                            <tbody>
                              <tr>
                                    <td><?= h($orderPhase2->description_step_1) ?></td>
                                    <td><?= h($orderPhase2->step_1_created) ?></td>
                                                                        <td><?php
                                            if($orderPhase2->step_1_completed)
                                            { ?>
                                            <i class="zmdi zmdi-thumb-up zmdi-hc-fw text-green"></i>
                                            <?php }
                                            else
                                            {?>
                                              <i class="zmdi zmdi-thumb-up zmdi-hc-fw text-red"></i>
                                            <?php } ?>
                                    </td>
                                <!--INIZIO CONTROLLO USER ASSIGNED -->
                                    <?php
                                              $step2 = 1;
                                            if($orderPhase2->step_1_user != '')
                                            { 
                                    ?>
                                    <td>
                                      <?php echo "Utente gia assegnato";?>
                                    </td>
                                    <td>
                                   <?php 
                                              if($orderPhase2->step_1_user == 1)
                                              {
                                              echo "Luca";
                                              }
                                              elseif($orderPhase2->step_1_user == 10)
                                              {
                                              echo "Amministrazione";
                                              }
                                              elseif($orderPhase2->step_1_user == 100)
                                              {
                                              echo "Commerciale";
                                              }
                                              elseif($orderPhase2->step_1_user == 101)
                                              {
                                              echo "Olti";
                                              } 
                                   ?>
                                  </td>
                                   <?php
                                            }
                                            else
                                            {
                                    ?>
                                    <td>
                                    <?= $this->Html->link("Assegna utente", ['controller' => 'OrderPhase2', 'action' => 'edit', $orderDetailValues->id,$step2], array('id' => 'action1')) ?>
                                    </td>
                                    <td></td>
                                    <?php
                                            }
                                    ?>
                                    <!-- FINE CONTROLLO USER ASSIGNED -->
                                    <?php
                                            if($orderPhase2->step_1_completed == 0)
                                            { 
                                    ?>
                               
                                    <td><i class="zmdi zmdi-play-circle zmdi-hc-fw text-green"></i>
                                      <?= $this->Html->link("Esegui azione", ['controller' => 'OrderPhase2', 'action' => 'execPhase2Step2', $orderDetailValues->id], array('id' => 'action2')) ?>
                                    </td><td></td>
                                    <?php
                                            }
                                            else
                                            {
                                    ?>
                                    <td><i class="zmdi zmdi-play-circle zmdi-hc-fw text-red"></i>
                                      <?= h("Azione già eseguita")?>
                                    </td>
                                                                    <td>
                                    <?= h($orderPhase2->step_1_completedtime) ?>
                                    </td>
                                    <?php
                                            }
                                    ?>
                                 <?php if($orderPhase2->step_1_notes == '')
                                          {
                                  ?>
                                          <td><?= $this->Html->link('Aggiungi nota', ['controller' => 'OrderPhase2', 'action' => 'edit', $orderDetailValues->id,$step2], array('id' => 'action1')) ?></td>
                                  <?php
                                          } 
                                        else
                                        {
                                  ?>
                                          <td>
                                            <?php 
                                          echo "".$this->Html->link(substr($orderPhase2->step_1_notes,0,15)." ... ", ['controller' => 'OrderPhase2', 'action' => 'edit', $orderDetailValues->id,$step2], array('id' => 'action1')) 
                                            ?></td>
                                  <?php
                                        }
                                  ?>
                                </tr>
                              <tr>
                                    <td><?= h($orderPhase2->description_step_2) ?></td>
                                    <td><?= h($orderPhase2->step_2_created) ?></td>
                                                                        <td><?php
                                            if($orderPhase2->step_2_completed)
                                            { ?>
                                            <i class="zmdi zmdi-thumb-up zmdi-hc-fw text-green"></i>
                                            <?php }
                                            else
                                            {?>
                                              <i class="zmdi zmdi-thumb-up zmdi-hc-fw text-red"></i>
                                            <?php } ?>
                                    </td>
                                <!--INIZIO CONTROLLO USER ASSIGNED -->
                                    <?php
                                              $step2 = 2;
                                            if($orderPhase2->step_2_user != '')
                                            { 
                                    ?>
                                    <td>
                                      <?php echo "Utente gia assegnato";?>
                                    </td>
                                    <td>
                                   <?php 
                                              if($orderPhase2->step_2_user == 1)
                                              {
                                              echo "Luca";
                                              }
                                              elseif($orderPhase2->step_2_user == 10)
                                              {
                                              echo "Amministrazione";
                                              }
                                              elseif($orderPhase2->step_2_user == 100)
                                              {
                                              echo "Commerciale";
                                              }
                                              elseif($orderPhase2->step_2_user == 101)
                                              {
                                              echo "Olti";
                                              } 
                                   ?>
                                  </td>
                                   <?php
                                            }
                                            else
                                            {
                                    ?>
                                    <td>
                                    <?= $this->Html->link("Assegna utente", ['controller' => 'OrderPhase2', 'action' => 'edit', $orderDetailValues->id,$step2], array('id' => 'action1')) ?>
                                    </td>
                                    <td></td>
                                    <?php
                                            }
                                    ?>
                                    <!-- FINE CONTROLLO USER ASSIGNED -->
                                    <?php
                                            if($orderPhase2->step_2_completed == 0)
                                            { 
                                    ?>
                               
                                    <td><i class="zmdi zmdi-play-circle zmdi-hc-fw text-green"></i>
                                      <?= $this->Html->link("Esegui azione", ['controller' => 'OrderPhase2', 'action' => 'execPhase2Step2', $orderDetailValues->id], array('id' => 'action2')) ?>
                                    </td><td></td>
                                    <?php
                                            }
                                            else
                                            {
                                    ?>
                                    <td><i class="zmdi zmdi-play-circle zmdi-hc-fw text-red"></i>
                                      <?= h("Azione già eseguita")?>
                                    </td>
                                                                    <td>
                                    <?= h($orderPhase2->step_2_completedtime) ?>
                                    </td>
                                    <?php
                                            }
                                    ?>
                                 <?php if($orderPhase2->step_2_notes == '')
                                          {
                                  ?>
                                          <td><?= $this->Html->link('Aggiungi nota', ['controller' => 'OrderPhase2', 'action' => 'edit', $orderDetailValues->id,$step2], array('id' => 'action1')) ?></td>
                                  <?php
                                          } 
                                        else
                                        {
                                  ?>
                                          <td>
                                            <?php 
                                          echo "".$this->Html->link(substr($orderPhase2->step_2_notes,0,15)." ... ", ['controller' => 'OrderPhase2', 'action' => 'edit', $orderDetailValues->id,$step2], array('id' => 'action1')) 
                                            ?></td>
                                  <?php
                                        }
                                  ?>
                                </tr>

                                <tr>
                                    <td><?= h($orderPhase2->description_step_3) ?></td>
                                    <td><?= h($orderPhase2->step_3_created) ?></td>
                                                                        <td><?php
                                            if($orderPhase2->step_3_completed)
                                            { ?>
                                             <i class="zmdi zmdi-thumb-up zmdi-hc-fw text-green"></i>
                                            <?php }
                                            else
                                            {?>
                                              <i class="zmdi zmdi-thumb-up zmdi-hc-fw text-red"></i>
                                            <?php } ?>
                                    </td>                                   <!--INIZIO CONTROLLO USER ASSIGNED -->
                                    <?php
                                              $step2 = 3;
                                            if($orderPhase2->step_3_user != '')
                                            { 
                                    ?>
                                    <td>
                                      <?php echo "Utente gia assegnato";?>
                                    </td>
                                    <td>
                                   <?php 
                                              if($orderPhase2->step_3_user == 1)
                                              {
                                              echo "Luca";
                                              }
                                              elseif($orderPhase2->step_3_user == 10)
                                              {
                                              echo "Amministrazione";
                                              }
                                              elseif($orderPhase2->step_3_user == 100)
                                              {
                                              echo "Commerciale";
                                              }
                                              elseif($orderPhase2->step_3_user == 101)
                                              {
                                              echo "Olti";
                                              } 
                                   ?>
                                  </td>
                                   <?php
                                            }
                                            else
                                            {
                                    ?>
                                    <td>
                                    <?= $this->Html->link("Assegna utente", ['controller' => 'OrderPhase2', 'action' => 'edit', $orderDetailValues->id,$step2], array('id' => 'action1')) ?>
                                    </td>
                                    <td></td>
                                    <?php
                                            }
                                    ?>
                                    <!-- FINE CONTROLLO USER ASSIGNED -->
 <?php
                                            if($orderPhase2->step_3_completed == 0)
                                            { 
                                    ?>
                                    <td><i class="zmdi zmdi-play-circle zmdi-hc-fw text-green"></i>
                                      <?= $this->Html->link("Esegui azione", ['controller' => 'OrderPhase2', 'action' => 'execPhase2Step3', $orderDetailValues->id], array('id' => 'action3')) ?>
                                    </td><td></td>
                                    <?php
                                            }
                                            else
                                            {
                                    ?>
                                    <td><i class="zmdi zmdi-play-circle zmdi-hc-fw text-red"></i>
                                      <?= h("Azione già eseguita")?>
                                    </td>
                                    <td>
                                    <?= h($orderPhase2->step_3_completedtime) ?>
                                    </td> 
                                    <?php
                                            }
                                    ?>
                                  <?php if($orderPhase2->step_3_notes == '')
                                          {
                                  ?>
                                          <td><?= $this->Html->link('Aggiungi nota', ['controller' => 'OrderPhase2', 'action' => 'edit', $orderDetailValues->id,$step2], array('id' => 'action1')) ?></td>
                                  <?php
                                          } 
                                        else
                                        {
                                  ?>
                                          <td>
                                            <?php 
                                          echo "".$this->Html->link(substr($orderPhase2->step_3_notes,0,15)." ... ", ['controller' => 'OrderPhase2', 'action' => 'edit', $orderDetailValues->id,$step2], array('id' => 'action1')) 
                                            ?></td>
                                  <?php
                                        }
                                  ?>
                                </tr>

                                <tr>
                                    <td><?= h($orderPhase2->description_step_4 )?></td>
                                    <td><?= h($orderPhase2->step_4_created) ?></td>
                                                                        <td><?php
                                            if($orderPhase2->step_4_completed)
                                            { ?>
                                             <i class="zmdi zmdi-thumb-up zmdi-hc-fw text-green"></i>
                                            <?php }
                                            else
                                            {?>
                                              <i class="zmdi zmdi-thumb-up zmdi-hc-fw text-red"></i>
                                            <?php } ?>
                                    </td> 
                                  <!--INIZIO CONTROLLO USER ASSIGNED -->
                                    <?php
                                              $step2 = 4;
                                            if($orderPhase2->step_4_user != '')
                                            { 
                                    ?>
                                    <td>
                                      <?php echo "Utente gia assegnato";?>
                                    </td>
                                    <td>
                                   <?php 
                                              if($orderPhase2->step_4_user == 1)
                                              {
                                              echo "Luca";
                                              }
                                              elseif($orderPhase2->step_4_user == 10)
                                              {
                                              echo "Amministrazione";
                                              }
                                              elseif($orderPhase2->step_4_user == 100)
                                              {
                                              echo "Commerciale";
                                              }
                                              elseif($orderPhase2->step_4_user == 101)
                                              {
                                              echo "Olti";
                                              } 
                                   ?>
                                  </td>
                                   <?php
                                            }
                                            else
                                            {
                                    ?>
                                    <td>
                                    <?= $this->Html->link("Assegna utente", ['controller' => 'OrderPhase2', 'action' => 'edit', $orderDetailValues->id,$step2], array('id' => 'action1')) ?>
                                    </td>
                                    <td></td>
                                    <?php
                                            }
                                    ?>
                                    <!-- FINE CONTROLLO USER ASSIGNED -->
 <?php
                                            if($orderPhase2->step_4_completed == 0)
                                            { 
                                    ?>
                                  
                                    <td><i class="zmdi zmdi-play-circle zmdi-hc-fw text-green"></i>
                                      <?= $this->Html->link("Esegui azione", ['controller' => 'OrderPhase2', 'action' => 'execPhase2Step4', $orderDetailValues->id], array('id' => 'action4')) ?>
                                    </td><td></td>
                                    <?php
                                            }
                                            else
                                            {
                                    ?>
                                    <td><i class="zmdi zmdi-play-circle zmdi-hc-fw text-red"></i>
                                      <?= h("Azione già eseguita")?>
                                    </td>
                                                                      <td>
                                    <?= h($orderPhase2->step_4_completedtime) ?>
                                    </td>
                                    <?php
                                            }
                                    ?>   
                                  <?php if($orderPhase2->step_4_notes == '')
                                          {
                                  ?>
                                          <td><?= $this->Html->link('Aggiungi nota', ['controller' => 'OrderPhase2', 'action' => 'edit', $orderDetailValues->id,$step2], array('id' => 'action1')) ?></td>
                                  <?php
                                          } 
                                        else
                                        {
                                  ?>
                                          <td>
                                            <?php 
                                          echo "".$this->Html->link(substr($orderPhase2->step_4_notes,0,15)." ... ", ['controller' => 'OrderPhase2', 'action' => 'edit', $orderDetailValues->id,$step2], array('id' => 'action1')) 
                                            ?></td>
                                  <?php
                                        }
                                  ?>
                                </tr>
                              
                                <tr>
                                    <td><?= h($orderPhase2->description_step_5) ?></td>
                                    <td><?= h($orderPhase2->step_5_created) ?></td>
                                                                        <td><?php
                                            if($orderPhase2->step_5_completed)
                                            { ?>
                                              <i class="zmdi zmdi-thumb-up zmdi-hc-fw text-green"></i>
                                            <?php }
                                            else
                                            {?>
                                              <i class="zmdi zmdi-thumb-up zmdi-hc-fw text-red"></i>
                                            <?php } ?>
                                    </td>                                  <!--INIZIO CONTROLLO USER ASSIGNED -->
                                    <?php
                                              $step2 = 5;
                                            if($orderPhase2->step_5_user != '')
                                            { 
                                    ?>
                                    <td>
                                      <?php echo "Utente gia assegnato";?>
                                    </td>
                                    <td>
                                   <?php 
                                              if($orderPhase2->step_5_user == 1)
                                              {
                                              echo "Luca";
                                              }
                                              elseif($orderPhase2->step_5_user == 10)
                                              {
                                              echo "Amministrazione";
                                              }
                                              elseif($orderPhase2->step_5_user == 100)
                                              {
                                              echo "Commerciale";
                                              }
                                              elseif($orderPhase2->step_5_user == 101)
                                              {
                                              echo "Olti";
                                              } 
                                   ?>
                                  </td>
                                   <?php
                                            }
                                            else
                                            {
                                    ?>
                                    <td>
                                    <?= $this->Html->link("Assegna utente", ['controller' => 'OrderPhase2', 'action' => 'edit', $orderDetailValues->id,$step2], array('id' => 'action1')) ?>
                                    </td>
                                    <td></td>
                                    <?php
                                            }
                                    ?>
                                    <!-- FINE CONTROLLO USER ASSIGNED --> 
                                                                    <?php
                                            if($orderPhase2->step_5_completed == 0)
                                            { 
                                    ?>
                                    <td><i class="zmdi zmdi-play-circle zmdi-hc-fw text-green"></i>
                                      <?= $this->Html->link("Esegui azione", ['controller' => 'OrderPhase2', 'action' => 'execPhase2Step5', $orderDetailValues->id], array('id' => 'action5')) ?>
                                    </td><td></td>
                                    <?php
                                            }
                                            else
                                            {
                                    ?>
                                    <td><i class="zmdi zmdi-play-circle zmdi-hc-fw text-red"></i>
                                      <?= h("Azione già eseguita")?>
                                    </td>
                                                                      <td>
                                    <?= h($orderPhase2->step_5_completedtime) ?>
                                    </td>
                                    <?php
                                            }
                                    ?>
                                  <?php if($orderPhase2->step_5_notes == '')
                                          {
                                  ?>
                                          <td><?= $this->Html->link('Aggiungi nota', ['controller' => 'OrderPhase2', 'action' => 'edit', $orderDetailValues->id,$step2], array('id' => 'action1')) ?></td>
                                  <?php
                                          } 
                                        else
                                        {
                                  ?>
                                          <td>
                                            <?php 
                                          echo "".$this->Html->link(substr($orderPhase2->step_5_notes,0,15)." ... ", ['controller' => 'OrderPhase2', 'action' => 'edit', $orderDetailValues->id,$step2], array('id' => 'action1')) 
                                            ?></td>
                                  <?php
                                        }
                                  ?>
                                </tr>
                               <tr>
                                    <td><?= h($orderPhase2->description_step_6) ?></td>
                                    <td><?= h($orderPhase2->step_6_created) ?></td>
                                                                        <td><?php
                                            if($orderPhase2->step_6_completed)
                                            { ?>
                                              <i class="zmdi zmdi-thumb-up zmdi-hc-fw text-green"></i>
                                            <?php }
                                            else
                                            {?>
                                              <i class="zmdi zmdi-thumb-up zmdi-hc-fw text-red"></i>
                                            <?php } ?>
                                    </td>                                  <!--INIZIO CONTROLLO USER ASSIGNED -->
                                    <?php
                                              $step2 = 6;
                                            if($orderPhase2->step_6_user != '')
                                            { 
                                    ?>
                                    <td>
                                      <?php echo "Utente gia assegnato";?>
                                    </td>
                                    <td>
                                                                      <?php 
                                              if($orderPhase2->step_6_user == 1)
                                              {
                                              echo "Luca";
                                              }
                                              elseif($orderPhase2->step_6_user == 10)
                                              {
                                              echo "Amministrazione";
                                              }
                                              elseif($orderPhase2->step_6_user == 100)
                                              {
                                              echo "Commerciale";
                                              }
                                              elseif($orderPhase2->step_6_user == 101)
                                              {
                                              echo "Olti";
                                              } 
                                   ?>
                                  </td>
                                   <?php
                                            }
                                            else
                                            {
                                    ?>
                                    <td>
                                    <?= $this->Html->link("Assegna utente", ['controller' => 'OrderPhase2', 'action' => 'edit', $orderDetailValues->id,$step2], array('id' => 'action1')) ?>
                                    </td>
                                    <td></td>
                                    <?php
                                            }
                                    ?>
                                    <!-- FINE CONTROLLO USER ASSIGNED --> 
                                                                    <?php
                                            if($orderPhase2->step_6_completed == 0)
                                            { 
                                    ?>
                                    <td><i class="zmdi zmdi-play-circle zmdi-hc-fw text-green"></i>
                                      <?= $this->Html->link("Esegui azione", ['controller' => 'OrderPhase2', 'action' => 'execPhase2Step6', $orderDetailValues->id], array('id' => 'action5')) ?>
                                    </td><td></td>
                                    <?php
                                            }
                                            else
                                            {
                                    ?>
                                    <td><i class="zmdi zmdi-play-circle zmdi-hc-fw text-red"></i>
                                      <?= h("Azione già eseguita")?>
                                    </td>
                                                                     <td>
                                    <?= h($orderPhase2->step_6_completedtime) ?>
                                    </td>
                                    <?php
                                            }
                                    ?>
                                 <?php if($orderPhase2->step_6_notes == '')
                                          {
                                  ?>
                                          <td><?= $this->Html->link('Aggiungi nota', ['controller' => 'OrderPhase2', 'action' => 'edit', $orderDetailValues->id,$step2], array('id' => 'action1')) ?></td>
                                  <?php
                                          } 
                                        else
                                        {
                                  ?>
                                          <td>
                                            <?php 
                                          echo "".$this->Html->link(substr($orderPhase2->step_6_notes,0,15)." ... ", ['controller' => 'OrderPhase2', 'action' => 'edit', $orderDetailValues->id,$step2], array('id' => 'action1')) 
                                            ?></td>
                                  <?php
                                        }
                                  ?>
                                </tr>
                               <tr>
                                    <td><?= h($orderPhase2->description_step_7) ?></td>
                                    <td><?= h($orderPhase2->step_7_created) ?></td>
                                                                        <td><?php
                                            if($orderPhase2->step_7_completed)
                                            { ?>
                                              <i class="zmdi zmdi-thumb-up zmdi-hc-fw text-green"></i>
                                            <?php }
                                            else
                                            {?>
                                              <i class="zmdi zmdi-thumb-up zmdi-hc-fw text-red"></i>
                                            <?php } ?>
                                    </td>                                   <!--INIZIO CONTROLLO USER ASSIGNED -->
                                    <?php
                                              $step2 = 7;
                                            if($orderPhase2->step_7_user != '')
                                            { 
                                    ?>
                                    <td>
                                      <?php echo "Utente gia assegnato";?>
                                    </td>
                                    <td>
                                   <?php 
                                              if($orderPhase2->step_7_user == 1)
                                              {
                                              echo "Luca";
                                              }
                                              elseif($orderPhase2->step_7_user == 10)
                                              {
                                              echo "Amministrazione";
                                              }
                                              elseif($orderPhase2->step_7_user == 100)
                                              {
                                              echo "Commerciale";
                                              }
                                              elseif($orderPhase2->step_7_user == 101)
                                              {
                                              echo "Olti";
                                              } 
                                   ?>
                                  </td>
                                   <?php
                                            }
                                            else
                                            {
                                    ?>
                                    <td>
                                    <?= $this->Html->link("Assegna utente", ['controller' => 'OrderPhase2', 'action' => 'edit', $orderDetailValues->id,$step2], array('id' => 'action1')) ?>
                                    </td>
                                    <td></td>
                                    <?php
                                            }
                                    ?>
                                    <!-- FINE CONTROLLO USER ASSIGNED -->
                                                                    <?php
                                            if($orderPhase2->step_7_completed == 0)
                                            { 
                                    ?>
                                    <td><i class="zmdi zmdi-play-circle zmdi-hc-fw text-green"></i>
                                      <?= $this->Html->link("Esegui azione", ['controller' => 'OrderPhase2', 'action' => 'execPhase2Step7', $orderDetailValues->id], array('id' => 'action5')) ?>
                                    </td><td></td>
                                    <?php
                                            }
                                            else
                                            {
                                    ?>
                                    <td><i class="zmdi zmdi-play-circle zmdi-hc-fw text-red"></i>
                                      <?= h("Azione già eseguita")?>
                                    </td>
                                                                     <td>
                                    <?= h($orderPhase2->step_7_completedtime) ?>
                                    </td>
                                    <?php
                                            }
                                    ?>
                                 <?php if($orderPhase2->step_7_notes == '')
                                          {
                                  ?>
                                          <td><?= $this->Html->link('Aggiungi nota', ['controller' => 'OrderPhase2', 'action' => 'edit', $orderDetailValues->id,$step2], array('id' => 'action1')) ?></td>
                                  <?php
                                          } 
                                        else
                                        {
                                  ?>
                                          <td>
                                            <?php 
                                          echo "".$this->Html->link(substr($orderPhase2->step_7_notes,0,15)." ... ", ['controller' => 'OrderPhase2', 'action' => 'edit', $orderDetailValues->id,$step2], array('id' => 'action1')) 
                                            ?></td>
                                  <?php
                                        }
                                  ?>
                                </tr>
                               <tr>
                                    <td><?= h($orderPhase2->description_step_8) ?></td>
                                    <td><?= h($orderPhase2->step_8_created) ?></td>
                                                                        <td><?php
                                            if($orderPhase2->step_8_completed)
                                            { ?>
                                              <i class="zmdi zmdi-thumb-up zmdi-hc-fw text-green"></i>
                                            <?php }
                                            else
                                            {?>
                                              <i class="zmdi zmdi-thumb-up zmdi-hc-fw text-red"></i>
                                            <?php } ?>
                                    </td>                                   <!--INIZIO CONTROLLO USER ASSIGNED -->
                                    <?php
                                              $step2 = 8;
                                            if($orderPhase2->step_8_user != '')
                                            { 
                                    ?>
                                    <td>
                                      <?php echo "Utente gia assegnato";?>
                                    </td>
                                    <td>
                                   <?php 
                                              if($orderPhase2->step_8_user == 1)
                                              {
                                              echo "Luca";
                                              }
                                              elseif($orderPhase2->step_8_user == 10)
                                              {
                                              echo "Amministrazione";
                                              }
                                              elseif($orderPhase2->step_8_user == 100)
                                              {
                                              echo "Commerciale";
                                              }
                                              elseif($orderPhase2->step_8_user == 101)
                                              {
                                              echo "Olti";
                                              } 
                                   ?>
                                  </td>
                                   <?php
                                            }
                                            else
                                            {
                                    ?>
                                    <td>
                                    <?= $this->Html->link("Assegna utente", ['controller' => 'OrderPhase2', 'action' => 'edit', $orderDetailValues->id,$step2], array('id' => 'action1')) ?>
                                    </td>
                                    <td></td>
                                    <?php
                                            }
                                    ?>
                                    <!-- FINE CONTROLLO USER ASSIGNED -->
                                                                    <?php
                                            if($orderPhase2->step_8_completed == 0)
                                            { 
                                    ?>
                                    <td><i class="zmdi zmdi-play-circle zmdi-hc-fw text-green"></i>
                                      <?= $this->Html->link("Esegui azione", ['controller' => 'OrderPhase2', 'action' => 'execPhase2Step8', $orderDetailValues->id], array('id' => 'action5')) ?>
                                    </td><td></td>
                                    <?php
                                            }
                                            else
                                            {
                                    ?>
                                    <td><i class="zmdi zmdi-play-circle zmdi-hc-fw text-red"></i>
                                      <?= h("Azione già eseguita")?>
                                    </td>
                                                                     <td>
                                    <?= h($orderPhase2->step_8_completedtime) ?>
                                    </td>
                                    <?php
                                            }
                                    ?> 
                                 <?php if($orderPhase2->step_8_notes == '')
                                          {
                                  ?>
                                          <td><?= $this->Html->link('Aggiungi nota', ['controller' => 'OrderPhase2', 'action' => 'edit', $orderDetailValues->id,$step2], array('id' => 'action1')) ?></td>
                                  <?php
                                          } 
                                        else
                                        {
                                  ?>
                                          <td>
                                            <?php 
                                          echo "".$this->Html->link(substr($orderPhase2->step_8_notes,0,15)." ... ", ['controller' => 'OrderPhase2', 'action' => 'edit', $orderDetailValues->id,$step2], array('id' => 'action1')) 
                                            ?></td>
                                  <?php
                                        }
                                  ?>
                                </tr>
                               <tr>
                                    <td><?= h($orderPhase2->description_step_9) ?></td>
                                    <td><?= h($orderPhase2->step_9_created) ?></td>
                                                                        <td><?php
                                            if($orderPhase2->step_9_completed)
                                            { ?>
                                              <i class="zmdi zmdi-thumb-up zmdi-hc-fw text-green"></i>
                                            <?php }
                                            else
                                            {?>
                                              <i class="zmdi zmdi-thumb-up zmdi-hc-fw text-red"></i>
                                            <?php } ?>
                                    </td> 
                                                                    <?php
                                            if($orderPhase2->step_9completed == 0)
                                            { 
                                    ?>                                  <!--INIZIO CONTROLLO USER ASSIGNED -->
                                    <?php
                                            $step2 = 9;
                                            if($orderPhase2->step_9_user != '')
                                            { 
                                    ?>
                                    <td>
                                      <?php echo "Utente gia assegnato";?>
                                    </td>
                                    <td>
                                   <?php 
                                              if($orderPhase2->step_9_user == 1)
                                              {
                                              echo "Luca";
                                              }
                                              elseif($orderPhase2->step_9_user == 10)
                                              {
                                              echo "Amministrazione";
                                              }
                                              elseif($orderPhase2->step_9_user == 100)
                                              {
                                              echo "Commerciale";
                                              }
                                              elseif($orderPhase2->step_9_user == 101)
                                              {
                                              echo "Olti";
                                              } 
                                   ?>
                                  </td>
                                   <?php
                                            }
                                            else
                                            {
                                    ?>
                                    <td>
                                    <?= $this->Html->link("Assegna utente", ['controller' => 'OrderPhase2', 'action' => 'edit', $orderDetailValues->id,$step2], array('id' => 'action1')) ?>
                                    </td>
                                    <td></td>
                                    <?php
                                            }
                                    ?>
                                    <!-- FINE CONTROLLO USER ASSIGNED -->
                                    <td><i class="zmdi zmdi-play-circle zmdi-hc-fw text-green"></i>
                                      <?= $this->Html->link("Esegui azione", ['controller' => 'OrderPhase2', 'action' => 'execPhase2Step9', $orderDetailValues->id], array('id' => 'action5')) ?>
                                    </td><td></td>
                                    <?php
                                            }
                                            else
                                            {
                                    ?>
                                    <td><i class="zmdi zmdi-play-circle zmdi-hc-fw text-red"></i>
                                      <?= h("Azione già eseguita")?>
                                    </td>
                                   <td>
                                    <?= h($orderPhase2->step_9_completedtime) ?>
                                    </td>
                                    <?php
                                            }
                                    ?>
                                 <?php if($orderPhase2->step_9_notes == '')
                                          {
                                  ?>
                                          <td><?= $this->Html->link('Aggiungi nota', ['controller' => 'OrderPhase2', 'action' => 'edit', $orderDetailValues->id,$step2], array('id' => 'action1')) ?></td>
                                  <?php
                                          } 
                                        else
                                        {
                                  ?>
                                          <td>
                                            <?php 
                                          echo "".$this->Html->link(substr($orderPhase2->step_9_notes,0,15)." ... ", ['controller' => 'OrderPhase2', 'action' => 'edit', $orderDetailValues->id,$step2], array('id' => 'action1')) 
                                            ?></td>
                                  <?php
                                        }
                                  ?>
                                </tr>
                                <!--  -->
                            </tbody>
                        </table>
<!-- -->
                        <table class="table table-bordered invoice__table">
                            <thead>
                                <tr class="text-uppercase">
                                    <th>PREPARAZIONE ALLA PRODUZIONE - STEP</th>
                                    <th>DATA PREVISTA ESECUZIONE</th>
                                    <th>ESEGUITO?</th>
                                    <th>ASSEGNA UTENTE</th>
                                    <th>UTENTE INCARICATO</th>
                                    <th>STATO AZIONE</th>
                                    <th>DATA EFFETTIVA ESECUZIONE</th>
                                    <th>NOTE</th>
                                </tr>
                            </thead>
                            <tbody>
                            <tr>
                                    <td><?= h($orderPhase3->description_step_1) ?></td>
                                    <td><?= h($orderPhase3->step_1_created) ?></td>
                                                                        <td><?php
                                            if($orderPhase2->step_1_completed)
                                            { ?>
                                            <i class="zmdi zmdi-thumb-up zmdi-hc-fw text-green"></i>
                                            <?php }
                                            else
                                            {?>
                                              <i class="zmdi zmdi-thumb-up zmdi-hc-fw text-red"></i>
                                            <?php } ?>
                                    </td> 
                                    <?php
                                            $step2 = 1;
                                            if($orderPhase3->step_1_user != '' || is_null($orderPhase3->step_1_user))
                                            { 
                                    ?>
                                    <td>
                                      <?php echo "Utente gia assegnato";?>
                                    </td>
                                    <td>
                                   <?php 
                                              if($orderPhase3->step_1_user == 1)
                                              {
                                              echo "Luca";
                                              }
                                              elseif($orderPhase3->step_1_user == 10)
                                              {
                                              echo "Amministrazione";
                                              }
                                              elseif($orderPhase3->step_1_user == 100)
                                              {
                                              echo "Commerciale";
                                              }
                                              elseif($orderPhase3->step_1_user == 101)
                                              {
                                              echo "Olti";
                                              } 
                                   ?>
                                  </td>
                                   <?php
                                            }
                                            else
                                            {
                                    ?>
                                    <td>
                                    <?= $this->Html->link("Assegna utente", ['controller' => 'OrderPhase3', 'action' => 'edit', $orderDetailValues->id,$step2], array('id' => 'action1')) ?>
                                    </td>
                              <?php
                                            }
                              ?>
                                                                  <?php
                                            if($orderPhase3->step_1_completed == 0)
                                            { 
                                    ?>
                                    <td><i class="zmdi zmdi-play-circle zmdi-hc-fw text-green"></i>
                                      <?= $this->Html->link("Esegui azione", ['controller' => 'OrderPhase3', 'action' => 'execPhase3Step1', $orderDetailValues->id], array('id' => 'action2')) ?>
                                    </td>
                                    <?php
                                            }
                                            else
                                            {
                                    ?>
                                    <td><i class="zmdi zmdi-play-circle zmdi-hc-fw text-red"></i>
                                      <?= h("Azione già eseguita")?>
                                    </td>
                                    <?php
                                            }
                                    ?>
                              <td>
                                    <?= h($orderPhase3->step_1_completedtime) ?>
                                    </td>
                               <?php 
                              if($orderPhase3->step_1_notes == '')
                                          {
                                  ?>
                                          <td><?= $this->Html->link('Aggiungi nota', ['controller' => 'OrderPhase3', 'action' => 'edit', $orderDetailValues->id,$step2], array('id' => 'action1')) ?></td>
                                  <?php
                                          } 
                                        else
                                        {
                                  ?>
                                          <td>
                                            <?php 
                                          echo "".$this->Html->link(substr($orderPhase3->step_1_notes,0,15)." ... ", ['controller' => 'OrderPhase3', 'action' => 'edit', $orderDetailValues->id,$step2], array('id' => 'action1')) 
                                            ?></td>
                                  <?php
                                        }
                                  ?>
                                </tr>
                              <tr>
                                    <td><?= h($orderPhase3->description_step_2) ?></td>
                                    <td><?= h($orderPhase3->step_2_created) ?></td>
                                                                        <td><?php
                                            if($orderPhase3->step_2_completed)
                                            { ?>
                                            <i class="zmdi zmdi-thumb-up zmdi-hc-fw text-green"></i>
                                            <?php }
                                            else
                                            {?>
                                              <i class="zmdi zmdi-thumb-up zmdi-hc-fw text-red"></i>
                                            <?php } ?>
                                    </td>
                                 <?php
                                            $step2 = 2;
                                            if($orderPhase3->step_2_user != '' || is_null($orderPhase3->step_2_user))
                                            { 
                                    ?>
                                    <td>
                                      <?php echo "Utente gia assegnato";?>
                                    </td>
                                    <td>
                                   <?php 
                                              if($orderPhase3->step_2_user == 1)
                                              {
                                              echo "Luca";
                                              }
                                              elseif($orderPhase3->step_2_user == 10)
                                              {
                                              echo "Amministrazione";
                                              }
                                              elseif($orderPhase3->step_2_user == 100)
                                              {
                                              echo "Commerciale";
                                              }
                                              elseif($orderPhase3->step_2_user == 101)
                                              {
                                              echo "Olti";
                                              } 
                                   ?>
                                  </td>
                                   <?php
                                            }
                                            else
                                            {
                                    ?>
                                    <td>
                                    <?= $this->Html->link("Assegna utente", ['controller' => 'OrderPhase3', 'action' => 'edit', $orderDetailValues->id,$step2], array('id' => 'action1')) ?>
                                    </td>
                              <?php
                                            }
                              ?>
                                    <?php
                                            if($orderPhase3->step_2_completed == 0)
                                            { 
                                    ?>
                                    <td><i class="zmdi zmdi-play-circle zmdi-hc-fw text-green"></i>
                                      <?= $this->Html->link("Esegui azione", ['controller' => 'OrderPhase1', 'action' => 'execPhase3Step2', $orderDetailValues->id], array('id' => 'action2')) ?>
                                    </td>
                                    <?php
                                            }
                                            else
                                            {
                                    ?>
                                    <td><i class="zmdi zmdi-play-circle zmdi-hc-fw text-red"></i>
                                      <?= h("Azione già eseguita")?>
                                    </td>
                                    <td>
                                    <?= h($orderPhase3->step_2_completedtime) ?>
                                    </td>
                                    <?php
                                            }
                                    ?> 
                                  <?php if($orderPhase3->step_2_notes == '')
                                          {
                                  ?>
                                          <td><?= $this->Html->link('Aggiungi nota', ['controller' => 'OrderPhase3', 'action' => 'edit', $orderDetailValues->id,$step2], array('id' => 'action1')) ?></td>
                                  <?php
                                          } 
                                        else
                                        {
                                  ?>
                                          <td>
                                            <?php 
                                          echo "".$this->Html->link(substr($orderPhase3->step_2_notes,0,15)." ... ", ['controller' => 'OrderPhase3', 'action' => 'edit', $orderDetailValues->id,$step2], array('id' => 'action1')) 
                                            ?></td>
                                  <?php
                                        }
                                  ?>
                                </tr>

                                <tr>
                                    <td><?= h($orderPhase3->description_step_3) ?></td>
                                    <td><?= h($orderPhase3->step_3_created) ?></td>
                                                                        <td><?php
                                            if($orderPhase3->step_3_completed)
                                            { ?>
                                             <i class="zmdi zmdi-thumb-up zmdi-hc-fw text-green"></i>
                                            <?php }
                                            else
                                            {?>
                                              <i class="zmdi zmdi-thumb-up zmdi-hc-fw text-red"></i>
                                            <?php } ?>
                                    </td> 
                                   <?php
                                            $step2 = 3;
                                            if($orderPhase3->step_3_user != '' || is_null($orderPhase3->step_3_user))
                                            { 
                                    ?>
                                    <td>
                                      <?php echo "Utente gia assegnato";?>
                                    </td>
                                    <td>
                                   <?php 
                                              if($orderPhase3->step_3_user == 1)
                                              {
                                              echo "Luca";
                                              }
                                              elseif($orderPhase3->step_3_user == 10)
                                              {
                                              echo "Amministrazione";
                                              }
                                              elseif($orderPhase3->step_3_user == 100)
                                              {
                                              echo "Commerciale";
                                              }
                                              elseif($orderPhase3->step_3_user == 101)
                                              {
                                              echo "Olti";
                                              } 
                                   ?>
                                  </td>
                                   <?php
                                            }
                                            else
                                            {
                                    ?>
                                    <td>
                                    <?= $this->Html->link("Assegna utente", ['controller' => 'OrderPhase3', 'action' => 'edit', $orderDetailValues->id,$step2], array('id' => 'action1')) ?>
                                    </td>
                              <?php
                                            }
                              ?>
 <?php
                                            if($orderPhase3->step_3_completed == 0)
                                            { 
                                    ?>
                                    <td><i class="zmdi zmdi-play-circle zmdi-hc-fw text-green"></i>
                                      <?= $this->Html->link("Esegui azione", ['controller' => 'OrderPhase1', 'action' => 'execPhase3Step3', $orderDetailValues->id], array('id' => 'action3')) ?>
                                    </td>
                                    <?php
                                            }
                                            else
                                            {
                                    ?>
                                    <td><i class="zmdi zmdi-play-circle zmdi-hc-fw text-red"></i>
                                      <?= h("Azione già eseguita")?>
                                    </td>
                                    <td>
                                    <?= h($orderPhase3->step_3_completedtime) ?>
                                    </td>
                                  <?php
                                            }
                                    ?>
                                 <?php if($orderPhase3->step_3_notes == '')
                                          {
                                  ?>
                                          <td><?= $this->Html->link('Aggiungi nota', ['controller' => 'OrderPhase3', 'action' => 'edit', $orderDetailValues->id,$step2], array('id' => 'action1')) ?></td>
                                  <?php
                                          } 
                                        else
                                        {
                                  ?>
                                          <td>
                                            <?php 
                                          echo "".$this->Html->link(substr($orderPhase3->step_3_notes,0,15)." ... ", ['controller' => 'OrderPhase3', 'action' => 'edit', $orderDetailValues->id,$step2], array('id' => 'action1')) 
                                            ?></td>
                                  <?php
                                        }
                                  ?>
                                </tr>

                                <tr>
                                    <td><?= h($orderPhase3->description_step_4 )?></td>
                                    <td><?= h($orderPhase3->step_4_created) ?></td>
                                                                        <td><?php
                                            if($orderPhase3->step_4_completed)
                                            { ?>
                                             <i class="zmdi zmdi-thumb-up zmdi-hc-fw text-green"></i>
                                            <?php }
                                            else
                                            {?>
                                              <i class="zmdi zmdi-thumb-up zmdi-hc-fw text-red"></i>
                                            <?php } ?>
                                    </td> 
                                   <?php
                                            $step2 = 4;
                                            if($orderPhase3->step_4_user != '' || is_null($orderPhase3->step_4_user))
                                            { 
                                    ?>
                                    <td>
                                      <?php echo "Utente gia assegnato";?>
                                    </td>
                                    <td>
                                   <?php 
                                              if($orderPhase3->step_4_user == 1)
                                              {
                                              echo "Luca";
                                              }
                                              elseif($orderPhase3->step_4_user == 10)
                                              {
                                              echo "Amministrazione";
                                              }
                                              elseif($orderPhase3->step_4_user == 100)
                                              {
                                              echo "Commerciale";
                                              }
                                              elseif($orderPhase3->step_4_user == 101)
                                              {
                                              echo "Olti";
                                              } 
                                   ?>
                                  </td>
                                   <?php
                                            }
                                            else
                                            {
                                    ?>
                                    <td>
                                    <?= $this->Html->link("Assegna utente", ['controller' => 'OrderPhase3', 'action' => 'edit', $orderDetailValues->id,$step2], array('id' => 'action1')) ?>
                                    </td>
                              <?php
                                            }
                              ?>
                                    <?php
                                            if($orderPhase3->step_4_completed == 0)
                                            { 
                                    ?>
                                    <td><i class="zmdi zmdi-play-circle zmdi-hc-fw text-green"></i>
                                      <?= $this->Html->link("Esegui azione", ['controller' => 'OrderPhase3', 'action' => 'execPhase3Step4', $orderDetailValues->id], array('id' => 'action4')) ?>
                                    </td>
                                    <?php
                                            }
                                            else
                                            {
                                    ?>
                                    <td><i class="zmdi zmdi-play-circle zmdi-hc-fw text-red"></i>
                                      <?= h("Azione già eseguita")?>
                                    </td>
                                    <td>
                                    <?= h($orderPhase3->step_3_completedtime) ?>
                                    </td>

                                    <?php
                                            }
                                    ?>
                                   <?php if($orderPhase3->step_4_notes == '')
                                          {
                                  ?>
                                          <td><?= $this->Html->link('Aggiungi nota', ['controller' => 'OrderPhase3', 'action' => 'edit', $orderDetailValues->id,$step2], array('id' => 'action1')) ?></td>
                                  <?php
                                          } 
                                        else
                                        {
                                  ?>
                                          <td>
                                            <?php 
                                          echo "".$this->Html->link(substr($orderPhase3->step_4_notes,0,15)." ... ", ['controller' => 'OrderPhase3', 'action' => 'edit', $orderDetailValues->id,$step2], array('id' => 'action1')) 
                                            ?></td>
                                  <?php
                                        }
                                  ?>
                                </tr>
                              
                                <tr>
                                    <td><?= h($orderPhase3->description_step_5) ?></td>
                                    <td><?= h($orderPhase3->step_5_created) ?></td>
                                                                        <td><?php
                                            if($orderPhase3->step_5_completed)
                                            { ?>
                                              <i class="zmdi zmdi-thumb-up zmdi-hc-fw text-green"></i>
                                            <?php }
                                            else
                                            {?>
                                              <i class="zmdi zmdi-thumb-up zmdi-hc-fw text-red"></i>
                                            <?php } ?>
                                    </td> 
                                    <?php
                                            $step2 = 5;
                                            if($orderPhase3->step_5_user != '' || is_null($orderPhase3->step_5_user))
                                            { 
                                    ?>
                                    <td>
                                      <?php echo "Utente gia assegnato";?>
                                    </td>
                                    <td>
                                   <?php 
                                              if($orderPhase3->step_5_user == 1)
                                              {
                                              echo "Luca";
                                              }
                                              elseif($orderPhase3->step_5_user == 10)
                                              {
                                              echo "Amministrazione";
                                              }
                                              elseif($orderPhase3->step_5_user == 100)
                                              {
                                              echo "Commerciale";
                                              }
                                              elseif($orderPhase3->step_5_user == 101)
                                              {
                                              echo "Olti";
                                              } 
                                   ?>
                                  </td>
                                   <?php
                                            }
                                            else
                                            {
                                    ?>
                                    <td>
                                    <?= $this->Html->link("Assegna utente", ['controller' => 'OrderPhase3', 'action' => 'edit', $orderDetailValues->id,$step2], array('id' => 'action1')) ?>
                                    </td>
                              <?php
                                            }
                              ?>
                                    <?php
                                            if($orderPhase3->step_5_completed == 0)
                                            { 
                                    ?>
                                    <td><i class="zmdi zmdi-play-circle zmdi-hc-fw text-green"></i>
                                      <?= $this->Html->link("Esegui azione", ['controller' => 'OrderPhase3', 'action' => 'execPhase3Step5', $orderDetailValues->id], array('id' => 'action5')) ?>
                                    </td>
                                    <?php
                                            }
                                            else
                                            {
                                    ?>
                                    <td><i class="zmdi zmdi-play-circle zmdi-hc-fw text-red"></i>
                                      <?= h("Azione già eseguita")?>
                                    </td>
                                    <td>
                                    <?= h($orderPhase3->step_5_completedtime) ?>
                                    </td>
                                    <?php
                                            }
                                    ?>
                                    <?php if($orderPhase3->step_5_notes == '')
                                          {
                                  ?>
                                          <td><?= $this->Html->link('Aggiungi nota', ['controller' => 'OrderPhase3', 'action' => 'edit', $orderDetailValues->id,$step2], array('id' => 'action1')) ?></td>
                                  <?php
                                          } 
                                        else
                                        {
                                  ?>
                                          <td>
                                            <?php 
                                          echo "".$this->Html->link(substr($orderPhase3->step_5_notes,0,15)." ... ", ['controller' => 'OrderPhase3', 'action' => 'edit', $orderDetailValues->id,$step2], array('id' => 'action1')) 
                                            ?></td>
                                  <?php
                                        }
                                  ?>
                                </tr>
                               <tr>
                                    <td><?= h($orderPhase3->description_step_6) ?></td>
                                    <td><?= h($orderPhase3->step_6_created) ?></td>
                                                                        <td><?php
                                            if($orderPhase3->step_6_completed)
                                            { ?>
                                              <i class="zmdi zmdi-thumb-up zmdi-hc-fw text-green"></i>
                                            <?php }
                                            else
                                            {?>
                                              <i class="zmdi zmdi-thumb-up zmdi-hc-fw text-red"></i>
                                            <?php } ?>
                                    </td> 
                                  <?php
                                            $step2 = 6;
                                            if($orderPhase3->step_6_user != '' || is_null($orderPhase3->step_6_user))
                                            { 
                                    ?>
                                    <td>
                                      <?php echo "Utente gia assegnato";?>
                                    </td>
                                    <td>
                                   <?php 
                                              if($orderPhase3->step_6_user == 1)
                                              {
                                              echo "Luca";
                                              }
                                              elseif($orderPhase3->step_6_user == 10)
                                              {
                                              echo "Amministrazione";
                                              }
                                              elseif($orderPhase3->step_6_user == 100)
                                              {
                                              echo "Commerciale";
                                              }
                                              elseif($orderPhase3->step_6_user == 101)
                                              {
                                              echo "Olti";
                                              } 
                                   ?>
                                  </td>
                                   <?php
                                            }
                                            else
                                            {
                                    ?>
                                    <td>
                                    <?= $this->Html->link("Assegna utente", ['controller' => 'OrderPhase3', 'action' => 'edit', $orderDetailValues->id,$step2], array('id' => 'action1')) ?>
                                    </td>
                              <?php
                                            }
                              ?>
                                    <?php
                                            if($orderPhase3->step_6_completed == 0)
                                            { 
                                    ?>
                                    <td><i class="zmdi zmdi-play-circle zmdi-hc-fw text-green"></i>
                                      <?= $this->Html->link("Esegui azione", ['controller' => 'OrderPhase3', 'action' => 'execPhase3Step6', $orderDetailValues->id], array('id' => 'action5')) ?>
                                    </td>
                                    <?php
                                            }
                                            else
                                            {
                                    ?>
                                    <td><i class="zmdi zmdi-play-circle zmdi-hc-fw text-red"></i>
                                      <?= h("Azione già eseguita")?>
                                    </td>
                                    <td>
                                    <?= h($orderPhase3->step_6_completedtime) ?>
                                    </td>                                    <?php
                                            }
                                    ?>
                                 <?php if($orderPhase3->step_6_notes == '')
                                          {
                                  ?>
                                          <td><?= $this->Html->link('Aggiungi nota', ['controller' => 'OrderPhase3', 'action' => 'edit', $orderDetailValues->id,$step2], array('id' => 'action1')) ?></td>
                                  <?php
                                          } 
                                        else
                                        {
                                  ?>
                                          <td>
                                            <?php 
                                          echo "".$this->Html->link(substr($orderPhase3->step_6_notes,0,15)." ... ", ['controller' => 'OrderPhase3', 'action' => 'edit', $orderDetailValues->id,$step2], array('id' => 'action1')) 
                                            ?></td>
                                  <?php
                                        }
                                  ?>
                                </tr>
                                <!--  -->
                            </tbody>
                        </table>
                    </div>

                    <button class="btn btn-danger btn--action " data-ma-action="print"><i class="zmdi zmdi-print"></i></button>
                </div> 