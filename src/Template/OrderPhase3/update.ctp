<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $orderPhase2
 */
?>
<div class="card bg-white card-shadow new-contact">
    <div class="card-body">
       <div class="row">
              <div class="col-md-8">
    <?= $this->Form->create($orderPhase3) ?>
    <fieldset>
        
        <?php
            echo "<h5>Ordine n°: ".$orderDetailValues->order_number." - riferimento:".$quoDetailValues->reference."</h5><br/>";
            ?>
      <legend><?= __('RESET STEP') ?></legend>
      <?php
      
            if($step == 1)
            {

            echo "Reset step: ".$orderPhase3->description_step_1."<br/>";
            echo $this->Form->hidden('step_1_completed' , ['value' => '0']);
            echo $this->Form->hidden('step_1_completedtime' , ['value' => NULL]);
            echo $this->Form->hidden('step_1_user' , ['value' => NULL]);
            echo $this->Form->hidden('step' , ['value' => $step]);  
            echo $this->Form->control('confirm', ['type' => 'text', 'label' => 'digitare la parola CONFERMA (attento alle maiuscole)']);
            }
            if($step == 2)
            {
            echo "Reset step: ".$orderPhase3->description_step_2."<br/>";
            echo $this->Form->hidden('step_2_completed' , ['value' => '0']);
            echo $this->Form->hidden('step_2_completedtime' , ['value' => NULL]);
            echo $this->Form->hidden('step_2_user' , ['value' => NULL]);
            echo $this->Form->hidden('step' , ['value' => $step]);  
            echo $this->Form->control('confirm', ['type' => 'text', 'label' => 'digitare la parola CONFERMA (attento alle maiuscole)']);
            }
            if($step == 3)
            {
            echo "Reset step: ".$orderPhase3->description_step_3."<br/>";
            echo $this->Form->hidden('step_3_completed' , ['value' => '0']);
            echo $this->Form->hidden('step_3_completedtime' , ['value' => NULL]);
            echo $this->Form->hidden('step_3_user' , ['value' => NULL]);
            echo $this->Form->hidden('step' , ['value' => $step]);  
            echo $this->Form->control('confirm', ['type' => 'text', 'label' => 'digitare la parola CONFERMA (attento alle maiuscole)']);
            }
            if($step == 4)
            {
            echo "Reset step: ".$orderPhase3->description_step_4."<br/>";
            echo $this->Form->hidden('step_4_completed' , ['value' => '0']);
            echo $this->Form->hidden('step_4_completedtime' , ['value' => NULL]);
            echo $this->Form->hidden('step_4_user' , ['value' => NULL]);
            echo $this->Form->hidden('step' , ['value' => $step]);  
            echo $this->Form->control('confirm', ['type' => 'text', 'label' => 'digitare la parola CONFERMA (attento alle maiuscole)']);
            }
            if($step == 5)
            {
            echo "Reset step: ".$orderPhase3->description_step_5."<br/>";
            echo $this->Form->hidden('step_5_completed' , ['value' => '0']);
            echo $this->Form->hidden('step_5_completedtime' , ['value' => NULL]);
            echo $this->Form->hidden('step_5_user' , ['value' => NULL]);
            echo $this->Form->hidden('step' , ['value' => $step]);  
            echo $this->Form->control('confirm', ['type' => 'text', 'label' => 'digitare la parola CONFERMA (attento alle maiuscole)']);
            }
            if($step == 6)
            {
            echo "Reset step: ".$orderPhase3->description_step_6."<br/>";
            echo $this->Form->hidden('step_6_completed' , ['value' => '0']);
            echo $this->Form->hidden('step_6_completedtime' , ['value' => NULL]);
            echo $this->Form->hidden('step_6_user' , ['value' => NULL]);
            echo $this->Form->hidden('step' , ['value' => $step]);  
            echo $this->Form->control('confirm', ['type' => 'text', 'label' => 'digitare la parola CONFERMA (attento alle maiuscole)']);
            }	
        ?>
    </fieldset>
      <?= $this->Form->button(__('Assegna'), ['class' => 'btn btn-primary']) ?>
    <?= $this->Form->end() ?>
         </div>
      </div>
         </div>
      </div>
