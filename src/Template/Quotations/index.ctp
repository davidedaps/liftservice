<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Supplier[]|\Cake\Collection\CollectionInterface $suppliers
 */
?>
<div class="card bg-white card-shadow">
    <div class="card-body">
        <h3><?= __('Elenco preventivi clienti') ?></h3>
            <table class="table table-responsive" id="data-table" >
            <thead class="thead-light">
            <tr>
                <th><?= h('Dettaglio') ?></th>
                <th scope="col"><?= h('Tipo impianto') ?></th>
                <th scope="col"><?= h('Indirizzo')?></th>
                <th scope="col"><?= h('Città') ?></th>
                <th scope="col"><?= h('Cliente') ?></th>
                <th scope="col"><?= h('Committente') ?></th>   
                <th scope="col"><?= h('Professionista 1') ?></th>
                <th scope="col"><?= h('Professionista 2') ?></th>
               <th scope="col"><?= h('Stato preventivo') ?></th>
                <th scope="col"><?= h('Data preventivo') ?></th>
                <th scope="col"><?= h('Importo preventivo') ?></th>       
            </tr>
        </thead>
        <tbody>
            <?php foreach ($quotations as $quotation): ?>
            <tr>
                <td><?= $this->Html->link(__("Visualizza"), ['action' => 'view', $quotation->id]) ?></td>
                <td><?= $this->Html->link(__($quotation->reference), ['action' => 'view', $quotation->id]) ?></td>
                <td><?= h($quotation->address) ?></td>
                <td><?= h($quotation->city) ?></td>
                <td><?= $quotation->has('customer') ? $this->Html->link($quotation->customer->name, ['controller' => 'Customers', 'action' => 'view', $quotation->customer->id]) : '' ?></td>
                <td><?= h($quotation->referent) ?></td>
                <td><?= $quotation->has('professional_1') ? $this->Html->link($professionalsList[$quotation->professional_1], ['controller' => 'Professionals', 'action' => 'view', $quotation->professional->id]) : '' ?></td>
                <td><?= $quotation->has('professional_2') ? $this->Html->link($professionalsList[$quotation->professional_2], ['controller' => 'Professionals', 'action' => 'view', $quotation->professional->id]) : '' ?></td>
                <td><?= h($quotation->status) ?></td>  
              <?php  
//                  $numero_settings = count($quotationStatusList) - 1;
//                  for ($i = 0; $i <= $numero_settings; $i++) 
//                  {
//                    if($quotation->status == $quotationStatusList[$i]->id )
//                    {
//                      echo "<td>".($quotationStatusList[$i]->description)."</td>";
//                    }
//                    else
//                    {
//                      //echo "<td><p>test</p></td>";
//                    }
//                  }
                ?>
                <td><?= h($quotation->quotation_date) ?></td>
                <td><?= h($this->Number->currency($quotation->value, "EUR")) ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
</div>