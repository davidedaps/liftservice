<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Quotation $quotation
 */
?>
<script>
$(document).on('show.bs.modal', '.modal', function () {
  $(this).appendTo('body');
});         
</script>
<div class="card bg-white card-shadow">
    <div class="card-body">
          <div class="responsive" style="float:left;">
            <h3><?= __('Preventivo: '.$quotation->reference) ?></h3>
          </div>
         <div class="responsive" style="float:left;padding-left:30px;">
          <?= // link al nuovo preventivo per questo cliente
                $this->Html->link(__("<i class=\"zmdi zmdi-calendar-check zmdi-hc-fw\"></i> Aggiungi azione / memo"), 
                            ['action' => 'add', 'controller' => 'Actions',$quotation->customer->id,$quotation->id],
                            ['title'=>'new action/memo','escape'=>false,'class'=>'btn btn-info btn--raised']) ?>
                      <?= // link per inserimento nuovo memo per questo preventivo
                $this->Html->link(__("<i class=\"zmdi zmdi-trending-up zmdi-hc-fw\"></i> Trasforma in ordine"), 
                            ['action' => 'add', 'controller' => 'OrdersCustomers',$quotation->id,],
                            ['title'=>'new quotation','escape'=>false,'class'=>'btn btn-success btn--raised']) ?>
          </div>
        <div style="clear:both"></div>
          <br/>
          <fieldset>
            <div class="row">
              <div class="col-md-6">
                  <p class="font-weight-bold">Cliente</p>
                  <?= $quotation->has('customer') ? $this->Html->link($quotation->customer->name, ['controller' => 'Customers', 'action' => 'view', $quotation->customer->id]) : '' ?>      
              </div>
               <div class="col-md-6">
                  <p class="font-weight-bold">Tipo impianto</p>
                  <?= h($quotation->reference); ?>
              </div>
            </div>
            <br/>
            <div class="row">
              <div class="col-md-3">
                  <p class="font-weight-bold">Importo preventivo</p>
                   <?= h($this->Number->currency($quotation->value, "EUR")) ?>
              </div>
              <div class="col-md-3">
                  <p class="font-weight-bold">Prevista commissione</p>
                   <?= h($quotation->commission) ?>
              </div>
              <div class="col-md-3">
                  <p class="font-weight-bold">Commitente</p>
                  <?= h($quotation->referent) ?>
              </div>
            </div>
            <br/>
            <div class="row">
              <div class="col-md-5">
                  <p class="font-weight-bold">Indirizzo</p>
                  <?= h($quotation->address) ?>
              </div>
              <div class="col-md-2">
                  <p class="font-weight-bold">CAP</p>
                  <?= h($quotation->cap) ?>
              </div>
               <div class="col-md-5">
                  <p class="font-weight-bold">Città</p>
                  <?= h($quotation->city) ?>
              </div>
            </div>
            <br/>
            <div class="row">
              <div class="col-md-6">
                  <p class="font-weight-bold">Professionista 1</p>
            <?= $quotation->has('professional_1') ? $this->Html->link($professionals->full_name, ['controller' => 'Professionals', 'action' => 'view', $quotation->professional_1]) : '' ?> 
              </div>
                <div class="col-md-6">
                  <p class="font-weight-bold">Professionista 2</p>
                <?= $quotation->has('professional_2') ? $this->Html->link($professionals2->full_name, ['controller' => 'Professionals', 'action' => 'view', $quotation->professional_2]) : '' ?>
              </div>
            </div>            
            <br/>
            <div class="row">
              <div class="col-md-4">
                  <p class="font-weight-bold">Stato preventivo</p>         
                  <?= h($quotation->status) ?>
              </div>
              <div class="col-md-4">
                  <p class="font-weight-bold">Data preventivo</p>
                  <?= h($quotation->quotation_date) ?>
              </div>
             <div class="col-md-4">
                  <p class="font-weight-bold">Note aggiuntive</p>
                  <?= h($quotation->balance_note) ?>
              </div>
            </div>
        </fieldset>
            <br/>    
    <?= $this->Form->end() ?>
  </div>
</div>
  
<!-- Button trigger modal -->
<div class="card bg-white card-shadow">
    <?= $this->element('modal_view', [
              // passo la variabile elementVariable all'elemento. 
              "elementId" => $quotation->id,
              "elementName" => $quotation->reference,
              "modalTitle" => "Elimina preventivo cliente",
              "modalText" => "Sei sicuro di eliminare questo preventivo cliente?"
          ]) ?>
</div>

<div class="card bg-white card-shadow">
  <div class="row">
    <div class="card-body col-md-6">
           <h3><?= __('Elenco aggiornamenti') ?></h3>
          <table class="table table-responsive" >
              <thead class="thead-light">
          <tr>
            <th scope="row"><?= __('Descrizione') ?></th>
            <th scope="row"><?= __('Ultimo aggiornamento') ?></th>
            <th scope="row"><?= __('Azione') ?></th>
          </tr>
            </thead>
              <tbody>
              <?php
                foreach ($actionsList as $action): ?>
                      <tr>         
                        <td><?= h($action->description) ?></td>
                        <td><?= h($action->modified->format('Y-m-d'))." / ";if(($action->creator_id) != 0) echo $users[$action->creator_id]; ?></td>
                        <td class="actions">
                            <?= $this->Html->link(__("<i class=\"zmdi zmdi-zoom-in zmdi-hc-fw\"></i>"), 
                                  ['action' => 'view', 'controller' => 'actions', $action->id],
                                  ['title'=>'view','escape'=>false,'class'=>'btn btn-info btn--raised btn-sm']) ?> 
                            <?= $this->Html->link(__("<i class=\"zmdi zmdi-edit zmdi-hc-fw\"></i>"), 
                                  ['action' => 'edit', 'controller' => 'actions', $action->id],
                                  ['title'=>'edit','escape'=>false,'class'=>'btn btn-warning btn--raised btn-sm']) ?>
                        </td>
                      </tr> 
              <?php endforeach; ?>
             </tbody>
          </table>
    </div>
    <div class="card-body col-md-6">
        <h3><?= __('Elenco azioni') ?></h3>
        <table class="table table-responsive" >
            <thead class="thead-light">
          <tr>
            <th scope="row"><?= __('Tipo') ?></th>
            <th scope="row"><?= __('Scadenza') ?></th>
            <th scope="row"><?= __('Utente') ?></th>
            <th scope="row"><?= __('Azioni') ?></th>
          </tr>
          </thead>
            <tbody>
            <?php //pr($actions_all);
              foreach ($actions_all as $action): ?>
                    <tr>         
                      <?php if($action->is_action == 1){;?>
                      <td><?= $this->Html->link(__("<i class=\"zmdi zmdi-zoom-in zmdi-hc-fw\"></i>".$action->action_type), ['action' => 'view', 'controller' => 'actions', $action->id],
                        ['title'=>'view','escape'=>false]) ?></td>
                      <?php } else { ?>
                       <td><?= $this->Html->link(__("<i class=\"zmdi zmdi-zoom-in zmdi-hc-fw\"></i>".$action->description), ['action' => 'view', 'controller' => 'actions', $action->id],
                        ['title'=>'view','escape'=>false]) ?></td>
                      <?php } ?>
                      <td><?= h($action->exec_date->format('d-m-Y')) ?></td>
                      <?php
                      if($action->user_id == '10')
                      {
                      ?><td>Amministrazione</td>
                      <?php
                      }
                      if($action->user_id == '100')
                      {
                      ?>
                      <td><p>Commericiale</p></td>
                      <?php
                      }
                      if($action->user_id == '101')
                      {
                      ?>
                      <td><p>Olti</p></td>
                      <?php
                      }
                     if($action->user_id == '1')
                      {
                      ?>
                      <td><p>Luca</p></td>
                      <?php
                      }
                      ?>
                        <td class="actions">

                            <?= $this->element('modal_change_action', [
                               // passo la variabile elementVariable all'elemento. 
                               "element_id" => $action->id,
                               "quotation_id" => $quotation->id
                            ]) ?>
                        </td>
                    </tr> 

            <?php endforeach; ?>

            </tbody>
        </table>
    </div>
  </div>
  <div class="row">
    <div class="card-body col-md-6">
        <h3><?= __('Elenco uploads') ?></h3>
        <table class="table table-responsive" >
            <thead class="thead-light">
          <tr>
            <th scope="row"><?= __('Nome') ?></th>
            <th scope="row"><?= __('Tipo allegato') ?></th>
            <th scope="row"><?= __('Scarica') ?></th>
          </tr>
          </thead>
            <tbody>
            <?php
              foreach ($uploads as $upload): ?>
                <?php $imagepath = '/files/uploads/filename/' . $upload->dir; ?>
                    <tr>         
                      <td><?= h($upload->filename) ?></td>
                      <td><?= h($upload->docfolder) ?></td>
                      <td><a href="<?= $imagepath . '/' . $upload->filename ?>" target="_blank"><i class="zmdi zmdi-download zmdi-hc-fw"></i></a></td>
                    </tr> 

            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
  </div>
</div>