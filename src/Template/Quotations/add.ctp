<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Quotation $quotation
 */
?>

<div class="card bg-white card-shadow new-contact">
    <div class="card-body">
         <h3><?= __('Aggiungi preventivo contatto') ?></h3>
          <br/>
          <?= $this->Form->create($quotation) ?>
          <fieldset>
            <div class="row">
<!--               <div class="col-md-3">
                  <div class="form-group">
                          <?php //echo $this->Form->control('quotation_number' , ['label' => 'Numero preventivo']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div> -->
              <div class="col-md-3">
                  <div class="form-group">
                      <?php //controllo se c'è un customer_id passato dalla view di customer
                          if (isset($customer_id)) { 
                              echo $this->Form->control('customer_name' , ['label' => 'Contatto', 
                                                                         'value' => $customer_detail,
                                                                        'type' => 'text',
                                                                        'readonly' => 'readonly']);
                              echo $this->Form->control('customer_id',['value' => $customer_id,'type' => 'hidden']);
                         // se non c'è customer_id chiedo a quale cliente associare il preventivo
                         } else {
                              echo $this->Form->control('customer_id' , 
                                  ['options' => $customers, 'label' => 'Seleziona il contatto','empty' => 'Scegli il nome...']);
 
                         } ?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
               <div class="col-md-6">
                  <div class="form-group">
                          <?php echo $this->Form->control('reference' , ['label' => 'Tipo impianto']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
            </div>
            <div class="row">
<!--                <div class="col-md-3">
                  <div class="form-group">
                          <?php //echo $this->Form->control('quotation_number' , ['label' => 'Indica il numero del preventivo']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div> -->
              <div class="col-md-3">
                  <div class="form-group">
                          <?php echo $this->Form->control('value' , ['label' => 'Importo preventivo']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
              <div class="col-md-3">
                  <div class="form-group">
                          <?php echo $this->Form->control('commission' , ['label' => 'Prevista commissione']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="form-group">
                          <?php echo $this->Form->control('address' , ['label' => 'Indirizzo installazione']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="form-group">
                          <?php echo $this->Form->control('cap' , ['label' => 'CAP']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
               <div class="col-md-4">
                  <div class="form-group">
                          <?php echo $this->Form->control('city' , ['label' => 'Città installazione']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="form-group">
                          <?php echo $this->Form->control('status' , ['label' => 'Stato preventivo', 'options' => $quotationStatusList,'empty' => 'indica lo stato preventivo']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="form-group">
                          <?php echo $this->Form->control('quotation_date' , ['label' => 'Data preventivo']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
             <div class="col-md-4">
                  <div class="form-group">
                          <?php echo $this->Form->control('balance_note' , ['label' => 'Note aggiuntive']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
               <div class="col-md-4">
                  <div class="form-group">
                          <?php echo $this->Form->control('referent' , ['label' => 'Committente',"placeholder"=>"Indicare la persona di riferimento del futuro ordine",]);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
                             <div class="col-md-4">
                  <div class="form-group">
                          <?php echo $this->Form->control('professional_1' , ['options' => $professionals,'label' => 'Professionista 1','empty' => 'indica un professionista']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
                             <div class="col-md-4">
                  <div class="form-group">
                          <?php echo $this->Form->control('professional_2' , ['options' => $professionals,'label' => 'Professionista 2','empty' => 'indica un professionista']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
<!--               <div class="col-md-4">
                  <div class="form-group">
                          <?php echo $this->Form->control('supplier' , ['label' => 'Supplier']);?>
                          <?php echo $this->Form->control('archived',['value' => 0,'type' => 'hidden']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div> -->
            </div>
        </fieldset>
       <!--<?= $this->Form->create($uploadOrder,['type' => 'file']) ?>-->
                                            <fieldset>
                                              <legend>Uploads</legend>
                                              <?php 
                                              echo $this->Form->control('filename', ['type' => 'file','required'=>false,'multiple' => false,'class' => 'form-control','label' => '']);
                                              //echo $this->Form->hidden('order_customer_id', ['value'=>$orderDetailValues->id]);
                                              //echo $this->Form->hidden('order_number', ['value'=>$orderDetailValues->order_number]);
                                              echo $this->Form->hidden('quotation_id', ['value'=>$quotation->id]);
                                              echo $this->Form->control('docfolder' , ['value'=>'Preventivo']);
                                              ?>
                                            </fieldset>
                                            <!--<?= $this->Form->button(__('Salva'), ['class' => 'btn btn-primary']) ?>-->
                                            <!--<?= $this->Form->end() ?>-->
     <?= $this->Form->button(__('Salva'), ['class' => 'btn btn-primary']) ?>
    <?= $this->Form->end() ?>
  </div>
</div>