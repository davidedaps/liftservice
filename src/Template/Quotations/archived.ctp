<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Supplier[]|\Cake\Collection\CollectionInterface $suppliers
 */
?>
<div class="card bg-white card-shadow">
    <div class="card-body">
        <h3><?= __('Elenco preventivi clienti ARCHIVIATI') ?></h3>
            <table class="table table-responsive" id="data-table" >
            <thead class="thead-light">
            <tr>
                <th scope="col"><?= h('Tipo impianto') ?></th>
                <th scope="col"><?= h('Indirizzo')?></th>
                <th scope="col"><?= h('Città') ?></th>
                <th scope="col"><?= h('Cliente') ?></th>
                <th scope="col"><?= h('Stato preventivo') ?></th>
                <th scope="col"><?= h('Data preventivo') ?></th>
                <th scope="col"><?= h('Importo preventivo') ?></th>
<!--                 <th scope="col"><?= h('Ordine cliente generato') ?></th>        -->
            </tr>
        </thead>
        <tbody>
            <?php foreach ($quotations as $quotation): ?>
            <tr>
                <td><?= $this->Html->link(__($quotation->reference), ['action' => 'view', $quotation->id]) ?></td>
                <td><?= h($quotation->address) ?></td>
                <td><?= h($quotation->city) ?></td>
                <td><?= $quotation->has('customer') ? $this->Html->link($quotation->customer->name, ['controller' => 'Customers', 'action' => 'view', $quotation->customer->id]) : '' ?></td>
                <td><? switch ($quotation->status) 
                              {
                                case '9':
                                echo "in preparazione";
                                break;
                                case '8':
                                echo "chiuso perso";
                                break;
                                case '7':
                                echo "chiuso vinto";
                                break;
                                case '6':
                                echo "quasi perso";
                                break;
                                case '5':
                                echo "non prevedibile";
                                break;
                                case '4':
                                echo "in corso";
                                break;
                                case '3':
                                echo "probabile vinto";
                                break;
                                case '2':
                                echo "quasi vinto medio periodo";
                                break;
                                case '1':
                                echo "quasi vinto breve periodo";
                                break;
                                default:
                                echo "----";
                                break;
                              }
                     ?>
                </td>
                <td><?= h($quotation->quotation_date) ?></td>
                <td><?= h($this->Number->currency($quotation->value, "EUR")) ?></td>
<!--                 <td><?= h($quotation->order_number) ?></td> -->
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
</div>