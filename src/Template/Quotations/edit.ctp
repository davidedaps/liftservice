<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Quotation $quotation
 */
?>

<div class="card bg-white card-shadow new-contact">
    <div class="card-body">
         <h3><?= __('Modifica preventivo cliente') ?></h3>
          <br/>
          <?= $this->Form->create($quotation,['type' => 'file']) ?>
          <fieldset>
            <div class="row">
              <div class="col-md-6">
                  <div class="form-group">
                          <?php echo $this->Form->control('customer_id' , ['options' => $customers, 'label' => 'Seleziona il cliente','empty' => 'Scegli il nome...']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
               <div class="col-md-6">
                  <div class="form-group">
                          <?php echo $this->Form->control('reference' , ['label' => 'Riferimento impianto']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
              
             
<!--                <div class="col-md-3">
                  <div class="form-group">
                          <?php echo $this->Form->control('quotation_number' , ['label' => 'Indica il numero del preventivo']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div> -->
              <div class="col-md-3">
                  <div class="form-group">
                          <?php echo $this->Form->control('value' , ['label' => 'Importo preventivo']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="form-group">
                          <?php echo $this->Form->control('commission' , ['label' => 'Prevista commissione']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
              <div class="col-md-3">
                  <div class="form-group">
                          <?php echo $this->Form->control('cap' , ['label' => 'CAP']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
              <div class="col-md-3">
                  <div class="form-group">
                          <?php echo $this->Form->control('address' , ['label' => 'Indirizzo installazione']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
               <div class="col-md-4">
                  <div class="form-group">
                          <?php echo $this->Form->control('city' , ['label' => 'Città installazione']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="form-group">
                          <?php echo $this->Form->control('status' , ['options' => $quotationStatusList,'empty' => 'indica lo stato preventivo']);?>
                          
                      <i class="form-group__bar"></i>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="form-group">
                          <?php echo $this->Form->control('quotation_date' , ['label' => 'Data preventivo']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
             <div class="col-md-4">
                  <div class="form-group">
                          <?php echo $this->Form->control('balance_note' , ['label' => 'Note aggiuntive']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
                             <div class="col-md-4">
                  <div class="form-group">
                          <?php echo $this->Form->control('referent' , ['label' => 'Committente']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
   <div class="col-md-4">
                  <div class="form-group">
                         <?php echo $this->Form->control('professional_1' , ['options' => $professionals,'empty' => 'indica un professionista']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
                 <div class="col-md-4">
                   <div class="form-group">
                          <?php echo $this->Form->control('professional_2' , ['options' => $professionals,'empty' => 'indica un professionista']);?>
                      <i class="form-group__bar"></i>
                  </div>               
            </div>
            </div>
        </fieldset>
         <!--<?= $this->Form->create($uploadOrder,['type' => 'file']) ?>-->
                                            <fieldset>
                                              <legend>Uploads</legend>
                                              <?php 
                                              echo $this->Form->control('filename', ['type' => 'file','required'=>false,'multiple' => false,'class' => 'form-control','label' => '']);
                                              //echo $this->Form->hidden('order_customer_id', ['value'=>$orderDetailValues->id]);
                                              //echo $this->Form->hidden('order_number', ['value'=>$orderDetailValues->order_number]);
                                              echo $this->Form->hidden('quotation_id', ['value'=>$quotation->id]);
                                              echo $this->Form->control('docfolder' , ['value'=>'Preventivo']);
                                              ?>
                                            </fieldset>
                                            <!--<?= $this->Form->button(__('Salva'), ['class' => 'btn btn-primary']) ?>-->
                                            <!--<?= $this->Form->end() ?>-->
     <?= $this->Form->button(__('Salva'), ['class' => 'btn btn-primary']) ?>
    <?= $this->Form->end() ?>
  </div>
</div>
<!-- Button trigger modal -->
    <!-- Button trigger modal -->
<div class="card bg-white card-shadow">
    <?= $this->element('modal_archive_quotation', [
              // passo la variabile elementVariable all'elemento. 
              "elementId" => $quotation->id,
              "elementName" => $quotation->reference,
              "modalTitle" => "Salva e archivia preventivo",
              "modalText" => "Sei sicuro di archiviare questo preventivo?"
          ]) ?>
</div>

