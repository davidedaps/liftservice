<script type="text/javascript">
$(document).ready(function()
{
  <?php 
  
  function clean($string) 
  {
    //$string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
    return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
  }

  foreach ($actions_type as $action): 
  //$descr_azione = str_replace(' ', '', trim($action->description));
  $descr_azione = clean($action->description);
  ?>
   $("#filter_<?php echo $descr_azione;?>").click(function()
    {
    
    var buttonState1 = $("#filter_<?php echo $descr_azione;?>").attr("aria-pressed");
    if(buttonState1 == 'true')
       {
           $(".listview__item_filter_<?php echo $descr_azione;?>").hide(200);
       }
    else
       {
           $(".listview__item_filter_<?php echo $descr_azione;?>").show(200);
       }
    }
                                                          
    );
  <?php endforeach; ?>
});
</script>
<div class="card bg-white card-shadow">
    <div class="card-body">
<button class="btn btn-primary collapsed" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample" style="text-color:white;">
Filtri visualizzazione azioni
</button>
        <div class="mt-2 collapse" id="collapseExample" style="">
<?php
          
          /*function clean($string) 
            {
              $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
              return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
            }*/
          ?>
          <?php foreach ($actions_type as $action):
          //$descr_azione= str_replace(' ', '', trim($action->description));
          $descr_azione = clean($action->description);?>
            <?php //echo "<button type=\"button\" data-toggle=\"button\" class=\"btn btn-info mb-1 active\"id=\"filter_".$descr_azione."\" aria-pressed=\"true\">$action->description</button>&nbsp;"; ?>
            <?php echo "<button type=\"button\" data-toggle=\"button\" class=\"btn btn-info mb-1 active\"id=\"filter_".$descr_azione."\" aria-pressed=\"true\">$action->description</button>&nbsp;"; ?>
                  <?php //echo $action->description?>
          <?php endforeach; ?>

        </div>
  </div><!--sia qui?-->
</div>
<div class="card bg-white card-shadow">
  <div class="card-body">
<button class="btn btn-primary collapsed" type="button" data-toggle="collapse" data-target="#collapseExample2" aria-expanded="false" aria-controls="collapseExample2" style="text-color:white;">
Ordini da attenzionare per utente collegato
</button>
  <div class="mt-2 collapse" id="collapseExample2" style="">
    <!-- INIZIO BOX ORDINI PER UTENTE -->
<div class="card bg-white card-shadow">
    <div class="card-body">
        <h3><?= __('Elenco ordini clienti da attenzionare'.' per utente: '.$loggeduser) ?></h3>
            <table class="table table-responsive" id="data-table" >
              <br/>
              <h3>Ordini in fase 1</h3>
            <thead class="thead-light">
            <tr>
                <th scope="col"><?= h('Numero ordine') ?></th>
                <th scope="col"><?= h('Numero impianto')?></th>
                <th scope="col"><?= h('Data ordine') ?></th>
                <th scope="col"><?= h('Cliente') ?></th>
                <th scope="col"><?= h('Indirizzo') ?></th>
                <th scope="col"><?= h('Step da eseguire') ?></th>
                <th scope="col"><?= h('Descrizione step da eseguire') ?></th>
                <th scope="col"><?= h('Utente incaricato') ?></th>
                <th scope="col"><?= h('Link') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($ordersToShow as $orderToShow): ?>
            <?php //pr($loggeduserid); ?>
            <?php
                  if($orderToShow->step_1_completed == 0 && $orderToShow->step_1_user == $loggeduserid)
                  {
            ?>
                  <tr>
                    <td><?= h($orderToShow->orders_customer->order_number); ?></td>
                    <td><?= h($orderToShow->orders_customer->installation_number); ?></td>
                    <td><?= h($orderToShow->orders_customer->contract_sign_date); ?></td>
                    <td><?= h($orderToShow->orders_customer->customer->name); ?></td>
                    <td><?= h($orderToShow->orders_customer->quotation->address." - ".$orderToShow->orders_customer->quotation->city); ?></td>
                    <td><?//= h($orderToShow->step_1_completed); ?>1</td>
                    <td><?= h($orderToShow->description_step_1); ?></td>
                    <td>
                    <?php
                      if($orderToShow->step_1_user == 1)
                        {
                        echo "Luca";
                        }
                        elseif($orderToShow->step_1_user == 10)
                        {
                        echo "Amministrazione";
                        }
                        elseif($orderToShow->step_1_user == 100)
                        {
                        echo "Commerciale";
                        }
                        elseif($orderToShow->step_1_user == 101)
                        {
                        echo "Olti";
                        }
                        elseif($orderToShow->step_1_user == 104)
                        {
                        echo "Sicurezza";
                        } 
                    ?>
                    </td>
                 
                    <td><?= $this->Html->link('Visualizza dettaglio', ['controller' => 'OrderPhase1', 'action' => 'view', $orderToShow->orders_customer->id], array('id' => 'action1')) ?></td>
                  </tr>
          <?php
                 }
                  elseif($orderToShow->step_2_completed == 0 && $orderToShow->step_2_user == $loggeduserid)
                  {
                  ?>
                  <tr>
                    <td><?= h($orderToShow->orders_customer->order_number); ?></td>
                    <td><?= h($orderToShow->orders_customer->installation_number); ?></td>
                    <td><?= h($orderToShow->orders_customer->contract_sign_date); ?></td>
                    <td><?= h($orderToShow->orders_customer->customer->name); ?></td>
                    <td><?= h($orderToShow->orders_customer->quotation->address." - ".$orderToShow->orders_customer->quotation->city); ?></td>
                    <td><?//= h($orderToShow->step_2_completed); ?>2</td>
                    <td><?= h($orderToShow->description_step_2); ?></td>
                                        <td>
                    <?php
                      if($orderToShow->step_2_user == 1)
                        {
                        echo "Luca";
                        }
                        elseif($orderToShow->step_2_user == 10)
                        {
                        echo "Amministrazione";
                        }
                        elseif($orderToShow->step_2_user == 100)
                        {
                        echo "Commerciale";
                        }
                        elseif($orderToShow->step_2_user == 101)
                        {
                        echo "Olti";
                        }
                        elseif($orderToShow->step_2_user == 104)
                        {
                        echo "Sicurezza";
                        }  
                    ?>
                    </td>
                    <td><?= $this->Html->link('Visualizza dettaglio', ['controller' => 'OrderPhase1', 'action' => 'view', $orderToShow->orders_customer->id], array('id' => 'action1')) ?></td>
                  </tr>
                <?php
                  }
                  elseif($orderToShow->step_3_completed == 0 && $orderToShow->step_3_user == $loggeduserid)
                  {
                  ?>
                  <tr>
                    <td><?= h($orderToShow->orders_customer->order_number); ?></td>
                    <td><?= h($orderToShow->orders_customer->installation_number); ?></td>
                    <td><?= h($orderToShow->orders_customer->contract_sign_date); ?></td>
                    <td><?= h($orderToShow->orders_customer->customer->name); ?></td>
                    <td><?= h($orderToShow->orders_customer->quotation->address." - ".$orderToShow->orders_customer->quotation->city); ?></td>
                    <td><?//= h($orderToShow->step_3_completed); ?>3</td>
                    <td><?= h($orderToShow->description_step_3); ?></td>
                                        <td>
                    <?php
                      if($orderToShow->step_3_user == 1)
                        {
                        echo "Luca";
                        }
                        elseif($orderToShow->step_3_user == 10)
                        {
                        echo "Amministrazione";
                        }
                        elseif($orderToShow->step_3_user == 100)
                        {
                        echo "Commerciale";
                        }
                        elseif($orderToShow->step_3_user == 101)
                        {
                        echo "Olti";
                        } 
                                            elseif($orderToShow->step_3_user == 104)
                        {
                        echo "Sicurezza";
                        } 
                    ?>
                    </td>
                    <td><?= $this->Html->link('Visualizza dettaglio', ['controller' => 'OrderPhase1', 'action' => 'view', $orderToShow->orders_customer->id], array('id' => 'action1')) ?></td>
                  </tr>
                  <?
                  }
                  elseif($orderToShow->step_4_completed == 0 && $orderToShow->step_4_user == $loggeduserid)
                  {
                  ?>
                  <tr>
                    <td><?= h($orderToShow->orders_customer->order_number); ?></td>
                    <td><?= h($orderToShow->orders_customer->installation_number); ?></td>
                    <td><?= h($orderToShow->orders_customer->contract_sign_date); ?></td>
                    <td><?= h($orderToShow->orders_customer->customer->name); ?></td>
                    <td><?= h($orderToShow->orders_customer->quotation->address." - ".$orderToShow->orders_customer->quotation->city); ?></td>           
                    <td><?//= h($orderToShow->step_4_completed); ?>4</td>
                    <td><?= h($orderToShow->description_step_4); ?></td>
                                        <td>
                    <?php
                      if($orderToShow->step_4_user == 1)
                        {
                        echo "Luca";
                        }
                        elseif($orderToShow->step_4_user == 10)
                        {
                        echo "Amministrazione";
                        }
                        elseif($orderToShow->step_4_user == 100)
                        {
                        echo "Commerciale";
                        }
                        elseif($orderToShow->step_4_user == 101)
                        {
                        echo "Olti";
                        }
                         elseif($orderToShow->step_4_user == 104)
                        {
                        echo "Sicurezza";
                        }   
                    ?>
                    </td>
                    <td><?= $this->Html->link('Visualizza dettaglio', ['controller' => 'OrderPhase1', 'action' => 'view', $orderToShow->orders_customer->id], array('id' => 'action1')) ?></td>
                  </tr>
                  <?
                  }
                  elseif($orderToShow->step_5_completed == 0 && $orderToShow->step_5_user == $loggeduserid)
                  {
                  ?>
                  <tr>
                    <td><?= h($orderToShow->orders_customer->order_number); ?></td>
                    <td><?= h($orderToShow->orders_customer->installation_number); ?></td>
                    <td><?= h($orderToShow->orders_customer->contract_sign_date); ?></td>
                    <td><?= h($orderToShow->orders_customer->customer->name); ?></td>
                    <td><?= h($orderToShow->orders_customer->quotation->address." - ".$orderToShow->orders_customer->quotation->city); ?></td>         
                    <td><?//= h($orderToShow->step_5_completed); ?>5</td>
                    <td><?= h($orderToShow->description_step_5); ?></td>
                                        <td>
                    <?php
                      if($orderToShow->step_5_user == 1)
                        {
                        echo "Luca";
                        }
                        elseif($orderToShow->step_5_user == 10)
                        {
                        echo "Amministrazione";
                        }
                        elseif($orderToShow->step_5_user == 100)
                        {
                        echo "Commerciale";
                        }
                        elseif($orderToShow->step_5_user == 101)
                        {
                        echo "Olti";
                        }
                                        elseif($orderToShow->step_5_user == 104)
                        {
                        echo "Sicurezza";
                        }     
                    ?>
                    </td>
                    <td><?= $this->Html->link('Visualizza dettaglio', ['controller' => 'OrderPhase1', 'action' => 'view', $orderToShow->orders_customer->id], array('id' => 'action1')) ?></td>
                  </tr>
                  <?
                  }?>
            <!-- fine foreach -->
            <?php endforeach; ?>
        </tbody>
    </table>
</div>

    
    <!--<iframe src="https://liftservice-test.cdinformatica.opalstacked.com/order-phase1/order-status-by-user" title="Ordini da attenzionare" width="100%" height="100%"></iframe>--!>
    
     <!-- FINE BOX ORDINI PER UTENTE -->
  

      <div class="card-body">
            <table class="table table-responsive" id="data-table" >
              <br/>
              <h3>Ordini in fase 2</h3>
            <thead class="thead-light">
            <tr>
                <th scope="col"><?= h('Numero ordine') ?></th>
                <th scope="col"><?= h('Numero impianto')?></th>
                <th scope="col"><?= h('Data ordine') ?></th>
                <th scope="col"><?= h('Cliente') ?></th>
                <th scope="col"><?= h('Indirizzo') ?></th>
                <th scope="col"><?= h('Step da eseguire') ?></th>
                <th scope="col"><?= h('Descrizione step da eseguire') ?></th>
                <th scope="col"><?= h('Utente incaricato') ?></th>
                <th scope="col"><?= h('Link') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($ordersToShow2 as $orderToShow2): ?>
            <?php
                  if($orderToShow2->step_1_completed == 0 && $orderToShow2->step_1_user == $loggeduserid)
                  {
            ?>
                  <tr>
                    <td><?= h($orderToShow2->orders_customer->order_number); ?><?= h($orderToShow2->orders_customer->order_id); ?></td>
                    <td><?= h($orderToShow2->orders_customer->installation_number); ?></td>
                    <td><?= h($orderToShow2->orders_customer->contract_sign_date); ?></td>
                    <td><?= h($orderToShow2->orders_customer->customer->name); ?></td>
                    <td><?= h($orderToShow2->orders_customer->quotation->address." - ".$orderToShow2->orders_customer->quotation->city); ?></td>
                    <td><?//= h($orderToShow2->step_1_completed); ?>1</td>
                    <td><?= h($orderToShow2->description_step_1); ?></td>
                    <td>
                    <?php
                      if($orderToShow2->step_1_user == 1)
                        {
                        echo "Luca";
                        }
                        elseif($orderToShow2->step_1_user == 10)
                        {
                        echo "Amministrazione";
                        }
                        elseif($orderToShow2->step_1_user == 100)
                        {
                        echo "Commerciale";
                        }
                        elseif($orderToShow2->step_1_user == 101)
                        {
                        echo "Olti";
                        }
                                            elseif($orderToShow2->step_1_user == 104)
                        {
                        echo "Sicurezza";
                        } 
                    ?>
                    </td>
                    <td><?= $this->Html->link('Visualizza dettaglio', ['controller' => 'OrderPhase1', 'action' => 'view', $orderToShow2->orders_customer->id], array('id' => 'action1')) ?></td>
                  </tr>
          <?php
                 }
                  elseif($orderToShow2->step_2_completed == 0 && $orderToShow2->step_2_user == $loggeduserid)
                  {
                  ?>
                  <tr>
                    <td><?= h($orderToShow2->orders_customer->order_number); ?><?= h($orderToShow2->orders_customer->order_id); ?></td>
                    <td><?= h($orderToShow2->orders_customer->installation_number); ?></td>
                    <td><?= h($orderToShow2->orders_customer->contract_sign_date); ?></td>
                    <td><?= h($orderToShow2->orders_customer->customer->name); ?></td>
                    <td><?= h($orderToShow2->orders_customer->quotation->address." - ".$orderToShow2->orders_customer->quotation->city); ?></td>
                    <td><?//= h($orderToShow2->step_2_completed); ?>2</td>
                    <td><?= h($orderToShow2->description_step_2); ?></td>
                                        <td>
                    <?php
                      if($orderToShow2->step_2_user == 1)
                        {
                        echo "Luca";
                        }
                        elseif($orderToShow2->step_2_user == 10)
                        {
                        echo "Amministrazione";
                        }
                        elseif($orderToShow2->step_2_user == 100)
                        {
                        echo "Commerciale";
                        }
                        elseif($orderToShow2->step_2_user == 101)
                        {
                        echo "Olti";
                        }
                                            elseif($orderToShow2->step_2_user == 104)
                        {
                        echo "Sicurezza";
                        } 
                    ?>
                    </td>
                    <td><?= $this->Html->link('Visualizza dettaglio', ['controller' => 'OrderPhase1', 'action' => 'view', $orderToShow2->orders_customer->id], array('id' => 'action1')) ?></td>
                  </tr>
                <?php
                  }
                  elseif($orderToShow2->step_3_completed == 0 && $orderToShow2->step_3_user == $loggeduserid)
                  {
                  ?>
                  <tr>
                    <td><?= h($orderToShow2->orders_customer->order_number); ?><?= h($orderToShow2->orders_customer->order_id); ?></td>
                    <td><?= h($orderToShow2->orders_customer->installation_number); ?></td>
                    <td><?= h($orderToShow2->orders_customer->contract_sign_date); ?></td>
                    <td><?= h($orderToShow2->orders_customer->customer->name); ?></td>
                    <td><?= h($orderToShow2->orders_customer->quotation->address." - ".$orderToShow2->orders_customer->quotation->city); ?></td>
                    <td><?//= h($orderToShow2->step_3_completed); ?>3</td>
                    <td><?= h($orderToShow2->description_step_3); ?></td>
                                        <td>
                    <?php
                      if($orderToShow2->step_3_user == 1)
                        {
                        echo "Luca";
                        }
                        elseif($orderToShow2->step_3_user == 10)
                        {
                        echo "Amministrazione";
                        }
                        elseif($orderToShow2->step_3_user == 100)
                        {
                        echo "Commerciale";
                        }
                        elseif($orderToShow2->step_3_user == 101)
                        {
                        echo "Olti";
                        }
                   elseif($orderToShow2->step_3_user == 104)
                        {
                        echo "Sicurezza";
                        }  
                    ?>
                    </td>
                    <td><?= $this->Html->link('Visualizza dettaglio', ['controller' => 'OrderPhase1', 'action' => 'view', $orderToShow2->orders_customer->id], array('id' => 'action1')) ?></td>
                  </tr>
                  <?
                  }
                  elseif($orderToShow2->step_4_completed == 0 && $orderToShow2->step_4_user == $loggeduserid)
                  {
                  ?>
                  <tr>
                    <td><?= h($orderToShow2->orders_customer->order_number); ?><?= h($orderToShow2->orders_customer->order_id); ?></td>
                    <td><?= h($orderToShow2->orders_customer->installation_number); ?></td>
                    <td><?= h($orderToShow2->orders_customer->contract_sign_date); ?></td>
                    <td><?= h($orderToShow2->orders_customer->customer->name); ?></td>
                    <td><?= h($orderToShow2->orders_customer->quotation->address." - ".$orderToShow2->orders_customer->quotation->city); ?></td>           
                    <td><?//= h($orderToShow2->step_4_completed); ?>4</td>
                    <td><?= h($orderToShow2->description_step_4); ?></td>
                                        <td>
                    <?php
                      if($orderToShow2->step_4_user == 1)
                        {
                        echo "Luca";
                        }
                        elseif($orderToShow2->step_4_user == 10)
                        {
                        echo "Amministrazione";
                        }
                        elseif($orderToShow2->step_4_user == 100)
                        {
                        echo "Commerciale";
                        }
                        elseif($orderToShow2->step_4_user == 101)
                        {
                        echo "Olti";
                        }
                   elseif($orderToShow2->step_4_user == 104)
                        {
                        echo "Sicurezza";
                        }  
                    ?>
                    </td>
                    <td><?= $this->Html->link('Visualizza dettaglio', ['controller' => 'OrderPhase1', 'action' => 'view', $orderToShow2->orders_customer->id], array('id' => 'action1')) ?></td>
                  </tr>
                  <?
                  }
                  /*pr($loggeduserid);
                  pr($orderToShow2->order_id);
                  pr($orderToShow2->completed_step_5 == 0);
                  pr($orderToShow2->step_5_user > $loggeduserid);
                  echo "string compare:<br/>";
                  echo strcmp($orderToShow2->step_5_user , $loggeduserid)."<br/>";
                  echo "fine string compare<br/>";
                  echo "<br/>";
                  echo var_dump($orderToShow2->step_5_user );
                  echo "<br/>";
                  echo var_dump($loggeduserid);
                  echo "<br/>"; 
                  echo "ordine-".$orderToShow2->order_id;
                  pr($orderToShow2->completed_step_5 == 0 );
                  pr($orderToShow2->step_5_user == $loggeduserid);*/
                  elseif($orderToShow2->completed_step_5 == 0 && $orderToShow2->step_5_user == $loggeduserid)
                  {
                  ?>
                  <tr>
                    <td><?= h($orderToShow2->orders_customer->order_number); echo " - ".$orderToShow2->order_id;?></td>
                    <td><?= h($orderToShow2->orders_customer->installation_number); ?></td>
                    <td><?= h($orderToShow2->orders_customer->contract_sign_date); ?></td>
                    <td><?= h($orderToShow2->orders_customer->customer->name); ?></td>
                    <td><?= h($orderToShow2->orders_customer->quotation->address." - ".$orderToShow2->orders_customer->quotation->city); ?></td>         
                    <td><?//= h($orderToShow2->step_5_completed); ?>5</td>
                    <td><?= h($orderToShow2->description_step_5); ?></td>
                                        <td>
                    <?php
                      if($orderToShow2->step_5_user == 1)
                        {
                        echo "Luca";
                        }
                        elseif($orderToShow2->step_5_user == 10)
                        {
                        echo "Amministrazione";
                        }
                        elseif($orderToShow2->step_5_user == 100)
                        {
                        echo "Commerciale";
                        }
                        elseif($orderToShow2->step_5_user == 101)
                        {
                        echo "Olti";
                        }
                   elseif($orderToShow2->step_5_user == 104)
                        {
                        echo "Sicurezza";
                        }  
                    ?>
                    </td>
                    <td><?= $this->Html->link('Visualizza dettaglio', ['controller' => 'OrderPhase1', 'action' => 'view', $orderToShow2->orders_customer->id], array('id' => 'action1')) ?></td>
                  </tr>
                  <?
                  }
                  elseif($orderToShow2->step_6_completed == 0 && $orderToShow2->step_6_user == $loggeduserid)
                  {
                  ?>
           <tr>
                    <td><?= h($orderToShow2->orders_customer->order_number); ?></td>
                    <td><?= h($orderToShow2->orders_customer->installation_number); ?></td>
                    <td><?= h($orderToShow2->orders_customer->contract_sign_date); ?></td>
                    <td><?= h($orderToShow2->orders_customer->customer->name); ?></td>
                    <td><?= h($orderToShow2->orders_customer->quotation->address." - ".$orderToShow2->orders_customer->quotation->city); ?></td>         
                    <td><?//= h($orderToShow2->step_6_completed); ?>5</td>
                    <td><?= h($orderToShow2->description_step_6); ?></td>
                                        <td>
                    <?php
                      if($orderToShow2->step_6_user == 1)
                        {
                        echo "Luca";
                        }
                        elseif($orderToShow2->step_6_user == 10)
                        {
                        echo "Amministrazione";
                        }
                        elseif($orderToShow2->step_6_user == 100)
                        {
                        echo "Commerciale";
                        }
                        elseif($orderToShow2->step_6_user == 101)
                        {
                        echo "Olti";
                        }
                   elseif($orderToShow2->step_6_user == 104)
                        {
                        echo "Sicurezza";
                        }  
                    ?>
                    </td>
                    <td><?= $this->Html->link('Visualizza dettaglio', ['controller' => 'OrderPhase1', 'action' => 'view', $orderToShow2->orders_customer->id], array('id' => 'action1')) ?></td>
                  </tr>
          <?
                  }
                  elseif($orderToShow2->step_7_completed == 0 && $orderToShow2->step_7_user == $loggeduserid)
                  {
                  ?>
<tr>
                    <td><?= h($orderToShow2->orders_customer->order_number); ?></td>
                    <td><?= h($orderToShow2->orders_customer->installation_number); ?></td>
                    <td><?= h($orderToShow2->orders_customer->contract_sign_date); ?></td>
                    <td><?= h($orderToShow2->orders_customer->customer->name); ?></td>
                    <td><?= h($orderToShow2->orders_customer->quotation->address." - ".$orderToShow2->orders_customer->quotation->city); ?></td>         
                    <td><?//= h($orderToShow2->step_7_completed); ?>5</td>
                    <td><?= h($orderToShow2->description_step_7); ?></td>
                                        <td>
                    <?php
                      if($orderToShow2->step_7_user == 1)
                        {
                        echo "Luca";
                        }
                        elseif($orderToShow2->step_7_user == 10)
                        {
                        echo "Amministrazione";
                        }
                        elseif($orderToShow2->step_7_user == 100)
                        {
                        echo "Commerciale";
                        }
                        elseif($orderToShow2->step_7_user == 101)
                        {
                        echo "Olti";
                        }
                   elseif($orderToShow2->step_7_user == 104)
                        {
                        echo "Sicurezza";
                        }  
                    ?>
                    </td>
                    <td><?= $this->Html->link('Visualizza dettaglio', ['controller' => 'OrderPhase1', 'action' => 'view', $orderToShow2->orders_customer->id], array('id' => 'action1')) ?></td>
                  </tr>
          <?
                  }
                  elseif($orderToShow2->step_8_completed == 0 && $orderToShow2->step_8_user == $loggeduserid)
                  {
                  ?>
                  <tr>
                    <td><?= h($orderToShow2->orders_customer->order_number); ?></td>
                    <td><?= h($orderToShow2->orders_customer->installation_number); ?></td>
                    <td><?= h($orderToShow2->orders_customer->contract_sign_date); ?></td>
                    <td><?= h($orderToShow2->orders_customer->customer->name); ?></td>
                    <td><?= h($orderToShow2->orders_customer->quotation->address." - ".$orderToShow2->orders_customer->quotation->city); ?></td>         
                    <td><?//= h($orderToShow2->step_8_completed); ?>5</td>
                    <td><?= h($orderToShow2->description_step_8); ?></td>
                                        <td>
                    <?php
                      if($orderToShow2->step_8_user == 1)
                        {
                        echo "Luca";
                        }
                        elseif($orderToShow2->step_8_user == 10)
                        {
                        echo "Amministrazione";
                        }
                        elseif($orderToShow2->step_8_user == 100)
                        {
                        echo "Commerciale";
                        }
                        elseif($orderToShow2->step_8_user == 101)
                        {
                        echo "Olti";
                        }
                   elseif($orderToShow2->step_8_user == 104)
                        {
                        echo "Sicurezza";
                        }  
                    ?>
                    </td>
                    <td><?= $this->Html->link('Visualizza dettaglio', ['controller' => 'OrderPhase1', 'action' => 'view', $orderToShow2->orders_customer->id], array('id' => 'action1')) ?></td>
                  </tr>
          <?
                  }
                  elseif($orderToShow2->step_9_completed == 0 && $orderToShow2->step_9_user == $loggeduserid)
                  {
                  ?>
<tr>
                    <td><?= h($orderToShow2->orders_customer->order_number); ?></td>
                    <td><?= h($orderToShow2->orders_customer->installation_number); ?></td>
                    <td><?= h($orderToShow2->orders_customer->contract_sign_date); ?></td>
                    <td><?= h($orderToShow2->orders_customer->customer->name); ?></td>
                    <td><?= h($orderToShow2->orders_customer->quotation->address." - ".$orderToShow2->orders_customer->quotation->city); ?></td>         
                    <td><?//= h($orderToShow2->step_9_completed); ?>5</td>
                    <td><?= h($orderToShow2->description_step_9); ?></td>
                                        <td>
                    <?php
                      if($orderToShow2->step_9_user == 1)
                        {
                        echo "Luca";
                        }
                        elseif($orderToShow2->step_9_user == 10)
                        {
                        echo "Amministrazione";
                        }
                        elseif($orderToShow2->step_9_user == 100)
                        {
                        echo "Commerciale";
                        }
                        elseif($orderToShow2->step_9_user == 101)
                        {
                        echo "Olti";
                        } 
                   elseif($orderToShow2->step_9_user == 104)
                        {
                        echo "Sicurezza";
                        }  
                    ?>
                    </td>
                    <td><?= $this->Html->link('Visualizza dettaglio', ['controller' => 'OrderPhase1', 'action' => 'view', $orderToShow2->orders_customer->id], array('id' => 'action1')) ?></td>
                  </tr>

                  <?
                  }
            ?>
            <!-- fine foreach -->
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
          <div class="card-body">
            <table class="table table-responsive" id="data-table" >
              <br/>
              <h3>Ordini in fase 3</h3>
            <thead class="thead-light">
            <tr>
                <th scope="col"><?= h('Numero ordine') ?></th>
                <th scope="col"><?= h('Numero impianto')?></th>
                <th scope="col"><?= h('Data ordine') ?></th>
                <th scope="col"><?= h('Cliente') ?></th>
                <th scope="col"><?= h('Indirizzo') ?></th>
                <th scope="col"><?= h('Step da eseguire') ?></th>
                <th scope="col"><?= h('Descrizione step da eseguire') ?></th>
                <th scope="col"><?= h('Utente incaricato') ?></th>
                <th scope="col"><?= h('Link') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($ordersToShow3 as $orderToShow3): ?>
            <?php //pr($ordersToShow3); ?>
            <?php
                  if($orderToShow3->step_1_completed == 0 && $orderToShow3->step_1_user == $loggeduserid)
                  {
            ?>
                  <tr>ciaone
                    <td><?= h($orderToShow3->orders_customer->order_number); ?></td>
                    <td><?= h($orderToShow3->orders_customer->installation_number); ?></td>
                    <td><?= h($orderToShow3->orders_customer->contract_sign_date); ?></td>
                    <td><?= h($orderToShow3->orders_customer->customer->name); ?></td>
                    <td><?= h($orderToShow3->orders_customer->quotation->address." - ".$orderToShow3->orders_customer->quotation->city); ?></td>
                    <td><?//= h($orderToShow3->step_1_completed); ?>1</td>
                    <td><?= h($orderToShow3->description_step_1); ?></td>
                    <td>
                    <?php
                      if($orderToShow3->step_1_user == 1)
                        {
                        echo "Luca";
                        }
                        elseif($orderToShow3->step_1_user == 10)
                        {
                        echo "Amministrazione";
                        }
                        elseif($orderToShow3->step_1_user == 100)
                        {
                        echo "Commerciale";
                        }
                        elseif($orderToShow3->step_1_user == 101)
                        {
                        echo "Olti";
                        }
                        elseif($orderToShow3->step_1_user == 104)
                        {
                        echo "Sicurezza";
                        }  
                    ?>
                    </td>
                    <td><?= h($orderToShow3->step_1_user); ?></td>
                    <td><?= $this->Html->link('Visualizza dettaglio', ['controller' => 'OrderPhase1', 'action' => 'view', $orderToShow3->orders_customer->id], array('id' => 'action1')) ?></td>
                  </tr>
                <?php
                 }
                  elseif($orderToShow3->step_2_completed == 0 && $orderToShow3->step_2_user == $loggeduserid)
                  {
                  ?>
                  <tr>
                    <td><?= h($orderToShow3->orders_customer->order_number); ?></td>
                    <td><?= h($orderToShow3->orders_customer->installation_number); ?></td>
                    <td><?= h($orderToShow3->orders_customer->contract_sign_date); ?></td>
                    <td><?= h($orderToShow3->orders_customer->customer->name); ?></td>
                    <td><?= h($orderToShow3->orders_customer->quotation->address." - ".$orderToShow3->orders_customer->quotation->city); ?></td>
                    <td><?//= h($orderToShow3->step_2_completed); ?>2</td>
                    <td><?= h($orderToShow3->description_step_2); ?></td>
                                        <td>
                    <?php
                      if($orderToShow3->step_2_user == 1)
                        {
                        echo "Luca";
                        }
                        elseif($orderToShow3->step_2_user == 10)
                        {
                        echo "Amministrazione";
                        }
                        elseif($orderToShow3->step_2_user == 100)
                        {
                        echo "Commerciale";
                        }
                        elseif($orderToShow3->step_2_user == 101)
                        {
                        echo "Olti";
                        }
                                            elseif($orderToShow3->step_2_user == 104)
                        {
                        echo "Sicurezza";
                        } 
                    ?>
                    </td>
                    <td><?= $this->Html->link('Visualizza dettaglio', ['controller' => 'OrderPhase1', 'action' => 'view', $orderToShow3->orders_customer->id], array('id' => 'action1')) ?></td>
                  </tr>
                <?php
                  }
                  elseif($orderToShow3->step_3_completed == 0 && $orderToShow3->step_3_user == $loggeduserid)
                  {
                  ?>
                  <tr>
                    <td><?= h($orderToShow3->orders_customer->order_number); ?></td>
                    <td><?= h($orderToShow3->orders_customer->installation_number); ?></td>
                    <td><?= h($orderToShow3->orders_customer->contract_sign_date); ?></td>
                    <td><?= h($orderToShow3->orders_customer->customer->name); ?></td>
                    <td><?= h($orderToShow3->orders_customer->quotation->address." - ".$orderToShow3->orders_customer->quotation->city); ?></td>
                    <td><?//= h($orderToShow3->step_3_completed); ?>3</td>
                    <td><?= h($orderToShow3->description_step_3); ?></td>
                                        <td>
                    <?php
                      if($orderToShow3->step_3_user == 1)
                        {
                        echo "Luca";
                        }
                        elseif($orderToShow3->step_3_user == 10)
                        {
                        echo "Amministrazione";
                        }
                        elseif($orderToShow3->step_3_user == 100)
                        {
                        echo "Commerciale";
                        }
                        elseif($orderToShow3->step_3_user == 101)
                        {
                        echo "Olti";
                        } 
                                            elseif($orderToShow3->step_3_user == 104)
                        {
                        echo "Sicurezza";
                        } 
                    ?>
                    </td>
                    <td><?= $this->Html->link('Visualizza dettaglio', ['controller' => 'OrderPhase1', 'action' => 'view', $orderToShow3->orders_customer->id], array('id' => 'action1')) ?></td>
                  </tr>
                  <?
                  }
                  elseif($orderToShow3->step_4_completed == 0 && $orderToShow3->step_4_user == $loggeduserid)
                  {
                  ?>
                  <tr>
                    <td><?= h($orderToShow3->orders_customer->order_number); ?></td>
                    <td><?= h($orderToShow3->orders_customer->installation_number); ?></td>
                    <td><?= h($orderToShow3->orders_customer->contract_sign_date); ?></td>
                    <td><?= h($orderToShow3->orders_customer->customer->name); ?></td>
                    <td><?= h($orderToShow3->orders_customer->quotation->address." - ".$orderToShow3->orders_customer->quotation->city); ?></td>           
                    <td><?//= h($orderToShow3->step_4_completed); ?>4</td>
                    <td><?= h($orderToShow3->description_step_4); ?></td>
                                        <td>
                    <?php
                      if($orderToShow3->step_4_user == 1)
                        {
                        echo "Luca";
                        }
                        elseif($orderToShow3->step_4_user == 10)
                        {
                        echo "Amministrazione";
                        }
                        elseif($orderToShow3->step_4_user == 100)
                        {
                        echo "Commerciale";
                        }
                        elseif($orderToShow3->step_4_user == 101)
                        {
                        echo "Olti";
                        } 
                                            elseif($orderToShow3->step_4_user == 104)
                        {
                        echo "Sicurezza";
                        } 
                    ?>
                    </td>
                    <td><?= $this->Html->link('Visualizza dettaglio', ['controller' => 'OrderPhase1', 'action' => 'view', $orderToShow3->orders_customer->id], array('id' => 'action1')) ?></td>
                  </tr>
                  <?
                  }
                  elseif($orderToShow3->step_5_completed == 0 && $orderToShow3->step_5_user == $loggeduserid)
                  {
                  ?>
                  <tr>
                    <td><?= h($orderToShow3->orders_customer->order_number); ?></td>
                    <td><?= h($orderToShow3->orders_customer->installation_number); ?></td>
                    <td><?= h($orderToShow3->orders_customer->contract_sign_date); ?></td>
                    <td><?= h($orderToShow3->orders_customer->customer->name); ?></td>
                    <td><?= h($orderToShow3->orders_customer->quotation->address." - ".$orderToShow3->orders_customer->quotation->city); ?></td>         
                    <td><?//= h($orderToShow3->step_5_completed); ?>5</td>
                    <td><?= h($orderToShow3->description_step_5); ?></td>
                                        <td>
                    <?php
                      if($orderToShow3->step_5_user == 1)
                        {
                        echo "Luca";
                        }
                        elseif($orderToShow3->step_5_user == 10)
                        {
                        echo "Amministrazione";
                        }
                        elseif($orderToShow3->step_5_user == 100)
                        {
                        echo "Commerciale";
                        }
                        elseif($orderToShow3->step_5_user == 101)
                        {
                        echo "Olti";
                        }
                                            elseif($orderToShow3->step_5_user == 104)
                        {
                        echo "Sicurezza";
                        } 
                    ?>
                    </td>
                    <td><?= $this->Html->link('Visualizza dettaglio', ['controller' => 'OrderPhase1', 'action' => 'view', $orderToShow3->orders_customer->id], array('id' => 'action1')) ?></td>
                  </tr>
                  <?
                  }
                  elseif($orderToShow3->step_6_completed == 0 && $orderToShow3->step_6_user == $loggeduserid)
                  {
                  ?>
                  <tr>
                    <td><?= h($orderToShow3->orders_customer->order_number); ?></td>
                    <td><?= h($orderToShow3->orders_customer->installation_number); ?></td>
                    <td><?= h($orderToShow3->orders_customer->contract_sign_date); ?></td>
                    <td><?= h($orderToShow3->orders_customer->customer->name); ?></td>
                    <td><?= h($orderToShow3->orders_customer->quotation->address." - ".$orderToShow3->orders_customer->quotation->city); ?></td>         
                    <td><?//= h($orderToShow3->step_6_completed); ?>5</td>
                    <td><?= h($orderToShow3->description_step_6); ?></td>
                                        <td>
                    <?php
                      if($orderToShow3->step_6_user == 1)
                        {
                        echo "Luca";
                        }
                        elseif($orderToShow3->step_6_user == 10)
                        {
                        echo "Amministrazione";
                        }
                        elseif($orderToShow3->step_6_user == 100)
                        {
                        echo "Commerciale";
                        }
                        elseif($orderToShow3->step_6_user == 101)
                        {
                        echo "Olti";
                        } 
                                            elseif($orderToShow3->step_6_user == 104)
                        {
                        echo "Sicurezza";
                        } 
                    ?>
                    </td>
                    <td><?= $this->Html->link('Visualizza dettaglio', ['controller' => 'OrderPhase1', 'action' => 'view', $orderToShow3->orders_customer->id], array('id' => 'action1')) ?></td>
                  </tr>
            <?php
                  }        
            ?>
            <!-- fine foreach -->
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
  </div>
</div>
    </div>
</div>



<!--- FINE BOX FILTRI AZIONI  --->
<!--- INIZIO BOX AVANZAMENTO ORDINI --->
<!-- <div class="card bg-white card-shadow">
    <div class="card-body">
<button class="btn btn-primary collapsed" type="button" data-toggle="collapse" data-target="#collapseOrders" aria-expanded="false" aria-controls="collapseExample" style="text-color:white;">
Visualizza stato avanzamento ordini
</button>
       <div class="mt-2 collapse" id="collapseOrders" style="">
<p>

         </p>
          
        </div>
  </div>
</div> -->
<!--- FINE BOX AVANZAMENTO ORDINI --->
<div class="card bg-white card-shadow">
  <div class="card-body">
    <div class="row todo">
        <?php 
          if (!$actions_list_todo->isEmpty()) { ?>		
          <div class="col-md-6">
              <div class="card-actions-todo simple-shadow">
                  <div class="toolbar toolbar--inner">
                    <div class="toolbar__label">
                      <h4>Elenco azioni da compiere</h4>
                    </div>
                  </div>
                  <div class="listview listview--bordered">
                  <?php foreach ($actions_list_todo as $action): ?>
                      
                           <?php $tipo_azione= str_replace(' ', '', trim($action->description)); ?>
                      <?php echo "<div class=\"listview__item_filter_".$tipo_azione."\">";?>
                     <?php echo "<div class=\"listview__item".$tipo_azione."\">";?>
                       <!--<div class="listview__item"> !-->
                          <div class="checkbox checkbox--char todo__item">
                              <label class="checkbox__char bg-green" >
                                <i class="zmdi zmdi-circle zmdi-hc-fw">
                                <?= $this->Html->link(__(''), ['action' => 'view', $action->id]) ?>
                                </i>
                              </label>
                              <div class="listview__content">
  <?php echo "<div class=\"listview_heading_".$tipo_azione."\">";?><?= $this->Html->link(__($action->action_type), ['action' => 'view', 'controller' => 'Actions', $action->id]) ?></div>
                                  
                                  <p>Scadenza: <?= ($action->is_action == 1 ) ? h($action->exec_date->format('d-m-Y H:i')) : h($action->modified->format('Y-m-d')) ?></p>
                              </div>
                              <div class="listview__attrs">
                                  <span><?= $this->Html->link(__($action->customer->name), ['action' => 'view', 'controller' => 'Customers', $action->customer->id]) ?></span>
                                  <span><?= $this->Html->link(__($action->quotation->label), ['action' => 'view', 'controller' => 'Quotations', $action->quotation->id]) ?></span>
                              </div>
                          </div>
                      </div>
                      <?php endforeach; ?>
                  </div>
              </div>
          </div>
</div>
          <?php 
          } else {
            ?>
          <div class="col-md-6">
              <div class="card-actions-todo simple-shadow">
                  <div class="toolbar toolbar--inner">
                      <div class="toolbar__label"><h4>Elenco azioni da compiere</h4></div>
                  </div>
                  <div class="listview listview--bordered">
                    <?php echo "<h3 style=\"text-align:center\">Complimenti, non hai nulla da fare qui.</h3>";?>
                  </div>
              </div>
          </div>
          <?php
          }
          ?>
          <?php 
          if (!$actions_list_expired->isEmpty()) { 
          ?>		
          <div class="col-md-6">
              <div class="card-actions-expired simple-shadow">
                  <div class="toolbar toolbar--inner">
                      <div class="toolbar__label">
                        <h4>Elenco azioni scadute </h4>
                      </div>
                  </div>
                  <div class="listview listview--bordered">
                  <?php foreach ($actions_list_expired as $action): ?>
                     <?php //pr($action);?>
                           <?php $tipo_azione = str_replace(' ', '', trim($action->action_type)); ?>
                      <?php echo "<div class=\"listview__item_filter_".$tipo_azione."\">";?>
                        <div class="listview__item"> 
                          <div class="checkbox checkbox--char todo__item">
                              <label class="checkbox__char bg-red" >
                                <i class="zmdi zmdi-dot-circle zmdi-hc-fw">
                                <?= $this->Html->link(__(''), ['action' => 'view', $action->id]) ?>
                                </i>
                                </label>
                              <div class="listview__content">
                                  
                                <?php echo "<div class=\"listview_heading_".$tipo_azione."\">";?><?= $this->Html->link(__($action->action_type), ['action' => 'view', 'controller' => 'Actions', $action->id]) ?></div>
                                  <p>Scadenza: <?= ($action->is_action == 1 ) ? h($action->exec_date->format('d-m-Y H:i')) : h($action->modified->format('Y-m-d')) ?></p>
                              </div>
                              <div class="listview__attrs">
                                  <span><?= $this->Html->link(__($action->customer->name), ['action' => 'view', 'controller' => 'Customers', $action->customer->id]) ?></span>
                                  <span><?= $this->Html->link(__($action->quotation->label), ['action' => 'view', 'controller' => 'Quotations', $action->quotation->id]) ?></span>
                              </div>
                          </div>
                         </div>
                      </div>
                      <?php endforeach; ?>
                  </div>
              </div>
          </div>
          <?php } else{ } ?>
          <a href="./actions/add">
          <button class="btn btn-danger btn--action zmdi zmdi-plus" data-toggle="modal" data-target="./actions/add"></button>
          </a>
  </div>
  <div class="row todo">
        <?php 
          if (!$actions_list_todo_7days->isEmpty()) { ?>		
          <div class="col-md-6">
              <div class="card-actions-todo7days simple-shadow">
                  <div class="toolbar toolbar--inner">
                    <div class="toolbar__label"><h4>Elenco azioni prossimi 7 giorni</h4></div>
                  </div>
                  <div class="listview listview--bordered">
                    <?php //pr($actions_list_todo_7days) ;?>
                  <?php foreach ($actions_list_todo_7days as $action): ?>
                       <?php echo "<div class=\"listview__item_filter_".$action->description."\">";?>
<!--                       <div class="listview__item"> -->
                          <div class="checkbox checkbox--char todo__item">
                              <label class="checkbox__char bg-orange" >
                                <i class="zmdi zmdi-circle zmdi-hc-fw">
                                <?= $this->Html->link(__(''), ['action' => 'view', $action->id]) ?>
                                </i>
                              </label>
                              <div class="listview__content">
                                <?php $tipo_azione= str_replace(' ', '', trim($action->action_type)); ?>
                                  <?php echo "<div class=\"listview_heading_".$tipo_azione."\">";?><?= $this->Html->link(__($action->action_type), ['action' => 'view', 'controller' => 'Actions', $action->id]) ?></div>
                                  <p>Scadenza: <?= ($action->is_action == 1 ) ? h($action->exec_date->format('d-m-Y H:i')) : h($action->modified->format('Y-m-d')) ?></p>
                              </div>
                              <div class="listview__attrs">
                                  <span><?= $this->Html->link(__($action->customer->name), ['action' => 'view', 'controller' => 'Customers', $action->customer->id]) ?></span>
                                  <span><?= $this->Html->link(__($action->quotation->label), ['action' => 'view', 'controller' => 'Quotations', $action->quotation->id]) ?></span>
                              </div>
                          </div>
                      </div>
                      <?php endforeach; ?>
                  </div>
              </div>
          </div>
          <?php 
          } else { ?>
          <div class="col-md-6">
              <div class="card-actions-todo7days simple-shadow">
                  <div class="toolbar toolbar--inner">
                      <div class="toolbar__label">Elenco azioni prossimi 7 giorni</div>
                  </div>
                  <div class="listview listview--bordered">
                    <?php echo "<h3 style=\"text-align:center\">Complimenti, non hai nulla da fare nei prossimi 7 giorni...</h3>";?>
                  </div>
              </div>
          </div>
          <?php } ?>
    </div>
  </div>
</div>
