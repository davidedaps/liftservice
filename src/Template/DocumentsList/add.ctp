<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DocumentsList $documentsList
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Documents List'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Documents Order Customer'), ['controller' => 'DocumentsOrderCustomer', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Documents Order Customer'), ['controller' => 'DocumentsOrderCustomer', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Documents Order Supplier'), ['controller' => 'DocumentsOrderSupplier', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Documents Order Supplier'), ['controller' => 'DocumentsOrderSupplier', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="documentsList form large-9 medium-8 columns content">
    <?= $this->Form->create($documentsList) ?>
    <fieldset>
        <legend><?= __('Add Documents List') ?></legend>
        <?php
            echo $this->Form->control('folder');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
