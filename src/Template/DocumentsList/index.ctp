<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DocumentsList[]|\Cake\Collection\CollectionInterface $documentsList
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Documents List'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Documents Order Customer'), ['controller' => 'DocumentsOrderCustomer', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Documents Order Customer'), ['controller' => 'DocumentsOrderCustomer', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Documents Order Supplier'), ['controller' => 'DocumentsOrderSupplier', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Documents Order Supplier'), ['controller' => 'DocumentsOrderSupplier', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="documentsList index large-9 medium-8 columns content">
    <h3><?= __('Documents List') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('folder') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($documentsList as $documentsList): ?>
            <tr>
                <td><?= $this->Number->format($documentsList->id) ?></td>
                <td><?= h($documentsList->folder) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $documentsList->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $documentsList->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $documentsList->id], ['confirm' => __('Are you sure you want to delete # {0}?', $documentsList->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
