<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DocumentsList $documentsList
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Documents List'), ['action' => 'edit', $documentsList->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Documents List'), ['action' => 'delete', $documentsList->id], ['confirm' => __('Are you sure you want to delete # {0}?', $documentsList->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Documents List'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Documents List'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Documents Order Customer'), ['controller' => 'DocumentsOrderCustomer', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Documents Order Customer'), ['controller' => 'DocumentsOrderCustomer', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Documents Order Supplier'), ['controller' => 'DocumentsOrderSupplier', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Documents Order Supplier'), ['controller' => 'DocumentsOrderSupplier', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="documentsList view large-9 medium-8 columns content">
    <h3><?= h($documentsList->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Folder') ?></th>
            <td><?= h($documentsList->folder) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($documentsList->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Documents Order Customer') ?></h4>
        <?php if (!empty($documentsList->documents_order_customer)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Order Customer Id') ?></th>
                <th scope="col"><?= __('Documents List Id') ?></th>
                <th scope="col"><?= __('Attachment') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($documentsList->documents_order_customer as $documentsOrderCustomer): ?>
            <tr>
                <td><?= h($documentsOrderCustomer->id) ?></td>
                <td><?= h($documentsOrderCustomer->order_customer_id) ?></td>
                <td><?= h($documentsOrderCustomer->documents_list_id) ?></td>
                <td><?= h($documentsOrderCustomer->attachment) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'DocumentsOrderCustomer', 'action' => 'view', $documentsOrderCustomer->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'DocumentsOrderCustomer', 'action' => 'edit', $documentsOrderCustomer->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'DocumentsOrderCustomer', 'action' => 'delete', $documentsOrderCustomer->id], ['confirm' => __('Are you sure you want to delete # {0}?', $documentsOrderCustomer->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Documents Order Supplier') ?></h4>
        <?php if (!empty($documentsList->documents_order_supplier)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Order Supplier Id') ?></th>
                <th scope="col"><?= __('Documents List Id') ?></th>
                <th scope="col"><?= __('Attachment') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($documentsList->documents_order_supplier as $documentsOrderSupplier): ?>
            <tr>
                <td><?= h($documentsOrderSupplier->id) ?></td>
                <td><?= h($documentsOrderSupplier->order_supplier_id) ?></td>
                <td><?= h($documentsOrderSupplier->documents_list_id) ?></td>
                <td><?= h($documentsOrderSupplier->attachment) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'DocumentsOrderSupplier', 'action' => 'view', $documentsOrderSupplier->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'DocumentsOrderSupplier', 'action' => 'edit', $documentsOrderSupplier->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'DocumentsOrderSupplier', 'action' => 'delete', $documentsOrderSupplier->id], ['confirm' => __('Are you sure you want to delete # {0}?', $documentsOrderSupplier->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
