<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Quotation $quotation
 */
?>

<div class="card bg-white card-shadow new-contact">
    <div class="card-body">
         <h3><?= __('Aggiungi azione/memo commerciale') ?></h3>
          <br/>
          <?= $this->Form->create() ?>
          <fieldset>
            <div class="row">
               <div class="col-md-6">
                  <div class="form-group">
                          <?php echo $this->Form->control('customer_id' , ['options' => $customers, 'label' => 'Seleziona il cliente']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
            </div>
        </fieldset>
     <?= $this->Form->button(__("<i class=\"zmdi zmdi-arrow-forward zmdi-hc-fw\"></i> Avanti"), ['class' => 'btn btn-info','action' => 'exec', 'escape' => false]) ?>
    <?= $this->Form->end() ?>
  </div>
</div>

