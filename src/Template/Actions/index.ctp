<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Customer[]|\Cake\Collection\CollectionInterface $customers
 */
?>
<?php if (isset($_GET['is_action'])) $is_action = $_GET['is_action'];
      else $is_action = 0;?>
<div class="card bg-white card-shadow">
    <div class="card-body">
        <?php if( $is_action == 0 ) { ?>
        <h3><?= __('Lista aggiornamenti') ?></h3>
        <?php
        } else { ?>
        <h3><?= __('Elenco azioni da svolgere') ?></h3>
        <?php
        } ?>
      <div class="d-block d-sm-none">
      <h4 style="color:blue;">
        Attenzione, scorrere verso destra per visualizzare tutte le informazioni. Grazie.
      </h4>
     </div>
        <?php   
          if($is_action == 0) { ?>
       <table class="table table-responsive" id="data-table" >
         
        <thead class="thead-light">
            <tr>
                <th scope="col"><?= h('Descrizione') ?></th>
                <th scope="col"><?= h('Cliente')?></th>
                <th scope="col"><?= h('Riferimento impianto') ?></th>
                <th scope="col"><?= h('Ultimo aggiornamento') ?></th>
            </tr>
        </thead>
        <tbody>
          <?php } else { ?>
       <table class="table table-responsive" id="data-table" >
        <thead class="thead-light">
            <tr>
                <th scope="col"><?= h('Azione') ?></th>
                <th scope="col"><?= h('Cliente')?></th>
                <th scope="col"><?= h('Riferimento impianto') ?></th>
                <th scope="col"><?= h('Data') ?></th>
                <th scope="col"><?= h('Persona incaricata') ?></th>
            </tr>
        </thead>
        <tbody>
          <?php } ?>
            
            <?php   
              if($is_action == 0) { 
                foreach ($actions as $action):
                  if($action->is_action == 0) {        
                    ?>
                      <?php 
                      $testoGiusto = $this->Text->truncate(
                      $action->description,
                      70,[
                      'ellipsis' => ' ...',
                      'exact' => true
                      ]);           
                      ?>
                    <tr> 
                        <td><?= $this->Html->link(__($testoGiusto), ['action' => 'view', $action->id]) ?></td>          
                        <td><?= $this->Html->link(__($action->customer->name), ['action' => 'view', 'controller' => 'Customers', $action->customer->id]) ?></td>
                        <td><?= $this->Html->link(__($action->quotation->label), ['action' => 'view', 'controller' => 'Quotations', $action->quotation->id]) ?></td>
                        <td><?= h($action->modified->format('Y-m-d'))." / "; if(($action->creator_id) != 0) echo $users[$action->creator_id];?></td>
                    </tr>
                    <?php 
                    }
                endforeach; 
                } else { 
                   foreach ($actions as $action): 
                     if($action->is_action == 1) {
                ?>
                  <tr>
                    <td><?= $this->Html->link(__("<i class=\"zmdi zmdi-zoom-in zmdi-hc-fw\"></i>".$action->description), ['action' => 'view', $action->id],
                        ['title'=>'view','escape'=>false]) ?></td>          
                    <td><?= $this->Html->link(__($action->customer->name), ['action' => 'view', 'controller' => 'Customers', $action->customer->id]) ?></td>
                    <td><?= $this->Html->link(__($action->quotation->label), ['action' => 'view', 'controller' => 'Quotations', $action->quotation->id]) ?></td>
                    <td><?= ($action->is_action == 1 ) ? h($action->exec_date->format('Y-m-d')) : h($action->exec_date) ?></td>
                    <td><?= ($action->user->id != 99 ) ? $this->Html->link(__($action->user->name), ['action' => 'view', 'controller' => 'Users', $action->user->id ]): '' ?></td>
                  </tr>
                <?php
                    } 
                  endforeach; 
                }
                ?>
          
        </tbody>
    </table>
</div>
</div>
