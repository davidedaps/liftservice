<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Quotation $quotation
 */
?>

<div class="card bg-white card-shadow new-contact">
    <div class="card-body">
<?php
          if($action->is_action == 0) { ?>
            <h3><?= __('Visualizza aggiornamento commerciale') ?></h3>
          <?php
          } else { ?>
            <h3><?= __('Visualizza azione') ?></h3>         
          <?php } ?>
          <br/>
          <?= $this->Form->create($action) ?>
          <fieldset>
            <div class="row">
              <div class="col-md-4">
                  <div class="form-group">
                    <label>Cliente:</label>
                           <?= $action->has('customer') ? $this->Html->link($action->customer->name, ['controller' => 'Customers', 'action' => 'view', $action->customer->id]) : '' ?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
               <div class="col-md-4">
                  <div class="form-group">
                          <label>Preventivo:</label>
                           <?= $action->has('quotation') ? $this->Html->link($action->quotation->reference, ['controller' => 'Quotations', 'action' => 'view', $action->quotation->id]) : '' ?>
                          <?php //echo $this->Form->control('quotation_id' , ['label' => 'Seleziona il preventivo cliente  ']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
            </div>
            <div class="row">
              <?php 
              if ($action->is_action == 0) { ?>
                    <div class="col-md-12">
                        <label>Descrizione</label>

                        <div class="form-group">
                                <?php echo ($action->description) ?>
                            <i class="form-group__bar"></i>
                        </div>
                    </div> <?php
              } else { ?>
                    <div class="col-md-3">
                        <label>Azione</label>

                        <div class="form-group">
                                <?php echo ($action->action_type) ?>
                            <i class="form-group__bar"></i>
                        </div>
                    </div> 
              <?php } ?>
              <div class="col-md-3">
                <?php
                if($action->is_action == 1) {
                echo "<label>Data scadenza azione</label><br/>
                      <div class=\"form-group\">";
                echo $action->exec_date->format('d-m-Y')."
                      <i class=\"form-group__bar\"></i>
                    </div>"; 
                } else {
                echo "<label>Data ultima modifica</label><br/>
                      <div class=\"form-group\">";
                echo $action->modified->format('d-m-Y')." - ";
                 if(($action->creator_id) != 0) echo $users[$action->creator_id];
                 echo     "<i class=\"form-group__bar\"></i>
                    </div>"; 
                }
                ?>
              </div>
              <?php
                if($action->is_action == 1) { 
                  ?>
             <div class="col-md-3">
               <label>Persona incaricata</label>
                  <div class="form-group">
                          <?= ($action->user->id != 99 ) ? $this->Html->link(__($action->user->name), ['action' => 'view', 'controller' => 'Users', $action->user->id ]): '' ?>
                          <?php //echo $this->Form->control('quotation_id' , ['label' => 'Seleziona il preventivo cliente  ']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
              <?php } ?>
              <?php
                if($action->is_action == 1) { 
                  ?>
             <div class="col-md-3">
               <label>Stato azione</label>
                  <div class="form-group">
                              <? switch ($action->status) 
                              {
                                case '0':
                                echo "aperta";
                                break;
                                case '1':
                                echo "chiusa";
                                break;
                              }
                     ?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
              <?php } ?>
            </div>
        </fieldset>
    <?= $this->Form->end() ?>
  </div>
</div>
  <!-- Button trigger modal -->
<div class="card bg-white card-shadow">
    <?= $this->element('modal_view', [
              // passo la variabile elementVariable all'elemento. 
              "elementId" => $action->id,
              "elementName" => $action->description,
              "modalTitle" => "Elimina memo commerciale",
              "modalText" => "Sei sicuro di eliminare questo memo commerciale?"
          ]) ?>
</div>
