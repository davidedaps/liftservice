<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Quotation $quotation
 */
?>
<div class="card bg-white card-shadow new-contact">
    <div class="card-body">
         <?php
          if($action->is_action == 0) {
          ?>
            <h3><?= __('Modifica aggiornamento commerciale') ?></h3>
          <?php
          } else {
          ?>
            <h3><?= __('Modifica azione') ?></h3>         
          <?php } ?>
          <br/>
          <?= $this->Form->create($action) ?>
            <?php echo $this->Form->hidden('creator_id', ['value' => $loggeduserid]); ?>
          <fieldset>
            <div class="row">
                <div class="col-md-6">
                    <label>Cliente</label>
                    <div class="form-group">
                        <?php echo $this->Form->control('customer_id', ['options' => $customers,'label' => '','disabled' => 'disabled']);?>
                        <i class="form-group__bar"></i>
                    </div>
                </div>     
                 <div class="col-md-6">
                    <label>Preventivo</label>
                    <div class="form-group">
                             <td><?= $action->has('quotation') ? $this->Html->link($action->quotation->reference, ['controller' => 'Quotations', 'action' => 'view', $action->quotation->id]) : '' ?></td>
                             <?php echo $this->Form->control('quotation_id', ['options' => $quotations,'label' => '',]);?>
                      <?php //echo $this->Form->control('quotation_id' , ['label' => 'Seleziona il preventivo cliente  ']);?>
                        <i class="form-group__bar"></i>
                    </div>
                </div>
             </div>
             <div class="row"><?php
                   if ($action->is_action == 0) { ?>
                     <div class="col-md-12">
                          <label>Descrizione</label>
                          <div class="form-group">
                                  <?php echo $this->Form->textarea('description' , ['label' => 'Descrizione attività']);?>
                              <i class="form-group__bar"></i>
                          </div>
                     </div><?php
                   } else {?>
                     <div class="col-md-6">
                       <label>Azione</label>
                          <div class="form-group">
                                  <?php echo $this->Form->control('action_type', ['label' => '','options' => $action_type]);?>
                              <i class="form-group__bar"></i>
                          </div>
                      </div><?php
                   } ?><?php 
                  if($action->is_action == 1) { ?>
                    <div class="col-md-6">
                          <div class="form-group">
                                  <?php echo $this->Form->control('exec_date' , ['label' => 'Scadenza azione']);?>
                              <i class="form-group__bar"></i>
                          </div>
                    </div> <?php
                  } else { ?>
                    <div class="col-md-6">
                        <label>Ultimo aggiornamento</label>
                        <div class="form-group">
                            <?php echo $action->modified->format('d-m-Y')." / ".$users[$action->creator_id];?>
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                    <?php
                  } 
                  if($action->is_action == 1) { ?>
                   <div class="col-md-6">
                     <label>Persona incaricata</label>
                        <div class="form-group">
                                <?php echo $this->Form->control('user_id', ['label' => '','options' => $users]);?>
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                    <?php
                  } else {

                  } ?>
                  <?php 
                  if($action->is_action == 1) {
                 ?>
                  <div class="col-md-6">
                      <div class="form-group">
                        <label>Stato azione</label>
                              <?php $stato_azione = ['0' => 'aperta', 
                                                        '1' => 'chiusa'];?>
                              <?php echo $this->Form->select('status', $stato_azione, ['empty' => 'seleziona lo stato']);?>
                          <i class="form-group__bar"></i>
                      </div>
                  </div> 
                   <?php
                  } else {

                  }
                    ?>
            </div>
        </fieldset>
     <?= $this->Form->button(__('Salva'), ['class' => 'btn btn-primary']) ?>
    <?= $this->Form->end() ?>
  </div>
</div>
<?php 
if($action->is_action == 1) { ?>
    <script>
    window.onload = function() 
    {
       $('.datetime-picker-editaction').flatpickr({
          defaultDate: "<?= $action->exec_date->format('Y-m-d') ?>",
          defaultHour: "<?= $action->exec_date->format('H')?>",
          defaultMinute: "<?= $action->exec_date->format('I')?>",
       })
    }
    </script>
<?php } else {
}
?>