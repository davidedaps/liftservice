<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Quotation $quotation
 */
?>
<!-- FR=>2019-08-07 - Utilizzo questa funziona  -->
<script type="text/javascript">
    $( document ).ready(function() 
    {
      $( "#is_action" ).click(function() 
      {
        if($(this).prop( "checked" ))
          {
            $("#user_id").hide();
            $("#status").hide();
            $("#exec_date").hide();
            $("#action_type").hide();
            //jQuery(this).prev("li").attr("descrizione", "descrizione1");
            $("#descrizione").show();
           
            console.log('aggiornamento');
          }
        else
          {
           $("#user_id").show();
            $("#status").show();
            $("#exec_date").show();
            $("#action_type").show();
             jQuery(this).prev("li").attr("descrizione", "descrizione1");
            $("#descrizione1").hide();
            console.log('azione');
          }
      });
    }); 
</script>
<div class="card bg-white card-shadow new-contact">
    <div class="card-body">
         <h3><?= __('Aggiungi azione o aggiornamento') ?></h3>
          <br/>
          <?= $this->Form->create($action) ?>
          <?php 
            $customer_id  = $customers->id;
          ?>
          <fieldset><?php echo $this->Form->hidden('customer_id', ['value' => $customer_id]); ?>
            <?php echo $this->Form->hidden('creator_id', ['value' => $loggeduserid]); ?>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label><span style="font-weight:bold;text-decoration:underline;">Aggiornamento</span></label>
                <?php echo $this->Form->checkbox('is_action', ['value' => '0', 'default' => 1, 'id' => 'is_action', 'hiddenField' => false]);?>
                </div>
              </div>
            </div>
            <div class="row">
               <div class="col-md-6">
                  <div class="form-group">
                          <?php echo $this->Form->control('quotation_id' , ['options' => $quotations, 'label' => 'Seleziona il preventivo cliente', 'value'=> $selected_id]);?>
                          <?php //echo $this->Form->control('quotation_id' , ['label' => 'Seleziona il preventivo cliente  ']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
              <div class="col-md-6" id="action_type">
               <div class="form-group">
                 <?php echo $this->Form->control('action_type' , ['options' => $actionsTypeList, 'label' => 'Tipo azione','empty' => 'seleziona il tipo di azione da svolgere']);?>
                </div>
              </div>
             <div class="col-md-12" id="descrizione" style="display:none;">
                  <label>Descrizione aggiornamento</label>
                  <div class="form-group">
                          <?php echo $this->Form->textarea('description' , ['label' => 'Descrizione attività']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
              <div class="col-md-6" id="exec_date">
                <label>Data esecuzione</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="zmdi zmdi-calendar"></i></span>
                    </div>
                  <input type="text" class="form-control datetime-picker" placeholder="inserisci una data di scadenza " name="exec_date">
                </div>
              </div>
             <div class="col-md-3" id="user_id">
                  <div class="form-group">
                          <?php echo $this->Form->control('user_id' , ['options' => $users, 'label' => 'Seleziona utente','empty' => 'seleziona utente','value'=> 99]);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
             <div class="col-md-3" id="status">
                  <div class="form-group">
                    <?php $statusaction = ['0' => 'aperta', 
                                           '1' => 'chiusa'
                                          ];?>
                          <?php echo $this->Form->control('status' , ['options' => $statusaction, 'label' => 'Stato azione','default' => '0','empty' => 'seleziona azione']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
            </div>
        </fieldset>
     <?= $this->Form->button(__('Salva'), ['class' => 'btn btn-primary']) ?>
    <?= $this->Form->end() ?>
  </div>
</div>

