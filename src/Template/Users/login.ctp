<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="login">
    <!-- Login -->
    <div class="login__block active" id="l-login">
        <div class="login__block__header">
            Devi fare prima login!
          <?= $this->Form->create() ?>
        </div>

        <div class="login__block__body">
            <div class="form-group form-group--float form-group--centered">
                <?= $this->Form->control('email') ?>
                <i class="form-group__bar"></i>
            </div>
        </div>

        <div class="form-group form-group--float form-group--centered">
            <?= $this->Form->control('password') ?>
            <i class="form-group__bar"></i>
        </div>
        <?= $this->Form->button('<i class="zmdi zmdi-long-arrow-right"></i>', ['class' => 'btn btn--icon login__block__btn',
                                                                              'escape' => false]) ?>      
        <?= $this->Form->end() ?>
    </div>
</div>