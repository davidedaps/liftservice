<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>
<div class="card bg-white card-shadow">
    <div class="card-body">
        <h3><?= __('Users') ?></h3>
        <table class="table table-responsive" id="data-table" >
            <thead class="thead-light">
                <tr>
                    <th scope="col"><?= h('Nome') ?></th>
                    <th scope="col"><?= h('Email') ?></th>
                    <th scope="col"><?= h('Ruolo') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($users as $user): ?>
                <?php if($user->id == 99)
                 {continue;}else{?>
                <tr>
                    <td><?= $this->Html->link(__($user->name), ['action' => 'view', $user->id]) ?></td>
                    <td><?= h($user->email) ?></td>
                    <td><?= h($user->role) ?></td>
                </tr>
                <?php }
              endforeach; ?>
            </tbody>
        </table>
    </div>
</div>