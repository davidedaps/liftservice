<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="card bg-white card-shadow">
    <div class="card-body">
        <h3 class="card-title"><?= __('Aggiunti utente') ?></h3>
        <?= $this->Form->create($user) ?>
        <fieldset>
            <?php
                echo $this->Form->control('name', ['label' => 'Nome']);
                echo $this->Form->control('email');
                echo $this->Form->control('password');
                $options = array(
                'admin' => 'Admin',
                'user' => 'User'
                );
                echo "Ruolo";
                echo $this->Form->select('role', $options, array(
                ));
            ?>
        </fieldset>
        <?= $this->Form->button(__('Salva'), ['class' => 'btn btn-primary']) ?>
        <?= $this->Form->end() ?>
    </div>
</div>