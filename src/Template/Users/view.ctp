<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="card bg-white card-shadow">
  <div class="card-body">
    <h3 class="card-title"><?= h($user->name) ?></h3>
    <table class="table vertical-table">
        <tr>
            <th scope="row"><?= __('Nome') ?></th>
            <td><?= h($user->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Email') ?></th>
            <td><?= h($user->email) ?></td>
        </tr>
              <tr>
        <th scope="row"><?= __('Ruolo') ?></th>
            <td><?= h($user->role) ?></td>
        </tr>
     
    </table>
  </div>   
</div>
  <!-- Button trigger modal -->
  <div class="card bg-white card-shadow">
      <?= $this->element('modal_view', [
                // passo la variabile elementVariable all'elemento. 
                "elementId" => $user->id,
                "elementName" => $user->name,
                "modalTitle" => "Elimina utente",
                "modalText" => "Sei sicuro di eliminare questo utente?"
            ]) ?>
  </div>