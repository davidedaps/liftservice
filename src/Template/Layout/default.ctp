<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'WebApps LiftService';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    
    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png">
    <link rel="manifest" href="img/favicon/site.webmanifest">
    <link rel="mask-icon" href="img/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
  
    <!-- Vendor styles -->
    <?= $this->Html->css('/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css') ?>
    <?= $this->Html->css('/vendors/bower_components/animate.css/animate.min.css') ?>
    <?= $this->Html->css('/vendors/bower_components/jquery.scrollbar/jquery.scrollbar.css') ?>
    <?= $this->Html->css('/vendors/bower_components/sweetalert2/dist/sweetalert2.min.css') ?>
    <?= $this->Html->css('/vendors/bower_components/select2/dist/css/select2.min.css') ?>
    <?= $this->Html->css('/vendors/bower_components/nouislider/distribute/nouislider.min.css') ?>
    <?= $this->Html->css('/vendors/bower_components/dropzone/dist/dropzone.css') ?>
    <?= $this->Html->css('/vendors/bower_components/rateYo/min/jquery.rateyo.min.css') ?>
    <?= $this->Html->css('/vendors/bower_components/fullcalendar/dist/fullcalendar.min.css') ?>
    <?= $this->Html->css('/vendors/bower_components/trumbowyg/dist/ui/trumbowyg.min.css') ?>
    <?= $this->Html->css('/vendors/flatpickr/flatpickr.min.css') ?>
  
    <!-- App styles -->
    <?= $this->Html->css('app.min.css') ?>
    <!-- Custom styles -->
    <?= $this->Html->css('https://fonts.googleapis.com/css?family=DM+Sans:400,400i,500,500i,700,700i&display=swap')?>
    <?= $this->Html->css('https://fonts.googleapis.com/css?family=Titillium+Web:200,300,300i,400,400i,700&display=swap')?>
    <?= $this->Html->css('style-liftservice.css') ?>

    <!-- Javascript -->
    <!-- Vendors -->
    <?= $this->Html->script('/vendors/bower_components/jquery/dist/jquery.min.js') ?>
    <?= $this->Html->script('/vendors/bower_components/popper.js/dist/umd/popper.min.js', ['block' => 'scriptBottom']) ?>
    <?= $this->Html->script('/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js', ['block' => 'scriptBottom']) ?>
    <?= $this->Html->script('/vendors/bower_components/jquery.scrollbar/jquery.scrollbar.min.js', ['block' => 'scriptBottom']) ?>
    <?= $this->Html->script('/vendors/bower_components/jquery-scrollLock/jquery-scrollLock.min.js', ['block' => 'scriptBottom']) ?>
    <?= $this->Html->script('/vendors/bower_components/autosize/dist/autosize.min.js', ['block' => 'scriptBottom']) ?>
    <?= $this->Html->script('/vendors/bower_components/select2/dist/js/select2.full.min.js', ['block' => 'scriptBottom']) ?>
    <?= $this->Html->script('/vendors/bower_components/dropzone/dist/min/dropzone.min.js', ['block' => 'scriptBottom']) ?>
    <?= $this->Html->script('/vendors/bower_components/trumbowyg/dist/trumbowyg.min.js', ['block' => 'scriptBottom']) ?>  
    <?= $this->Html->script('/vendors/bower_components/remarkable-bootstrap-notify/dist/bootstrap-notify.min.js', ['block' => 'scriptBottom']) ?>
    <?= $this->Html->script('/vendors/bower_components/sweetalert2/dist/sweetalert2.min.js', ['block' => 'scriptBottom']) ?>
    <?= $this->Html->script('/vendors/flatpickr/flatpickr.min.js', ['block' => 'scriptBottom']) ?>
    <?= $this->Html->script('https://npmcdn.com/flatpickr/dist/l10n/it.js', ['block' => 'scriptBottom']) ?>
    <?= $this->Html->script('https://cdn.datatables.net/plug-ins/1.10.11/sorting/date-eu.js', ['block' => 'scriptBottom']) ?>
    
    <?= $this->Html->script('https://cdn.datatables.net/buttons/3.0.1/js/dataTables.buttons.js', ['block' => 'scriptBottom']) ?>
    <?= $this->Html->script('https://cdn.datatables.net/buttons/3.0.1/js/buttons.dataTables.js', ['block' => 'scriptBottom']) ?>
  
  
        <!-- Vendors: Data tables -->
    <?= $this->Html->script('/vendors/bower_components/datatables.net/js/jquery.dataTables.min.js', ['block' => 'scriptBottom']) ?>
    <?= $this->Html->script('/vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js', ['block' => 'scriptBottom']) ?>
    <?= $this->Html->script('/vendors/bower_components/datatables.net-buttons/js/buttons.print.min.js', ['block' => 'scriptBottom']) ?>
    <?= $this->Html->script('/vendors/bower_components/jszip/dist/jszip.min.js', ['block' => 'scriptBottom']) ?>
    <?= $this->Html->script('/vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js', ['block' => 'scriptBottom']) ?>
  

    <!-- App functions and actions -->
    <?= $this->Html->script('app.min.js', ['block' => 'scriptBottom']) ?>
    <!-- Custom js -->
    <?= $this->Html->script('liftservice.js', ['block' => 'scriptBottom']) ?> 

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body data-ma-theme="teal">
  <main class="main">
            <div class="page-loader">
                <div class="page-loader__spinner">
                    <svg viewBox="25 25 50 50">
                        <circle cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
                    </svg>
                </div>
            </div>
            <?php if ( $logged ) { ?>
            <header class="header">
                <div class="navigation-trigger hidden-xl-up" data-ma-action="aside-open" data-ma-target=".sidebar">
                    <div class="navigation-trigger__inner">
                        <i class="navigation-trigger__line"></i>
                        <i class="navigation-trigger__line"></i>
                        <i class="navigation-trigger__line"></i>
                    </div>
                </div>

                <div class="header__logo hidden-sm-down">
                    <h1><?= $this->Html->link(__('WebApps Lift Service'), ['action' => 'index']) ?></h1>
                </div>
            </header>
              
            <?= $this->element('sidebar') ?>
            
            <section class="content">
              <?= $this->Flash->render() ?>
              <?= $this->fetch('content') ?>
             
              <footer class="footer hidden-xs-down">                
                    <p>© Material Admin Responsive. All rights reserved. CD informatica.</p>
              </footer>
            </section>
    <?php } else { ?>
              <?= $this->Flash->render() ?>
              <?= $this->fetch('content') ?>
             
              <footer class="footer hidden-xs-down">                
                    <p>© Material Admin Responsive. All rights reserved. CD informatica.</p>
              </footer>
    <?php } ?>
  </main>
  <?= $this->fetch('scriptBottom'); ?>
</body>
</html>
