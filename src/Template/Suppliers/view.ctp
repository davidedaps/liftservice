<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Supplier $supplier
 */
?>
<div class="card bg-white card-shadow new-contact">
    <div class="card-body">
     <h3><?= __('Dettaglio fornitore') ?> 
          <?= // link al nuovo preventivo per questo fornitore
              $this->Html->link(__("<i class=\"zmdi zmdi-trending-up zmdi-hc-fw\"></i> Nuovo preventivo"), 
                          ['action' => 'add', 'controller' => 'QuotationsSuppliers', $supplier->id],
                          ['title'=>'new quotation','escape'=>false,'class'=>'btn btn-success btn--raised']) ?>
      </h3>
      <br/> 
            <div class="row">
              <div class="col-md-8">
                      <p class="font-weight-bold">Ragione Sociale</p>
                          <?= h($supplier->name) ?>
              </div>
            </div>
      <br/> 
            <div class="row">
               <div class="col-md-4">
                      <p class="font-weight-bold">Email principale</p>
                          <?= h($supplier->email1) ?>
              </div>
              <div class="col-md-4">
                      <p class="font-weight-bold">Email alternativa</p>
                          <?= h($supplier->email2) ?>
              </div>
              <div class="col-md-4">
                      <p class="font-weight-bold">Email alternativa</p>
                          <?= h($supplier->email3) ?>
              </div>
            </div>
      <br/> 
            <div class="row">
               <div class="col-md-4">
                      <p class="font-weight-bold">Cellulare princiale</p>
                          <?= h($supplier->cell1) ?>
              </div>
              <div class="col-md-4">
                      <p class="font-weight-bold">Cellulare alternativo</p>
                          <?= h($supplier->cell2) ?>
              </div>
              <div class="col-md-4">
                      <p class="font-weight-bold">Cellulare alternativo</p>
                          <?= h($supplier->cell3) ?>
              </div>
            </div>
      <br/> 
            <div class="row">
             <div class="col-md-4">
                      <p class="font-weight-bold">Telefono</p>
                          <?= h($supplier->tel1) ?>
              </div>
              <div class="col-md-4">
                      <p class="font-weight-bold">Telefono alternativo</p>
                          <?= h($supplier->tel2) ?>
              </div>
              <div class="col-md-4">
                      <p class="font-weight-bold">Telefono alternativo</p>
                          <?= h($supplier->tel3) ?>
              </div>
            </div>
      <br/> 
            <div class="row">
              <div class="col-md-6">
                      <p class="font-weight-bold">Contatto principale</p>
                          <?= h($supplier->contact1) ?>
              </div>
              <div class="col-md-6">
                      <p class="font-weight-bold">Contatto alternativo</p>
                          <?= h($supplier->contact2) ?>
              </div>
            </div>
      <br/> 
            <div class="row">
              <div class="col-md-6">
                      <p class="font-weight-bold">Partita IVA</p>
                          <?= h($supplier->vat_number) ?>
              </div>
              <div class="col-md-6">
                      <p class="font-weight-bold">Codice Fiscale</p>
                          <?= h($supplier->fiscal_code) ?>
              </div>
            </div>
      <br/> 
            <div class="row">
              <div class="col-md-6">
                      <p class="font-weight-bold">SDI</p>
                          <?= h($supplier->sdi_code) ?>
              </div>
              <div class="col-md-6">
                      <p class="font-weight-bold">Email PEC</p>
                          <?= h($supplier->pec) ?>
              </div> 
            </div>
      <br/> 
            <div class="row">
              <div class="col">
                      <p class="font-weight-bold">Note</p>
                          <?= h($supplier->note) ?>
              </div>
            </div>
      <br/> 
  </div>
</div>
<!-- Button trigger modal -->
    <!-- Button trigger modal -->
<div class="card bg-white card-shadow">
    <?= $this->element('modal_view', [
              // passo la variabile elementVariable all'elemento. 
              "elementId" => $supplier->id,
              "elementName" => $supplier->name,
              "modalTitle" => "Elimina fornitore",
              "modalText" => "Sei sicuro di eliminare questo fornitore?"
          ]) ?>
</div>
<div class="row">
  <div class="card bg-white card-shadow col-md-7">
      <div class="card-body">
        <h3><?= __('Dettaglio preventivi cliente') ?></h3>
             <table class="table table-responsive">
          <thead class="thead-light">
              <tr>
                <th scope="col"><?= __('Numero preventivo') ?></th>
                <th scope="col"><?= __('Data') ?></th>
                <th scope="col"><?= __('Riferimento') ?></th>
                <th scope="col"><?= __('Prezzo') ?></th>
              </tr>
          </thead>
            <tbody>
  <?php
    foreach ($quotationSupplierList as $quotationSupplier): ?>

          <tr>         
            <td><?= h($quotationSupplier->offer_number) ?></td>
            <td><?= h($quotationSupplier->date) ?></td>
            <td><?= $this->Html->link(__($quotationSupplier->description), ['action' => 'view', 'controller' => 'QuotationsSuppliers', $quotationSupplier->id]) ?></td>
            <td><?= h($quotationSupplier->price) ?></td>
          </tr> 

  <?php endforeach; ?>
                </tbody>
    </table>
      </div>
  </div>

  <div class="card bg-white card-shadow col-md-4">
<!--       <div class="card-body">
          <h3><?= __('Elenco uploads') ?></h3>
          <table class="table table-responsive" >
              <thead class="thead-light">
          <tr>
            <th scope="row"><?= __('Nome') ?></th>
            <th scope="row"><?= __('Scarica') ?></th>
          </tr>
            </thead>
              <tbody>
              <?php
                foreach ($uploads as $upload): ?>
                  <?php $imagepath = '/files/uploads/filename/' . $upload->dir; ?>
                      <tr>         
                        <td><?= h($upload->filename) ?></td>
                        <td><a href="<?= $imagepath . '/' . $upload->filename ?>" target="_blank"><i class="zmdi zmdi-download zmdi-hc-fw"></i></a></td>
                      </tr> 

              <?php endforeach; ?>
              </tbody>
          </table>
    </div> -->
  </div>
</div>
