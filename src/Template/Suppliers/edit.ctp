<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Supplier $supplier
 */
?>
<div class="card bg-white card-shadow new-contact">
    <div class="card-body">
         <h3><?= __('Aggiungi fornitore') ?></h3>
          <br/>
          <?= $this->Form->create($supplier) ?>
          <fieldset>
            <div class="row">
              <div class="col-md-12">
                  <div class="form-group">
                          <?php echo $this->Form->control('name' , ['label' => 'Nome']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
               <div class="col-md-4">
                  <div class="form-group">
                          <?php echo $this->Form->control('email1' , ['label' => 'Email']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="form-group">
                          <?php echo $this->Form->control('email2' , ['label' => 'Email alt.']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="form-group">
                          <?php echo $this->Form->control('email3' , ['label' => 'Email alt.']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
               <div class="col-md-4">
                  <div class="form-group">
                          <?php echo $this->Form->control('cell1' , ['label' => 'Mobile']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="form-group">
                          <?php echo $this->Form->control('cell2' , ['label' => 'Mobile alt.']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="form-group">
                          <?php echo $this->Form->control('cell3' , ['label' => 'Mobile alt.']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
             <div class="col-md-4">
                  <div class="form-group">
                          <?php echo $this->Form->control('tel1' , ['label' => 'Tel.']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="form-group">
                          <?php echo $this->Form->control('tel2' , ['label' => 'Tel alt.']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="form-group">
                          <?php echo $this->Form->control('tel3' , ['label' => 'Tel alt.']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="form-group">
                          <?php echo $this->Form->control('contact1' , ['label' => 'Contatto rif.']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="form-group">
                          <?php echo $this->Form->control('contact2' , ['label' => 'Contatto alt.']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="form-group">
                          <?php echo $this->Form->control('vat_number' , ['label' => 'Partita iva']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="form-group">
                          <?php echo $this->Form->control('fiscal_code' , ['label' => 'Codice fiscale']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div> 
              <div class="col-md-6">
                  <div class="form-group">
                          <?php echo $this->Form->control('sdi_code' , ['label' => 'Codice SDI']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="form-group">
                          <?php echo $this->Form->control('pec' , ['label' => 'Pec']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div> 

              <?php        
                  //echo $this->Form->control('quotations._ids', ['label' => 'Preventivi'], ['options' => $quotations]);
              ?>
            </div>
        </fieldset>
     <?= $this->Form->button(__('Salva'), ['class' => 'btn btn-primary']) ?>
    <?= $this->Form->end() ?>
  </div>
</div>
