<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Supplier[]|\Cake\Collection\CollectionInterface $suppliers
 */
?>
<div class="card bg-white card-shadow">
    <div class="card-body">
        <div class="responsive" style="float:left;">
          <h3><?= __('Elenco fornitori') ?></h3>
        </div>
         <div class="responsive" style="float:left;padding-left:30px;">
              <?= // link al nuovo preventivo per questo cliente
              $this->Html->link(__("<i class=\"zmdi zmdi-account-add zmdi-hc-fw\"></i> Nuovo fornitore"), 
                          ['action' => 'add', 'controller' => 'Suppliers'],
                          ['title'=>'new supplier','escape'=>false,'class'=>'btn btn-info btn--raised']) ?>
          </div>
        <div style="clear:both"></div>
          <br/>
            <table class="table table-responsive" id="data-table" >
            <thead class="thead-light">
            <tr>
                <th scope="col"><?= h('Ragione sociale') ?></th>
                <th scope="col"><?= h('Email')?></th>
                <th scope="col"><?= h('Mobile') ?></th>
                <th scope="col"><?= h('Tel') ?></th>
                <th scope="col"><?= h('Contatto di rif.') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($suppliers as $supplier): ?>
            <tr>
                <td><?= $this->Html->link(__($supplier->name), ['action' => 'view', $supplier->id]) ?></td>
                <td><?= h($supplier->email1) ?></td>
                <td><?= h($supplier->cell1) ?></td>
                <td><?= h($supplier->tel1) ?></td>
                <td><?= h($supplier->contact1) ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
</div>