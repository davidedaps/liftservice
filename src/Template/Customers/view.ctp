<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Supplier $customer
 */
?>
<div class="card bg-white card-shadow new-contact">
    <div class="card-body">
     <h3><?= __('Dettaglio contatti') ?> 
          <?= // link al nuovo preventivo per questo cliente
              $this->Html->link(__("<i class=\"zmdi zmdi-trending-up zmdi-hc-fw\"></i> Nuovo preventivo"), 
                          ['action' => 'add', 'controller' => 'Quotations', $customer->id],
                          ['title'=>'new quotation','escape'=>false,'class'=>'btn btn-success btn--raised']) ?>
      </h3>
      <br/> 
            <div class="row">
              <div class="col-md-8">
                      <p class="font-weight-bold">Ragione Sociale</p>
                          <?= h($customer->name) ?>
              </div>
              <div class="col-md-4">
                      <p class="font-weight-bold">Provenienza</p>
                          <?= h($customer->source) ?>
              </div>
            </div>
      <br/> 
            <div class="row">
               <div class="col-md-4">
                      <p class="font-weight-bold">Email principale</p>
                          <?= h($customer->email1) ?>
              </div>
              <div class="col-md-4">
                      <p class="font-weight-bold">Email alternativa</p>
                          <?= h($customer->email2) ?>
              </div>
              <div class="col-md-4">
                      <p class="font-weight-bold">Email alternativa</p>
                          <?= h($customer->email3) ?>
              </div>
            </div>
      <br/> 
            <div class="row">
               <div class="col-md-4">
                      <p class="font-weight-bold">Cellulare princiale</p>
                          <?= h($customer->cell1) ?>
              </div>
              <div class="col-md-4">
                      <p class="font-weight-bold">Cellulare alternativo</p>
                          <?= h($customer->cell2) ?>
              </div>
              <div class="col-md-4">
                      <p class="font-weight-bold">Cellulare alternativo</p>
                          <?= h($customer->cell3) ?>
              </div>
            </div>
      <br/> 
            <div class="row">
             <div class="col-md-4">
                      <p class="font-weight-bold">Telefono</p>
                          <?= h($customer->tel1) ?>
              </div>
              <div class="col-md-4">
                      <p class="font-weight-bold">Telefono alternativo</p>
                          <?= h($customer->tel2) ?>
              </div>
              <div class="col-md-4">
                      <p class="font-weight-bold">Telefono alternativo</p>
                          <?= h($customer->tel3) ?>
              </div>
            </div>
      <br/> 
            <div class="row">
              <div class="col-md-6">
                      <p class="font-weight-bold">Contatto principale</p>
                          <?= h($customer->contact1) ?>
              </div>
              <div class="col-md-6">
                      <p class="font-weight-bold">Contatto alternativo</p>
                          <?= h($customer->contact2) ?>
              </div>
            </div>
      <br/> 
            <div class="row">
              <div class="col-md-6">
                      <p class="font-weight-bold">Partita IVA</p>
                          <?= h($customer->vat_number) ?>
              </div>
              <div class="col-md-6">
                      <p class="font-weight-bold">Codice Fiscale</p>
                          <?= h($customer->fiscal_code) ?>
              </div>
            </div>
      <br/> 
            <div class="row">
              <div class="col-md-6">
                      <p class="font-weight-bold">SDI</p>
                          <?= h($customer->sdi_code) ?>
              </div>
              <div class="col-md-6">
                      <p class="font-weight-bold">Email PEC</p>
                          <?= h($customer->pec) ?>
              </div> 
            </div>
      <br/> 
            <div class="row">
              <div class="col">
                      <p class="font-weight-bold">Note</p>
                          <?= h($customer->note) ?>
              </div>
            </div>
      <br/> 
  </div>
</div>
<!-- Button trigger modal -->
<div class="card bg-white card-shadow">
    <?= $this->element('modal_view', [
              // passo la variabile elementVariable all'elemento. 
              "elementId" => $customer->id,
              "elementName" => $customer->name,
              "modalTitle" => "Elimina cliente",
              "modalText" => "Sei sicuro di eliminare questo cliente?"
          ]) ?>
</div>

<div class="row">
  <div class="card bg-white card-shadow col-md-7">
      <div class="card-body">
        <h3><?= __('Dettaglio preventivi cliente') ?></h3>
             <table class="table table-responsive">
          <thead class="thead-light">
              <tr>
                <th scope="col"><?= __('Numero preventivo') ?></th>
                <th scope="col"><?= __('Data') ?></th>
                <th scope="col"><?= __('Riferimento') ?></th>
                <th scope="col"><?= __('Note di bilancio') ?></th>
              </tr>
          </thead>
            <tbody>
  <?php

    foreach ($quotationList as $quotation): ?>

          <tr>         
            <td><?= h($quotation->quotation_number) ?></td>
            <td><?= h($quotation->contact_date) ?></td>
            <td><?= $this->Html->link(__($quotation->reference), ['action' => 'view', 'controller' => 'Quotations', $quotation->id]) ?></td>
            <td><?= h($quotation->balance_note) ?></td>
          </tr> 

  <?php endforeach; ?>
                </tbody>
    </table>
      </div>
  </div>

  <div class="card bg-white card-shadow col-md-4">
      <div class="card-body">
          <h3><?= __('Elenco uploads') ?></h3>
          <table class="table table-responsive" >
              <thead class="thead-light">
          <tr>
            <th scope="row"><?= __('Nome') ?></th>
            <th scope="row"><?= __('Scarica') ?></th>
          </tr>
            </thead>
              <tbody>
              <?php
                foreach ($uploads as $upload): ?>
                  <?php $imagepath = '/files/uploads/filename/' . $upload->dir; ?>
                      <tr>         
                        <td><?= h($upload->filename) ?></td>
                        <td><a href="<?= $imagepath . '/' . $upload->filename ?>" target="_blank"><i class="zmdi zmdi-download zmdi-hc-fw"></i></a></td>
                      </tr> 

              <?php endforeach; ?>
              </tbody>
          </table>
    </div>
  </div>
</div>

    
