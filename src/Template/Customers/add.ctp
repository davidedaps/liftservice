<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Supplier $supplier
 */
?>
<div class="card bg-white card-shadow new-contact">
    <div class="card-body">
         <h3><?= __('Aggiungi contatto') ?></h3>
          <br/>
          <?= $this->Form->create($customer,['type' => 'file']) ?>
          <fieldset>
            <div class="row">
              <div class="col-md-8">
                  <div class="form-group">
                          <?php echo $this->Form->control('name' , ['label' => 'Nome', 'class' => 'is-valid']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="form-group">
                    <label>Indica la provenienza</label>  
                          <?php echo $this->Form->select('source', $sourceQueryList, ['label' => 'Provenienza contatto','empty' => 'indica la provenienza']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
            </div>
            <div class="row">
               <div class="col-md-4">
                  <div class="form-group">
                          <?php echo $this->Form->control('email1' , ['label' => 'Email', 'class' => 'is-valid']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="form-group">
                          <?php echo $this->Form->control('email2' , ['label' => 'Email alt.']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="form-group">
                          <?php echo $this->Form->control('email3' , ['label' => 'Email alt.']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
            </div>
            <div class="row">
               <div class="col-md-4">
                  <div class="form-group">
                          <?php echo $this->Form->control('cell1' , ['label' => 'Mobile']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="form-group">
                          <?php echo $this->Form->control('cell2' , ['label' => 'Mobile alt.']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="form-group">
                          <?php echo $this->Form->control('cell3' , ['label' => 'Mobile alt.']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
            </div>
            <div class="row">
             <div class="col-md-4">
                  <div class="form-group">
                          <?php echo $this->Form->control('tel1' , ['label' => 'Tel.']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="form-group">
                          <?php echo $this->Form->control('tel2' , ['label' => 'Tel alt.']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="form-group">
                          <?php echo $this->Form->control('tel3' , ['label' => 'Tel alt.']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                  <div class="form-group">
                          <?php echo $this->Form->control('contact1' , ['label' => 'Contatto rif.']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="form-group">
                          <?php echo $this->Form->control('contact2' , ['label' => 'Contatto alt.']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                  <div class="form-group">
                          <?php echo $this->Form->control('vat_number' , ['label' => 'Partita iva']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="form-group">
                          <?php echo $this->Form->control('fiscal_code' , ['label' => 'Codice fiscale']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                  <div class="form-group">
                          <?php echo $this->Form->control('sdi_code' , ['label' => 'Codice SDI']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="form-group">
                          <?php echo $this->Form->control('pec' , ['label' => 'Pec']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div> 
            </div>
            <div class="row">
              <div class="col">
                  <div class="form-group">
                          <?php echo $this->Form->control('note' , ['label' => 'Note']);?>
                      <i class="form-group__bar"></i>
                  </div>
              </div>
            </div>
        </fieldset>
     <?= $this->Form->button(__('Salva'), ['class' => 'btn btn-primary']) ?>
    <?= $this->Form->end() ?>
  </div>
</div>
