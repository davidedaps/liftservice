<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Customer[]|\Cake\Collection\CollectionInterface $customers
 */
?>
<div class="card bg-white card-shadow">
    <div class="card-body">
        <div class="responsive" style="float:left;">
          <h3><?= __('Elenco clienti') ?></h3>
        </div>
        <div style="clear:both"></div>
          <br/>
            <table class="table table-responsive" id="data-table" >
            <thead class="thead-light">
            <tr>
                <th scope="col"><?= h('Ragione sociale') ?></th>
                <th scope="col"><?= h('Email')?></th>
                <th scope="col"><?= h('Mobile') ?></th>
                <th scope="col"><?= h('Tel') ?></th>
                <th scope="col"><?= h('Contatto di rif.') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($customers as $customer): ?>
            <tr>
                <td><?= $this->Html->link(__($customer->name), ['action' => 'view', $customer->id]) ?></td>
                <td><?= h($customer->email1) ?></td>
                <td><?= h($customer->cell1) ?></td>
                <td><?= h($customer->tel1) ?></td>
                <td><?= h($customer->contact1) ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
</div>
