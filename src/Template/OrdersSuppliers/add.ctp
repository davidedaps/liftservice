<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\OrdersSupplier $ordersSupplier
 */
?>
<div class="card bg-white card-shadow new-contact">
    <div class="card-body">
    <?= $this->Form->create($ordersSupplier) ?>
    <fieldset>
        <h3><?= __('Aggiungi ordine fornitore') ?></h3>
          <div class="row">
              <div class="col-md-8">
                  <div class="form-group"> 
                    <?php  echo $this->Form->control('confirmation_date',['label'=>'Data ordine fornitore']); ?>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="form-group"> 
                    <?php  echo $this->Form->control('order_number',['label'=>'Numero ordine fornitore']); ?>
                  </div>
              </div>
          </div>
      <div class="row">
              <div class="col-md-8">
                 <div class="form-group"> 
                    <?php echo $this->Form->control('payment_type',['label'=>'Tipo pagamento']); ?>
                 </div>
              </div>
              <div class="col-md-4">
                 <div class="form-group"> 
                    <?php echo $this->Form->control('order_amount',['label'=>'Totale ordine fornitore']); ?>
                 </div>
              </div><?php //pr($orderCustomerList); ?>
        <?php echo $this->Form->control('quotation_id',['value' => $quotationsToAdd->quotation_id,'type' => 'hidden']);?>  
        
         <?php echo $this->Form->control('quotation_supplier_id', ['value' => $quotationsToAdd->id,'type' => 'hidden']); ?>
        <?php
               //pr($orderCustomerList);
        ?>
        <?php echo $this->Form->control('orders_customer_id', ['value' => $orderCustomerList->id,'type' => 'hidden']); ?>
    </fieldset>
    <?= $this->Form->button(__('Salva')) ?>
    <?= $this->Form->end() ?>
</div>
</div>
