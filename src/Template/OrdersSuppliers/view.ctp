<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\OrdersSupplier $ordersSupplier
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Orders Supplier'), ['action' => 'edit', $ordersSupplier->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Orders Supplier'), ['action' => 'delete', $ordersSupplier->id], ['confirm' => __('Are you sure you want to delete # {0}?', $ordersSupplier->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Orders Suppliers'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Orders Supplier'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Quotations'), ['controller' => 'Quotations', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Quotation'), ['controller' => 'Quotations', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="ordersSuppliers view large-9 medium-8 columns content">
    <h3><?= h($ordersSupplier->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Quotation') ?></th>
            <td><?= $ordersSupplier->has('quotation') ? $this->Html->link($ordersSupplier->quotation->ID, ['controller' => 'Quotations', 'action' => 'view', $ordersSupplier->quotation->ID]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($ordersSupplier->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Payment Type') ?></th>
            <td><?= $this->Number->format($ordersSupplier->payment_type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Order Amount') ?></th>
            <td><?= $this->Number->format($ordersSupplier->order_amount) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Confirmation Date') ?></th>
            <td><?= h($ordersSupplier->confirmation_date) ?></td>
        </tr>
    </table>
</div>
