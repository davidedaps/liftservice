<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\OrdersSupplier $ordersSupplier
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $ordersSupplier->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $ordersSupplier->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Orders Suppliers'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Quotations'), ['controller' => 'Quotations', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Quotation'), ['controller' => 'Quotations', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="ordersSuppliers form large-9 medium-8 columns content">
    <?= $this->Form->create($ordersSupplier) ?>
    <fieldset>
        <legend><?= __('Edit Orders Supplier') ?></legend>
        <?php
            echo $this->Form->control('quotation_id', ['options' => $quotations]);
            echo $this->Form->control('confirmation_date');
            echo $this->Form->control('payment_type');
            echo $this->Form->control('order_amount');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
