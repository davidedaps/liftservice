<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Customer[]|\Cake\Collection\CollectionInterface $customers
 */
?>
<div class="card bg-white card-shadow">
    <div class="card-body">
        <h3><?= __('Elenco ordini fornitori') ?></h3>
            <table class="table table-responsive" id="data-table" >
            <thead class="thead-light">
            <tr>
                <th scope="col"><?= h('Riferimento ordine') ?></th>
                <th scope="col"><?= h('Fornitore') ?></th>
                <th scope="col"><?= h('Importo') ?></th>              
                <th scope="col"><?= h('Data ordine')?></th>
                <th scope="col"><?= h('Modalità di pagamento') ?></th>
            </tr>
        </thead>
        <tbody>
          
            <?php foreach ($ordersSuppliers as $orderSupplier): ?>
            <tr>
                <td><?= $this->Html->link(__($orderSupplier->quotations_supplier->description), ['action' => 'view', $orderSupplier->quotations_supplier->id,'controller'=>'QuotationsSuppliers']) ?></td>
                <td><?= $this->Html->link(__($orderSupplier->quotations_supplier->supplier->name), ['action' => 'view', $orderSupplier->quotations_supplier->supplier->id,'controller'=>'Suppliers']) ?></td>
                <td><?= h($this->Number->currency($orderSupplier->order_amount, "EUR")) ?></td>
                <td><?= h($orderSupplier->confirmation_date) ?></td>
                <td><?= h($orderSupplier->payment_type) ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
</div>
