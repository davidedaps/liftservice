<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\OrderPhase2 $orderPhase2
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Order Phase2'), ['action' => 'edit', $orderPhase2->order_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Order Phase2'), ['action' => 'delete', $orderPhase2->order_id], ['confirm' => __('Are you sure you want to delete # {0}?', $orderPhase2->order_id)]) ?> </li>
        <li><?= $this->Html->link(__('List Order Phase2'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Order Phase2'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="orderPhase2 view large-9 medium-8 columns content">
    <h3><?= h($orderPhase2->order_id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Description Step 1') ?></th>
            <td><?= h($orderPhase2->description_step_1) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Description Step 2') ?></th>
            <td><?= h($orderPhase2->description_step_2) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Description Step 3') ?></th>
            <td><?= h($orderPhase2->description_step_3) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Description Step 4') ?></th>
            <td><?= h($orderPhase2->description_step_4) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Description Step 5') ?></th>
            <td><?= h($orderPhase2->description_step_5) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Description Step 6') ?></th>
            <td><?= h($orderPhase2->description_step_6) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Description Step 7') ?></th>
            <td><?= h($orderPhase2->description_step_7) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Description Step 8') ?></th>
            <td><?= h($orderPhase2->description_step_8) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Description Step 9') ?></th>
            <td><?= h($orderPhase2->description_step_9) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Order Id') ?></th>
            <td><?= $this->Number->format($orderPhase2->order_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Phase Completed') ?></th>
            <td><?= $this->Number->format($orderPhase2->phase_completed) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Step 1 Completed') ?></th>
            <td><?= $this->Number->format($orderPhase2->step_1_completed) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Step 2 Completed') ?></th>
            <td><?= $this->Number->format($orderPhase2->step_2_completed) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Step 3 Completed') ?></th>
            <td><?= $this->Number->format($orderPhase2->step_3_completed) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Step 4 Completed') ?></th>
            <td><?= $this->Number->format($orderPhase2->step_4_completed) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Completed Step 5') ?></th>
            <td><?= $this->Number->format($orderPhase2->completed_step_5) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Step 5 Completedtime') ?></th>
            <td><?= $this->Number->format($orderPhase2->step_5_completedtime) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Completed Step 6') ?></th>
            <td><?= $this->Number->format($orderPhase2->completed_step_6) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Step 6 Completedtime') ?></th>
            <td><?= $this->Number->format($orderPhase2->step_6_completedtime) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Completed Step 7') ?></th>
            <td><?= $this->Number->format($orderPhase2->completed_step_7) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Step 7 Completedtime') ?></th>
            <td><?= $this->Number->format($orderPhase2->step_7_completedtime) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Completed Step 8') ?></th>
            <td><?= $this->Number->format($orderPhase2->completed_step_8) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Step 8 Completedtime') ?></th>
            <td><?= $this->Number->format($orderPhase2->step_8_completedtime) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Completed Step 9') ?></th>
            <td><?= $this->Number->format($orderPhase2->completed_step_9) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Step 9 Completedtime') ?></th>
            <td><?= $this->Number->format($orderPhase2->step_9_completedtime) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Step 1 Created') ?></th>
            <td><?= h($orderPhase2->step_1_created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Step 1 Completedtime') ?></th>
            <td><?= h($orderPhase2->step_1_completedtime) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Step 2 Created') ?></th>
            <td><?= h($orderPhase2->step_2_created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Step 2 Completedtime') ?></th>
            <td><?= h($orderPhase2->step_2_completedtime) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Step 3 Created') ?></th>
            <td><?= h($orderPhase2->step_3_created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Step 3 Completedtime') ?></th>
            <td><?= h($orderPhase2->step_3_completedtime) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Step 4 Created') ?></th>
            <td><?= h($orderPhase2->step_4_created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Step 4 Completedtime') ?></th>
            <td><?= h($orderPhase2->step_4_completedtime) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Step 5 Created') ?></th>
            <td><?= h($orderPhase2->step_5_created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Step 6 Created') ?></th>
            <td><?= h($orderPhase2->step_6_created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Step 7 Created') ?></th>
            <td><?= h($orderPhase2->step_7_created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Step 8 Created') ?></th>
            <td><?= h($orderPhase2->step_8_created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Step 9 Created') ?></th>
            <td><?= h($orderPhase2->step_9_created) ?></td>
        </tr>
    </table>
</div>
