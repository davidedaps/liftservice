<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\OrderPhase2[]|\Cake\Collection\CollectionInterface $orderPhase2
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Order Phase2'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="orderPhase2 index large-9 medium-8 columns content">
    <h3><?= __('Order Phase2') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('order_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('phase_completed') ?></th>
                <th scope="col"><?= $this->Paginator->sort('description_step_1') ?></th>
                <th scope="col"><?= $this->Paginator->sort('step_1_created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('step_1_completed') ?></th>
                <th scope="col"><?= $this->Paginator->sort('step_1_completedtime') ?></th>
                <th scope="col"><?= $this->Paginator->sort('description_step_2') ?></th>
                <th scope="col"><?= $this->Paginator->sort('step_2_created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('step_2_completed') ?></th>
                <th scope="col"><?= $this->Paginator->sort('step_2_completedtime') ?></th>
                <th scope="col"><?= $this->Paginator->sort('description_step_3') ?></th>
                <th scope="col"><?= $this->Paginator->sort('step_3_created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('step_3_completed') ?></th>
                <th scope="col"><?= $this->Paginator->sort('step_3_completedtime') ?></th>
                <th scope="col"><?= $this->Paginator->sort('description_step_4') ?></th>
                <th scope="col"><?= $this->Paginator->sort('step_4_created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('step_4_completed') ?></th>
                <th scope="col"><?= $this->Paginator->sort('step_4_completedtime') ?></th>
                <th scope="col"><?= $this->Paginator->sort('description_step_5') ?></th>
                <th scope="col"><?= $this->Paginator->sort('step_5_created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('completed_step_5') ?></th>
                <th scope="col"><?= $this->Paginator->sort('step_5_completedtime') ?></th>
                <th scope="col"><?= $this->Paginator->sort('description_step_6') ?></th>
                <th scope="col"><?= $this->Paginator->sort('step_6_created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('completed_step_6') ?></th>
                <th scope="col"><?= $this->Paginator->sort('step_6_completedtime') ?></th>
                <th scope="col"><?= $this->Paginator->sort('description_step_7') ?></th>
                <th scope="col"><?= $this->Paginator->sort('step_7_created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('completed_step_7') ?></th>
                <th scope="col"><?= $this->Paginator->sort('step_7_completedtime') ?></th>
                <th scope="col"><?= $this->Paginator->sort('description_step_8') ?></th>
                <th scope="col"><?= $this->Paginator->sort('step_8_created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('completed_step_8') ?></th>
                <th scope="col"><?= $this->Paginator->sort('step_8_completedtime') ?></th>
                <th scope="col"><?= $this->Paginator->sort('description_step_9') ?></th>
                <th scope="col"><?= $this->Paginator->sort('step_9_created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('completed_step_9') ?></th>
                <th scope="col"><?= $this->Paginator->sort('step_9_completedtime') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($orderPhase2 as $orderPhase2): ?>
            <tr>
                <td><?= $this->Number->format($orderPhase2->order_id) ?></td>
                <td><?= $this->Number->format($orderPhase2->phase_completed) ?></td>
                <td><?= h($orderPhase2->description_step_1) ?></td>
                <td><?= h($orderPhase2->step_1_created) ?></td>
                <td><?= $this->Number->format($orderPhase2->step_1_completed) ?></td>
                <td><?= h($orderPhase2->step_1_completedtime) ?></td>
                <td><?= h($orderPhase2->description_step_2) ?></td>
                <td><?= h($orderPhase2->step_2_created) ?></td>
                <td><?= $this->Number->format($orderPhase2->step_2_completed) ?></td>
                <td><?= h($orderPhase2->step_2_completedtime) ?></td>
                <td><?= h($orderPhase2->description_step_3) ?></td>
                <td><?= h($orderPhase2->step_3_created) ?></td>
                <td><?= $this->Number->format($orderPhase2->step_3_completed) ?></td>
                <td><?= h($orderPhase2->step_3_completedtime) ?></td>
                <td><?= h($orderPhase2->description_step_4) ?></td>
                <td><?= h($orderPhase2->step_4_created) ?></td>
                <td><?= $this->Number->format($orderPhase2->step_4_completed) ?></td>
                <td><?= h($orderPhase2->step_4_completedtime) ?></td>
                <td><?= h($orderPhase2->description_step_5) ?></td>
                <td><?= h($orderPhase2->step_5_created) ?></td>
                <td><?= $this->Number->format($orderPhase2->completed_step_5) ?></td>
                <td><?= $this->Number->format($orderPhase2->step_5_completedtime) ?></td>
                <td><?= h($orderPhase2->description_step_6) ?></td>
                <td><?= h($orderPhase2->step_6_created) ?></td>
                <td><?= $this->Number->format($orderPhase2->completed_step_6) ?></td>
                <td><?= $this->Number->format($orderPhase2->step_6_completedtime) ?></td>
                <td><?= h($orderPhase2->description_step_7) ?></td>
                <td><?= h($orderPhase2->step_7_created) ?></td>
                <td><?= $this->Number->format($orderPhase2->completed_step_7) ?></td>
                <td><?= $this->Number->format($orderPhase2->step_7_completedtime) ?></td>
                <td><?= h($orderPhase2->description_step_8) ?></td>
                <td><?= h($orderPhase2->step_8_created) ?></td>
                <td><?= $this->Number->format($orderPhase2->completed_step_8) ?></td>
                <td><?= $this->Number->format($orderPhase2->step_8_completedtime) ?></td>
                <td><?= h($orderPhase2->description_step_9) ?></td>
                <td><?= h($orderPhase2->step_9_created) ?></td>
                <td><?= $this->Number->format($orderPhase2->completed_step_9) ?></td>
                <td><?= $this->Number->format($orderPhase2->step_9_completedtime) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $orderPhase2->order_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $orderPhase2->order_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $orderPhase2->order_id], ['confirm' => __('Are you sure you want to delete # {0}?', $orderPhase2->order_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
