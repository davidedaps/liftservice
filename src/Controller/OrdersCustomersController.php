<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Controller\QuotationsController;
use App\Controller\CustomersController;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\Filesystem\File;
use Cake\Http\Response;
use Cake\I18n\Time;
use Cake\View\ViewBuilder;
/**
 * OrdersCustomers Controller
 *
 * @property \App\Model\Table\OrdersCustomersTable $OrdersCustomers
 *
 * @method \App\Model\Entity\OrdersCustomer[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class OrdersCustomersController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {      
        // In a controller or table method.
        $ordersCustomers = $this->OrdersCustomers->find('all')
        ->where(['OrdersCustomers.archived ' =>(0)])
        ->contain(['Customers', 'Quotations'])
        ->limit(100);

        $ordersCustomers = $ordersCustomers->toArray();
        $this->set(compact('ordersCustomers'));
        
    }

      public function archived()
    {      
        /*$ordersCustomers = $this->OrdersCustomers->find('all'),[
    'conditions' => ['archived =' =>(1)],
    'contain' => ['Customers', 'Quotations']
      ];*/
        
        // In a controller or table method.
        $ordersCustomers = $this->OrdersCustomers->find('all')
        ->where(['OrdersCustomers.archived =' =>(1)])
        ->contain(['Customers', 'Quotations'])
        ->limit(100);

        $ordersCustomers = $ordersCustomers->toArray();
        $this->set(compact('ordersCustomers'));

        
    }
  
   public function expecteddate()
    {            
        // In a controller or table method.
        $ordersCustomers = $this->OrdersCustomers->find('all')
        ->where(['OrdersCustomers.archived ' =>(0)])  
        ->order(['OrdersCustomers.expected_delivery_date' => 'DESC'])
        ->contain(['Customers', 'Quotations'])
        ->limit(100);

        $ordersCustomers = $ordersCustomers->toArray();
        $this->set(compact('ordersCustomers'));

    }
  
  public function ics($id = NULL)
    {
        // Imposta l'intestazione del file ICS
        $this->response = $this->response
            ->withType('text/calendar')
            ->withDownload('appointments.ics');

        // Recupera gli appuntamenti dal database o da qualsiasi altra sorgente dati
        //$appointments = $this->Appointments->find('all');
        $appuntamento = $this->loadModel('OrdersCustomers')->get($id, [
        'contain' => ['Customers']
        ]);
    
        $dataInizio = date('Ymd\THis', strtotime($appuntamento->expected_delivery_date));
        $dataFine = date('Ymd\THis', strtotime($appuntamento->expected_delivery_date)+8);
        
//         echo $dataInizio;
//         echo "<br/>";
//         echo $dataFine;
        //exit();
        // Costruisci il contenuto del file ICS
        $icsContent = "BEGIN:VCALENDAR\r\n";
        //foreach ($appointments as $appointment) {
            $icsContent .= "BEGIN:VEVENT\r\n";
            $icsContent .= "DTSTART:" . date('Ymd\THis', strtotime($dataInizio)) . "\r\n";
            $icsContent .= "DTEND:" . date('Ymd\THis', strtotime($dataFine)) . "\r\n";
            $icsContent .= "LOCATION:" . $appuntamento->location . "\r\n";
            $icsContent .= "SUMMARY:" . $appuntamento->order_number." - ".$appuntamento->customer->name. "\r\n";
            $icsContent .= "END:VEVENT\r\n";
        //}
        $icsContent .= "END:VCALENDAR\r\n";

        // Imposta il contenuto del file ICS nella risposta
        $this->response->getBody()->write($icsContent);

        return $this->response;
    }

    /**
     * View method
     *
     * @param string|null $id Orders Customer id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
//         $ordersCustomer = $this->OrdersCustomers->get($id, [
//             'contain' => ['Quotations', 'Lifts']
//         ]);
        $ordersCustomer = $this->OrdersCustomers->get($id, [
        'contain' => ['Customers','Quotations']
        ]);


        $this->set('ordersCustomer', $ordersCustomer);

    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($id = null)
    {
    
      
        //FR - aggiungo la trasformazione da preventivo a ordine
        $quotationsToAdd = TableRegistry::get('Quotations');
        //$quotationsToAdd = $quotationsToAdd->get($id);

        $quotationsToAdd = $quotationsToAdd->get($id, [
        'contain' => ['Customers']
        ]);
      
        //pr($quotationsToAdd);
        //FR - mi preparo qualche campo utile
        $contattoId = $quotationsToAdd->customer->id;
        $contattoNome = $quotationsToAdd->customer->name;
      
        //FR - mi calcolo il primo numero ordine disponibile da proporre all'utente
        
        $num_order_year = TableRegistry::get('Settings');
        $num_order_yearQuery= $num_order_year->find('all')
        ->where(['type' => ('anno_num_ordine')]);
         
        $num_order_year_value = $num_order_yearQuery->first();
        $this->set(compact('num_order_year_value'));
      
        $last_num_order = TableRegistry::get('Settings');
        $last_num_orderQuery= $last_num_order->find('all')
        ->where(['type' => ('numeratore_ordine')]);
         
        $last_num_order_value = $last_num_orderQuery->first();
        
        $this->set(compact('last_num_order_value'));
        
      
       
        $ordersCustomer = $this->OrdersCustomers->newEntity();
        if ($this->request->is('post')) 
          {
            $ordersCustomer = $this->OrdersCustomers->patchEntity($ordersCustomer, $this->request->getData());
            //pr ($ordersCustomer);
            if ($this->OrdersCustomers->save($ordersCustomer)) 
            {
                $this->Flash->success(__('The orders customer has been saved.'));
              
              
              
              //FR -> 2019-07-18 - dopo la trasformazione in ordine, setto il preventivo come archiviato e scrivo il numero ordine nel campo 'order_number' delle quotations
              $quotationToUpd = TableRegistry::get("Quotations");
              $query = $quotationToUpd->query();
              $result = $query->update()
              ->set(['archived' => 1])
              ->set(['order_number' => $ordersCustomer->id])
              ->where(['id' => $quotationsToAdd->id])
              ->execute();
              
            
              //FR - mi salvo il cliente e poi procedo al salvataggio dell'ordine
              // Prima controllo se l'utente vuole che il contatto intestatario del preventivo diventi cliente
              $diventaCliente = $this->request->getData('is_customer');
              $creaCliente = $this->request->getData('new_customer');

                if($diventaCliente == 1)
                {
                  $customerToUpd = TableRegistry::get("Customers");
                  $query = $customerToUpd->query();
                  $result = $query->update()
                  ->set(['is_customer' => 1])
                  ->where(['id' => $quotationsToAdd->customer_id])
                  ->execute();  
                  
                  //FR - aggiorno il contatore del numero ordine
                  
                  $intNumOrd = (int)$last_num_order_value->description;
                  $newNumOrd = $intNumOrd+1;
                  $newNumOrdDef = (string)$newNumOrd;
                  
                  //pr($newNumOrdDef);
                  
                  $num_order_ToUpd = TableRegistry::get("Settings");
                  $queryNumOrd = $num_order_ToUpd->query();
                  $result = $queryNumOrd->update()
                  ->set(['description' => $newNumOrdDef])
                  ->where(['type' => 'numeratore_ordine'])
                  ->execute();  
                  
                  
                  //FR -> chiamo la funzione che valorizza la Phase1 dell'ordine.
                  
                  $this->SetOrderPhase1($ordersCustomer->id);
                 
                  //return $this->redirect(['action' => 'index']); 
                  
//                   pr($ordersCustomer->id);
//                   pr("cia");
                  
                  return $this->redirect(['controller' => 'OrderPhase1', 'action' => 'add', $ordersCustomer->id]);
                }
              //FR - fine parte salvataggio cliente

              if($creaCliente == 1)
              {
              $order_id = $this->request->getData('id');
                
              return $this->redirect(['action' => 'exec',  $ordersCustomer->id]); 
              }
            
            }

            $this->Flash->error(__('The orders customer could not be saved. Please, try again.'));

        }
         
        $quotations = $this->OrdersCustomers->Quotations->find('list');
        //$this->set(compact('ordersCustomer', 'quotations', 'lifts','quotationsToAdd'));
        $this->set(compact('ordersCustomer', 'quotations','quotationsToAdd'));
              
    }
      
    
  
    public function exec($order_id = null)
  
    {
        //FR 2019-11-25: in questa parte ci arrivo solo se devo creare un nuovo cliente da associare all'ordine. 
        //Quindi vado a inserire il cliente, e poi faccio l'update dell'ordine associando il nuovo cliente appena creato
      
        $this->loadModel('Customers');
        $customer = $this->Customers->newEntity();
      
        if ($this->request->is('post')) {
            // controllo che ci siano file da slavare, se non ci sno file caricati non salvo nulla nel DB
            $data = $this->request->getData();
            if($data['uploads']['0']['error'] == 4) 
               unset($data['uploads']);
            $customer = $this->Customers->patchEntity($customer, $data, ['associated' => ['Uploads']]);
            if ($this->Customers->save($customer)) {
                $this->Flash->success(__('The customer EXEC has been saved.'));

                  $orderToUpd = TableRegistry::get("OrdersCustomers");
                  $query = $orderToUpd->query();
                  $result = $query->update()
                  ->set(['customer_id' => $customer->id])
                  ->where(['id' => $order_id])
                  ->execute();  
              
                  $id = $customer->id;
//                   return $this->redirect
//                   (
//                     ['controller' => 'Quotations', 'action' => 'index',$id]
//                   );
                  return $this->redirect(['controller' => 'OrderPhase1', 'action' => 'add',$order_id]);
            }
            $this->Flash->error(__('The customer EXEC could not be saved. Please, try again.'));
        }
      
        $this->set(compact('customer'));
        
        //FR - aggiungo la provenienza dai settings
        $source = TableRegistry::get('Settings');
        $sourceQuery= $source->find('list')
        ->where(['type' => 'provenienza'])
        ->order(['description' => 'DESC']);

        $sourceQueryList = $sourceQuery->toList();
        $this->set(compact('sourceQueryList'));
        //return $this->redirect(['action' => 'index',  $order_id]); 
              
    }

    /**
     * Edit method
     *
     * @param string|null $id Orders Customer id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $ordersCustomer = $this->OrdersCustomers->get($id, [
            'contain' => []
        ]);
    
        if ($this->request->is(['patch', 'post', 'put'])) {
            $ordersCustomer = $this->OrdersCustomers->patchEntity($ordersCustomer, $this->request->getData());
            if ($this->OrdersCustomers->save($ordersCustomer)) {
                $this->Flash->success(__('The orders customer has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The orders customer could not be saved. Please, try again.'));
        }
        $quotations = $this->OrdersCustomers->Quotations->find('list');
        $lifts = $this->OrdersCustomers->Lifts->find('list');
        $this->set(compact('ordersCustomer', 'quotations', 'lifts'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Orders Customer id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $ordersCustomer = $this->OrdersCustomers->get($id);
        if ($this->OrdersCustomers->delete($ordersCustomer)) {
            $this->Flash->success(__('The orders customer has been deleted.'));
        } else {
            $this->Flash->error(__('The orders customer could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
  
    public function SetOrderPhase1 ($id = null)
    {
      $this->loadModel("OrderPhase1");
      
      $orderPhase = $this->OrderPhase1->newEntity();
      $orderPhase->order_id = $id;
      
      $stepPhase = TableRegistry::get('Settings');
      $stepPhaseQuery= $stepPhase->find('list')
      ->where(['type' => 'stepfase1'])
      ->order(['description' => 'DESC']);   

      $sourcestepPhase = $stepPhaseQuery->toList();

      $this->set(compact('sourcestepPhase'));
      
          if ($this->OrderPhase1->save($orderPhase)) 
          {
            return true;
          } 
          else 
          {
           return false;
          }
      
    }
}
