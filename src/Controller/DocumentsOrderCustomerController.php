<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * DocumentsOrderCustomer Controller
 *
 * @property \App\Model\Table\DocumentsOrderCustomerTable $DocumentsOrderCustomer
 *
 * @method \App\Model\Entity\DocumentsOrderCustomer[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DocumentsOrderCustomerController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['OrderCustomers', 'DocumentsLists']
        ];
        $documentsOrderCustomer = $this->paginate($this->DocumentsOrderCustomer);

        $this->set(compact('documentsOrderCustomer'));
    }

    /**
     * View method
     *
     * @param string|null $id Documents Order Customer id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $documentsOrderCustomer = $this->DocumentsOrderCustomer->get($id, [
            'contain' => ['OrderCustomers', 'DocumentsLists']
        ]);

        $this->set('documentsOrderCustomer', $documentsOrderCustomer);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $documentsOrderCustomer = $this->DocumentsOrderCustomer->newEntity();
        if ($this->request->is('post')) {
            $documentsOrderCustomer = $this->DocumentsOrderCustomer->patchEntity($documentsOrderCustomer, $this->request->getData());
            if ($this->DocumentsOrderCustomer->save($documentsOrderCustomer)) {
                $this->Flash->success(__('The documents order customer has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The documents order customer could not be saved. Please, try again.'));
        }
        $orderCustomers = $this->DocumentsOrderCustomer->OrderCustomers->find('list', ['limit' => 200]);
        $documentsLists = $this->DocumentsOrderCustomer->DocumentsLists->find('list', ['limit' => 200]);
        $this->set(compact('documentsOrderCustomer', 'orderCustomers', 'documentsLists'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Documents Order Customer id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $documentsOrderCustomer = $this->DocumentsOrderCustomer->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $documentsOrderCustomer = $this->DocumentsOrderCustomer->patchEntity($documentsOrderCustomer, $this->request->getData());
            if ($this->DocumentsOrderCustomer->save($documentsOrderCustomer)) {
                $this->Flash->success(__('The documents order customer has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The documents order customer could not be saved. Please, try again.'));
        }
        $orderCustomers = $this->DocumentsOrderCustomer->OrderCustomers->find('list', ['limit' => 200]);
        $documentsLists = $this->DocumentsOrderCustomer->DocumentsLists->find('list', ['limit' => 200]);
        $this->set(compact('documentsOrderCustomer', 'orderCustomers', 'documentsLists'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Documents Order Customer id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $documentsOrderCustomer = $this->DocumentsOrderCustomer->get($id);
        if ($this->DocumentsOrderCustomer->delete($documentsOrderCustomer)) {
            $this->Flash->success(__('The documents order customer has been deleted.'));
        } else {
            $this->Flash->error(__('The documents order customer could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
