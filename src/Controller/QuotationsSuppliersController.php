<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Controller\UploadsController;
use Cake\ORM\TableRegistry;

/**
 * QuotationsSuppliers Controller
 *
 * @property \App\Model\Table\QuotationsSuppliersTable $QuotationsSuppliers
 *
 * @method \App\Model\Entity\QuotationsSupplier[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class QuotationsSuppliersController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {

        $quotationsSuppliers = $this->QuotationsSuppliers->find('all')
        //->where(['archived !=' => 1])
        ->order(['quotation_date' => 'DESC' ])
        ->contain(['Suppliers', 'Quotations']);
        $quotationsSuppliers = $quotationsSuppliers->all();

        $this->set(compact('quotationsSuppliers'));
    }

    /**
     * View method
     *
     * @param string|null $id Quotations Supplier id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $quotationsSupplier = $this->QuotationsSuppliers->get($id, [
            'contain' => ['Suppliers', 'Quotations']
        ]);

        $this->set('quotationsSupplier', $quotationsSupplier);
        
        //cerco gli uploads assocaiti al preventivo corrente
        $uploads = TableRegistry::get('Uploads');
        $uploads = $uploads->find('all')
          ->where(['quotation_supplier_id' => ($id)])
          ->order(['filename' => 'ASC']);
        
        $uploads = $uploads->all();
        $this->set(compact('uploads'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $quotationsSupplier = $this->QuotationsSuppliers->newEntity();
        if ($this->request->is('post')) {
            // controllo che ci siano file da slavare, se non ci sno file caricati non salvo nulla nel DB
            $data = $this->request->getData();
            if($data['uploads']['0']['error'] == 4) 
               unset($data['uploads']);
            $quotationsSupplier = $this->QuotationsSuppliers->patchEntity($quotationsSupplier, $data, ['associated' => ['Uploads']]);
            if ($this->QuotationsSuppliers->save($quotationsSupplier)) {
                $this->Flash->success(__('The quotations supplier has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The quotations supplier could not be saved. Please, try again.'));
        }
        $suppliers = $this->QuotationsSuppliers->Suppliers->find('list', ['limit' => 200]);
//         $quotations = $this->QuotationsSuppliers->Quotations->find('list', ['limit' => 200]);
      
        $quotations = $this->QuotationsSuppliers->Quotations->find('list', [
            'valueField' => function ($quotations) {
            return $quotations->get('label');
            }
        ]);
      
        $this->set(compact('quotationsSupplier', 'suppliers', 'quotations'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Quotations Supplier id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $quotationsSupplier = $this->QuotationsSuppliers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            // controllo che ci siano file da slavare, se non ci sno file caricati non salvo nulla nel DB
            $data = $this->request->getData();
            if($data['uploads']['0']['error'] == 4) 
               unset($data['uploads']);
            $quotationsSupplier = $this->QuotationsSuppliers->patchEntity($quotationsSupplier, $data, ['associated' => ['Uploads']]);
            if ($this->QuotationsSuppliers->save($quotationsSupplier)) {
                $this->Flash->success(__('The quotations supplier has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The quotations supplier could not be saved. Please, try again.'));
        }
        $suppliers = $this->QuotationsSuppliers->Suppliers->find('list', ['limit' => 200]);
        $quotations = $this->QuotationsSuppliers->Quotations->find('list', ['limit' => 200]);
        $this->set(compact('quotationsSupplier', 'suppliers', 'quotations'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Quotations Supplier id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $quotationsSupplier = $this->QuotationsSuppliers->get($id);
        if ($this->QuotationsSuppliers->delete($quotationsSupplier)) {
            $this->Flash->success(__('The quotations supplier has been deleted.'));
        } else {
            $this->Flash->error(__('The quotations supplier could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
