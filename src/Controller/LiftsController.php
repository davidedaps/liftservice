<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Lifts Controller
 *
 * @property \App\Model\Table\LiftsTable $Lifts
 *
 * @method \App\Model\Entity\Lift[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class LiftsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $lifts = $this->paginate($this->Lifts);

        $this->set(compact('lifts'));
    }

    /**
     * View method
     *
     * @param string|null $id Lift id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $lift = $this->Lifts->get($id, [
            'contain' => ['OrdersCustomers']
        ]);

        $this->set('lift', $lift);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $lift = $this->Lifts->newEntity();
        if ($this->request->is('post')) {
            $lift = $this->Lifts->patchEntity($lift, $this->request->getData());
            if ($this->Lifts->save($lift)) {
                $this->Flash->success(__('The lift has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The lift could not be saved. Please, try again.'));
        }
        $this->set(compact('lift'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Lift id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $lift = $this->Lifts->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $lift = $this->Lifts->patchEntity($lift, $this->request->getData());
            if ($this->Lifts->save($lift)) {
                $this->Flash->success(__('The lift has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The lift could not be saved. Please, try again.'));
        }
        $this->set(compact('lift'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Lift id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $lift = $this->Lifts->get($id);
        if ($this->Lifts->delete($lift)) {
            $this->Flash->success(__('The lift has been deleted.'));
        } else {
            $this->Flash->error(__('The lift could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
