<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Controller\QuotationsController;
use App\Controller\UploadsController;
use Cake\ORM\TableRegistry;
/**
 * Customers Controller
 *
 * @property \App\Model\Table\CustomersTable $Customers
 *
 * @method \App\Model\Entity\Customer[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CustomersController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        //$customers = $this->paginate($this->Customers);
        $customers = $this->Customers->find('all');
        $customers = $customers->all();

        $this->set(compact('customers'));
      
    }
  
    public function listcustomer()
    {
      //$customers = $this->paginate($this->Customers);
      $customers = $this->Customers->find('all')
      ->where(['is_customer' => ('1')]);
      $customers = $customers->all();

      $this->set(compact('customers'));

    }
  
    public function listcontact()
    {
      //$customers = $this->paginate($this->Customers);
      $customers = $this->Customers->find('all')
      ->where(['is_customer' => ('0')]);
      $customers = $customers->all();

      $this->set(compact('customers'));

    }

    /**
     * View method
     *
     * @param string|null $id Customer id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $customer = $this->Customers->get($id, [
            'contain' => ['Actions', 'Quotations']
        ]);

        $this->set('customer', $customer);
      
        $quotations = TableRegistry::get('Quotations');
        $quotationsQuery= $quotations->find('all')
        ->where(['customer_id' => ($id)])
        ->order(['quotation_date' => 'DESC']);
         
        $quotationList = $quotationsQuery->toArray();
        $this->set(compact('quotationList'));
        
        //cerco gli uploads assocaiti al preventivo corrente
        $uploads = TableRegistry::get('Uploads');
        $uploads = $uploads->find('all')
          ->where(['customer_id' => ($id)])
          ->order(['filename' => 'ASC']);
        
        $uploads = $uploads->all();
        $this->set(compact('uploads'));
    }
    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $customer = $this->Customers->newEntity();
        if ($this->request->is('post')) {
            // controllo che ci siano file da slavare, se non ci sno file caricati non salvo nulla nel DB
            $data = $this->request->getData();
            // if($data['uploads']['0']['error'] == 4) 
               // unset($data['uploads']);
            //$customer = $this->Customers->patchEntity($customer, $data, ['associated' => ['Uploads']]);
          
            $customer = $this->Customers->patchEntity($customer, $data);
            if ($this->Customers->save($customer)) {
                $this->Flash->success(__('The customer has been saved.'));
              
//                 $this->Customer->save($customer);
//                 $lastInsertedId = $customer->id; 
//                  pr($lastInsertedId);
              
                  $id = $customer->id;
                  return $this->redirect
                  (
                    ['controller' => 'Quotations', 'action' => 'add',$id]
                  );
            }
            $this->Flash->error(__('The customer could not be saved. Please, try again.'));
        }
        $this->set(compact('customer'));
      
      
        //FR - aggiungo la provenienza dai settings
        $source = TableRegistry::get('Settings');
        $sourceQuery= $source->find('list')
          ->where(['type' => 'provenienza'])
          ->order(['description' => 'DESC']);
         
        $sourceQueryList = $sourceQuery->toList();
        $this->set(compact('sourceQueryList'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Customer id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $customer = $this->Customers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            // controllo che ci siano file da slavare, se non ci sno file caricati non salvo nulla nel DB
            $data = $this->request->getData();
            if($data['uploads']['0']['error'] == 4) 
               unset($data['uploads']);
            $customer = $this->Customers->patchEntity($customer, $data, ['associated' => ['Uploads']]);
            
            if ($this->Customers->save($customer)) {
                $this->Flash->success(__('Cliente salvato correttamente'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The customer could not be saved. Please, try again.'));
        }
        $this->set(compact('customer'));
      
              //FR - aggiungo la provenienza dai settings
        $source = TableRegistry::get('Settings');
        $sourceQuery= $source->find('list', [
        'keyField' => 'description',
        'valueField' => 'description'
        ])
          ->where(['type' => 'provenienza'])
          ->order(['description' => 'DESC']);
         
        $sourceQueryList = $sourceQuery->toList();
        $this->set(compact('sourceQueryList'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Customer id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $customer = $this->Customers->get($id);
        if ($this->Customers->delete($customer)) {
            $this->Flash->success(__('The customer has been deleted.'));
        } else {
            $this->Flash->error(__('The customer could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
