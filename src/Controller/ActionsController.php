<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Controller\SettingsController;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
use Cake\Inflector;

/**
 * Actions Controller
 *
 * @property \App\Model\Table\ActionsTable $Actions
 *
 * @method \App\Model\Entity\Action[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ActionsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $actions = $this->Actions->find('all')
        ->where(['archived !=' => (1)])
        ->contain(['Customers', 'Quotations', 'Users']);

        $actions = $actions->all();
      
        $users = $this->Actions->Users->find('list')->toArray();
        
        $this->set(compact('actions','users'));
      
//         $actionTypes = $this->Settings->find('all')
//         ->where(['type =' => 'tipo azione'])
//         ->order(['description' => 'DESC' ]);
//         $actionTypes = $actionTypes->toList();

        $this->set(compact('$actionTypes'));
    }
  
    public function changeStatus($id = null, $quotation_id = null)
    {
      $query = $this->Actions->query();
      $query->update()
      ->set(['status' => 1])
      ->where(['id' => $id])
      ->execute();
      
      return $this->redirect(['controller' => 'Quotations', 'action' => 'view',  $quotation_id]);
    }

    /**
     * View method
     *
     * @param string|null $id Action id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $action = $this->Actions->get($id, [
            'contain' => ['Customers', 'Quotations', 'Users']
        ]);

        $users = $this->Actions->Users->find('list')->toArray();
        //$this->set('action', $action);
         $this->set(compact('action','users'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
  
      public function add($id = null, $selected_id = null)
    {        
        
        if (is_null($id) && is_null($selected_id))
        {

        if ($this->request->is(['patch', 'post', 'put'])) { 
            $customer_id = $this->request->getData('customer_id');
            $this->redirect(['action' => 'exec',  $customer_id]); 
        }

        $customers = $this->Actions->Customers->find('list');
        $this->set(compact('id', 'customers'));
        
        }
        
        elseif (!is_null($id) && !is_null($selected_id))
          
        {
          $this->redirect(['action' => 'exec',  $id,$selected_id]); 

        }
  
      }
    public function exec($id = null, $selected_id = null)
    {
        //FR - selected_id è id preventivo, id è id cliente
        $action = $this->Actions->newEntity();
      
        if ($this->request->is('post')) {
            $action = $this->Actions->patchEntity($action, $this->request->getData());
     
            if($action->exec_date == '')
            {
              $now = Time::now();
              $action->exec_date = $now;
            }
            if ($this->Actions->save($action)) {
              
                // se non è impostata la variabile $is_action allora devo aggiornare a 1, perchè sto salvando un'azione 
                // il parametro $is_action arriva da exec.ctp
              
                if ( !isset($action->is_action) )
                {
                   
                    $query = $this->Actions->query();
                    $query->update()
                    ->set(['is_action' => 1])
                    ->where(['id' => $action->id])
                    ->execute();
                    return $this->redirect
                      (
                      ['controller' => 'Quotations', 'action' => 'view',$action->quotation_id]
                      );
                }
 
                $this->Flash->success(__('The action has been saved.'));
              
                if($action->is_action == 0)
                {
                   return $this->redirect
                  (
                    ['controller' => 'Quotations', 'action' => 'view',$action->quotation_id]
                  );
                }
                else
                {
                }
            }
            $this->Flash->error(__('The action could not be saved. Please, try again.'));
        }
      
        $customers = $this->Actions->Customers->get($id);
      
        //FR - aggiungo i tipi di azione dai settings
//         $actionsType = TableRegistry::get('Settings');
//         $actionsTypeQuery= $actionsType->find('list')
//           ->where(['type' => 'tipo azione'])
//           ->order(['description' => 'DESC']);
         
//         $actionsTypeList = $actionsTypeQuery->toList();
//         $this->set(compact('actionsTypeList'));

      
//         $quotations = $this->Actions->Quotations->find('list', ['limit' => 20])
//           ->where(['customer_id' => $id])
//           ->toArray();
        $quotations = $this->Actions->Quotations->find('list', [
            'conditions' => ['customer_id' => $id]
          ,
            'keyField' => 'id',
            'valueField' => function ($quotations) {
            return $quotations->get('label');
            }
        ])->toArray();
        $users = $this->Actions->Users->find('list', ['limit' => 200]);
        $this->set(compact('action', 'customers', 'quotations', 'users','selected_id'));
      
    }

    /**
     * Edit method
     *
     * @param string|null $id Action id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $action = $this->Actions->get($id, [
            'contain' => ['Users']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $action = $this->Actions->patchEntity($action, $this->request->getData());
            if ($this->Actions->save($action)) {
                $this->Flash->success(__('The action has been saved.'));
            if($action->is_action == 0)
                {
                   return $this->redirect
                  (
                    ['controller' => 'Actions', 'action' => 'exec',$action->customer_id]
                  );
                }
                else
                {
                return $this->redirect
                  (
                    ['controller' => 'Quotations', 'action' => 'view',$action->quotation_id]
                  );
                }
            }
            $this->Flash->error(__('The action could not be saved. Please, try again.'));
        }
        $customers = $this->Actions->Customers->find('list');
        $quotations = $this->Actions->Quotations->find('list');
        $users = $this->Actions->Users->find('list')->toArray();
        $this->set(compact('action', 'customers', 'quotations', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Action id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null, $quotation_id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $action = $this->Actions->get($id);
        if ($this->Actions->delete($action)) {
            $this->Flash->success(__('The action has been deleted.'));
        } else {
            $this->Flash->error(__('The action could not be deleted. Please, try again.'));
        }
        
        if (isset($quotation_id)) return $this->redirect(['controller' => 'Quotations', 'action' => 'view', $quotation_id]);
        else return $this->redirect(['action' => 'index']);
       
    }
  
}
