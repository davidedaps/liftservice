<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Memo Controller
 *
 * @property \App\Model\Table\MemoTable $Memo
 *
 * @method \App\Model\Entity\Memo[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MemoController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['OrderCustomers']
        ];
        $memo = $this->paginate($this->Memo);

        $this->set(compact('memo'));
    }

    /**
     * View method
     *
     * @param string|null $id Memo id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $memo = $this->Memo->get($id, [
            'contain' => ['OrderCustomers']
        ]);

        $this->set('memo', $memo);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $memo = $this->Memo->newEntity();
        if ($this->request->is('post')) {
            $memo = $this->Memo->patchEntity($memo, $this->request->getData());
            if ($this->Memo->save($memo)) {
                $this->Flash->success(__('The memo has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The memo could not be saved. Please, try again.'));
        }
        $orderCustomers = $this->Memo->OrderCustomers->find('list', ['limit' => 200]);
        $this->set(compact('memo', 'orderCustomers'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Memo id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $memo = $this->Memo->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $memo = $this->Memo->patchEntity($memo, $this->request->getData());
            if ($this->Memo->save($memo)) {
                $this->Flash->success(__('The memo has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The memo could not be saved. Please, try again.'));
        }
        $orderCustomers = $this->Memo->OrderCustomers->find('list', ['limit' => 200]);
        $this->set(compact('memo', 'orderCustomers'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Memo id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $memo = $this->Memo->get($id);
        if ($this->Memo->delete($memo)) {
            $this->Flash->success(__('The memo has been deleted.'));
        } else {
            $this->Flash->error(__('The memo could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
