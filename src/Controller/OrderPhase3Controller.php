<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
use Cake\Mailer\Email;

/**
 * OrderPhase3 Controller
 *
 * @property \App\Model\Table\OrderPhase3Table $OrderPhase3
 *
 * @method \App\Model\Entity\OrderPhase3[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class OrderPhase3Controller extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $orderPhase3 = $this->paginate($this->OrderPhase3);

        $this->set(compact('orderPhase3'));
    }
  
  public function orderStatus($id = null)
    {
     
        $ordersToShow  = $this->OrderPhase3->find('all')
                        ->where(['phase_completed !=' => 1])
          //->contain(['OrdersCustomers']);
        ->contain(['OrdersCustomers' => ['Customers','Quotations']]);
        $ordersToShow = $ordersToShow->all();

        $this->set(compact('ordersToShow'));    
    
//          $this->loadModel('OrderPhase2');
    
//         $customersDetail = TableRegistry::get('Customers');
//         $customersDetailQuery= $customersDetail->find('all');

//         $customersDetailList = $customersDetailQuery->toArray();
//         $this->set(compact('customersDetailList'));
    
  
    }

    /**
     * View method
     *
     * @param string|null $id Order Phase3 id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $orderPhase3 = $this->OrderPhase3->get($id, [
           'contain' => ['OrdersCustomers']
        ]);

        $this->set('orderPhase3', $orderPhase3);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($idOrder)
    {
      
      $descriptionPhase3 = TableRegistry::get('Settings');
      $descriptionPhase3Query= $descriptionPhase3->find('all')
                              ->where(['type =' => 'stepfase3']);

      $descriptionPhase3List = $descriptionPhase3Query->toArray();
      $this->set(compact('descriptionPhase3List'));
      
      //FR - mi preparo un'po di campi da inserire
      
      $orderId = $idOrder;
      $phase_completed = 0;
      $numero = 0;
      
      $descriptionStep1 = $descriptionPhase3List[0]->description;
      //$step_1_created = Time::now();
      $step_1_completed = 0;
      
      $descriptionStep2 = $descriptionPhase3List[1]->description;
     // $step_2_created = Time::now();
      $step_2_completed = 0;  
      
      $descriptionStep3= $descriptionPhase3List[2]->description;
      //$step_3_created = Time::now();
      $step_3_completed = 0;  
      
      $descriptionStep4= $descriptionPhase3List[3]->description;
      //$step_4_created = Time::now();
      $step_4_completed = 0; 
      
      $descriptionStep5= $descriptionPhase3List[4]->description;
      //$step_5_created = Time::now();
      $step_5_completed = 0; 
      
      $descriptionStep6= $descriptionPhase3List[5]->description;
      //$step_6_created = Time::now();
      $step_6_completed = 0; 
 
      $orderPhase3= $this->OrderPhase3->newEntity();
      
      $orderPhase3->order_id = $orderId;
      $orderPhase3->phase_completed = $phase_completed;
      
      $orderPhase3->description_step_1 = $descriptionStep1;
      //$orderPhase3->step_1_created = $step_1_created;
      $orderPhase3->step_1_completed= $step_1_completed;
        
      $orderPhase3->description_step_2 = $descriptionStep2;
      //$orderPhase3->step_2_created = $step_2_created;
      $orderPhase3->step_2_completed= $step_2_completed;
      
      $orderPhase3->description_step_3 = $descriptionStep3;
      //$orderPhase3->step_3_created = $step_3_created;
      $orderPhase3->step_3_completed= $step_3_completed;
      
      $orderPhase3->description_step_4 = $descriptionStep4;
      //$orderPhase3->step_4_created = $step_4_created;
      $orderPhase3->step_4_completed= $step_4_completed;
      
      $orderPhase3->description_step_5 = $descriptionStep5;
      //$orderPhase3->step_5_created = $step_5_created;
      $orderPhase3->step_5_completed= $step_5_completed;
      
      $orderPhase3->description_step_6 = $descriptionStep6;
      //$orderPhase3->step_6_created = $step_6_created;
      $orderPhase3->step_6_completed= $step_6_completed;  
      
//         //$orderPhase3 = $this->OrderPhase3->newEntity();
//         if ($this->request->is('post')) {
//             $orderPhase3 = $this->OrderPhase3->patchEntity($orderPhase3, $this->request->getData());
//             if ($this->OrderPhase3->save($orderPhase3)) {
//                 $this->Flash->success(__('The order phase3 has been saved.'));

//                 return $this->redirect(['action' => 'index']);
//             }
//             $this->Flash->error(__('The order phase3 could not be saved. Please, try again.'));
//         }
//         $this->set(compact('orderPhase3'));
        if ($this->OrderPhase3->save($orderPhase3)) {
          $this->Flash->success(__('The order phase3has been saved.'));

          //return $this->redirect(['action' => 'view']);
          return $this->redirect(['controller' => 'OrderPhase1', 'action' => 'view', $orderId]);
      }
      $this->Flash->error(__('The order phase3 could not be saved. Please, try again.'));

       $this->set(compact('orderPhase3'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Order Phase3 id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null,$step2= null)
    {
        $orderPhase3 = $this->OrderPhase3->get($id, [
            'contain' => []
        ]);
        
        $this->set('step2', $step2);
      
            //FR => 2022-07-26 - estraggo l'elenco utennti per assegnarli all'azione
        
        $users = TableRegistry::get('Users');
        $usersQuery = $users->find('list')->toArray();

        //$usersValues = $usersQuery->all();
        $this->set(compact('usersQuery'));
        //pr($usersValues);
        
        if ($this->request->is(['patch', 'post', 'put'])) {
            $orderPhase3 = $this->OrderPhase3->patchEntity($orderPhase3, $this->request->getData());
            if ($this->OrderPhase3->save($orderPhase3)) {
                $this->Flash->success(__('The order phase3 has been saved.'));

               return $this->redirect(['controller' => 'OrderPhase1','action' => 'view',$id]);
            }
            $this->Flash->error(__('The order phase3 could not be saved. Please, try again.'));
        }
        $this->set(compact('orderPhase3'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Order Phase3 id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $orderPhase3 = $this->OrderPhase3->get($id);
        if ($this->OrderPhase3->delete($orderPhase3)) {
            $this->Flash->success(__('The order phase3 has been deleted.'));
        } else {
            $this->Flash->error(__('The order phase3 could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
  
  public function update($id = null,$step1 = null)
    {
   
        $get_data = $this->request->getData();
        $orderPhase3 = $this->OrderPhase3->get($id, [
            'contain' => []
        ]);
      
        $this->set('step', $step1);

      $orderDetail = TableRegistry::get('Orders_customers');
      $orderDetailQuery= $orderDetail->find('all')
                              ->where(['id =' => $id]);

      $orderDetailValues = $orderDetailQuery->first();
      $this->set(compact('orderDetailValues'));
        
        

      $quoDetail = TableRegistry::get('Quotations');
      $quoDetailQuery= $quoDetail->find('all')
                              ->where(['id =' => $orderDetailValues->quotation_id]);

      $quoDetailValues =  $quoDetailQuery->first();
      $this->set(compact('quoDetailValues'));  
        

        if ($this->request->is(['patch', 'post', 'put'])) {
            $orderPhase3 = $this->OrderPhase3->patchEntity($orderPhase3, $this->request->getData());
          
          
      
          if (strcmp($get_data['confirm'], "CONFERMA") == 0) {
            pr($get_data);
            if($get_data['step'] == 1)
            {
            $articlesTable = TableRegistry::getTableLocator()->get('OrderPhase3');
            $article = $articlesTable->get($id); // Return article with id 12

            $article->step_1_completed = '0';
            $article->step_1_completedtime = NULL;
            $article->step_1_created = NULL;
            $article->step_1_user = NULL;            
            $articlesTable->save($article);
                           return $this->redirect(['controller'=> 'OrderPhase1','action' => 'view', $id]);
            }
            elseif($get_data['step'] == 2)
            {
            $articlesTable = TableRegistry::getTableLocator()->get('OrderPhase3');
            $article = $articlesTable->get($id); // Return article with id 12

            $article->step_2_completed = '0';
            $article->step_2_completedtime = NULL;
            $article->step_2_created = NULL;
            $article->step_2_user = NULL;            
            $articlesTable->save($article);
                           return $this->redirect(['controller'=> 'OrderPhase1','action' => 'view', $id]);
            }
            elseif($get_data['step'] == 3)
            {
            $articlesTable = TableRegistry::getTableLocator()->get('OrderPhase3');
            $article = $articlesTable->get($id); // Return article with id 12

            $article->step_3_completed = '0';
            $article->step_3_completedtime = NULL;
            $article->step_3_created = NULL;
            $article->step_3_user = NULL;            
            $articlesTable->save($article);
                           return $this->redirect(['controller'=> 'OrderPhase1','action' => 'view', $id]);
            }
            elseif($get_data['step'] == 4)
            {
            $articlesTable = TableRegistry::getTableLocator()->get('OrderPhase3');
            $article = $articlesTable->get($id); // Return article with id 12

            $article->step_4_completed = '0';
            $article->step_4_completedtime = NULL;
            $article->step_4_created = NULL;
            $article->step_4_user = NULL;            
            $articlesTable->save($article);
                       return $this->redirect(['controller'=> 'OrderPhase1','action' => 'view', $id]);
            }
            elseif($get_data['step'] == 5)
            {
            $articlesTable = TableRegistry::getTableLocator()->get('OrderPhase3');
            $article = $articlesTable->get($id); // Return article with id 12

            $article->step_5_completed = '0';
            $article->step_5_completedtime = NULL;
            $article->step_5_created = NULL;
            $article->step_5_user = NULL;            
            $articlesTable->save($article);
                          return $this->redirect(['controller'=> 'OrderPhase1','action' => 'view', $id]);
            }
            elseif($get_data['step'] == 6)
            {
            $articlesTable = TableRegistry::getTableLocator()->get('OrderPhase3');
            $article = $articlesTable->get($id); // Return article with id 12

            $article->step_6_completed = '0';
            $article->step_6_completedtime = NULL;
            $article->step_6_created = NULL;
            $article->step_6_user = NULL;            
            $articlesTable->save($article);
                          return $this->redirect(['controller'=> 'OrderPhase1','action' => 'view', $id]);
            }
            
          }
          else{
            $this->Flash->error(__('The order phase1 could not be saved. Please, try again.'));}
        }
        $this->set(compact('orderPhase3'));
    }
  
  
  
  
      public function execPhase3Step1($id= null)
    {
      
              $orderPhase3 = $this->OrderPhase3->get($id, [
              'contain' => []
              ]);
      //FR => 2020-30-06 - eseguo la query di upd per settare completato lo step 1
      
              $step1ToUpd = TableRegistry::get("OrderPhase3");
              $query = $step1ToUpd->query();
              $result = $query->update()
              ->set(['step_1_completed' => 1])
              ->where(['order_id' => $id])
              ->execute();
      
      //FR => 2020-30-06 - eseguo la query di upd per settare completato lo step 1
              $time = Time::now();
              $step1DateToUpd = TableRegistry::get("OrderPhase3");
              $query = $step1DateToUpd->query();
              $result = $query->update()
              ->set(['step_1_completedtime' => $time])
              ->where(['order_id' => $id])
              ->execute();
   
  
        $loguser = $this->request->session()->read('Auth.User');
      if($loguser['id']!= $orderPhase3->step_1_user)
                {
                $time = Time::now();
                $note = $orderPhase3->step_1_notes;
                $note = $note." - Azione eseguita da:".$loguser['name']." in data: ".$time;  
                $step1CheckUserToUpd = TableRegistry::get("OrderPhase3");
                $query = $step1CheckUserToUpd->query();
                $result = $query->update()
                ->set(['step_1_notes' => $note])
                ->where(['order_id' => $id])
                ->execute();
                }
            
                return $this->redirect($this->referer());
  
   }
   public function execPhase3Step2($id= null)
    {
      
              $orderPhase3= $this->OrderPhase2->get($id, [
              'contain' => []
              ]);
      //FR => 2020-30-06 - eseguo la query di upd per settare completato lo step 1
      
              $step1ToUpd = TableRegistry::get("OrderPhase3");
              $query = $step1ToUpd->query();
              $result = $query->update()
              ->set(['step_2_completed' => 1])
              ->where(['order_id' => $id])
              ->execute();
      
      //FR => 2020-30-06 - eseguo la query di upd per settare completato lo step 1
              $time = Time::now();
              $step1DateToUpd = TableRegistry::get("OrderPhase3");
              $query = $step1DateToUpd->query();
              $result = $query->update()
              ->set(['step_2_completedtime' => $time])
              ->where(['order_id' => $id])
              ->execute();
   
  
        $loguser = $this->request->session()->read('Auth.User');
      if($loguser['id']!= $orderPhase3->step_2_user)
                {
                $time = Time::now();
                $note = $orderPhase3->step_2_notes;
                $note = $note." - Azione eseguita da:".$loguser['name']." in data: ".$time;  
                $step1CheckUserToUpd = TableRegistry::get("OrderPhase3");
                $query = $step1CheckUserToUpd->query();
                $result = $query->update()
                ->set(['step_2_notes' => $note])
                ->where(['order_id' => $id])
                ->execute();
                }
            
                return $this->redirect($this->referer());
  
   }
   public function execPhase3Step3($id= null)
    {
      
              $orderPhase3 = $this->OrderPhase3->get($id, [
              'contain' => []
              ]);
      //FR => 2020-30-06 - eseguo la query di upd per settare completato lo step 1
      
              $step1ToUpd = TableRegistry::get("OrderPhase3");
              $query = $step1ToUpd->query();
              $result = $query->update()
              ->set(['step_3_completed' => 1])
              ->where(['order_id' => $id])
              ->execute();
      
      //FR => 2020-30-06 - eseguo la query di upd per settare completato lo step 1
              $time = Time::now();
              $step1DateToUpd = TableRegistry::get("OrderPhase3");
              $query = $step1DateToUpd->query();
              $result = $query->update()
              ->set(['step_3_completedtime' => $time])
              ->where(['order_id' => $id])
              ->execute();
   
  
        $loguser = $this->request->session()->read('Auth.User');
      if($loguser['id']!= $orderPhase3->step_3_user)
                {
                $time = Time::now();
                $note = $orderPhase3->step_3_notes;
                $note = $note." - Azione eseguita da:".$loguser['name']." in data: ".$time;  
                $step1CheckUserToUpd = TableRegistry::get("OrderPhase3");
                $query = $step1CheckUserToUpd->query();
                $result = $query->update()
                ->set(['step_3_notes' => $note])
                ->where(['order_id' => $id])
                ->execute();
                }
            
            
                return $this->redirect($this->referer());
  
   }
     public function execPhase3Step4($id= null)
    {
      
              $orderPhase3 = $this->OrderPhase3->get($id, [
              'contain' => []
              ]);
      //FR => 2020-30-06 - eseguo la query di upd per settare completato lo step 1
      
              $step1ToUpd = TableRegistry::get("OrderPhase3");
              $query = $step1ToUpd->query();
              $result = $query->update()
              ->set(['step_4_completed' => 1])
              ->where(['order_id' => $id])
              ->execute();
      
      //FR => 2020-30-06 - eseguo la query di upd per settare completato lo step 1
              $time = Time::now();
              $step1DateToUpd = TableRegistry::get("OrderPhase3");
              $query = $step1DateToUpd->query();
              $result = $query->update()
              ->set(['step_4_completedtime' => $time])
              ->where(['order_id' => $id])
              ->execute();
   
  
        $loguser = $this->request->session()->read('Auth.User');
      if($loguser['id']!= $orderPhase3->step_4_user)
                {
                $time = Time::now();
                $note = $orderPhase3->step_4_notes;
                $note = $note." - Azione eseguita da:".$loguser['name']." in data: ".$time;  
                $step1CheckUserToUpd = TableRegistry::get("OrderPhase3");
                $query = $step1CheckUserToUpd->query();
                $result = $query->update()
                ->set(['step_4_notes' => $note])
                ->where(['order_id' => $id])
                ->execute();
                }
            
            
                return $this->redirect($this->referer());
  
   }
    public function execPhase3Step5($id= null)
    {
      
              $orderPhase3 = $this->OrderPhase3->get($id, [
              'contain' => []
              ]);
      //FR => 2020-30-06 - eseguo la query di upd per settare completato lo step 1
      
              $step1ToUpd = TableRegistry::get("OrderPhase3");
              $query = $step1ToUpd->query();
              $result = $query->update()
              ->set(['step_5_completed' => 1])
              ->where(['order_id' => $id])
              ->execute();
      
      //FR => 2020-30-06 - eseguo la query di upd per settare completato lo step 1
              $time = Time::now();
              $step1DateToUpd = TableRegistry::get("OrderPhase3");
              $query = $step1DateToUpd->query();
              $result = $query->update()
              ->set(['step_5_completedtime' => $time])
              ->where(['order_id' => $id])
              ->execute();
   
  
        $loguser = $this->request->session()->read('Auth.User');
      
      if($loguser['id']!= $orderPhase3->step_5_user)
                {
                $time = Time::now();
                $note = $orderPhase3->step_5_notes;
                $note = $note." - Azione eseguita da:".$loguser['name']." in data: ".$time;  
                $step1CheckUserToUpd = TableRegistry::get("OrderPhase3");
                $query = $step1CheckUserToUpd->query();
                $result = $query->update()
                ->set(['step_5_notes' => $note])
                ->where(['order_id' => $id])
                ->execute();
                }
            
            
                return $this->redirect($this->referer());
  
   }
  public function execPhase3Step6($id= null)
    {
      
              $orderPhase3= $this->OrderPhase2->get($id, [
              'contain' => []
              ]);
      //FR => 2020-30-06 - eseguo la query di upd per settare completato lo step 1
      
              $step1ToUpd = TableRegistry::get("OrderPhase3");
              $query = $step1ToUpd->query();
              $result = $query->update()
              ->set(['step_6_completed' => 1])
              ->where(['order_id' => $id])
              ->execute();
      
      //FR => 2020-30-06 - eseguo la query di upd per settare completato lo step 1
              $time = Time::now();
              $step1DateToUpd = TableRegistry::get("OrderPhase3");
              $query = $step1DateToUpd->query();
              $result = $query->update()
              ->set(['step_6_completedtime' => $time])
              ->where(['order_id' => $id])
              ->execute();
   
  
        $loguser = $this->request->session()->read('Auth.User');
      if($loguser['id']!= $orderPhase3->step_6_user)
                {
                $time = Time::now();
                $note = $orderPhase3->step_6_notes;
                $note = $note." - Azione eseguita da:".$loguser['name']." in data: ".$time;  
                $step1CheckUserToUpd = TableRegistry::get("OrderPhase3");
                $query = $step1CheckUserToUpd->query();
                $result = $query->update()
                ->set(['step_6_notes' => $note])
                ->where(['order_id' => $id])
                ->execute();
                }
            
            
                return $this->redirect($this->referer());
  
   }
  
  
}
