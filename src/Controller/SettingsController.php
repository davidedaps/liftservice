<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Utility\Text;
/**
 * Settings Controller
 *
 *
 * @method \App\Model\Entity\Setting[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SettingsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        //$settings = $this->paginate($this->Settings);
        $settings = $this->Settings->find('all');

        $this->set(compact('settings'));
    }

    /**
     * View method
     *
     * @param string|null $id Setting id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $setting = $this->Settings->get($id, [
            'contain' => []
        ]);

        $this->set('setting', $setting);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $setting = $this->Settings->newEntity();
        if ($this->request->is('post')) {
            $setting = $this->Settings->patchEntity($setting, $this->request->getData());
          
             if($setting->type == 'tipo azione')
            {
              $setting->shortcode = Text::truncate($setting->description,20, ['exact' => false]);
              $setting->shortcode = strtolower($setting->shortcode);
              $setting->shortcode = Text::slug($setting->shortcode);
            }
          
            if ($this->Settings->save($setting)) {
                $this->Flash->success(__('The setting has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The setting could not be saved. Please, try again.'));
        }
        $this->set(compact('setting'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Setting id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $setting = $this->Settings->get($id, [
            'contain' => []
        ]);
      
        if ($this->request->is(['patch', 'post', 'put'])) {
            $setting = $this->Settings->patchEntity($setting, $this->request->getData());
          
            if($setting->type == 'tipo azione')
            {
              $setting->shortcode = Text::truncate($setting->description,20, ['exact' => false]);
              $setting->shortcode = strtolower($setting->shortcode);
              $setting->shortcode = Text::slug($setting->shortcode);
            }
              
           
            if ($this->Settings->save($setting)) {
                $this->Flash->success(__('The setting has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The setting could not be saved. Please, try again.'));
        }
        $this->set(compact('setting'));
      
        $tipo_impostazione = [
            'tipo azione' => 'tipo azione',
            'provenienza' => 'provenienza',
            'tipo pagamento' => 'tipo pagamento',
            'stato preventivo' => 'stato preventivo',
            'stepfase1' => 'stepfase1',
            'stepfase2' => 'stepfase2',
            'stepfase3' => 'stepfase3'
          ];

        $this->set('tipo_impostazione', $tipo_impostazione); 
        
        //FR => estraggo gli utenti da mostrare per abilitarli all'esecuzione dello step
      
        $users = TableRegistry::get('Users');
        $usersQuery= $users->find('all');

        $usersList = $usersQuery->toList();
        $this->set(compact('usersList'));
      
      
    }

    /**
     * Delete method
     *
     * @param string|null $id Setting id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $setting = $this->Settings->get($id);
        if ($this->Settings->delete($setting)) {
            $this->Flash->success(__('The setting has been deleted.'));
        } else {
            $this->Flash->error(__('The setting could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
