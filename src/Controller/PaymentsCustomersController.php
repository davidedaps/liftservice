<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
/**
 * PaymentsCustomers Controller
 *
 * @property \App\Model\Table\PaymentsCustomersTable $PaymentsCustomers
 *
 * @method \App\Model\Entity\PaymentsCustomer[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PaymentsCustomersController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
      
       
      
        $paymentsCustomers = $this->PaymentsCustomers->find('all',[
            'contain' => ['OrdersCustomers']
        ]);
        $paymentsCustomers = $paymentsCustomers->all();
        //$paymentsCustomers = $paymentsCustomers->toArray();

        $this->set(compact('paymentsCustomers'));
      
      
        $orderDetail = TableRegistry::get('Quotations');
      
        $orderDetailQuery = $orderDetail->find('all');
        //->where(['id' => $paymentsCustomer->OrdersCustomers['quotation_id']]);
      
        $orderDetailQuery = $orderDetailQuery->first();
  
        $this->set('orderDetailQuery', $orderDetailQuery);  
    }

    /**
     * View method
     *
     * @param string|null $id Payments Customer id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $paymentsCustomer = $this->PaymentsCustomers->get($id, [
            'contain' => ['ordersCustomers']
        ]);
      
        $this->set('paymentsCustomer', $paymentsCustomer);

        $orderDetail = TableRegistry::get('Quotations');
      
        $orderDetailQuery = $orderDetail->find('all')
        ->where(['id' => $paymentsCustomer->OrdersCustomers['quotation_id']]);
      
        $orderDetailQuery = $orderDetailQuery->first();
  
        $this->set('orderDetailQuery', $orderDetailQuery);
        
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        //FR - aggiungo i tipi di azione dai settings
        $orderList = TableRegistry::get('OrdersCustomers');
        $orderListQuery= $orderList->find ('list', ['keyField' => 'id', 'valueField' => 'order_number']);

        $orderListDetail = $orderListQuery->all();
        $this->set(compact('orderListDetail'));

        
        $paymentsCustomer = $this->PaymentsCustomers->newEntity();
      

        if ($this->request->is('post')) {
            $paymentsCustomer = $this->PaymentsCustomers->patchEntity($paymentsCustomer, $this->request->getData());
          
            if ($this->PaymentsCustomers->save($paymentsCustomer)) {
                $this->Flash->success(__('The payments customer has been saved.'));

                return $this->redirect(['action' => 'add']);
            }
            $this->Flash->error(__('The payments customer could not be saved. Please, try again.'));
        }
        $orderCustomers = $this->PaymentsCustomers->OrdersCustomers->find('list', ['limit' => 200]);
        $this->set(compact('paymentsCustomer'));
        //$this->set(compact('paymentsCustomer', 'ordersCustomers'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Payments Customer id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $paymentsCustomer = $this->PaymentsCustomers->get($id, [
            'contain' => ['OrdersCustomers']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $paymentsCustomer = $this->PaymentsCustomers->patchEntity($paymentsCustomer, $this->request->getData());
            if ($this->PaymentsCustomers->save($paymentsCustomer)) {
                $this->Flash->success(__('The payments customer has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The payments customer could not be saved. Please, try again.'));
        }
        $orderCustomers = $this->PaymentsCustomers->OrdersCustomers->find('list', ['limit' => 200]);
        $this->set(compact('paymentsCustomer', 'orderCustomers'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Payments Customer id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $paymentsCustomer = $this->PaymentsCustomers->get($id);
        if ($this->PaymentsCustomers->delete($paymentsCustomer)) {
            $this->Flash->success(__('The payments customer has been deleted.'));
        } else {
            $this->Flash->error(__('The payments customer could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
