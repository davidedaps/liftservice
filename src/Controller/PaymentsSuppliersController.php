<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
/**
 * PaymentsSuppliers Controller
 *
 * @property \App\Model\Table\PaymentsSuppliersTable $PaymentsSuppliers
 *
 * @method \App\Model\Entity\PaymentsSupplier[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PaymentsSuppliersController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
      
//         $paymentsSuppliers = $this->PaymentsSuppliers->find('all',[
//             'contain' => ['OrdersSuppliers']
//         ]);
        $paymentsSuppliers = $this->PaymentsSuppliers->find('all')
                ->contain(['OrdersCustomers'])  ;
        $paymentsSuppliers = $paymentsSuppliers->all();
      
        
        $this->set(compact('paymentsSuppliers'));

        //pr($paymentsSuppliers);

      
    }

    /**
     * View method
     *
     * @param string|null $id Payments Supplier id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $paymentsSupplier = $this->PaymentsSuppliers->get($id, [
            'contain' => ['OrdersSuppliers']
        ]);

        $this->set('paymentsSupplier', $paymentsSupplier);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
         //FR - aggiungo i tipi di azione dai settings
        //$orderList = TableRegistry::get('OrdersSuppliers');
        $orderList = TableRegistry::get('OrdersCustomers');
        $orderListQuery= $orderList->find ('list', ['keyField' => 'id', 'valueField' => 'order_number']);
      
        $orderListDetail = $orderListQuery->all();

        $this->set(compact('orderListDetail'));
      

        $paymentsSupplier = $this->PaymentsSuppliers->newEntity();
        if ($this->request->is('post')) {
            $paymentsSupplier = $this->PaymentsSuppliers->patchEntity($paymentsSupplier, $this->request->getData());
            if ($this->PaymentsSuppliers->save($paymentsSupplier)) {
                $this->Flash->success(__('The payments supplier has been saved.'));

                return $this->redirect(['action' => 'add']);
            }
            $this->Flash->error(__('The payments supplier could not be saved. Please, try again.'));
        }
        //$ordersSuppliers = $this->PaymentsSuppliers->OrdersSuppliers->find('list', ['limit' => 200]);
      
//         $orderCustomers = $this->PaymentsSuppliers->OrdersSuppliers->find('list', ['limit' => 200]);
//         $this->set(compact('ordersCustomers'));
      
        $this->set(compact('paymentsSupplier'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Payments Supplier id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $paymentsSupplier = $this->PaymentsSuppliers->get($id, [
            'contain' => []
        ]);

        $orderSupplier = TableRegistry::get('OrdersSuppliers');
        $orderSupplierQuery= $orderSupplier->find('all')
        ->where(['id' => ($paymentsSupplier->order_supplier_id)]);
         
        $orderSupplierList = $orderSupplierQuery->toArray();
        $this->set(compact('orderSupplierList'));
      

        if ($this->request->is(['patch', 'post', 'put'])) {
            $paymentsSupplier = $this->PaymentsSuppliers->patchEntity($paymentsSupplier, $this->request->getData());
            if ($this->PaymentsSuppliers->save($paymentsSupplier)) {
                $this->Flash->success(__('The payments supplier has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The payments supplier could not be saved. Please, try again.'));
        }
        $orderSuppliers = $this->PaymentsSuppliers->OrdersSuppliers->find('list', ['limit' => 200]);
        $this->set(compact('paymentsSupplier', 'orderSuppliers'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Payments Supplier id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $paymentsSupplier = $this->PaymentsSuppliers->get($id);
        if ($this->PaymentsSuppliers->delete($paymentsSupplier)) {
            $this->Flash->success(__('The payments supplier has been deleted.'));
        } else {
            $this->Flash->error(__('The payments supplier could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
  
//   protected function _getFullName()
// {
//     //return $this->_properties['order_number'] . ' ' . $this->_properties['supplier']. ' ' . $this->_properties['confirmation_date'];
    
//     $name = $this->order_number . ' ' . $this->supplier;
//     return $name;
// }
  
}
