<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\I18n\Date;
use Cake\Utility\Text;


/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{
  


    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
//     public function isAuthorized($user)
//     {
//         // By default deny access.
//         return false;
//     }
// FR => 2019-08-07 - Utilizzo questa funzione per andare a definire nel dettaglio per ogni ruolo cosa può fare e cosa no.
    public function CheckRole()
      
    {
       if ( $this->Auth->user('role') == 'admin' ) 
       {
            return true;
       }
             if ( $this->Auth->user('role') == 'user' ) 
       {
            return true;
       }
// FR => 2019-08-07 - Se ho necessità di profilare nel dettaglio alcune autorizzazioni, posso o ampliare questa oppure andare a sovrascriverla direttamente nel controller coinvolto. 
//        $controller = ($this->request->params[‘controller’]);
//        if ( $this->Auth->user('role') == 'admin' && $controller == 'Users' ) 
//        {
//             return true;
//         }
      
    }
//  FR => 2019-08-07 - Specifico solo nell'app controller la funzione isAutorized. Questa verrà ereditata da tutti i controller. 

   public function isAuthorized($user)
    {
        $allowed = $this->CheckRole();
        return $allowed;
    }
    
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);
        $this->loadComponent('Flash');

        /*
         * Enable the following component for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');      
      
        /* FR - Parte relativa all'autenticazione */
      
        $this->loadComponent('Auth', [
            'authorize'=> 'Controller',
            'authenticate' => [
                'Form' => [
                    'fields' => [
                        'username' => 'email',
                        'password' => 'password'
                    ]
                ]
            ],
            'loginAction' => [
                'controller' => 'Users',
                'action' => 'login'
            ],
             // If unauthorized, return them to page they were just on
            'unauthorizedRedirect' => $this->referer()
        ]);
      
   
        //salvo il nome dell'utente e una variable per ricordarmi se l'utente è loggato
        $loggeduser = $this->Auth->user('name');
        $role = $this->Auth->user('role');
        $loggeduseremail = $this->Auth->user('email');
        $loggeduserid = $this->Auth->user('id');
        $logged = false;
      
        
        //FR=>2019-08-07 - aggiungo i 3 parametri necessari alla gestione del tipo di impostazione dal menu impostazioni
      
        $settings_type = ['tipo azione'=>'azione','tipo pagamento'=>'pagamento', 'provenienza'=>'provenienza'];
      
      
        $quotationStatus = [ 'quasi vinto breve periodo' => 'quasi vinto breve periodo',
              'in corso' => 'in corso' ,
              'probabile vinto' => 'probabile vinto',
              'non prevedibile' => 'non prevedibile' ,
              'quasi perso' => 'quasi perso' ,
              'chiuso vinto' => 'chiuso vinto' ,
              'chiuso perso' => 'chiuso perso' ,
              'in preparazione' => 'in preparazione'] ;
      
      
        //salvo l'array source_quotation che serve per creare la lista "provenienza" nelle quotations
        $source_quotation = ['portale IGV' => 'portale IGV', 'Lifting Italia' => 'Lifting Italia', 
                        'Nova' => 'Nova', 'Lomi' => 'Lomi', 'SB' => 'SB', 'Costa' => 'Costa',
                        'Ufficio' => 'Ufficio', 'Tina' => 'Tina', 'IGV Generico' => 'IGV Generico',
                        'Cliente LF' => 'Cliente LF', 'Internet' => 'Internet', 'ARE' => 'ARE', 'Altro' => 'Altro'];
      
        //FR=>2019-07-02=> creo l'array elenco azioni possibili
      
         $actionsType = TableRegistry::get('Settings');
        $actionsTypeQuery= $actionsType->find('list',[
        'keyField' => 'description',
        'valueField' => 'description'
        ])
          ->where(['type' => 'tipo azione'])
          ->order(['description' => 'DESC']);
         
      
        $actionsTypeList = $actionsTypeQuery->toArray();
        $this->set(compact('actionsTypeList'));


//         $action_type = ['Fare 1° telefonata' => 'Fare 1° telefonata', 'Chiamare' => 'Chiamare', 'Fissare sopralluogo' => 'Fissare sopralluogo', 
//                         'Fissare incontro perfezionamento offerta' => 'Fissare incontro perfezionamento offerta', 
//                         'Sollecitare riscontro' => 'Sollecitare riscontro', 'Preparare preventivo' => 'Preparare preventivo', 
//                         'Inviare offerta' => 'Inviare offerta',
//                         'Scrivere mail' => 'Scrivere mail', 'Aggiornare Eli' => 'Aggiornare Eli', 'Altro' => 'Altro'];
      
       //FR=>2019-07-16=> creo l'array elenco modalità di pagamento 
        $payments_type = ['Bonifico' => 'Bonifico', 'Assegno' => 'Assegno', 
                        'Riba' => 'Riba', 'Da concordare' => 'Da concordare'];
      
        $ordersToShow = $this->orderStatusByUser();
        $ordersToShow2 = $this->orderStatusByUserPhase2();
        $ordersToShow3 = $this->orderStatusByUserPhase3();
        $this->set(compact('ordersToShow'));  
        //FR=>2019-07-02=> DASHBOARD  - eseguo le query per recuperare le azioni/memo da mostrare all'utente
        //FR=>2019-07-02=> Prima prendo le azioni già scadute.
      
        $today = Date::now();
      
        $query = TableRegistry::get('Actions');
        $query = $query->find('all')
          ->where(['is_action' => '1' ])
          ->andWhere(['user_id' => $loggeduserid])
          ->andWhere(['Actions.status' => 0])
          ->andWhere(['exec_date <=' => $today->format('Y-m-d H:i:s')])
          ->contain(['Customers', 'Quotations', 'Users'])
          ->order(['exec_date'=>'ASC']);
        $actions_list_expired = $query->all();
        $this->set(compact('actions_list_expired'));
      
        //DC in tomorrow salvo domani, così da vedere in home solo le azioni da compiere oggi
        // in futuredays salvo la data fra 7 gg
        $tomorrow =  new Date('+1 days');
        $tomorrow = $tomorrow->format('Y-m-d H:i:s');
        $futuredays =  new Date('+7 days');
        $futuredays = $futuredays->format('Y-m-d H:i:s');
        
        // azioni da compiere oggi
        $query = TableRegistry::get('Actions');
        $query = $query->find()
          ->where(function ($exp) use ($today, $tomorrow) {
                      return $exp->between('exec_date', $today, $tomorrow);
                  })
          ->andWhere(['is_action' => '1' ])
          ->andWhere(['user_id' => $loggeduserid])
          ->andWhere(['Actions.status' => 0])
          ->contain(['Customers', 'Quotations', 'Users']);
      
        $actions_list_todo = $query->all();
        $this->set(compact('actions_list_todo','actions_list_todo'));
        
        // azioni da compiere nei prossimi 7 giorni
        $query = TableRegistry::get('Actions');
        $query = $query->find()
          ->where(function ($exp) use ($tomorrow, $futuredays) {
                      return $exp->between('exec_date', $tomorrow, $futuredays);
                  })
          ->andWhere(['is_action' => '1' ])
          ->andWhere(['user_id' => $loggeduserid])
          ->andWhere(['Actions.status' => 0])
          ->contain(['Customers', 'Quotations', 'Users']);
      
        $actions_list_todo_7days = $query->all();
        $this->set(compact('actions_list_todo_7days','actions_list_todo_7days'));

        
        //FR=>2019-07-03=> Recupero l'elenco clienti che mi serve per popolare la modale con il più da dashboard
      
        $query = TableRegistry::get('Customers');
        $customers = $query->find('list');
        $this->set(compact('customers'));
      
      
        //FR=>2019-09-27 => Estraggo dalle impostazioni l'elenco delle azioni per visualizzare i pulsanti in homepage
      
        $query = TableRegistry::get('Settings');
        $query = $query->find('all')
        ->where(['type' => 'tipo azione' ]);
        $actions_type = $query->all();
        $this->set(compact('actions_type'));

        //FR=>2019-07-02=> Setto alcune variabili che mi servono
      
        if(!empty($loggeduser))
        $logged = true;    
      
        $this->set('loggeduser',$loggeduser);
        $this->set('role',$role);
        $this->set('loggeduseremail',$loggeduseremail);
        $this->set('logged',$logged);
        $this->set('source_quotation',$source_quotation);
        $this->set('loggeduserid',$loggeduserid);
        $this->set('action_type',$actionsTypeList);
        //$this->set('action_type',$actionsTypeList);
        $this->set('payments_type',$payments_type);

        // Allow the display action so our PagesController
        // continues to work. Also enable the read only actions.
        // $this->Auth->allow(['display', 'view', 'index']);
      
      /* FR - Fine parte relativa all'autenticazione */
    }

    public function orderStatusByUser($id = null)
    {
        $this->loadModel('OrderPhase1');
        $ordersToShow  = $this->OrderPhase1->find('all')
                        ->where(['phase_completed !=' => 1])
        ->contain(['OrdersCustomers' => ['Customers','Quotations']]);
        $ordersToShow = $ordersToShow->all();

        return $ordersToShow;
  
    }
  
      public function orderStatusByUserPhase2($id = null)
    {
        $this->loadModel('OrderPhase2');
        $ordersToShow2  = $this->OrderPhase2->find('all')
                        ->where(['phase_completed !=' => 1])
          //->contain(['OrdersCustomers']);
        ->contain(['OrdersCustomers' => ['Customers','Quotations']]);
        $ordersToShow2 = $ordersToShow2->all();

        $this->set(compact('ordersToShow2'));  
        //return $ordersToShow2;
  
    }
  
      public function orderStatusByUserPhase3($id = null)
    {
        $this->loadModel('OrderPhase3');
        $ordersToShow3  = $this->OrderPhase3->find('all')
                        ->where(['phase_completed !=' => 1])
          //->contain(['OrdersCustomers']);
        ->contain(['OrdersCustomers' => ['Customers','Quotations']]);
        $ordersToShow3 = $ordersToShow3->all();

        $this->set(compact('ordersToShow3'));  
        //return $ordersToShow3;
  
    }
  
  
  
  
}
