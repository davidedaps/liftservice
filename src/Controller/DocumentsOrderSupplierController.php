<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * DocumentsOrderSupplier Controller
 *
 * @property \App\Model\Table\DocumentsOrderSupplierTable $DocumentsOrderSupplier
 *
 * @method \App\Model\Entity\DocumentsOrderSupplier[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DocumentsOrderSupplierController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['OrderSuppliers', 'DocumentsLists']
        ];
        $documentsOrderSupplier = $this->paginate($this->DocumentsOrderSupplier);

        $this->set(compact('documentsOrderSupplier'));
    }

    /**
     * View method
     *
     * @param string|null $id Documents Order Supplier id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $documentsOrderSupplier = $this->DocumentsOrderSupplier->get($id, [
            'contain' => ['OrderSuppliers', 'DocumentsLists']
        ]);

        $this->set('documentsOrderSupplier', $documentsOrderSupplier);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $documentsOrderSupplier = $this->DocumentsOrderSupplier->newEntity();
        if ($this->request->is('post')) {
            $documentsOrderSupplier = $this->DocumentsOrderSupplier->patchEntity($documentsOrderSupplier, $this->request->getData());
            if ($this->DocumentsOrderSupplier->save($documentsOrderSupplier)) {
                $this->Flash->success(__('The documents order supplier has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The documents order supplier could not be saved. Please, try again.'));
        }
        $orderSuppliers = $this->DocumentsOrderSupplier->OrderSuppliers->find('list', ['limit' => 200]);
        $documentsLists = $this->DocumentsOrderSupplier->DocumentsLists->find('list', ['limit' => 200]);
        $this->set(compact('documentsOrderSupplier', 'orderSuppliers', 'documentsLists'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Documents Order Supplier id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $documentsOrderSupplier = $this->DocumentsOrderSupplier->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $documentsOrderSupplier = $this->DocumentsOrderSupplier->patchEntity($documentsOrderSupplier, $this->request->getData());
            if ($this->DocumentsOrderSupplier->save($documentsOrderSupplier)) {
                $this->Flash->success(__('The documents order supplier has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The documents order supplier could not be saved. Please, try again.'));
        }
        $orderSuppliers = $this->DocumentsOrderSupplier->OrderSuppliers->find('list', ['limit' => 200]);
        $documentsLists = $this->DocumentsOrderSupplier->DocumentsLists->find('list', ['limit' => 200]);
        $this->set(compact('documentsOrderSupplier', 'orderSuppliers', 'documentsLists'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Documents Order Supplier id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $documentsOrderSupplier = $this->DocumentsOrderSupplier->get($id);
        if ($this->DocumentsOrderSupplier->delete($documentsOrderSupplier)) {
            $this->Flash->success(__('The documents order supplier has been deleted.'));
        } else {
            $this->Flash->error(__('The documents order supplier could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
