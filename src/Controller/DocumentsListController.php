<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * DocumentsList Controller
 *
 * @property \App\Model\Table\DocumentsListTable $DocumentsList
 *
 * @method \App\Model\Entity\DocumentsList[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DocumentsListController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
      
        $documentsList = $this->documentsList->find('all');
        $documentsList = $documentsList->all();

        $this->set(compact('documentsList'));
    }

    /**
     * View method
     *
     * @param string|null $id Documents List id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $documentsList = $this->DocumentsList->get($id, [
            'contain' => ['DocumentsOrderCustomer', 'DocumentsOrderSupplier']
        ]);

        $this->set('documentsList', $documentsList);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $documentsList = $this->DocumentsList->newEntity();
        if ($this->request->is('post')) {
            $documentsList = $this->DocumentsList->patchEntity($documentsList, $this->request->getData());
            if ($this->DocumentsList->save($documentsList)) {
                $this->Flash->success(__('The documents list has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The documents list could not be saved. Please, try again.'));
        }
        $this->set(compact('documentsList'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Documents List id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $documentsList = $this->DocumentsList->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $documentsList = $this->DocumentsList->patchEntity($documentsList, $this->request->getData());
            if ($this->DocumentsList->save($documentsList)) {
                $this->Flash->success(__('The documents list has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The documents list could not be saved. Please, try again.'));
        }
        $this->set(compact('documentsList'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Documents List id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $documentsList = $this->DocumentsList->get($id);
        if ($this->DocumentsList->delete($documentsList)) {
            $this->Flash->success(__('The documents list has been deleted.'));
        } else {
            $this->Flash->error(__('The documents list could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
