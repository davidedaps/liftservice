<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
use Cake\Mailer\Email;
/**
 * OrderPhase1 Controller
 *
 * @property \App\Model\Table\OrderPhase1Table $OrderPhase1
 *
 * @method \App\Model\Entity\OrderPhase1[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class OrderPhase1Controller extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
  
    public function dataConsegna($id = null)
    {
      $data = $this->request->getData();
      $datacompleta = $data['registered']['year']."-".$data['registered']['month']."-".$data['registered']['day'];
      $id = $data["orderid"];
      $tablename = TableRegistry::get("OrdersCustomers");
      $query = $tablename->query();
      $result = $query->update()
                    ->set(['expected_delivery_date' => $datacompleta])
                    ->where(['id' => $id])
                    ->execute();
      
      
      return $this->redirect(['action' => 'view', $id]);
    }
  
  
  public function orderStatus($id = null)
    {
     
        $ordersToShow  = $this->OrderPhase1->find('all')
                        ->where(['phase_completed !=' => 1])
          //->contain(['OrdersCustomers']);
        ->contain(['OrdersCustomers' => ['Customers','Quotations']]);
        $ordersToShow = $ordersToShow->all();

        $this->set(compact('ordersToShow'));    
    
//          $this->loadModel('OrderPhase2');
    
//         $customersDetail = TableRegistry::get('Customers');
//         $customersDetailQuery= $customersDetail->find('all');

//         $customersDetailList = $customersDetailQuery->toArray();
//         $this->set(compact('customersDetailList'));
    
  
    }
  
   public function orderStatusByUser($id = null)
    {
     
        $ordersToShow  = $this->OrderPhase1->find('all')
                        ->where(['phase_completed !=' => 1])
          //->contain(['OrdersCustomers']);
        ->contain(['OrdersCustomers' => ['Customers','Quotations']]);
        $ordersToShow = $ordersToShow->all();

        $this->set(compact('ordersToShow'));    
    
//          $this->loadModel('OrderPhase2');
    
//         $customersDetail = TableRegistry::get('Customers');
//         $customersDetailQuery= $customersDetail->find('all');

//         $customersDetailList = $customersDetailQuery->toArray();
//         $this->set(compact('customersDetailList'));
    
  
    }
  
  
    public function index()
    {
        $orderPhase1 = $this->paginate($this->OrderPhase1);

        $this->set(compact('orderPhase1'));
    }

    /**
     * View method
     *
     * @param string|null $id Order Phase1 id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
      /*** FASE 1 ****/
        $orderPhase1 = $this->OrderPhase1->get($id, [
            'contain' => []
        ]);

        $this->set('orderPhase1', $orderPhase1);
      
    
      
      
      //FR=>2020-08-10 mi leggo dalle impostazioni i tipi cartella da associare agli uploads
      
      $folder_list = TableRegistry::get('Settings');
      
       $folder_listQuery= $folder_list->find('list', [
        'keyField' => 'description',
        'valueField' => 'description'
        ])
        ->where(['type' => 'tipologia cartella']);
      
//       $folder_listQuery = $folder_list->find('all')
//       ->where(['type' => 'tipologia cartella']);
      
      $folder_list = $folder_listQuery->toArray();
      
      $this->set(compact('folder_list'));
      
       //FR=>2020-08-10 mi leggo dagli uploads i file legati all'ordine per mostrarli nell'elenco uploads
      
      $uploadsList = TableRegistry::get('Uploads');
      $uploadsListQuery= $uploadsList->find('all')
                              ->where(['order_customer_id =' => $id]);

      $uploadsList = $uploadsListQuery->all();
      $this->set(compact('uploadsList'));
      
      
      $folder_name = TableRegistry::get("Settings");
      $folder_nameQuery= $folder_name->find('list', [
      'keyField' => 'id',
      'valueField' => 'description'
      ])
      ->where(['type' => 'tipologia cartella']);

      $folder_name = $folder_nameQuery->all();
      $this->set(compact('folder_name'));
      
              //FR => 2022-07-26 - estraggo l'elenco utennti per assegnarli all'azione
        
        $users = TableRegistry::get('Users');
        $usersQuery = $users->find('all');

        $usersValues = $usersQuery->all();
        $this->set(compact('usersValues'));
        //pr($usersValues);
      
        //FR=>2020-02-14-estraggo i dati dalla testa dell'ordine per mostrare alcuni campi
      
      $orderDetail = TableRegistry::get('Orders_customers');
      $orderDetailQuery= $orderDetail->find('all')
                              ->where(['id =' => $id]);

      $orderDetailValues = $orderDetailQuery->first();
      $this->set(compact('orderDetailValues'));
      
      //FR=>2020-02-14-estraggo anche il cliente  per mostrare alcuni campi
      
      $orderCustomer = TableRegistry::get('Customers');
      $orderCustomerQuery= $orderCustomer->find('all')
                      ->where(['id =' => $orderDetailValues->customer_id]);

      $orderCustomerValues = $orderCustomerQuery->first();
      $this->set(compact('orderCustomerValues'));
      
      //FR=>2020-02-14-estraggo anche il relativo preventivo per mostrare alcuni campi
      
      $orderQuotation = TableRegistry::get('Quotations');
      $orderQuotationQuery= $orderQuotation->find('all')
                      ->where(['order_number =' => $id]);

      $orderQuotationValues = $orderQuotationQuery->first();
      $this->set(compact('orderQuotationValues'));
      
      //FR=>2020-02.18 - estraggo i dati dei pagamenti cliente relativi all'ordine

        $orderPayment = TableRegistry::get('PaymentsCustomers');
        $orderPaymentQuery = $orderPayment->find('all')
        ->where(['order_customer_id' => $id]);
         
        $orderPaymentList = $orderPaymentQuery->all();
        $this->set(compact('orderPaymentList'));
      
      //FR=>2020-02-25 - estraggo i dati dell' ordine fornitore legati all'ordine cliente

      $checkOrderSupplier = false;
      
      $orderSupplier = TableRegistry::get('OrdersSuppliers');
      $orderSupplierQuery= $orderSupplier->find('all')
      ->where(['orders_customer_id =' => $id]);
      //->where(['order_number =' => $orderDetailValues->order_number]);
      

      /*$orderSupplierValues = $orderSupplierQuery->first();
    
      if(is_null($orderSupplierValues)) 
      {
        //echo "1 if";
        $checkOrderSupplier = false;
        $orderSupplierValues ="";
      }
      else
      {
        $checkOrderSupplier = true;
      }

      $this->set(compact('orderSupplierValues'));  
      
       //FR=>2020-02-25 - estraggo i dati dei pagamenti degli ordini fornitore legati all'ordine cliente
        pr($checkOrderSupplier);
      if($checkOrderSupplier == true)
      {*/
    
      /*$orderPaymentSupplier = TableRegistry::get('PaymentsSuppliers');

      $orderPaymentSupplierQuery = $orderPaymentSupplier->find('all')
      ->where(['order_supplier_id' => $orderSupplierValues->id]);

      $orderPaymentSupplierList = $orderPaymentSupplierQuery->all();*/
        
      //pr($id);
      $orderPaymentSupplier = TableRegistry::get('PaymentsSuppliers');

      $orderPaymentSupplierQuery = $orderPaymentSupplier->find('all')
      ->where(['order_supplier_id' =>  $id]);

      //pr($orderPaymentSupplierQuery);
      $orderPaymentSupplierList = $orderPaymentSupplierQuery->all();  
      $this->set(compact('orderPaymentSupplierList'));
        
   /*     
      }
      
      else
      
      {
        $orderPaymentSupplierList = "";
      }

      $this->set(compact('orderPaymentSupplierList'));
      //pr($orderPaymentSupplierList);}*/
      
      
      //FR=>2020-06-26 - estraggo le descrizioni degli step fase 1 e me le metto in un array
      
      $descriptionPhase1 = TableRegistry::get('Settings');
      $descriptionPhase1Query = $descriptionPhase1->find('all')
      ->where(['type' => 'stepfase1']);
      
      $descriptionPhase1List = $descriptionPhase1Query->toArray();
      $this->set(compact('descriptionPhase1List'));
      //pr($orderPaymentSupplierList);
      
      /********************/
      /*** FINE FASE 1 ****/
      /********************/
      
      /**********************/
      /*** INIZIO FASE 2 ****/
      /**********************/
       
      $orderPhase2 = TableRegistry::get('OrderPhase2');
      $orderPhase2Query = $orderPhase2->find('all')
       ->where(['order_id' => $id]);
      
      $orderPhase2 = $orderPhase2Query->first();
      $this->set(compact('orderPhase2'));
      
      //pr($orderPhase2Query);
      
      //pr($orderPhase2List);
      
      $descriptionPhase2 = TableRegistry::get('Settings');
      $descriptionPhase2Query = $descriptionPhase2->find('all')
      ->where(['type' => 'stepfase2']);
      
      $descriptionPhase2List = $descriptionPhase2Query->toArray();
      $this->set(compact('descriptionPhase2List'));
      
      
      /**********************/
      /*** FINE FASE 2 ****/
      /**********************/
      
            /**********************/
      /*** INIZIO FASE 3****/
      /**********************/
       
      $orderPhase3 = TableRegistry::get('OrderPhase3');
      $orderPhase3Query = $orderPhase3->find('all')
       ->where(['order_id' => $id]);
      
      $orderPhase3 = $orderPhase3Query->first();
      $this->set(compact('orderPhase3'));
      
      //pr($orderPhase2Query);
      
      //pr($orderPhase2List);
      
      $descriptionPhase2 = TableRegistry::get('Settings');
      $descriptionPhase2Query = $descriptionPhase2->find('all')
      ->where(['type' => 'stepfase2']);
      
      $descriptionPhase2List = $descriptionPhase2Query->toArray();
      $this->set(compact('descriptionPhase2List'));
      
      
      /**********************/
      /*** FINE FASE 3 ****/
      /**********************/
      //FR=>2020-02-25 - calcolo i totali degli incassi e delle spese per mostrarli nella pagina dell'ordine cliente
      
      if($checkOrderSupplier = true)
      {
      $somma_spese = 0;
      $somma_incassi = 0;
        
      if($orderPaymentList !='')
      {      
         foreach ($orderPaymentList as $orderPayment)
         {
          $somma_incassi += $orderPayment->amount;
         }
      }
        
      if($orderPaymentSupplierList !='')
      {  
     foreach ($orderPaymentSupplierList as $orderPaymentSupplier)
     {
      $somma_spese += $orderPaymentSupplier->amount;
     }
      }
        
     $saldo = $somma_incassi - $somma_spese;
     $this->set(compact('somma_incassi','somma_spese','saldo'));
      
      }
      else
      {
        $saldo = "";
      }
      /**********/
      /* GESTIONE UPLOAD DOC */
      /**/
      
      $this->loadModel('Uploads');
      //$uploadOrder = TableRegistry::get('Uploads');
      
      $uploadOrder = $this->Uploads->newEntity();

        if ($this->request->is('post')) {
            // controllo che ci siano file da slavare, se non ci sno file caricati non salvo nulla nel DB
            $data = $this->request->getData();
            
            $uploadOrder = $this->Uploads->patchEntity($uploadOrder, $data);
   
            if ($this->Uploads->save($uploadOrder)) {
              
                $this->Flash->success(__('Documento salvato correttamente.'));

                    return $this->redirect
                    (
                    ['controller' => 'OrderPhase1', 'action' => 'view',$id]
                    );
                   
            }
            debug($uploadOrder->validationErrors);
            $this->Flash->error(__('Il documento non è stato salvato! Riprova!'));
        }
         $this->set(compact('uploadOrder'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($idOrder)
    {
     //FR -> vado a leggermi dalle opzioni gli step della fase 1
      
 
      $descriptionPhase1 = TableRegistry::get('Settings');
      $descriptionPhase1Query= $descriptionPhase1->find('all')
                              ->where(['type =' => 'stepfase1']);

      $descriptionPhase1List = $descriptionPhase1Query->toArray();
      $this->set(compact('descriptionPhase1List'));
      
      //FR - mi preparo un'po di campi da inserire
      
      $orderId = $idOrder;
      $phase_completed = 0;
      $numero = 0;

      //pr($descriptionPhase1List);

      $descriptionStep1 = $descriptionPhase1List[0]->description;
      
      //$step_1_created = Time::now();
      $step_1_completed = 0;
      
      $descriptionStep2 = $descriptionPhase1List[1]->description;
//       $step_2_created = $step_1_created;
//       $step_2_created = $step_2_created->modify('+'.$descriptionPhase1List[0]->timing.'days');
      $step_2_completed = 0;  
      
      $descriptionStep3= $descriptionPhase1List[2]->description;
//       $step_3_created = $step_2_created;
//       $step_3_created = $step_3_created->modify('+'.$descriptionPhase1List[1]->timing.'days');
      $step_3_completed = 0;  
      
      $descriptionStep4= $descriptionPhase1List[3]->description;
//       $step_4_created = $step_3_created;
//       $step_4_created = $step_4_created->modify('+'.$descriptionPhase1List[2]->timing.'days');
      $step_4_completed = 0; 
      
      $descriptionStep5= $descriptionPhase1List[4]->description;
//       $step_5_created = $step_4_created;     
//       $step_5_created = $step_5_created->modify('+'.$descriptionPhase1List[3]->timing.'days');
      $step_5_completed = 0; 
 
      
      //pr("1".$step_1_created."2".$step_2_created."3".$step_3_created."4".$step_4_created."5".$step_5_created);
      $orderPhase1 = $this->OrderPhase1->newEntity();
      
      $orderPhase1->order_id = $orderId;
      $orderPhase1->phase_completed = $phase_completed;
      
      $orderPhase1->description_step_1 = $descriptionStep1;
      //$orderPhase1->step_1_created = $step_1_created;
      $orderPhase1->step_1_completed= $step_1_completed;
        
      $orderPhase1->description_step_2 = $descriptionStep2;
      //$orderPhase1->step_2_created = $step_2_created;
      $orderPhase1->step_2_completed= $step_2_completed;
      
      $orderPhase1->description_step_3 = $descriptionStep3;
      //$orderPhase1->step_3_created = $step_3_created;
      $orderPhase1->step_3_completed= $step_3_completed;
      
      $orderPhase1->description_step_4 = $descriptionStep4;
      //$orderPhase1->step_4_created = $step_4_created;
      $orderPhase1->step_4_completed= $step_4_completed;
      
      $orderPhase1->description_step_5 = $descriptionStep5;
      //$orderPhase1->step_5_created = $step_5_created;
      $orderPhase1->step_5_completed= $step_5_completed;      
              
      if ($this->OrderPhase1->save($orderPhase1)) {
          $this->Flash->success(__('The order phase1 has been saved.'));

          //return $this->redirect(['action' => 'view']);
          return $this->redirect(['controller' => 'OrderPhase2', 'action' => 'add', $orderId]);
      }
      $this->Flash->error(__('The order phase1 could not be saved. Please, try again.'));

       $this->set(compact('orderPhase1'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Order Phase1 id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null,$step = null)
    {
     
        $orderPhase1 = $this->OrderPhase1->get($id, [
            'contain' => []
        ]);
      
        $this->set('step', $step);

      //FR => 2022-07-26 - estraggo l'elenco utennti per assegnarli all'azione
        
        $users = TableRegistry::get('Users');
        $usersQuery = $users->find('list')->toArray();
        $this->set(compact('usersQuery'));
      
        if ($this->request->is(['patch', 'post', 'put'])) {
            $orderPhase1 = $this->OrderPhase1->patchEntity($orderPhase1, $this->request->getData());
          
            if ($this->OrderPhase1->save($orderPhase1)) {
                $this->Flash->success(__('The order phase1 has been saved.'));

                return $this->redirect(['action' => 'view',$id]);
            }
            $this->Flash->error(__('The order phase1 could not be saved. Please, try again.'));
        }
        $this->set(compact('orderPhase1'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Order Phase1 id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $orderPhase1 = $this->OrderPhase1->get($id);
        if ($this->OrderPhase1->delete($orderPhase1)) {
            $this->Flash->success(__('The order phase1 has been deleted.'));
        } else {
            $this->Flash->error(__('The order phase1 could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
  
  
      public function update($id = null,$step1 = null)
    {
   
        $get_data = $this->request->getData();
        $orderPhase1 = $this->OrderPhase1->get($id, [
            'contain' => []
        ]);
      
        $this->set('step', $step1);

      $orderDetail = TableRegistry::get('Orders_customers');
      $orderDetailQuery= $orderDetail->find('all')
                              ->where(['id =' => $id]);

      $orderDetailValues = $orderDetailQuery->first();
      $this->set(compact('orderDetailValues'));
        
        
        
        
      $quoDetail = TableRegistry::get('Quotations');
      $quoDetailQuery= $quoDetail->find('all')
                              ->where(['id =' => $orderDetailValues->quotation_id]);

      $quoDetailValues =  $quoDetailQuery->first();
      $this->set(compact('quoDetailValues'));  
        
        
        
        
        
        if ($this->request->is(['patch', 'post', 'put'])) {
            $orderPhase1 = $this->OrderPhase1->patchEntity($orderPhase1, $this->request->getData());
          
          
      
          if (strcmp($get_data['confirm'], "CONFERMA") == 0) {
            //pr($get_data);
            if($get_data['step'] == 1)
            {
            $articlesTable = TableRegistry::getTableLocator()->get('OrderPhase1');
            $article = $articlesTable->get($id); // Return article with id 12

            $article->step_1_completed = '0';
            $article->step_1_completedtime = NULL;
            $article->step_1_created = NULL;
            $article->step_1_user = NULL;            
            $articlesTable->save($article);
                          return $this->redirect(['action' => 'view', $id]);
            }
            elseif($get_data['step'] == 2)
            {
            $articlesTable = TableRegistry::getTableLocator()->get('OrderPhase1');
            $article = $articlesTable->get($id); // Return article with id 12

            $article->step_2_completed = '0';
            $article->step_2_completedtime = NULL;
            $article->step_2_created = NULL;
            $article->step_2_user = NULL;            
            $articlesTable->save($article);
                          return $this->redirect(['action' => 'view', $id]);
            }
            elseif($get_data['step'] == 3)
            {
            $articlesTable = TableRegistry::getTableLocator()->get('OrderPhase1');
            $article = $articlesTable->get($id); // Return article with id 12

            $article->step_3_completed = '0';
            $article->step_3_completedtime = NULL;
            $article->step_3_created = NULL;
            $article->step_3_user = NULL;            
            $articlesTable->save($article);
                          return $this->redirect(['action' => 'view', $id]);
            }
            elseif($get_data['step'] == 4)
            {
            $articlesTable = TableRegistry::getTableLocator()->get('OrderPhase1');
            $article = $articlesTable->get($id); // Return article with id 12

            $article->step_4_completed = '0';
            $article->step_4_completedtime = NULL;
            $article->step_4_created = NULL;
            $article->step_4_user = NULL;            
            $articlesTable->save($article);
                          return $this->redirect(['action' => 'view', $id]);
            }
            elseif($get_data['step'] == 5)
            {
            $articlesTable = TableRegistry::getTableLocator()->get('OrderPhase1');
            $article = $articlesTable->get($id); // Return article with id 12

            $article->step_5_completed = '0';
            $article->step_5_completedtime = NULL;
            $article->step_5_created = NULL;
            $article->step_5_user = NULL;            
            $articlesTable->save($article);
                          return $this->redirect(['action' => 'view', $id]);
            }

            
          }
          else{
            $this->Flash->error(__('The order phase1 could not be saved. Please, try again.'));}
        }
        $this->set(compact('orderPhase1'));
    }
  
  
    public function execStep1($id= null)
    {
      $orderDetail = TableRegistry::get('Orders_customers');
      $orderDetailQuery= $orderDetail->find('all')
                              ->where(['id =' => $id]);

      $orderDetailValues = $orderDetailQuery->first();
      $this->set(compact('orderDetailValues'));
      $orderCustomer = TableRegistry::get('Customers');
      $orderCustomerQuery= $orderCustomer->find('all')
                      ->where(['id =' => $orderDetailValues->customer_id]);

      $orderCustomerValues = $orderCustomerQuery->first();
      $this->set(compact('orderCustomerValues'));
      
      //FR=>2020-02-14-estraggo anche il relativo preventivo per mostrare alcuni campi
      
      $orderQuotation = TableRegistry::get('Quotations');
      $orderQuotationQuery= $orderQuotation->find('all')
                      ->where(['order_number =' => $id]);

      $orderQuotationValues = $orderQuotationQuery->first();
      $this->set(compact('orderQuotationValues'));
      
      $datiOrdine = $orderCustomerValues->name. " - " .$orderQuotationValues->reference. " - " .$orderQuotationValues->address. " - " .$orderQuotationValues->city; 

              $orderPhase1 = $this->OrderPhase1->get($id, [
              'contain' => []
              ]);
      //$datiOrdine = $orderCustomerValues->name. " - " .$orderQuotationValues->reference. " - " .$orderQuotationValues->address. " - " .$orderQuotationValues->city; 
      //FR => 2020-30-06 - eseguo la query di upd per settare completato lo step 1
      
              $step1ToUpd = TableRegistry::get("OrderPhase1");
              $query = $step1ToUpd->query();
              $result = $query->update()
              ->set(['step_1_completed' => 1])
              ->where(['order_id' => $id])
              ->execute();
      
      //FR => 2020-30-06 - eseguo la query di upd per settare completato lo step 1
              $time = Time::now();
              $step1DateToUpd = TableRegistry::get("OrderPhase1");
              $query = $step1DateToUpd->query();
              $result = $query->update()
              ->set(['step_1_completedtime' => $time])
              ->where(['order_id' => $id])
              ->execute();
      
      //FR => 2023-12-09 - verifica: se l'utente che preme il tasto esegui azione è diverso da quello previsto, salvo l'informazione nelle note con data ora e utente effettivo
      //$this->Auth->user('username') 
             
      $loguser = $this->request->session()->read('Auth.User');
      if($loguser['id']!= $orderPhase1->step_1_user)
                {
                $time = Time::now();
                $note = $orderPhase1->step_1_notes;
                $note = $note." - Azione eseguita da:".$loguser['name']." in data: ".$time;  
                $step1CheckUserToUpd = TableRegistry::get("OrderPhase1");
                $query = $step1CheckUserToUpd->query();
                $result = $query->update()
                ->set(['step_1_notes' => $note])
                ->where(['order_id' => $id])
                ->execute();
                }
            
              return $this->redirect($this->referer());
  
   }
  
     public function execStep2($id= null)

    {

       
      $orderDetail = TableRegistry::get('Orders_customers');
      $orderDetailQuery= $orderDetail->find('all')
                              ->where(['id =' => $id]);

      $orderDetailValues = $orderDetailQuery->first();
      $this->set(compact('orderDetailValues'));
      $orderCustomer = TableRegistry::get('Customers');
      $orderCustomerQuery= $orderCustomer->find('all')
                      ->where(['id =' => $orderDetailValues->customer_id]);

      $orderCustomerValues = $orderCustomerQuery->first();
      $this->set(compact('orderCustomerValues'));
      
      //FR=>2020-02-14-estraggo anche il relativo preventivo per mostrare alcuni campi
      
      $orderQuotation = TableRegistry::get('Quotations');
      $orderQuotationQuery= $orderQuotation->find('all')
                      ->where(['order_number =' => $id]);

      $orderQuotationValues = $orderQuotationQuery->first();
      $this->set(compact('orderQuotationValues'));
      
      $datiOrdine = $orderCustomerValues->name. " - " .$orderQuotationValues->reference. " - " .$orderQuotationValues->address. " - " .$orderQuotationValues->city; 

              $orderPhase1 = $this->OrderPhase1->get($id, [
              'contain' => []
              ]);
     //$datiOrdine = $orderCustomerValues->name. " - " .$orderQuotationValues->reference. " - " .$orderQuotationValues->address. " - " .$orderQuotationValues->city; 
      //FR => 2020-30-06 - eseguo la query di upd per settare completato lo step 1
      
              $step1ToUpd = TableRegistry::get("OrderPhase1");
              $query = $step1ToUpd->query();
              $result = $query->update()
              ->set(['step_2_completed' => 1])
              ->where(['order_id' => $id])
              ->execute();
      
      //FR => 2020-30-06 - eseguo la query di upd per settare completato lo step 1
              $time = Time::now();
              $step1DateToUpd = TableRegistry::get("OrderPhase1");
              $query = $step1DateToUpd->query();
              $result = $query->update()
              ->set(['step_2_completedtime' => $time])
              ->where(['order_id' => $id])
              ->execute();
            
      $loguser = $this->request->session()->read('Auth.User');
      if($loguser['id']!= $orderPhase1->step_2_user)
                {
                $time = Time::now();
                $note = $orderPhase1->step_1_notes;
                $note = $note." - Azione eseguita da:".$loguser['name']." in data: ".$time;  
                $step1CheckUserToUpd = TableRegistry::get("OrderPhase1");
                $query = $step1CheckUserToUpd->query();
                $result = $query->update()
                ->set(['step_2_notes' => $note])
                ->where(['order_id' => $id])
                ->execute();
                }
       
       
                return $this->redirect($this->referer());
  
   }
  
   public function execStep3($id= null)
    {
      $orderDetail = TableRegistry::get('Orders_customers');
      $orderDetailQuery= $orderDetail->find('all')
                              ->where(['id =' => $id]);

      $orderDetailValues = $orderDetailQuery->first();
      $this->set(compact('orderDetailValues'));
      $orderCustomer = TableRegistry::get('Customers');
      $orderCustomerQuery= $orderCustomer->find('all')
                      ->where(['id =' => $orderDetailValues->customer_id]);

      $orderCustomerValues = $orderCustomerQuery->first();
      $this->set(compact('orderCustomerValues'));
      
      //FR=>2020-02-14-estraggo anche il relativo preventivo per mostrare alcuni campi
      
      $orderQuotation = TableRegistry::get('Quotations');
      $orderQuotationQuery= $orderQuotation->find('all')
                      ->where(['order_number =' => $id]);

      $orderQuotationValues = $orderQuotationQuery->first();
      $this->set(compact('orderQuotationValues'));
      
      $datiOrdine = $orderCustomerValues->name. " - " .$orderQuotationValues->reference. " - " .$orderQuotationValues->address. " - " .$orderQuotationValues->city; 

              $orderPhase1 = $this->OrderPhase1->get($id, [
              'contain' => []
              ]);
     //$datiOrdine = $orderCustomerValues->name. " - " .$orderQuotationValues->reference. " - " .$orderQuotationValues->address. " - " .$orderQuotationValues->city; 
      //FR => 2020-30-06 - eseguo la query di upd per settare completato lo step 1
      
              $step1ToUpd = TableRegistry::get("OrderPhase1");
              $query = $step1ToUpd->query();
              $result = $query->update()
              ->set(['step_3_completed' => 1])
              ->where(['order_id' => $id])
              ->execute();
      
      //FR => 2020-30-06 - eseguo la query di upd per settare completato lo step 1
              $time = Time::now();
              $step1DateToUpd = TableRegistry::get("OrderPhase1");
              $query = $step1DateToUpd->query();
              $result = $query->update()
              ->set(['step_3_completedtime' => $time])
              ->where(['order_id' => $id])
              ->execute();
   
  
      $loguser = $this->request->session()->read('Auth.User');
      if($loguser['id']!= $orderPhase1->step_3_user)
                {
                $time = Time::now();
                $note = $orderPhase1->step_3_notes;
                $note = $note." - Azione eseguita da:".$loguser['name']." in data: ".$time;  
                $step1CheckUserToUpd = TableRegistry::get("OrderPhase1");
                $query = $step1CheckUserToUpd->query();
                $result = $query->update()
                ->set(['step_3_notes' => $note])
                ->where(['order_id' => $id])
                ->execute();
                }
            
                return $this->redirect($this->referer());
  
   }
   public function execStep4($id= null)
    {
            $orderDetail = TableRegistry::get('Orders_customers');
      $orderDetailQuery= $orderDetail->find('all')
                              ->where(['id =' => $id]);

      $orderDetailValues = $orderDetailQuery->first();
      $this->set(compact('orderDetailValues'));
      $orderCustomer = TableRegistry::get('Customers');
      $orderCustomerQuery= $orderCustomer->find('all')
                      ->where(['id =' => $orderDetailValues->customer_id]);

      $orderCustomerValues = $orderCustomerQuery->first();
      $this->set(compact('orderCustomerValues'));
      
      //FR=>2020-02-14-estraggo anche il relativo preventivo per mostrare alcuni campi
      
      $orderQuotation = TableRegistry::get('Quotations');
      $orderQuotationQuery= $orderQuotation->find('all')
                      ->where(['order_number =' => $id]);

      $orderQuotationValues = $orderQuotationQuery->first();
      $this->set(compact('orderQuotationValues'));
      
      $datiOrdine = $orderCustomerValues->name. " - " .$orderQuotationValues->reference. " - " .$orderQuotationValues->address. " - " .$orderQuotationValues->city; 

              $orderPhase1 = $this->OrderPhase1->get($id, [
              'contain' => []
              ]);
     
      $orderDetail = TableRegistry::get('Orders_customers');
      $orderDetailQuery= $orderDetail->find('all')
                              ->where(['id =' => $id]);

      $orderDetailValues = $orderDetailQuery->first();
      $this->set(compact('orderDetailValues'));
      $orderCustomer = TableRegistry::get('Customers');
      $orderCustomerQuery= $orderCustomer->find('all')
                      ->where(['id =' => $orderDetailValues->customer_id]);

      $orderCustomerValues = $orderCustomerQuery->first();
      $this->set(compact('orderCustomerValues'));
      
      //FR=>2020-02-14-estraggo anche il relativo preventivo per mostrare alcuni campi
      
      $orderQuotation = TableRegistry::get('Quotations');
      $orderQuotationQuery= $orderQuotation->find('all')
                      ->where(['order_number =' => $id]);

      $orderQuotationValues = $orderQuotationQuery->first();
      $this->set(compact('orderQuotationValues'));
      
      $datiOrdine = $orderCustomerValues->name. " - " .$orderQuotationValues->reference. " - " .$orderQuotationValues->address. " - " .$orderQuotationValues->city; 

      //FR => 2020-30-06 - eseguo la query di upd per settare completato lo step 1
      
              $step1ToUpd = TableRegistry::get("OrderPhase1");
              $query = $step1ToUpd->query();
              $result = $query->update()
              ->set(['step_4_completed' => 1])
              ->where(['order_id' => $id])
              ->execute();
      
      //FR => 2020-30-06 - eseguo la query di upd per settare completato lo step 1
              $time = Time::now();
              $step1DateToUpd = TableRegistry::get("OrderPhase1");
              $query = $step1DateToUpd->query();
              $result = $query->update()
              ->set(['step_4_completedtime' => $time])
              ->where(['order_id' => $id])
              ->execute();
   
  
    //FR=> 2020-30-06 - invio la mail per notifdicare l'esecuzione dello step1
                    $loguser = $this->request->session()->read('Auth.User');
      if($loguser['id']!= $orderPhase1->step_4_user)
                {
                $time = Time::now();
                $note = $orderPhase1->step_4_notes;
                $note = $note." - Azione eseguita da:".$loguser['name']." in data: ".$time;  
                $step1CheckUserToUpd = TableRegistry::get("OrderPhase1");
                $query = $step1CheckUserToUpd->query();
                $result = $query->update()
                ->set(['step_4_notes' => $note])
                ->where(['order_id' => $id])
                ->execute();
                }
                return $this->redirect($this->referer());
  
   }
   public function execStep5($id= null)
    {
      
              $orderPhase1 = $this->OrderPhase1->get($id, [
              'contain' => []
              ]);
     
      $orderDetail = TableRegistry::get('Orders_customers');
      $orderDetailQuery= $orderDetail->find('all')
                              ->where(['id =' => $id]);

      $orderDetailValues = $orderDetailQuery->first();
      $this->set(compact('orderDetailValues'));
      $orderCustomer = TableRegistry::get('Customers');
      $orderCustomerQuery= $orderCustomer->find('all')
                      ->where(['id =' => $orderDetailValues->customer_id]);

      $orderCustomerValues = $orderCustomerQuery->first();
      $this->set(compact('orderCustomerValues'));
      
      //FR=>2020-02-14-estraggo anche il relativo preventivo per mostrare alcuni campi
      
      $orderQuotation = TableRegistry::get('Quotations');
      $orderQuotationQuery= $orderQuotation->find('all')
                      ->where(['order_number =' => $id]);

      $orderQuotationValues = $orderQuotationQuery->first();
      $this->set(compact('orderQuotationValues'));
      
      $datiOrdine = $orderCustomerValues->name. " - " .$orderQuotationValues->reference. " - " .$orderQuotationValues->address. " - " .$orderQuotationValues->city; 

      //FR => 2020-30-06 - eseguo la query di upd per settare completato lo step 1
      
              $step1ToUpd = TableRegistry::get("OrderPhase1");
              $query = $step1ToUpd->query();
              $result = $query->update()
              ->set(['step_5_completed' => 1])
              ->where(['order_id' => $id])
              ->execute();
      
      //FR => 2020-30-06 - eseguo la query di upd per settare completato lo step 1
              $time = Time::now();
              $step1DateToUpd = TableRegistry::get("OrderPhase1");
              $query = $step1DateToUpd->query();
              $result = $query->update()
              ->set(['step_5_completedtime' => $time])
              ->where(['order_id' => $id])
              ->execute();
     
              $time = Time::now();
              $step1DateToUpd = TableRegistry::get("OrderPhase1");
              $query = $step1DateToUpd->query();
              $result = $query->update()
              ->set(['phase_completed' => 1])
              ->where(['order_id' => $id])
              ->execute();
   
  
      $loguser = $this->request->session()->read('Auth.User');
      if($loguser['id']!= $orderPhase1->step_5_user)
                {
                $time = Time::now();
                $note = $orderPhase1->step_5_notes;
                $note = $note." - Azione eseguita da:".$loguser['name']." in data: ".$time;  
                $step1CheckUserToUpd = TableRegistry::get("OrderPhase1");
                $query = $step1CheckUserToUpd->query();
                $result = $query->update()
                ->set(['step_5_notes' => $note])
                ->where(['order_id' => $id])
                ->execute();
                }
            
                return $this->redirect($this->referer());
  
   }

}
