<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
/**
 * OrdersSuppliers Controller
 *
 * @property \App\Model\Table\OrdersSuppliersTable $OrdersSuppliers
 *
 * @method \App\Model\Entity\OrdersSupplier[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class OrdersSuppliersController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {    
      
      //$ordersSuppliers = $this->OrdersSuppliers->find('all');
      
//         $ordersSuppliers = $this->OrdersSuppliers->find('all'), [
//             'contain' => ['QuotationsSuppliers' => 'Suppliers']
//         ]);
        
//         $ordersSuppliers = $ordersSuppliers->toArray();
//         $this->set(compact('ordersSuppliers'));
      
       $ordersSuppliers = $this->OrdersSuppliers->find('all',[
            'contain' => ['QuotationsSuppliers' => 'Suppliers']
        ]);
        $ordersSuppliers = $ordersSuppliers->toArray();
        $this->set(compact('ordersSuppliers'));
    }

    /**
     * View method
     *
     * @param string|null $id Orders Supplier id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $ordersSupplier = $this->OrdersSuppliers->get($id, [
            'contain' => ['Quotations']
        ]);

        $this->set('ordersSupplier', $ordersSupplier);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($id = null)
    {
        //FR - aggiungo la trasformazione da preventivo a ordine
        $quotationsToAdd = TableRegistry::get('QuotationsSuppliers');
        //$quotationsToAdd = $quotationsToAdd->get($id);

        $quotationsToAdd = $quotationsToAdd->get($id, [
        'contain' => ['Suppliers']
        ]);
        $this->set(compact('quotationsToAdd'));
      
         //FR=>2020-02-20 - mi vado a prendere l'id dell'ordine cliente che è stato generato dal preventivo cliente associato al preventivo fornitore

        $orderCustomer = TableRegistry::get('OrdersCustomers');
        $orderCustomerQuery= $orderCustomer->find('all')
        ->where(['quotation_id' => $quotationsToAdd->quotation_id]);
      
        $orderCustomerList = $orderCustomerQuery->first();
        $this->set(compact('orderCustomerList'));
      
        //pr($orderCustomerList);
      
        //FR - mi preparo qualche campo utile
        $fornitoreId = $quotationsToAdd->supplier->id;
        $fornitoreNome = $quotationsToAdd->supplier->name;
        $idOrdineFornitore = $quotationsToAdd->id;
      
        //pr($idOrdineFornitore);
      
        $ordersSupplier = $this->OrdersSuppliers->newEntity();

        if ($this->request->is('post')) 
          {
            $ordersSupplier = $this->OrdersSuppliers->patchEntity($ordersSupplier, $this->request->getData());

            if ($this->OrdersSuppliers->save($ordersSupplier))

            {
                $this->Flash->success(__('The orders supplier has been saved.'));   
              
              //FR -> 2019-07-18 - dopo la trasformazione in ordine, setto il preventivo come archiviato e scrivo il numero ordine nel campo 'order_number' delle quotations
              $quotationToUpd = TableRegistry::get("QuotationsSuppliers");
              $query = $quotationToUpd->query();
              $result = $query->update()
              ->set(['archived' => 1])
              //->set(['order_number' => $ordersCustomer->id])
              ->where(['id' => $quotationsToAdd->id])
              ->execute();

                  //FR -> chiamo la funzione che valorizza la Phase1 dell'ordine.
                  
                  //$this->SetOrderPhase1($ordersCustomer->id);
                 
                  return $this->redirect(['action' => 'index']); 
                  
                  //return $this->redirect(['controller' => 'OrderPhase1', 'action' => 'add', $ordersCustomer->id]);
                
              //FR - fine parte salvataggio cliente

            }

            $this->Flash->error(__('The orders supplier could not be saved. Please, try again.'));

        }
      
        
         
        $quotations = $this->OrdersSuppliers->Quotations->find('list');
        //$this->set(compact('ordersSupplier', 'quotations', 'lifts','quotationsToAdd'));
        $this->set(compact('ordersSupplier', 'quotations', 'quotationsToAdd'));
              
    }
    //}

    /**
     * Edit method
     *
     * @param string|null $id Orders Supplier id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $ordersSupplier = $this->OrdersSuppliers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $ordersSupplier = $this->OrdersSuppliers->patchEntity($ordersSupplier, $this->request->getData());
            if ($this->OrdersSuppliers->save($ordersSupplier)) {
                $this->Flash->success(__('The orders supplier has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The orders supplier could not be saved. Please, try again.'));
        }
        $quotations = $this->OrdersSuppliers->Quotations->find('list', ['limit' => 200]);
        $this->set(compact('ordersSupplier', 'quotations'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Orders Supplier id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $ordersSupplier = $this->OrdersSuppliers->get($id);
        if ($this->OrdersSuppliers->delete($ordersSupplier)) {
            $this->Flash->success(__('The orders supplier has been deleted.'));
        } else {
            $this->Flash->error(__('The orders supplier could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
