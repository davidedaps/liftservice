<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Controller\ActionsController;
use App\Controller\UploadsController;
use App\Controller\ProfessionalsController;
use Cake\ORM\TableRegistry;
/**
 * Quotations Controller
 *
 * @property \App\Model\Table\QuotationsTable $Quotations
 *
 * @method \App\Model\Entity\Quotation[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class QuotationsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $quotations = $this->Quotations->find('all')
        ->where(['archived !=' => 1])
        ->order(['quotation_date' => 'DESC' ])
        ->contain(['Customers', 'Professionals']);
        $quotations = $quotations->toArray();

        //pr($quotations);
      
        $this->set(compact('quotations'));

        $quotationStatus = TableRegistry::get('Settings');
        $quotationStatusQuery= $quotationStatus->find('all')
        ->where(['type' => 'stato preventivo']);

        $quotationStatusList = $quotationStatusQuery->toArray();
        $this->set(compact('quotationStatusList'));

      
        
      // Mi estraggo l'elenco dei professionisti per mostrarli
      
        $professionals = TableRegistry::get("Professionals");
        $professionalsQuery= $professionals->find('list', [
        'keyField' => 'id',
        'valueField' => 'full_name'
        ]);

        $professionalsList = $professionalsQuery->toArray();
        $this->set(compact('professionalsList'));
      
    }
  
    public function archived($id = null)
    {
       //$quotation = $this->Quotations->get($id);
      
        if ($this->request->is(['patch', 'post', 'put'])) {
            // controllo che ci siano file da slavare, se non ci sno file caricati non salvo nulla nel DB
            $data = $this->request->getData();
//             if($data['uploads']['0']['error'] == 4) 
//                unset($data['uploads']);
//             $quotation = $this->Quotations->patchEntity($quotation, $data, ['associated' => ['Uploads']]);
            
//             if ($this->Quotations->save($quotation)) {
//                 $this->Flash->success(__('Preventivo salvato correttamente.'));

//                 return $this->redirect(['action' => 'view', $id]);
//             }
            $this->Flash->error(__('Il preventivo non è stato salvato! Riprova!'));
        }
      
      
        if (isset($id)) 
        {
              $quotationToUpd = TableRegistry::get("Quotations");
              $query = $quotationToUpd->query();
              $result = $query->update()
              ->set(['archived' => 1])
              ->where(['id' => $id])
              ->execute();
        }

        $quotations = $this->Quotations->find('all')
            ->where(['archived =' => 1])
            ->order(['quotation_date' => 'DESC' ])
            ->contain(['Customers']);
        $quotations = $quotations->all();

        $this->set(compact('quotations'));
//                   if ($this->Quotations->save($quotation)) {
//                 $this->Flash->success(__('Preventivo salvato correttamente.'));

//                 return $this->redirect(['action' => 'view', $id]);
//             }
            $this->Flash->error(__('Il preventivo non è stato salvato! Riprova!'));
    }
  
    /**
     * View method
     *
     * @param string|null $id Quotation id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $quotation = $this->Quotations->get($id, [
            'contain' => ['Customers', 'Actions', 'OrdersCustomers','Professionals']
        ]);
      
        $this->set('quotation', $quotation);

//               //FR - aggiungo i tipi di azione dai settings
//         $quotationStatusV = TableRegistry::get('Settings');
//         $quotationStatusQueryV= $quotationStatusV->find('all')
//          ->where(['id' => $quotation->status]);
//         $quotationStatusListV = $quotationStatusQueryV->first();
     
//         $this->set(compact('quotationStatusListV'));
      
      
        //FR -mi leggo dai settings i nomi delle cartelle
            
      $folder_list = TableRegistry::get('Settings');
      
       $folder_listQuery= $folder_list->find('list', [
        'keyField' => 'description',
        'valueField' => 'description'
        ])
        ->where(['type' => 'tipologia cartella']);
      
//       $folder_listQuery = $folder_list->find('all')
//       ->where(['type' => 'tipologia cartella']);
      
      $folder_list = $folder_listQuery->toArray();
      
      $this->set(compact('folder_list'));
      
  
         //FR - aggiungo le actions legate al preventivo cliente 
        $actions = TableRegistry::get('Actions');
        $actionsQuery= $actions->find('all')
          ->where(['quotation_id' => ($id)])
          ->andWhere(['is_action' => 0])
          ->contain('Users')
          ->order(['exec_date' => 'DESC']);
         
        $actionsList = $actionsQuery->toArray();
        $this->set(compact('actionsList'));
      
        //estraggo la descrizione del tipo azione dalle impostazioni
        
        $description_TypeAction = TableRegistry::get("Settings");
        $description_TypeActionQuery= $description_TypeAction->find('list', [
        'keyField' => 'id',
        'valueField' => 'description'
        ])
        ->where(['type' => 'tipo azione']);

        $description_TypeActionList = $description_TypeActionQuery->toArray();
        $this->set(compact('description_TypeAction'));
     
        //fine estrazione descrizione
        
        //cerco gli uploads assocaiti al preventivo corrente
        $uploads = TableRegistry::get('Uploads');
        $uploads = $uploads->find('all')
          ->where(['quotation_id' => ($id)])
          ->order(['filename' => 'ASC']);
 
        $uploads = $uploads->all();
        $this->set(compact('uploads'));
      
        //carico l'anagrafica professionisti assocaiti al preventivo corrente
        $professionals = TableRegistry::get('Professionals');
        $professionals = $professionals->find('all')
          ->where(['id' => ($quotation->professional_1)]);
 
        $professionals = $professionals->first();
        $this->set(compact('professionals'));

      
              //carico l'anagrafica professionisti assocaiti al preventivo corrente
        $professionals2 = TableRegistry::get('Professionals');
        $professionals2 = $professionals2->find('all')
          ->where(['id' => ($quotation->professional_2)]);
 
        $professionals2 = $professionals2->first();
        $this->set(compact('professionals2'));
      


        // cerco tutte le azioni da svolgere e le metto in ordine di scadenza
        $actions_all = TableRegistry::get('Actions');
        $loggeduserid = $this->Auth->user('id');
        $actions_all = $actions_all->find('all')
          ->where(['is_action' => '1' ])
          //->andWhere(['user_id' => $loggeduserid])
          ->andWhere(['Actions.status' => 0])
          ->andWhere(['quotation_id' => ($id)])
          ->order(['exec_date' => 'DESC']);
        $actions_all = $actions_all->all();
        $this->set(compact('actions_all'));
      
      
       
        // recupero gli utenti per mostrare il nome
        $actionsUsers = TableRegistry::get('Actions');
        $users = $actionsUsers->Users->find('list')->toArray();
        $this->set(compact('users'));
      
      
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($customer_id = null)
    {
        $this->loadModel('Uploads');
      
        $quotation = $this->Quotations->newEntity();
      
        $upload = $this->Uploads->newEntity();
        
        $customers = $this->Quotations->Customers->find('list');
//         $suppliers = $this->Quotations->Suppliers->find('list', ['limit' => 200]);
        if (isset($customer_id)) 
        {
          $customer_detail = $this->Quotations->Customers->find('list')
                                  ->where(['Customers.id' => $customer_id])
                                  ->toArray();
        }
       
       //mostro i professionisti da associare al preventivo corrente
        $professionals = TableRegistry::get('Professionals');
        $professionals = $professionals->find('list')
          ->toArray();
        //$professionals = $professionals->toArray();
        $this->set(compact('professionals'));
      
       //FR - aggiungo i tipi di azione dai settings
        $payments = TableRegistry::get('Settings');
        $paymentsQuery= $payments->find('list')
          ->where(['type' => 'tipo azione'])
          ->order(['description' => 'DESC']);
         
        $paymentsQueryList = $paymentsQuery->toList();
        $this->set(compact('paymentsQueryList'));
      
        $quotationStatusList = [ 'quasi vinto breve periodo' => 'quasi vinto breve periodo',
              'in corso' => 'in corso' ,
              'probabile vinto' => 'probabile vinto',
              'non prevedibile' => 'non prevedibile' ,
              'quasi perso' => 'quasi perso' ,
              'chiuso vinto' => 'chiuso vinto' ,
              'chiuso perso' => 'chiuso perso' ,
              'in preparazione' => 'in preparazione'] ;
      
      
      
       //$this->set(compact('quotation', 'customers', 'suppliers', 'customer_id', 'customer_detail','quotationStatusList'));
       //$this->set(compact('quotation', 'customers', 'customer_id','quotationStatusList','customer_detail'));
       //$this->set(compact('quotation', 'customers', 'customer_id','quotationStatusList','customer_detail'));
       $this->set(compact('quotation', 'customers', 'customer_id','quotationStatusList'));
      
        if ($this->request->is('post')) 
        {
          $data = $this->request->getData();
          
          
//             // controllo che ci siano file da slavare, se non ci sno file caricati non salvo nulla nel DB
//             $data = $this->request->getData();
//             $quotation = $this->Quotations->patchEntity($quotation, $data);
      
               
//             if ($this->Quotations->save($quotation)) {
              
              
//                 $this->Flash->success(__('Preventivo salvato correttamente.'));

// // FR - cerco l'ultimo id del preventivo inserito per poter fare redirect diretto alla add del memo.             
// //                 $result = $this->Quotation>save($this->request->data);
// //                 $insertedId = $result->id;
// //                 pr($insertedId);
              
//                     $id = $quotation->customer_id;
//                     $id_quotation = $quotation->id;
//                     return $this->redirect
//                     (
//                     ['controller' => 'Actions', 'action' => 'exec',$id,$id_quotation]
//                     );
                   
//             }
          
//             $this->Flash->error(__('Il preventivo non è stato salvato! Riprova!'));
          if(!empty($data['filename']['name']))
            {
            // controllo che ci siano file da slavare, se non ci sno file caricati non salvo nulla nel DB

            $upload = $this->Uploads->patchEntity($upload, $data);
   
            if ($this->Uploads->save($upload)) {
              
                $this->Flash->success(__('Documento caricato correttamente.'));

//                     return $this->redirect
//                     (
//                     ['controller' => 'Quotations', 'action' => 'view',$id]
//                     );
                   
            }
           else $this->Flash->error(__('Il documento non è stato caricato! Riprova!'));
            }
          
            $quotation = $this->Quotations->patchEntity($quotation, $data, ['associated' => ['Uploads']]);
        
            
            if ($this->Quotations->save($quotation)) {
                $this->Flash->success(__('Preventivo salvato correttamente.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Il preventivo non è stato salvato! Riprova!'));
               
        }
        $this->set(compact('quotation'));

        }
       

   

    /**
     * Edit method
     *
     * @param string|null $id Quotation id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
      $quotation = $this->Quotations->get($id);
      
      $this->loadModel('Uploads');
      //$uploadOrder = TableRegistry::get('Uploads');
      
      $uploadOrder = $this->Uploads->newEntity();
      
      $quotationStatusList = [ 'quasi vinto breve periodo' => 'quasi vinto breve periodo',
            'in corso' => 'in corso' ,
            'probabile vinto' => 'probabile vinto',
            'non prevedibile' => 'non prevedibile' ,
            'quasi perso' => 'quasi perso' ,
            'chiuso vinto' => 'chiuso vinto' ,
            'chiuso perso' => 'chiuso perso' ,
            'in preparazione' => 'in preparazione'] ;

      $this->set(compact('quotationStatusList'));
      
            
      $professionals = TableRegistry::get('Professionals');
      $professionals = $professionals->find('list')
        ->toArray();
      //$professionals = $professionals->toArray();
      $this->set(compact('professionals'));

      $data = $this->request->getData();


       $this->set(compact('uploadOrder'));
      

        if ($this->request->is(['patch', 'post', 'put'])) 
        {
            if(!empty($data['filename']['name']))
            {
            // controllo che ci siano file da slavare, se non ci sno file caricati non salvo nulla nel DB

            $uploadOrder = $this->Uploads->patchEntity($uploadOrder, $data);
   
            if ($this->Uploads->save($uploadOrder)) {
              
                $this->Flash->success(__('Documento caricato correttamente.'));

//                     return $this->redirect
//                     (
//                     ['controller' => 'Quotations', 'action' => 'view',$id]
//                     );
                   
            }
           else $this->Flash->error(__('Il documento non è stato caricato! Riprova!'));
            }
          
            $quotation = $this->Quotations->patchEntity($quotation, $data, ['associated' => ['Uploads']]);
        
            
            if ($this->Quotations->save($quotation)) {
                $this->Flash->success(__('Preventivo salvato correttamente.'));

                return $this->redirect(['action' => 'view', $id]);
            }
            $this->Flash->error(__('Il preventivo non è stato salvato! Riprova!'));
               
        }
        $this->set(compact('quotation'));
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $quotation = $this->Quotations->get($id);
        if ($this->Quotations->delete($quotation)) {
            $this->Flash->success(__('The quotation has been deleted.'));
        } else {
            $this->Flash->error(__('The quotation could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
