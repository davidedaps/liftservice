<?php

/**
 * Example listener which will change the upload folder and filename for an uploaded image
 * 
 * Should be in `src/Event`
 * 
 * @category Example
 * @package UploadFilenameListener.php
 * 
 * @author David Yell <neon1024@gmail.com>
 * @when 03/03/15
 *
 */

namespace App\Event;

use Cake\Event\Event;
use Cake\Event\EventListenerInterface;
use Cake\Utility\Text;
use Cake\Utility\Inflector;
use Proffer\Lib\ProfferPath;

class UploadFilenameListener implements EventListenerInterface
{
    public function implementedEvents()
    {
        return [
            'Proffer.afterPath' => 'change',
        ];
    }

    /**
     * Rename a file and change it's upload folder before it's processed
     *
     * @param Event $event The event class with a subject of the entity
     * @param ProfferPath $path
     * @return ProfferPath $path
     */
    public function change(Event $event, ProfferPath $path)
    {
        // Detect and select the right file extension
//         switch ($event->getSubject()->filename['type']) {
//             default:
//             case "image/jpeg":
//                 $ext = '.jpg';
//                 break;
//             case "image/png":
//                 $ext = '.png';
//                 break;
//             case "image/gif":
//                 $ext = '.gif';
//                 break;
//             case "application/pdf":
//                 $ext = '.pdf';
//                 break;
//         }
        // If a seed is set in the data already, we'll use that rather than make a new one each time we upload
        if (empty($event->getSubject()->dir)) {
            //$path->setSeed(date('Y-m-d-His'));
            //$path->setSeed($event->getSubject()->order_number);
            $path->setSeed($event->getSubject()->quotation_id);
        }
        // Create a new filename removing space
        //debug($event->getSubject());
        //$filename = strstr($event->getSubject()->filename['name'], "ù", "_");
      
        $filename= Text::slug($event->getSubject()->filename['name'], '_');

        $newFilename = $event->getSubject()->filename['name'];
      
      
        //$str = "converti' questo che fa ' schifo";
        $str = $event->getSubject()->filename['name'];
        $text= htmlspecialchars($str, ENT_QUOTES);
        $trim = str_replace("&#039;", "_", $text);
        $trim2 = str_replace("+", "_", $str);
        //$filename = $event->subject()->get('name');
        //$filename = $event->getSubject()->name;
        //$newFilename = strtr($filename, "ù", "_");
        // Change the filename in both the path to be saved, and in the entity data for saving to the db
      
        $path->setFilename($trim2);
        // Must return the modified path instance, so that things are saved in the right place

        return $path;
    }
}