<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     3.0.0
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\View;

use Cake\View\View;

/**
 * Application View
 *
 * Your application’s default view class
 *
 * @link https://book.cakephp.org/3.0/en/views.html#the-app-view
 */
class AppView extends View
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading helpers.
     *
     * e.g. `$this->loadHelper('Html');`
     *
     * @return void
     */
    public function initialize()
    {

       $_templates = [
         'dateWidget' => '<ul class="list-inline"><li class="list-inline-item day">{{day}}</li>&nbsp;<li class="list-inline-item month">{{month}}</li>&nbsp;<li class="list-inline-item year">{{year}}</li>&nbsp;<li class="list-inline-item hour">{{hour}}</li>&nbsp;<li class="list-inline-item minute">{{minute}}</li>&nbsp;<li class="list-inline-item second">{{second}}</li>&nbsp;<li class="list-inline-item meridian">{{meridian}}</li></ul>',
         'error' => '<div class="help-block">{{content}}</div>',
         'help' => '<div class="help-block">{{content}}</div>',
         'inputContainer' => '<div class="form-group {{type}}{{required}}">{{content}}{{help}}</div>',
         'inputContainerError' => '<div class="form-group {{type}}{{required}} has-error">{{content}}{{error}}{{help}}</div>',
         'checkboxWrapper' => '<div class="checkbox"><label>{{input}}{{label}}</label></div>',
         'multipleCheckboxWrapper' => '<div class="checkbox">{{label}}</div>',  
         'nestingLabel' => '{{hidden}}{{input}}<label{{attrs}} class="radio__label">{{text}}</label>',
         'radioWrapper' => '<div class="radio">{{label}}</div>',
         'staticControl' => '<p class="form-control-static">{{content}}</p>',
         'inputGroupAddon' => '<span class="{{class}}">{{content}}</span>',
         'inputGroupContainer' => '<div class="input-group">{{prepend}}{{content}}{{append}}</div>',
         'input' => '<input class="form-control" type="{{type}}" name="{{name}}"{{attrs}}/><i class="form-group__bar"></i>',
         'textarea' => '<textarea class="form-control" name="{{name}}"{{attrs}}>{{value}}</textarea><i class="form-group__bar"></i>',
         'select' => '<select class="form-control select2" name="{{name}}"{{attrs}}>{{content}}</select><i class="form-group__bar"></i>',
         'selectMultiple' => '<select class="form-control select2" name="{{name}}[]" multiple="multiple"{{attrs}}>{{content}}</select><i class="form-group__bar"></i>',
         'submit' => '<button type="submit" class="btn btn-primary">{{content}}</button>'
       ];
      
       $this->Form->setTemplates($_templates);
      
    }
}
